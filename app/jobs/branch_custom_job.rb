class BranchCustomJob < ApplicationJob
   queue_as :default
   def perform(params,user_ip)
     logger.info "-----------------Branch Api Call----------------"
     logger.info "data :: #{params.inspect}, user_ip :: #{user_ip}"
     data = params["data"]
     #data["branch_key"] = "key_test_ilJk8Kq7A52Ai8J09DkMolidCujWPMsR"
     data["branch_key"] = "key_live_flJg9QCXB43vk5MY1qhmfahmqFfYLPFF"
     # data["user_data"]["http_origin"] = "c65f433f-a2fe-4321-a494-f19d80603b21"
     # data["user_data"]["mac_address"] = "83a54d80-b87c-459f-88b0-3e8965938e80"
     #data["branch_key"] = "key_test_ilJk8Kq7A52Ai8J09DkMolidCujWPMsR"
     #data["branch_key"] = "key_live_flJg9QCXB43vk5MY1qhmfahmqFfYLPFF"
     #resp = `curl -vvv -d '#{data.to_json}' 'https://api2.branch.io/v2/event/standard'`
     data["user_data"]["http_origin"] = "c65f433f-a2fe-4321-a494-f19d80603b21"
     data["user_data"]["mac_address"] = "83a54d80-b87c-459f-88b0-3e8965938e80"
     resp = `curl -vvv -H "X-IP-Override: #{user_ip}" -d '#{data.to_json}' 'https://api2.branch.io/v2/event/standard'`
     logger.info "resp :: #{resp.inspect}"
   end
end