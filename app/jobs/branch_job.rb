class BranchJob < ApplicationJob
   queue_as :default
   def perform(params,user_ip)
     logger.info "-----------------Branch Api Call----------------"
     logger.info "data :: #{params.inspect}, user_ip :: #{user_ip}"
     data = params["data"]
     if Rails.env.production?
      data["branch_key"] = "key_live_flJg9QCXB43vk5MY1qhmfahmqFfYLPFF"
     else
       data["branch_key"] = "key_test_ilJk8Kq7A52Ai8J09DkMolidCujWPMsR"
     end
     #data["branch_key"] = "key_live_flJg9QCXB43vk5MY1qhmfahmqFfYLPFF"
     # data["user_data"]["http_origin"] = "c65f433f-a2fe-4321-a494-f19d80603b21"
     # data["user_data"]["mac_address"] = "83a54d80-b87c-459f-88b0-3e8965938e80"
     #data["branch_key"] = "key_test_ilJk8Kq7A52Ai8J09DkMolidCujWPMsR"
     #data["branch_key"] = "key_live_flJg9QCXB43vk5MY1qhmfahmqFfYLPFF"
     #resp = `curl -vvv -d '#{data.to_json}' 'https://api2.branch.io/v2/event/standard'`
     data["user_data"]["http_origin"] = "c65f433f-a2fe-4321-a494-f19d80603b21"
     data["user_data"]["mac_address"] = "83a54d80-b87c-459f-88b0-3e8965938e80"
     data["customer_event_alias"] = "Fresh_subscription"
     if data["coupon_type"].present? && data["coupon_type"] == "user_referral"
        data["customer_event_alias"] = "referral"
     end
     if data["free_trail"].present? && data["free_trail"] == "true"
        data["customer_event_alias"] = "ooredo_free_trial"
     end
     resp = `curl -vvv -H "X-IP-Override: #{user_ip}" -d '#{data.to_json}' 'https://api2.branch.io/v2/event/standard'`
     logger.info "resp :: #{resp.inspect}"
     if Rails.env.production? 
        branch_log = Logger.new('/mnt/branch_events.log')
        branch_log.info("=====================Start of Final data for Branch event Logs=======================")
        branch_log.info("Final Parameters from Branch data ----------------------> #{data.to_json.inspect} <--------------------------------")
        branch_log.info("=====================Final branch post response #{resp.inspect}=======================")
        branch_log.info("=====================End of Final data for Branch event Logs=======================")
    end
   end
end