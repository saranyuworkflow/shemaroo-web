class User
	def self.sign_up(sign_up_params)
	 HTTP.post "users?region=#{$region}", sign_up_params,"user"
	end

	def self.sign_in(sign_in_params)
	 HTTP.post "users/sign_in?region=#{$region}",sign_in_params,"user"
	end

	def self.verify_otp(otp,mobile_no,type_device,name_device)
	  HTTP.get "users/verification/#{otp}?type=msisdn&mobile_number=#{mobile_no}&device_type=#{type_device}&device_name=#{name_device}&region=#{$region}","user"
	end
  def self.fp_verify_otp(user_forgot_password_otp_data)
    HTTP.post "users/otp_verification?region=#{$region}",user_forgot_password_otp_data,"user"
  end
  def self.mobile_signup(data_params)
    HTTP.post "v2/users/authenticate_user?region=#{$region}",data_params,"user"
  end
  
	def self.resend_otp(resend_params)
	  HTTP.post "users/resend_verification_link",resend_params,"user"
	end

	def self.get_all_user_profiles(session_id)
       HTTP.get "users/#{session_id}/profiles?region=#{$region}","user"
	end

	def self.get_user_profile(profile_id,session_id)
       HTTP.get "users/#{session_id}/profiles/#{profile_id}?region=#{$region}","user"
	end

	def self.sign_out(session_id)
      HTTP.post "users/#{session_id}/sign_out?region=#{$region}" ,{},"user"
	end

	def self.sign_in(sign_in_params)
	 HTTP.post "users/sign_in?region=#{$region}",sign_in_params,"user"
	end

	def self.get_user_account_details(session_id)
      HTTP.get "users/#{session_id}/account?region=#{$region}","user"
	end
  def self.change_password(session_id,user_password_data)
    HTTP.post "users/#{session_id}/change_password?region=#{$region}", user_password_data,"user"
  end

  def self.reset_pass(user_reset_password_data)
     HTTP.post "users/reset_password?region=#{$region}",user_reset_password_data, "user"
  end

  def self.reset_password_email(pwd_data)
      HTTP.post "users/reset_password?region=#{$region}",pwd_data,"user"
  end

	def self.update_profile(session_id,profile_id,profile_params)
     HTTP.put "users/#{session_id}/profiles/#{profile_id}?region=#{$region}",profile_params,"user"
    end

    def self.delete_profile(session_id,profile_id)
     HTTP.delete "users/#{session_id}/profiles/#{profile_id}?region=#{$region}","user"
    end
    def self.forgot_pass(user_forgot_password_data)
      HTTP.post "users/forgot_password?region=#{$region}", user_forgot_password_data,"user"
    end
    def self.update_account_details(session_id,user_data)
      HTTP.post "users/#{session_id}/account?region=#{$region}",user_data,"user"
    end

    def self.add_profile(session_id,profile_data)
      HTTP.post "users/#{session_id}/profiles?region=#{$region}",profile_data,"user"
    end

    def self.assign_profile(session_id,user_profile_data)
      HTTP.post "users/#{session_id}/assign_profile?region=#{$region}",user_profile_data,"user"	
    end

    def self.activate_tv_code(code_params)
      HTTP.post "generate_session_tv?region=#{$region}",code_params,"user"
    end

    def self.delete_watch_history(session_id)
      HTTP.delete "users/#{session_id}/playlists/watchhistory/clear_all?region=#{$region}","user"
    end

    def self.add_watch_later(session_id,watch_later_params)
       HTTP.post "users/#{session_id}/playlists/watchlater?region=#{$region}&version=new",watch_later_params,"user"
    end

    def self.get_user_watch_list(session_id)
      HTTP.get  "users/#{session_id}/playlists/watchlater/listitems?region=#{$region}&version=new","user"
    end
    def self.get_user_watch_list_with_pagination(session_id,page_no,page_size)
      HTTP.get "users/#{session_id}/playlists/watchlater/listitems?region=#{$region}&page=#{page_no}&page_size=#{page_size}&version=new", "user"
    end
    
   def self.delete_user_watchlater(session_id,playlist_id)
    HTTP.delete  "users/#{session_id}/playlists/watchlater/listitems/#{playlist_id}?region=#{$region}&version=new","user"
   end

   def self.verify_password(session_id,password)
      HTTP.get "users/#{session_id}/verify_password?password=#{password}&region=#{$region}","user"
   end

   def self.user_verify_password(session_id,password_data)
      HTTP.post "users/#{session_id}/verify_password?region=#{$region}",password_data,"user"
   end

   def self.create_pin(session_id,pin_data)
     HTTP.post "users/#{session_id}/account?region=#{$region}",pin_data,"user"
   end

   def self.send_persistant_playbacktime(session_id,persistant_params)
     HTTP.post "users/#{session_id}/playlists/watchhistory?region=#{$region}",persistant_params,"user"
   end

   def self.get_continue_watch_list(session_id)
     HTTP.get "users/#{session_id}/playlists/watchhistory/listitems?region=#{$region}&version=new","user"
   end
   def self.get_continue_watch_with_pagination(session_id,page_no,page_size)
     HTTP.get "users/#{session_id}/playlists/watchhistory/listitems?region=#{$region}&page=#{page_no}&page_size=#{page_size}&version=new","user"
   end
   def self.delete_user_continue_watching(session_id,playlist_id)
    HTTP.delete  "users/#{session_id}/playlists/watchhistory/listitems/#{playlist_id}?region=#{$region}","user"
   end

   def self.get_user_recommendation_list(session_id)
     HTTP.get "users/#{session_id}/recommended_items?region=#{$region}","user"
   end
   def self.get_user_recommendation_list_with_pagination(session_id,page_no,page_size)
     HTTP.get "users/#{session_id}/recommended_items?region=#{$region}&page=#{page_no}&page_size=#{page_size}","user"
   end

   def self.get_all_playlists_v2(session_id)
      HTTP.get "v2/users/#{session_id}/playlists?region=#{$region}&rand=#{DateTime.now.to_datetime.strftime('%Q')}","user"
   end
   def self.get_profile_playlist_items(session_id,playlist_id)
     HTTP.get "users/#{session_id}/playlists/#{playlist_id}/listitems?region=#{$region}&rand=#{DateTime.now.to_datetime.strftime('%Q')}&page_size=1000","user"
  end

  def self.get_user_watch_details(list_params)
    #HTTP.get "users/#{session_id}/get_all_details?region=#{$region}",list_params,"user"
    HTTP.post "v2/users/get_all_details?region=#{$region}",list_params,"user"
  end

  def self.get_modified_plans(session_id,plans_params)
    HTTP.post "users/#{session_id}/get_modify_plans?region=#{$region}",plans_params,"user"
  end

  def self.get_user_modified_plans(session_id,plans_params,plan_status)
    HTTP.post "users/#{session_id}/get_modify_plans?region=#{$region}&status=#{plan_status}",plans_params,"user"
  end
  
  def self.get_modified_plan_amount(session_id,modified_params)
    HTTP.post "users/#{session_id}/get_modified_amount?region=#{$region}",modified_params,"user"
  end

  def self.send_init_transaction(session_id,init_params)
   HTTP.post "users/#{session_id}/transactions?region=#{$region}",init_params,"user"
  end

  def self.send_final_payment_info(session_id,payement_params)
    HTTP.post "users/#{session_id}/transactions/cse_payment?region=#{$region}",payement_params,"user"
  end

  def self.remove_pack(session_id,remove_pack_info)
    HTTP.post "users/#{session_id}/unsubscribe_pack?region=#{$region}",remove_pack_info,"user"
  end

  def self.reactivate_pack(session_id,reactivate_pack_info)
    HTTP.post "users/#{session_id}/reactivate_user_plan?region=#{$region}",reactivate_pack_info,"user"
  end

  def self.promocode_apply(session_id,promocode_params)
    HTTP.post "v2/users/#{session_id}/apply_coupon_code?region=#{$region}",promocode_params,"user"
  end

  def self.get_item_catalog_id(item_params)
    HTTP.post "users/get_catalog_id",item_params,"user"
  end

  def self.send_feedback(feedback_params,catalog_id)
    HTTP.post "catalogs/#{catalog_id}/items?region=#{$region}",feedback_params,"user"
  end
      
  def self.pre_register(register_params)
    HTTP.post "catalogs/5c52a6a77fe6133877004336/items?region=#{$region}",register_params,"user"
  end


 #  def self.registered_devices_delete(session_id,device_id)
 #    HTTP.post "users/#{session_id}/delete_device?region=#{$region}",device_id,"user"
 #  end
  
 #  def self.get_registered_devices(session_id)
 #    HTTP.get "users/#{session_id}/get_all_devices?region=#{$region}", "user"
 #  end

 #  def self.update_transaction_det(session_id,t_id,update_params)
 #   HTTP.post "users/#{session_id}/transactions/update_transaction_det?region=#{$region}",update_params,"user"
 # end

  def self.registered_devices_delete(session_id,device_id)
    HTTP.post "users/#{session_id}/delete_device?region=#{$region}",device_id,"user"
  end

  def self.get_registered_devices(session_id)
    HTTP.get "users/#{session_id}/get_all_devices?region=#{$region}", "user"
  end  

  def self.update_transaction_det(session_id,t_id,update_params)
   HTTP.post "users/#{session_id}/transactions/update_transaction_det?region=#{$region}",update_params,"user"
  end

  def self.zip_code_canada(zip_code)
   HTTP.get "get_tax/#{zip_code}?auth_token=3zZmzoHg8z6SM3wpDoyw&region=#{$region}", "user"
  end

  def self.validate_coupon(coupon_data)
    HTTP.post "v2/apply_coupon_code?region=#{$region}",coupon_data,"user"
  end

  def self.validate_coupon_before_register(coupon_data)
    HTTP.get "check_coupon_code/#{coupon_data}?region=#{$region}&t=#{DateTime.now.strftime('%s')}","user"
  end

  def self.send_ghoori_registration_payment(payement_data)
    HTTP.post "users/third_party_registration?region=#{$region}",payement_data,"user"
  end

  def self.ghoori_secure_payment(payement_data)
    HTTP.post "payment_complete/ghoori/secure_payment?region=BD",payement_data,"user"
  end


  def self.secure_payment(payment_type,payment_data)
   HTTP.post "payment_complete/#{payment_type}/secure_payment?region=#{$region}",payment_data,"user"
  end

  def self.external_signin(user_data)
   HTTP.post "users/external_auth/sign_in?region=#{$region}",user_data,"user"
  end

  def  self.forgot_pin_mail(pin_data)
    HTTP.post "get_parental_pin?region=#{$region}",pin_data,"user"
  end

  def self.merge_social_acc(acnt_dat)
     HTTP.post "merge_social_acc?region=#{$region}",acnt_dat,"user"
  end

  def self.validate_order_id(order_dat)
     HTTP.post "validate_order_id?region=#{$region}",order_dat,"user"
  end

  def self.validate_branch_data(params)
   d1 = params["data"]
   # d1["branch_key"] = "key_test_ilJk8Kq7A52Ai8J09DkMolidCujWPMsR"
   # resp = `curl -vvv -d '#{d1.to_json}' 'https://api2.branch.io/v2/event/standard'`
  end

  def self.branch_bkd_dt(branch_data)
   HTTP.post "post_event_data?region=#{$region}",branch_data,"user"
  end

  def self.payu_response_call(transaction_params)
    HTTP.post "payment_complete/payu/secure_payment?region=#{$region}", transaction_params, "user"
  end 
  
  def self.get_user_prop(user_id)
    HTTP.get "users/get_status_properties?user_id=#{user_id}&region=#{$region}","user"
  end


  def self.vendor_pck_act(pck_data)
   HTTP.post "users/vendor_registration?region=#{$region}",pck_data,"user"
  end

  def self.ref_creation(user_data)
      HTTP.post "user_referral_code?region=#{$region}",user_data,"user"
    end

  def self.add_bnk_account(user_id,bank_data)
    HTTP.post "users/#{user_id}/bank_details?region=#{$region}",bank_data,"user"
  end

   def self.get_user_earnings(user_id,page_no)
    HTTP.get "users/#{user_id}/my_earnings?page=#{page_no}&region=#{$region}", "user"
  end  

  def self.get_user_earnings_with_pagination(user_id,st_dt,end_dt,page_no)
    HTTP.get "users/#{user_id}/my_earnings?start_date=#{st_dt}&end_date=#{end_dt}&page=#{page_no}&region=#{$region}", "user"
  end  

  def self.claim_earnings(user_id)
    HTTP.post "users/#{user_id}/claim_earnings?region=#{$region}",{},"user"
  end

  def self.razor_pay_sucess_call(transaction_params)
    HTTP.post "payment_complete/razor_pay/secure_payment?region=#{$region}", transaction_params, "user"
  end

  def self.get_contests
    HTTP.get "contests?start_date=#{Time.now.strftime("%Y-%m-%d")}&region=#{$region}","gmfc"
  end

  def self.get_cntst_qsts(analy_id,cnt_id,slot_id,lng_code)
    HTTP.get "contest/contest_details?analytical_id=#{analy_id}&contest_id=#{cnt_id}&language_code=#{lng_code}&start_time=#{Time.now.strftime("%Y%m%d%H%M")}&region=#{$region}","gmfc"
  end

  def self.send_cntst_answers(ans_params)
    HTTP.post "user_contest_details?region=#{$region}", ans_params, "gmfc"
  end

  def self.gmfc_user_register(user_params)
     HTTP.post "users?region=#{$region}", user_params, "gmfc"
   end

  def self.gmfc_otp_verify(otp_params)
     HTTP.post "user/verify_otp?region=#{$region}", otp_params, "gmfc"
   end

   def self.cntst_reg_chk(analy_id)
     HTTP.get "users/#{analy_id}?region=#{$region}", "gmfc"
   end

   def self.get_gmfc_winners(cnst_id,date)
     HTTP.get "get_contest_winners/#{cnst_id}?region=#{$region}&start_date=#{date}", "gmfc"
   end

   def self.external_otp(user_params)
     HTTP.post "users/get_otp?region=#{$region}",user_params,"users"
   end

   def self.external_verify_otp(profile_params)
     HTTP.post "users/update_user_info?region=#{$region}", profile_params, "users"
   end

  def self.external_check_user(u_id)
     HTTP.get "/users/verify_details?region=#{$region}&user_id=#{u_id}", "users"
   end

   def self.update_language(user_params)
     HTTP.post "users/update_user_info?region=#{$region}", user_params, "users"
   end

   def self.send_sso_events(sso_data)
    HTTP.post "users/custom_events?region=#{$region}",sso_data,"users"
  end

  def self.add_aoc_token_data(token_data)
    HTTP.post "/users/add_aoc_token_data", token_data, "users"
  end

  def self.validate_vpa(session_id,order_id,psp_supported,bank_supported)
    HTTP.post "/users/#{session_id}/process_transaction?region=#{$region}&order_id=#{order_id}&psp_supported=#{psp_supported}&bank_supported=#{bank_supported}", order_id, "users"
  end

  def self.validate_card_details(session_id,order_id,is_subscription)
    HTTP.post "/users/#{session_id}/process_transaction?region=#{$region}&order_id=#{order_id}&isSubscriptionAvailable=#{is_subscription}", order_id, "users"
  end

end




