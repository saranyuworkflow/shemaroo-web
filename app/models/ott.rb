class Ott
  
 def self.get_configuration
  HTTP.get "catalogs/message/items/app-config-params?region=#{$region}","catalog"
 end

 def self.get_home_list(page_no,page_size,lang)
  if Rails.env.production?
    HTTP.get "catalog_lists/new-homepage-list?platform_type=#{PLATFORM_TYPE}&page=#{page_no}&page_size=#{page_size}&region=#{$region}&language_code=#{lang}&version=new","catalog"
  else
    HTTP.get "catalog_lists/homepage-list?platform_type=web&page=#{page_no}&page_size=#{page_size}&region=#{$region}&language_code=#{lang}&version=new","catalog"
  end
 end

  def self.get_home_tabs
    HTTP.get "catalog_lists/catalog-tabs?platform_type=#{PLATFORM_TYPE}&region=#{$region}","catalog"
  end

  def self.payment_checkout(plan_id, sub_id, app_type)
    HTTP.get "payment_checkout/subscription/#{sub_id}/plan/#{plan_id}?region=#{$region}&app_type=#{app_type}", "catalog"
  end

	def self.subscription_plans
	  HTTP.get "catalog_lists/subscription-list?platform_type=#{PLATFORM_TYPE}&region=#{$region}","catalog"
	  # HTTP.get "catalog_lists/subscription-list?&region=#{$region}","catalog"
	end

  def self.get_items_list(list_id,lang)
  	HTTP.get "catalog_lists/#{list_id}?platform_type=#{PLATFORM_TYPE}&region=#{$region}&page=0&page_size=10&language_code=#{lang}","catalog"
  end

  def self.get_items_list_with_pagination(list_id,page_no,page_size,lang)
    HTTP.get "catalog_lists/#{list_id}?platform_type=#{PLATFORM_TYPE}&region=#{$region}&page=#{page_no}&page_size=#{page_size}&language_code=#{lang}","catalog"
  end

  def self.get_catalog_details(catalog_slug)
    HTTP.get "catalogs/#{catalog_slug}/items?region=#{$region}","catalog"
  end

  def self.get_catalog_details_with_pagination(catalog_slug)
   HTTP.get "catalogs/#{catalog_slug}/items?region=#{$region}&page=0&page_size=100","catalog"

  end

  def self.get_items_details(catalog_slug,item_slug,lang)
     HTTP.get "catalogs/#{catalog_slug}/items/#{item_slug}?platform_type=web&region=#{$region}&language_code=#{lang}","catalog"
  end


  def self.get_items_content_info(catalog_slug,item_slug,lang)
     HTTP.get "catalogs/#{catalog_slug}/items/#{item_slug}/content_info?platform_type=web&region=#{$region}&language_code=#{lang}","catalog"
  end


  def self.get_items_genre(catalog_slug,genre,lang)
     HTTP.get "catalogs/#{catalog_slug}/items?platform_type=web&genre=#{genre}&region=#{$region}&page=0&page_size=100&language_code=#{lang}","catalog"
  end


  def self.get_all_epsiodes(catalog_slug,show_slug,lang)
    HTTP.get "catalogs/#{catalog_slug}/items/#{show_slug}/episodes?platform_type=web&region=#{$region}&language_code=#{lang}","catalog"
  end
 
  def self.get_all_epsiodes_with_pagination(catalog_slug,show_slug,page_no,lang)
    HTTP.get "catalogs/#{catalog_slug}/items/#{show_slug}/episodes?platform_type=web&page=#{page_no}&page_size=10&region=#{$region}&language_code=#{lang}","catalog"
  end

  def self.get_seasons_epsiodes(show_slug,lang)
    HTTP.get "catalogs/shows/subcategories/#{show_slug}/episodes?region=#{$region}&language_code=#{lang}","catalog"
  end

  def self.get_all_trailers(catalog_slug,item_slug,lang)
    HTTP.get "catalogs/#{catalog_slug}/items/#{item_slug}/videolists?platform_type=web&region=#{$region}&language_code=#{lang}","catalog"
  end

  def self.get_episode_details(catalog_slug,show_slug,episode_slug,lang)
    HTTP.get "catalogs/shows/#{show_slug}/episodes/#{episode_slug}?platform_type=web&region=#{$region}&language_code=#{lang}","catalog"
  end

  def self.get_all_catalogs
    HTTP.get "catalogs?region=#{$region}","catalog"
  end

  def self.get_search_results(search_name,type,lang)
    HTTP.get "search?q=#{search_name}&category=#{type}&region=#{$region}&language_code=#{lang}","catalog"
  end
  def self.get_trending_results(lang,catry)
    HTTP.get "catalog_lists/trending-search?platform_type=#{PLATFORM_TYPE}&region=#{$region}&category=#{catry}&language_code=#{lang}", "catalog"
  end

  def self.get_user_region(ip_addrs)
    HTTP.get "regions/ip/#{ip_addrs}","catalog"
  end

  def self.user_plans(user_session,lang)
    HTTP.get "users/#{user_session}/user_plans?region=#{$region}&language_code=#{lang}","catalog"
  end

  def self.user_upgrade_plans(user_session,lang)
    HTTP.post  "v2/users/#{user_session}/get_modify_plans?region=#{$region}&language_code=#{lang}","","catalog"
  end

  def self.set_upgrade_plan(user_session,lang,plan_data)
    HTTP.post  "users/#{user_session}/get_upgrade_price?region=#{$region}&language_code=#{lang}",plan_data,"catalog"
  end

  def self.get_all_channels
    HTTP.get "catalog_lists/all-channels?platform_type=#{PLATFORM_TYPE}&page_size=150&region=#{$region}","catalogs"
  end

  def self.get_channel_programs(catalog_id,channel_id)
    HTTP.get "catalogs/#{catalog_id}/items/#{channel_id}/programs?region=#{$region}&status=any","catalogs"
  end

  def self.get_more_livetv(catalog_id,category_type)
    HTTP.get "catalogs/#{catalog_id}/items?region=#{$region}&status=any&content_category=#{category_type}","catalogs"
  end

  def self.get_all_items(type)
    HTTP.get "content/#{type}/all?region=#{$region}","catalogs"
  end

   def self.get_all_items_with_pagination(type,page_no,page_size)
    HTTP.get "content/#{type}/all?page=#{page_no}&page_size=#{page_size}&region=#{$region}","catalogs"
  end

  def self.get_category_landing(category_name)
    HTTP.get "catalogs/5ce402cded8f7d7e0c000000/items/#{category_name}?region=#{$region}","catalogs"
  end
  
  def self.get_associated_videos(catalog_slug,item_slug)
    HTTP.get "catalogs/#{catalog_slug}/items/#{item_slug}/videolists?region=#{$region}","catalogs"
  end

  def  self.get_landing_page_data
     HTTP.get "catalogs/5cde4e212995d3778700005e/items/5dfbbb8c2995d338900000d8?status=any&region=any","catalogs"
  end
  def  self.get_promotion_landing(item_slug, catalog_id)
     HTTP.get "catalogs/#{catalog_id}/items/#{item_slug}?status=any&region=#{$region}","catalogs"
  end

  def self.all_access_plans
    HTTP.get "catalog_lists/all-access-subscription-list?platform_type=#{PLATFORM_TYPE}&region=#{$region}","catalog"
  end

  def self.new_plans(lang)
   HTTP.get "catalog_lists/new-subscription-list?platform_type=#{PLATFORM_TYPE}&region=#{$region}&language_code=#{lang}","catalog"
  end

  def self.get_new_home_tabs(lang)
    HTTP.get "catalog_lists/newhometab.gzip?platform_type=#{PLATFORM_TYPE}&region=#{$region}&language_code=#{lang}&version=new","catalog"
  end

  def self.get_browse_categories(lang)
    HTTP.get "catalog_lists/browse-api.gzip?platform_type=#{PLATFORM_TYPE}&region=#{$region}&language_code=#{lang}","catalog"
  end

  def self.get_app_strings(lang)
    HTTP.get "app_strings?language_code=#{lang}&region=#{$region}","catalog"
  end

  def self.get_app_popup(lang)
    HTTP.get "app_strings?language_code=#{lang}&region=#{$region}&type=popup","catalog"
  end

  def self.get_search_category_list(lang)
    HTTP.get "catalog_lists/search-list?platform_type=#{PLATFORM_TYPE}&language_code=#{lang}&region=#{$region}","catalog"
  end

  def self.get_search_kids_category_list(lang)
    HTTP.get "catalog_lists/search-kids-list?platform_type=#{PLATFORM_TYPE}&language_code=#{lang}&region=#{$region}","catalog"
  end

  def self.get_v2_search_results(key,cat,lang)
     HTTP.get "v2/search?q=#{key}&category=#{cat}&language_code=#{lang}&region=#{$region}","catalog"
  end

  def self.get_v2_search_results_theme(key,cat,lang,theme)
    HTTP.get "v2/search?q=#{key}&category=#{cat}&theme=#{theme}&language_code=#{lang}&region=#{$region}","catalog"
  end

  def self.get_season_item_list(season_id,lang)
    HTTP.get "catalogs/shows/subcategories/#{season_id}/subcategories/episodes?region=#{$region}","catalog"
  end

 def self.get_channel_list
    HTTP.get "catalog_lists/new-home-channels?platform_type=web&region=#{$region}","catalog"
  end

  def self.get_channel_list_pagination(page_no,page_size)
    HTTP.get "catalog_lists/new-home-channels?page_size=#{page_size}&page=#{page_no}&platform_type=web&region=#{$region}","catalog"
  end

  def self.get_all_channel_playlist(cat_slug,page_no,page_size,lang)
    HTTP.get "catalog_lists/#{cat_slug}?page_size=#{page_size}&page=#{page_no}&language_code=#{lang}&platform_type=web&region=#{$region}","catalog"
  end

  def self.get_media_list(catalog_slug,show_slug,lang)
    HTTP.get "v3/catalogs/#{catalog_slug}/items/#{show_slug}/programs?platform_type=web&language_code=#{lang}&region=#{$region}","catalog"
  end

  def self.get_more_channel_list(show_slug)
    HTTP.get "catalog_lists/#{show_slug}?platform_type=web&language_code=en&region=#{$region}","catalog"
  end

end  

