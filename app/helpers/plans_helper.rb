module PlansHelper
	def get_gradient title
		case title
    when "Bollywood classic packs"
      return "gradient2"
		when "Bollywood packs"
			return "gradient3"
     when "Kids pack"
      return "gradient5"
    when "Punjabi packs"
      return "gradient8"
    when "Gujarathi packs"
      return "gradient4"
    when "Bhakti packs"
      return "gradient6"
    when "Ibadaat packs"
      return "gradient7"
    when "Combo packs"
      return "gradient4"
    end
	end

  def get_gradient_color title
    case title
    when "Bollywood classic packs"
      return "gradientbg2"
    when "Bollywood packs"
      return "gradientbg3"
     when "Kids pack"
      return "gradientbg5"
    when "Punjabi packs"
      return "gradientbg8"
    when "Gujarathi packs"
      return "gradientbg4"
    when "Bhakti packs"
      return "gradientbg6"
    when "Ibadaat packs"
      return "gradientbg7"
    when "Bollywood plus packs"
      return "gradientbg9"
    when "Combo packs"
      return "gradientbg4"
    when "All access pack"
      return "gradientbg1"
    end
  end

  def get_signature_key(packs)
   secret_key = "7c32c524a67f405812ca"
    all_plan_id = "" 
    packs.each do |p| 
    plan_id = p["plan_id"].blank? ? p[:plan_id] : p["plan_id"]
    all_plan_id += plan_id 
    end 
    session_id = cookies[:user_id]
    region = @region
    sec_key = secret_key+session_id+region+all_plan_id 
    md5_sign = Gibberish::MD5(sec_key) 
    return md5_sign
  end

    def get_payment_signature_key(plan_id)
     secret_key = "7c32c524a67f405812ca"
     sec_key = secret_key+cookies[:user_id]+$region+plan_id 
     md5_sign = Gibberish::MD5(sec_key) 
    return md5_sign
   end

 def get_plan_active_status(plan_ids,p_id)
    plan_status = "false"
    status = plan_ids.collect{|x|x if(x.split("$")[0] == p_id && x.split("$")[2] == "active")}.compact.flatten
    unless status.empty?
      plan_status = "true"
    end
    return plan_status
 end

 def get_currency_symbol
  symbol = "$"
  case $region
   when "IN"
   symbol = "₹"
   when  "US"
    symbol = "$"
  when "BD"
    symbol = "BDT"
  when  'MY'
    symbol = "MYR"
  end

 end

 def check_plan_box_st(p_title)
   st = ""
   desc_st = "" 
   if $region == "MY"
      st = "static"
      desc_st = "display:none"
    if p_title  == "monthly" || p_title == "weekly"
      st = ""
      desc_st = ""
    end
     # if p_title != "monthly"
     #  st = "static"
     #  desc_st = "display:none"
     # end
   end
   return st,desc_st
 end

  def get_plan_desc(type)
   desc = "Get this plan with 7 days free trial & additional discount of 20% with Celcom billing"
   if type.downcase == "weekly"
    desc = "Special offer of 7 days free trial for Celcom users"
   end
   return desc
  end

  def get_pln_box_color catgry
   begin
    color = ""
    config_resp = Rails.cache.fetch("design_configuration_list", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_configuration      
       }
     if catgry == "all_access_pack"
       catgry = "all"
     end
     #byebug
     color = config_resp["data"]["params_hash2"]["config_params"]["view_plan_tags"].collect{|x|"#"+x[1] if x[0] == catgry}.compact.first
     if color.nil?
      #color = "#8BC76D"
      color = "#D51F53"
     end
  rescue
    Rails.cache.delete("design_configuration_list")
  end
  color = color
  return color
  end

  def catalog_plans_count(plans_info, subscription_title)
    count =0
    plans_info["data"]["catalog_list_items"].each do |subs_type|
      count =subs_type["plans"].length if subs_type["title"] == subscription_title
    end
    return count 
  end

end
