module ApplicationHelper
 def get_image_url(i,layout_type)
  image_url = ""
   if layout_type == "t_16_9_banner"
     image_url = i["thumbnails"]["xl_image_16_5"]["url"] if i["thumbnails"].has_key?("xl_image_16_5")
   elsif layout_type == "t_2_3_movie" || layout_type == "t_2_3_movie_static" || layout_type == "movies"
     image_url = i["thumbnails"]["medium_2_3"]["url"] if i["thumbnails"]["medium_2_3"]
   elsif layout_type == "t_16_9_big" || layout_type == "t_16_9_epg" || layout_type == "t_1_1_play" || layout_type == "videos"
     image_url = i["thumbnails"]["medium_16_9"]["url"] if i["thumbnails"].has_key?("medium_16_9")
   elsif layout_type == "t_16_9_small"  || layout_type == "t_16_9_small_meta" || layout_type == "video" || layout_type == "shows"
      image_url = i["thumbnails"]["medium_16_9"]["url"] if i["thumbnails"].has_key?("small_16_9")
    elsif layout_type == "t_1_1_plain" 
      image_url = i["thumbnails"]["xl_image_1_1"]["url"]  if i["thumbnails"].has_key?("xl_image_1_1")
    elsif layout_type == "t_comb_16_9_list" || layout_type == "t_16_9_big_meta" 
      image_url = i["thumbnails"]["large_16_9"]["url"] if i["thumbnails"].has_key?("large_16_9")
    elsif layout_type == "t_comb_1_1_image"
      image_url = i["thumbnails"]["small_16_9"]["url"] if i["thumbnails"].has_key?("small_16_9")
    elsif  layout_type == "t_1_1_album" || layout_type == "t_1_1_album_meta"
      image_url = i["thumbnails"]["medium_1_1"]["url"]
    elsif layout_type == "t_playlist"
      image_url = i["thumbnails"]["xl_image_16_9"]["url"] if i["thumbnails"].has_key?("xl_image_16_9")
    elsif layout_type == "t_16_9_playlist"
      image_url = i["thumbnails"]["xl_image_16_9"]["url"] if i["thumbnails"].has_key?("xl_image_16_9")
   end
   return image_url
 end


 def get_banner_image_url(i,layout_type)
  image_url = ""
   if i["thumbnails"].has_key?("list_item_banner") && i["thumbnails"]["list_item_banner"]["url"].present?
    #image_url = i["thumbnails"]["list_item_banner_web"]["url"]
    image_url = i["thumbnails"]["list_item_banner"]["url"]
   else
     if layout_type == "t_16_9_banner"
       #image_url = i["thumbnails"]["xl_image_16_5"]["url"]
       image_url = i["thumbnails"]["xl_image_16_9"]["url"] if i["thumbnails"].has_key?("xl_image_16_9")
     elsif layout_type == "t_2_3_movie" || layout_type == "t_2_3_movie_static" || layout_type == "movies"
       image_url = i["thumbnails"]["medium_2_3"]["url"] 
     elsif layout_type == "t_16_9_big" || layout_type == "t_16_9_epg" || layout_type == "t_1_1_play" || layout_type == "videos"
       image_url = i["thumbnails"]["medium_16_9"]["url"] if i["thumbnails"].has_key?("medium_16_9")
     elsif layout_type == "t_16_9_small"  || layout_type == "t_16_9_small_meta" || layout_type == "video" || layout_type == "shows"
        image_url = i["thumbnails"]["small_16_9"]["url"] if i["thumbnails"].has_key?("small_16_9")
      elsif layout_type == "t_1_1_plain" 
        image_url = i["thumbnails"]["xl_image_1_1"]["url"]  if i["thumbnails"].has_key?("xl_image_1_1")
      elsif layout_type == "t_comb_16_9_list" || layout_type == "t_16_9_big_meta" 
        image_url = i["thumbnails"]["large_16_9"]["url"] if i["thumbnails"].has_key?("large_16_9")
      elsif layout_type == "t_comb_1_1_image"
        image_url = i["thumbnails"]["small_16_9"]["url"] if i["thumbnails"].has_key?("small_16_9")
      elsif  layout_type == "t_1_1_album" || layout_type == "t_1_1_album_meta"
        image_url = i["thumbnails"]["medium_1_1"]["url"]
      elsif layout_type == "t_playlist_featured" 
        image_url = i["thumbnails"]["xl_image_16_9"]["url"] if i["thumbnails"].has_key?("xl_image_16_9")
     end
 end
   return image_url
 end

 def get_ixigo_banner_img(i)
  image_url = i["thumbnails"]["xl_image_16_9"]["url"]
  if i.has_key?("list_item_object") && i["list_item_object"]["banner_image"].present?
    image_url = i["list_item_object"]["banner_image"]
  end
  return image_url
 end

 def get_mobile_banner_image(i,layout_type)
    image_url = ""
   if i["thumbnails"].has_key?("list_item_banner") && i["thumbnails"]["list_item_banner"]["url"].present?
    image_url = i["thumbnails"]["list_item_banner"]["url"]
   else
    image_url = i["thumbnails"]["xl_image_16_9"]["url"]
   end
   return image_url
 end

 def get_item_color(item)
  begin
    color = ""
    config_resp = Rails.cache.fetch("design_configuration_list_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_configuration      
       }
    @free_trail = config_resp["data"]["params_hash2"]["config_params"]["free_trial_config"]["free_trail"]
    @free_trail_message = config_resp["data"]["params_hash2"]["config_params"]["free_trial_config"]["free_trail_msg"]
     color = config_resp["data"]["params_hash2"]["config_params"]["layout_scheme"].collect{|x|"#"+x["start_color"]+"|"+"#"+x["end_color"] if (x["scheme"] == item)}.compact.first
     if color.nil?
      color = "#8BC76D|#1F9FB9"
     end
  rescue
    Rails.cache.delete("design_configuration_list")
  end
  return color
 end



 def get_title_item_color(i)
   begin
    color = ""
    config_resp = Rails.cache.fetch("design_configuration_list", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_configuration      
       }
     if i == "all_access_pack"
       i = "all"
     end
     color = config_resp["data"]["params_hash2"]["config_params"]["layout_scheme"].collect{|x|"#"+x["start_color"]+"|"+"#"+x["end_color"] if (x["scheme"] == i)}.compact.first
     if color.nil?
      color = "#8BC76D|#1F9FB9"
     end
  rescue
    Rails.cache.delete("design_configuration_list")
  end
  color = color.split("|")
  new_color = "background: linear-gradient(to right, #{color[0]},#{color[1]})"
  return new_color
 end

  def get_item_url(i)
    begin
      url  = ""
      if i["theme"] == "show" && i["subcategory_flag"] == "no"
        if i.has_key?("last_episode") && !i['last_episode'].blank?
          url = "/shows/#{i['last_episode']['show_object']['friendly_id']}"
          #url = "/#{i['catalog_object']['friendly_id']}/#{i['last_episode']['show_object']['friendly_id']}"
        else
          #url = "/#{i['catalog_object']['friendly_id']}/#{i['friendly_id']}"
           url = "/shows/#{i['friendly_id']}"
        end
      elsif i["theme"] == "show_episode" || i["theme"] == "episode"
        p "333333333333333333333333"
        url = "/#{i['catalog_object']['friendly_id']}/#{i['show_object']['friendly_id']}/#{i['friendly_id']}"
      else
       url = "/#{i['catalog_object']['friendly_id']}/#{i['friendly_id']}"
      if i["theme"] == "movie" 
        url = "/movies/#{i['friendly_id']}"
      elsif i["theme"] == "video"
        if i['catalog_object']['layout_type'] == "song"
          url = "/music/#{i['friendly_id']}"
        end     
      end
    end
     rescue
      url = "/"
     end
  return url
  end

  def get_continue_item_url(i)
    begin
      url  = ""
      if i["theme"] == "show" && i["subcategory_flag"] == "no"
        if i.has_key?("last_episode") && !i['last_episode'].blank?
          url = "/#{i['catalog_object']['friendly_id']}/#{i['last_episode']['show_object']['friendly_id']}"
        else
          url = "/#{i['catalog_object']['friendly_id']}/#{i['friendly_id']}"
        end
      elsif i["theme"] == "show_episode" || i["theme"] == "episode"
        url = "/#{i['catalog_object']['friendly_id']}/#{i['show_object']['friendly_id']}/#{i['friendly_id']}"
      else
       url = "/#{i['catalog_object']['friendly_id']}/#{i['friendly_id']}"
      end
     rescue
      url = "/"
     end
     url = url+"?time="+(i["play_back_time"].split(':').map{ |a| a.to_i }.inject(0) { |a, b| a * 60 + b}).to_s
  return url
  end


  def get_meta_details
   keywords = "watch movies online, Bollywood Movies, Marathi Movies, Gujarati movies, Live Darshan, Live TV, kids movies,naatak, video, Gurubani"
   if params[:lang].present? && params[:lang] != "en"
     user_lang = (params[:lang] == "hi") ? "Hindi": "Gujarati"
     title = "ShemarooMe in #{user_lang} - Watch Movie, TV Shows, Live TV Online"
    description = "Watch Unlimited bollywood movies, TV Serials & Devotional content. Watch high quality old classic movies, TV serials, bollywood music, kids shows in various #{user_lang}, devotional content in regional languages such as #{user_lang} only at ShemarooMe."
   else
   title = "Bollywood Hindi Movies, TV Shows, Short Films, Music, Kids Shows & More -ShemarooMe"
   description = "Unlimited bollywood movies, TV Serials & Devotional content. Watch high quality old classic movies, TV serials, bollywood music, kids shows in various languages, devotional content in regional languages such as Gujarati & Punjabi only at ShemarooMe. Download ShemarooMe app to get access to unlimited Bollywood Movies, TV serials, Bhakti, Gurubani & Ibaadat."
   end

    if params[:action] == "show_catalog_item" || params[:action] == "indiv_category_landing" && ["movie","show","livetv","video"].include?(params[:catalog_name])
      if params[:catalog_name] == "movie"
        title = "Watch Online Movies in Various Language Like Hindi, Marathi, Gujarati And Many More" unless params[:lang].present?
        title = "Movies Online - Explore Bollywood Movies, Hindi Movies and Regional Movie on ShemarooMe Hindi" if params[:lang].present? && params[:lang] =="hi"
        title = "Movies Online - Explore Bollywood Movies, Gujarati Movies and Regional Movie on ShemarooMe in Gujarati" if params[:lang].present? && params[:lang] =="gu"
        description = "Watch online movies only on ShemarooMe. Watch full online movies in various languages like Hindi movies, Gujarati Movies, Punjabi Movies, Marathi movies on ShemarooMe. Download ShemarooMe app to get unlimited access to movies."  unless params[:lang].present?
        description = "Stream movies online and Browse the best of Bollywood movies, Hindi movies, Gujarati movies, Marathi movies on ShemarooMe in Hindi. Watch popular films like comedy movies, romantic movies, drama movies, Action movies Etc. Download ShemarooMe app to get unlimited access to movies." if params[:lang].present? && params[:lang] =="hi"
        description = "Stream movies online and Browse the best of Bollywood movies, Gujarati movies, Marathi movies on ShemarooMe in Gujarati. Watch popular films like comedy movies, romantic movies, drama movies, Action movies Etc. Download ShemarooMe app to get unlimited access to movies." if params[:lang].present? && params[:lang] =="gu"
        keywords = "movies, watch movie online, online hindi movie watch, gujarati movie, punjabi movie, marathi movie"
      elsif params[:catalog_name] == "show"
        title = "ShemarooMe in Hindi - Watch Online Shows | Shows| Kids Shows | Tv Shows" if params[:lang].present? && params[:lang] =="hi"
        title = "Watch Online Shows | Shows| Kids Shows - ShemarooMe" unless params[:lang].present?
        title = "ShemarooMe in Gujarati - Watch Gujarati Online Shows | Shows| Gujarati Kids Shows | Tv Shows" if params[:lang].present? && params[:lang] =="gu"
        description = "Watch online Shows only on ShemarooMe In Hindi. Browse shows, TV shows, Kids Shows in various languages like Hindi, Gujarati, Punjabi, Marathi on ShemarooMe. Download ShemarooMe app to get unlimited access on Shows." if params[:lang].present? && params[:lang] =="hi"
        description = "Watch online Shows only on ShemarooMe In Gujarati. Browse shows, TV shows, Kids Shows in various languages like Hindi, Gujarati, Punjabi, Marathi on ShemarooMe. Download ShemarooMe app to get unlimited access on Shows." if params[:lang].present? && params[:lang] =="gu"
         description = "Watch online Shows only on ShemarooMe. Watch  online TV shows, Kids Shows in various languages like hindi, Gujarati , Punjabi , Marathi on ShemarooMe. Download ShemarooMe appto get unlimited access on Shows." unless params[:lang].present?
        keywords = "kids shows, tv shows,shows, tv program"
      elsif params[:catalog_name] == "livetv"
        title = "Watch Live TV Channels : Bhakti Live Streaming, Shemaroo TV, News Channels" unless params[:lang].present?
        title = "ShemarooMe Hindi - Watch Live TV Channels : Bhakti Live Streaming, Shemaroo TV, News Channels" if params[:lang].present? && params[:lang] =="hi"
        title = "ShemarooMe Gujarati - Watch Live TV Channels : Bhakti Live Streaming, Shemaroo TV, News Channels" if params[:lang].present? && params[:lang] =="gu"
        description = "Watch Live TV Channels on ShemarooMe. Watch online tv streaming channel like Bhakti channel, shemaroo Channels in various languages like hindi, Gujarati,Marathi on ShemarooMe. Download ShemarooMe appto get unlimited access on live TV." unless params[:lang].present?
        description = "Watch Live TV Channels on ShemarooMe in Hindi. Watch online tv streaming channels like Bhakti channel, Shemaroo Channels in various languages like Hindi, Gujarati, Marathi on ShemarooMe. Download ShemarooMe app to get unlimited access on live TV." if params[:lang].present? && params[:lang] =="hi"
        description = "Watch Live TV Channels on ShemarooMe in Gujarati. Watch online tv streaming channels like Bhakti channel, Shemaroo Channels in various languages like Hindi, Gujarati, Marathi on ShemarooMe. Download ShemarooMe app to get unlimited access on live TV." if params[:lang].present? && params[:lang] =="gu"
        keywords = "live, live tv, live streaming, live channels" unless params[:lang].present?
        keywords = "live, live tv, live streaming, live channels" if params[:lang].present? && params[:lang] =="hi"
        keywords = "live, live tv, live streaming, live channels, live darshan" if params[:lang].present? && params[:lang] =="gu"
      elsif params[:catalog_name] == "video"

         title = "Watch Online Video Song | Watch Filmigaane Online|Natak| ShemarooMe" unless params[:lang].present?
        title = "ShemarooMe in Hindi - Watch Online Video Song | Watch Filmigaane Online| ShemarooMe" if params[:lang].present? && params[:lang] =="hi"
         title = "ShemarooMe in Gujarati - Watch Online Video Song | Watch Filmigaane Online" if params[:lang].present? && params[:lang] =="gu"
        description = "Watch Oline Video song and Filmi gaane on ShemarooMe In Hindi. Watch Free filmi gaane, plays, natak online on shemarooMe  in various languages like Hindi, Gujarati, Marathi on ShemarooMe. Download ShemarooMe app to get unlimited access on Video songs, filmi gaane, natak etc." if params[:lang].present? && params[:lang] =="hi"
        description = "Watch Online Video song and Filmiganne on ShemarooMe In Gujarati. Watch Free filmi gaane, plays, natak online ShemarooMe in various languages like Hindi, Gujarati, Marathi on ShemarooMe. Download ShemarooMe app to get unlimited access on Video songs and filmi gaane." if params[:lang].present? && params[:lang] =="gu"
        description = "Watch Oline Video song and Filmigaane on ShemarooMe. Watch Free filmigaane, plays, natak online shemarooMe  in various languages like hindi, Gujarati,Marathi on ShemarooMe. Download ShemarooMe app to get unlimited access on Video songs, Natak and filmigaane." unless params[:lang].present?
        keywords = "video song, filmigaane, marathi natak,gujarati natak"
      end
    elsif params[:action] == "index"
      if params[:lang].present? && params[:lang] == "gu"  
        title = "ShemarooMe in Gujarati - Watch Gujarati Movies, Gujarati Web Series,Gujarati  TV Shows, LiveTv, Natak, Original"
        description = "Watch online Gujarati movies, Gujarati web series, live tv in ShemarooMe Gujarati. Browse Your favorite movies, music videos, live TV channels, Web series, natak etc. Download ShemarooMe app to get unlimited access to movies, web series, and many more."
        keywords = "online movie, web series, Originals, Live darshan,natak, video songs"
      elsif params[:lang].present? && params[:lang] == "hi"  
        title = "ShemarooMe in Hindi - Watch Movies, Web Series, TV Shows, LiveTv, Natak, Live Darshan"
        description = "Watch online movies, web series, live tv, live darshan On ShemarooMe Hindi. Browse Your favorite movies, music videos, live TV channels, Web series, live darshan Natak, etc. Download ShemarooMe app to get unlimited access on movies, Natak, live Darshan, And shows."
        keywords ="watch movies online, Bollywood Movies, Marathi Movies, Gujarati movies, Live Darshan, Live TV, kids movies,naatak, video, Gurubani"
      end
    end


   image = "#{SITE}/assets/shemaroome_logo.png" 
   if !@item_details.nil? 
    if @item_details["theme"] == "live"
      if @item_details["friendly_id"] == "prati-shirdi-shirgaon-pune-live-darshan12"
        title = "Live Darshan- Mahaganpati Ganpati Temple Ranjangaon on ShemarooMe"
        description = "Watch out Mahaganpati Ganpati Temple Ranjangaon live darshan only on ShemarooMe. Browse few more channels like ISKON Girgoan, Parthi Shirdi, Jivadi Temple on ShemarooMe Channels."
        keywords = "Live Darshan,  Ganpati Temple ,  live TV, Ranjangaon"
      elsif  @item_details["friendly_id"] == "shree-jivdani-mata-temple-mumbai"
        title = "Live Darshan- Shree Jivdani Temple On ShemarooMe"
        description = "Watch out  Shree Jivadani Temple live darshan only on ShemarooMe. Browse few more channels like ISKON Girgoan, Parthi Shirdi,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV,jivdani temple, jivdani temple Live Darshan, Virar Mumbai"
      elsif @item_details["friendly_id"] == "shree-mata-mansa-devi-temple-panchkula-haryana"
        title = "Live Darshan- Mata Mansa Devi Mandir Panchkula Haryana On ShemarooMe"
        description = "Watch out Mata Mansa Devi Mandir Panchkula Haryana live darshan only on ShemarooMe. Browse few more channels like ISKON Girgoan, Shree Jivdani Temple,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, mansa devi mandir, mata mansa devi, Panchkula Haryana"
      elsif @item_details["friendly_id"] == "prati-shirdi-shirgaon-pune-live-darshan"
        title = "Live Darshan- Prati Shirdi Shirgaon Pune On ShemarooMe"
        description = "Watch out Prati Shirdi Shirgaon Pune live darshan only on ShemarooMe. Browse few more channels like Mahalaxmi temple, ISCON Girgoan,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, shirdi sai baba live, sai baba live darshan, Pune"
      elsif @item_details["friendly_id"] == "shree-karni-mata-mandir-rajasthan"
        title = "Live Darshan- Shree Karni Mata |  Karni Mata Temple Rajasthan On ShemarooMe"
        description = "Watch out Shree Karni Mata Temple Rajasthan live darshan only on ShemarooMe. Browse few more channels like Mahalaxmi temple, ISCON Girgoan,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, Shree Karni Mata, Karni Mata Temple, Rajasthan"
      elsif @item_details["friendly_id"] == "shree-shani-shingnapur-live-darshan"
        title = "Live Darshan- Shree Shani Shingnapur  On ShemarooMe"
        description = "Watch out Shree Shani Shingnapur live darshan only on ShemarooMe. Browse few more channels like Mahalaxmi temple, ISCON Girgoan,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, shani shingnapur temple, shani shingnapur darshan, shani shingnapur mandir, Maharshtra"
      elsif @item_details["friendly_id"] == "mumba-devi-temple-mumbai-live-darshan"
        title = "Live Darshan- Mumba Devi Temple Mumbai On ShemarooMe"
        description = "Watch out Mumba Devi Temple Mumbai live darshan only on ShemarooMe. Browse few more channels like Mahalaxmi temple, ISCON Girgoan,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, mumba devi temple, mumba devi mandir, mumbadevi mumbai, Mumbai"
      elsif @item_details["friendly_id"] == "shri-icchapuran-balaji-mandir-rajasthan-live-darshan"
        title = "Live Darshan- Shri Icchapuran Balaji Mandir Rajasthan On ShemarooMe"
        description = "Watch out Shri Icchapuran Balaji Mandir Rajasthan live darshan only on ShemarooMe. Browse few more channels like Mahalaxmi temple, ISCON Girgoan,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, balaji mandir Darshan, balaji ka mandir, Rajasthan"
      elsif @item_details["friendly_id"] == "shree-vigneshwara-temple-ozar-live-darshan"
        title = "Live Darshan- Shree Vigneshwara Temple, Ozar, Live Darshan  On ShemarooMe"
        description = "Watch out Shree Vigneshwara Temple, Ozar live darshan only on ShemarooMe. Browse few more channels like Mahalaxmi temple, ISCON Girgoan,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, vigneshwara temple , Ozar"
      elsif @item_details["friendly_id"] == "saileela-tv"
        title = "Live Darshan- Saileela Tv On ShemarooMe"
        description = "Watch out Saileela Tv live darshan only on ShemarooMe. Browse few more channels like Mahalaxmi temple, ISCON Girgoan,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, saileela, watch saileela"
      elsif @item_details["friendly_id"] == "mahalakshmi-mandir-mumbai-live-darshan"
        title = "Live Darshan- Shree Mahalakshmi Mandir Mumbai On ShemarooMe"
        description = "Watch out Shree Mahalakshmi Mandir Mumbai live darshan only on ShemarooMe. Browse few more channels like Karani Mata temple, ISCON Girgoan,Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, mahalaxmi temple, mahalaxmi mandir Live Darshan , Mumbai"
      elsif @item_details["friendly_id"] == "iskcon-girgaon-mumbai"
        title = "Live Darshan -ISKCON Girgaon  Mumbai On ShemarooMe"
        description = "Watch out ISKCON Girgaon  Mumbai live darshan only on ShemarooMe. Browse few more channels like Karani Mata temple, Shree Jivdani Mata temple, Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, ISKCON Temple,  Hare  krisna, Hare Krishna Temple Live Darshan Mumbai , Mumbai"
      elsif @item_details["friendly_id"] == "babulnath-mandir-mumbai"
        title = "Live Darshan -  Babulnath Mandir Mumbai On ShemaroMe"
        description = "Watch out Babulnath Mandir Mumbai live darshan only on ShemarooMe. Browse few more channels like Karani Mata temple, Shree Jivdani Mata temple, Mahaganpati Ganpati on ShemarooMe Channels."
        keywords = "Live Darshan, live TV, babulnath mandir mumbai, babulnath mandir, babulnath temple mumbai, Mumbai"
      elsif @item_details["friendly_id"] == "mahaganpati-ganpati-temple-ranjangaon-live-darshan"
        title = "Live Darshan- Mahaganpati Ganpati Temple Ranjangaon on ShemarooMe"
        description = "Watch out Mahaganpati Ganpati Temple Ranjangaon live darshan only on ShemarooMe. Browse few more channels like ISKON Girgoan, Parthi Shirdi, Jivadi Temple on ShemarooMe Channels."
        keywords = "Live Darshan,  Ganpati Temple ,  live TV, Ranjangaon"

      end
    else
      title = @item_details["title"]
      genres = @item_details['genres'].join(",")
      title = "Watch Online #{@item_details['language'].capitalize} #{@item_details['theme'].capitalize} #{@item_details['title']} - ShemarooMe"
      description = "Watch #{@item_details["title"]} #{@item_details['theme'].capitalize} online on ShemarooMe. Stream full #{@item_details['language'].capitalize} #{@item_details['theme'].capitalize} online #{@item_details["title"]}. Download ShemarooMe app and subscribe to ShemarooMe Premium to stream latest & old Indian Hindi Bollywood, Bollywood Premiere, Bollywood Classic, Gujarati, Marathi, Kids, Bhakti, Comedy and Punjabi movies. Watch #{genres}  #{@item_details["title"]}  movie online. Stream #{@item_details['language']} #{@item_details['theme'].capitalize} #{@item_details["title"]}  only on ShemarooMe."
    end
    
    if @item_details['theme'] == "movie"   
     language = @item_details['language']
     genres = @item_details['genres'].join(", ")
     title = "Watch Online #{@item_details['language'].capitalize} #{@item_details['theme'].capitalize}  #{@item_details['title']} - ShemarooMe"
     keywords = "Watch online #{@item_details['title']} Movie, #{@item_details['title']} Movie Watch Online, Watch #{@item_details['title']} Movie Online"  
    description = "Watch #{@item_details["title"]} #{@item_details['language'].capitalize} #{@item_details['theme'].capitalize} online on ShemarooMe. Stream full movie online #{@item_details["title"]}. Download ShemarooMe app and subscribe to ShemarooMe Premium to stream latest & old Indian Hindi Bollywood, Bollywood Premiere, Bollywood Classic, Gujarati, Marathi, Kids, Bhakti, Comedy and Punjabi movies. Watch #{genres}  #{@item_details["title"]}  movie online. Stream #{@item_details['language']}  movie  #{@item_details["title"]}  only on ShemarooMe."
    if  @item_details["catalog_object"] && (@item_details["catalog_object"]["plan_category_type"] == "bollywood_classic" ||    @item_details["catalog_object"]["plan_category_type"] == "bollywood_plus")
      title = "Watch Online Full Movie #{@item_details["title"]} |#{@item_details["title"]} Movie - ShemarooMe"
      keywords = "#{@item_details["title"]}, Full Movie #{@item_details["title"]}, Watch #{@item_details["title"]} Online, #{@item_details["title"]} movie Online, Online hindi movie #{@item_details["title"]}, #{@item_details["title"]} Movie"
      description = "Watch #{@item_details["title"]} movie. Access full #{@item_details["title"]} Movie online on ShemarooMe.  Stream hindi Movie #{@item_details["title"]} only on ShemarooMe. Download the ShemarooMe app to get unlimited Bollywood movie access."    
    end
    end
    #description = "Enjoy #{@item_details['title']} online in HD on shemaroome.com"
    image = @item_details["thumbnails"]['large_16_9']['url'] rescue ""
   elsif !@episode_details.nil?
     #title = @episode_details["title"]
     genres = @episode_details['genres'].join(", ")
     title = "Watch Online #{@episode_details['language'].capitalize} #{@episode_details['theme'].capitalize} #{@episode_details['title']} - ShemarooMe"
     #description = @episode_details["description"]
    description = "Watch #{@episode_details["title"]} #{@episode_details['language'].capitalize} #{@episode_details['theme'].capitalize} online on ShemarooMe. Stream full #{@episode_details['language'].capitalize} Show online #{@episode_details["title"]}. Download ShemarooMe app and subscribe to ShemarooMe Premium to stream latest & old Indian Hindi Bollywood, Bollywood Premiere, Bollywood Classic, Gujarati, Marathi, Kids, Bhakti, Comedy and Punjabi movies. Watch #{genres}  #{@episode_details["title"]}  Movie online. Stream #{@episode_details['language']}  Show  #{@episode_details["title"]}  only on ShemarooMe."

     #{@episode_details['language']}   #{@episode_details['theme']}  #{@episode_details['title']} ."  
     image = @episode_details["thumbnails"]['large_16_9']['url']
  elsif !@meta_title.nil?
    title = @meta_title
    description = @meta_desc unless @meta_desc.nil?
    keywords = @meta_keywords unless @meta_keywords.nil?
    end 
    description = "Hi, Have you tried ShemarooMe? They have the bestcollection of Movies, Music, Shows, Plays from Bollywood,Gujarati, Marathi, Punjabi, Bengali. They also have Kids and devotional content.Please use my referral code #{params[:ref_code]} and subscribe to ShemarooMe on and https://play.google.com/store/apps/details?id=com.saranyu.shemarooworld get 20% off on any subscription pack." if params[:action] == "referral_code"
   return title,description,image,keywords
  end

  def get_image_height(layout_type)
    image_name = "horizontal"
   case layout_type
    when "t_1_1_album"
     image_name = "square"
    when "t_2_3_movie"
      image_name = "vertical"
    when "t_16_9_big_meta"
      image_name = "square_big_title"
    when "t_16_9_big"
      image_name = "square_big"  
    when "t_16_9_small_meta" 
      image_name = "horizontal_title"
    when  "t_16_9_epg"
      image_name = "horizontal_title"
  end
  return image_name
end

def get_duration_time(time)
 new_time = time.split(":")
 t = new_time[0]+"h:"+new_time[1]+"m:"+new_time[2]+"s"
end

def get_program_time(item)
  start_time  = (DateTime.parse(item['start_time'])+330.minutes).strftime("%I:%M %P")
  end_time  = (DateTime.parse(item['stop_time'])+330.minutes).strftime("%I:%M %P")
  return start_time +" - "+ end_time 
end

def list_get_program_time(item)
  start_time  = (DateTime.parse(item['start_time'])+330.minutes).strftime("%I:%M %P")
  end_time  = (DateTime.parse(item['end_time'])+330.minutes).strftime("%I:%M %P")
  return start_time +" - "+ end_time 
end

def get_load_more_item_image_url(type,item)
  image_url = item["thumbnails"]["medium_16_9"]['url']
  if type == "t_2_3_movie" || type == "movies"
    image_url = item['thumbnails']['medium_2_3']['url']
  end
  return image_url
end

def get_preview_time(item)
  time = "00:00"
  p item.inspect
  p "@@@@@@@@@@@@@@2"
  unless item["preview_start"].blank?
    start_time = item["preview_start"][0..-4].split(':').map{ |a| a.to_i }.inject(0) { |a, b| a * 60 + b}.to_s
    end_time = item["preview_end"][0..-4].split(':').map{ |a| a.to_i }.inject(0) { |a, b| a * 60 + b}.to_s
    time = start_time+":"+end_time
  end
  return time
end

def get_preview_url(url)
  play_url = ""
  key = ""
  unless url.blank?
    url = sign_smarturl_preview url
    p url.inspect
    p "WWWWWWWWWWWWWWWW"
    if url.include?("adaptive_urls")
      play_url = url["adaptive_urls"].collect{|x|x["playback_url"] if x["label"] == "laptop_free_#{$region.downcase}_logo"}.compact.first
      play_url = url["adaptive_urls"].collect{|x|x["playback_url"] if x["label"] == "laptop_free_in"}.compact.first if play_url.nil?
    else
      play_url = url.collect{|x|x["adaptive_url"]}.compact.first
    end
    p play_url.inspect
    play_url,key = play_url.present? ? encrypt_preview_play_url(play_url) : ""
  end
  return play_url+"preview"+key
end

def sign_smarturl_preview smarturl,val=nil
    smart_url_key = PLAY_URL_TOKEN
    url = "#{smarturl}" + "?service_id=1&protocol=hls&play_url=yes"
    base_url = "#{url}&play_url=yes&us="
    signature = Digest::MD5.hexdigest("#{smart_url_key}#{base_url}")
    signed_url =  "#{base_url}#{signature}"
    header = {"Accept" => "application/json", "Cache-Control" => "no-cache"}
    resp = Typhoeus.get(signed_url,headers:header)
    obj = JSON.parse(resp.body)
    return obj
  end

 def encrypt_preview_play_url(url)
    new_url = ""
    aes = OpenSSL::Cipher::Cipher.new('AES-128-CBC')
    aes.encrypt
    key = aes.random_key
    aes.key = key
    e_key = Base64.encode64(key).gsub(/\n/, '')
    encrypted = aes.update(url) + aes.final
    new_url = Base64.encode64(encrypted).gsub(/\n/, '')
    return new_url,e_key
  end

 def get_image_type(type)
    output = "vertical-image"
    if type == "t_16_9_small" || type == "t_16_9_small_meta"
      output = "horizontal-image"
    end 
    return output
  end

def get_item_box_type(item,theme)
 class_name = "three-feature-box"
 if(theme == "live"|| theme == "linear")
  class_name = "one-feature-box"
 elsif item.has_key?("preview") && item['preview']['preview_available'] == false ||  !item.has_key?("preview") 
   class_name = "two-feature-box"
 end
 return class_name
end

def get_premium_tag(i)
 is_premium = "true"
 if i['access_control']['is_free'] == true && i['access_control']['premium_tag'] == false
  is_premium = "false"
 end
 return is_premium
end

def get_search_premium_tag(item)
  premium_check = "true"
  if $region == "IN"
    if item.has_key?("access_control") && item['access_control']['is_free'] == false
      #premium_check = "false"
    end
  end
  return premium_check
end

def get_new_plan_expiry_date(date,type)
  if type.downcase == "monthly"
    new_date = (DateTime.parse(date)+1.year).strftime("%d %b %Y")
  else
    new_date = (DateTime.parse(date)+1.month).strftime("%d %b %Y")
  end
  return new_date
end

def check_all_access_month(status,type)
  output = true
  if status == false && type == "monthly"
    output = false
  end
  return output
end

def check_all_access_quar(status,type)
  output = true
  if status == false && type == "quarterly"
    output = false
  end
  return output
end



  def get_item_meta_det(item)
  unless item.blank?
    title = item['title']
    desc = item['description']
    keywords = item['keywords']
    theme = item["theme"]
    genres = item["genres"].join(",")
    if item["theme"] == "live" || item["theme"] == "linear"
      if item["friendly_id"] == "prati-shirdi-shirgaon-pune-live-darshan"
        title = "Prati Shirdi Live Darshan - From Shirgaon, Pune | ShemarooMe"
        desc = "The temple is thirteen years old & situated in the village of Shirgaon. Watch Prati Shirdi, Pune Live Darshan Online at ShemarooMe."
        keywords = "Prati Shirdi Live Darshan"
      elsif item["friendly_id"] == "shree-shani-shingnapur-live-darshan"
        title = "Shani Shingnapur Live Darshan Online - from Shingnapur, Maharashtra | ShemarooMe"
        desc = "The popular temple of Lord Shani, the ‘Shani Shingnapur Temple’ is located in Shingnapur village of Ahmednagar district in Maharashtra. Have shani shingnapur live darshan at ShemarooMe"
        keywords = "Shani Shingnapur Live Darshan" 
      elsif item["friendly_id"] == "mumba-devi-temple-mumbai-live-darshan"
        title = "Mumba Devi Live Darshan Online - from Mumbai | ShemarooMe"
        desc = "Mumbadevi temple is dedicated to Goddess ‘Mumba’ and is located in south Mumbai. Have Mumba Devi Live Darshan at ShemarooMe."
        keywords = "Mumba Devi Live Darshan"   
      elsif item["friendly_id"] == "shree-vigneshwara-temple-ozar-live-darshan"
        title = "Shree Vigneshwara Temple Live Darshan Online - from Ozar, Maharashtra | ShemarooMe"
        desc = "The Vigneshwara Temple of Ozar is one of the Ashtavinayaka, the eight revered shrines of Ganesha in Maharashtra, India. Have Vigneshwara Temple Live Darshan at ShemarooMe."
        keywords = "Vigneshwara Temple Live Darshan"     
      elsif item["friendly_id"] == "shri-icchapuran-balaji-mandir-rajasthan-live-darshan"
        title = "Iccha Puran Balaji Sardarshahar Live Darshan Online - from Rajasthan | ShemarooMe"
        desc = "Icchapuran Balaji Temple is a very attractive and beautiful temple of Hanumanji outside Sardarshahar town. Have Icchapuran Balaji Live Darshan at ShemarooMe."
        keywords = "Icchapuran Balaji Live Darshan"
      elsif item["friendly_id"] == "mahalakshmi-mandir-mumbai-live-darshan"
        title = "Mahalakshmi Live Darshan Online - from Mumbai | ShemarooMe"
        desc = "The Mahalakshmi Temple is one of the oldest temples situated in the city of Mumbai, dedicated to Goddess Mahalakshmi - 'Goddess of Wealth'. Have Mahalakshmi Temple Live Darshan at ShemarooMe."
        keywords = "Mahalakshmi Live Darshan"  
      elsif item["friendly_id"] == "sisganj-sahib"
        title = "Gurdwara Sisganj Sahib Live Darshan - from Delhi | ShemarooMe"
        desc = "Gurdwara Sis Ganj Sahib in Delhi was constructed to commemorate the martyrdom site of the ninth Sikh Guru, Guru Tegh Bahadur. Have Gurdwara Sis Ganj Sahib Live Darshan at ShemarooMe."
        keywords = "Sisganj Sahib Live Darshan"
      elsif item["friendly_id"] == "iskcon-girgaon-mumbai"
        title = "ISKCON Live Darshan - ISKCON Mumbai Temple Live Darshan Online | ShemarooMe"
        desc = "The International Society for Krishna Consciousness (ISKCON), is also known as the Hare Krishna movement. Have ISKCON Girgaon Live Darshan at ShemarooMe."
        keywords = "ISKCON Girgaon Live Darshan"
      elsif item["friendly_id"] == "shree-karni-mata-mandir-rajasthan"
        title = "Karni Mata Mandir Live Darshan Online - from Rajasthan | ShemarooMe"
        desc = "Karni Mata Temple is situated at Deshnoke, 30 km from Bikaner, in Rajasthan, India. It is also known as the Temple of Rats. Have Karni Mata Temple Live Darshan at ShemarooMe."
        keywords = "Karni Mata Live Darshan"    
      elsif item["friendly_id"] == "shemaroo-bollywood-classic"
        title = "Classic TV Channel: Classic TV Shows, Movies & Music Channel | ShemarooMe"
        desc = "Watch the best of classic movies, comedy shows, interviews with yesteryear superstars and Filmigaane, available 24x7 at ShemarooMe's classic TV channel"
        keywords = "Classic TV Channel"      
      elsif item["friendly_id"] == "shemaroo-kids"
        title = "Kids TV Channels: Kids TV Shows | ShemarooMe"
        desc = "TV for Kids: Enjoy the 24x7 animated kids channel curated, with a mix of movies, rhymes, mantras and stories, to inculcate Indian values - ShemarooMe"
        keywords = "Kids TV Channel"  
      elsif item["friendly_id"] == "shemaroo-bhakti"
        title = "Devotional Bhakti channel: Devotional TV | ShemarooMe"
        desc = "Watch the curated channel of ShemarooMe which brings the best of Indian devotional content like shlokas, strotas, chants, bhakti geet, documentaries and live stream from famous temples."
        keywords = "Bhakti Channel"  
      elsif item["friendly_id"] == "shemaroo-gujarati"
        title = "Gujarati TV - Gujarati Movies, TV Shows, Music Online | Shemaroo"
        desc = "Enjoy the latest blockbuster Gujarati movies, famous Gujarati plays and songs online, available 24x7 - ShemarooMe"
        keywords = "Gujarati TV Channel"  
      elsif item["friendly_id"] == "shemaroo-ibaadat"
        title = "Ibadat Channel - Deen Ki Baatein, Dua, Naat & Nasheed, & More | ShemarooMe"
        desc = "Enjoy the 24x7 channel of ShemarooMe dedicated to islam with curated content of quran discourse, documentaries, qawalis, naats and live stream from famous masjids."
        keywords = "Ibadat Channel"
      elsif item["friendly_id"] == "shemaroo-punjabi"
        title = "Punjabi TV - Punjabi Channel for Punjabi Superhit Movies, Music & Shows | ShemarooMe"
        desc = "Enjoy this 24x7 Punjabi channel for gurbani music, Punjabi movies, music videos and telefilms."
        keywords = "Punjabi Channel"    
      end
    end
    return title,desc,keywords,theme,genres 
  end
  end

  def get_device_img(item)
    image_name = ""
    device_type = item['device_type'].downcase
    class_name = ""
    p device_type.inspect
    case device_type
      when "web"
        image_name = "desktop.svg"
        class_name = "web_device"
      when "android"
        image_name = "android.svg"
        class_name = "mobile_device"
       when "ios"
        image_name = "iphone.svg"
        class_name = "mobile_device"
      when "tv"
        if item["device_name"].downcase == "rokutv"
          image_name = "tv_icon.svg"
          class_name = "rokutv_device"
        elsif item["device_name"].downcase == "firestick"
          image_name = "fire_tv.svg"
          class_name = "firetv_device"
        elsif item["device_name"].downcase == "appletv"
          image_name = "tv_icon.svg"
          class_name = "appletv_device"
        else
          image_name = "tv_icon.svg"
          class_name = "rokutv_device"
        end
    end
    return image_name, class_name

  end

  def get_video_duration(item)
     dur = item['duration_string'].split(":")
     time = "PT"+dur[1]+"M"+dur[2]+"S"
     return time
  end

  def get_banner_item_url(item)
     url = item["seo_web_url"]
      url = "/#{params[:lang]}"+url if params[:lang].present? && params[:lang] != "en"
     class_name = ""
     if item["list_item_object"]["link"] && item.has_key?("list_item_object") && item["list_item_object"].has_key?("banner_flag") && item["list_item_object"]["banner_flag"] == "non-media"
      case item["list_item_object"]["link"]
       when "view_plans"
        url = "/plans"
       when "register"
        url = "javascript:void(0)"
        class_name = "register"
       when "login"
        url = "javascript:void(0)"
        class_name = "user_login"
       when "static_banner"
        url = "javascript:void(0)"
       else
         if params[:lang].present? && params[:lang] != "en"

          url = "/#{params[:lang]}/#{item["list_item_object"]["link"]}/collection"
         else
          url = "/#{item["list_item_object"]["link"]}/collection"
         end
       end
     end
    return url,class_name
  end
 
  def get_lazy_pay_due_date
    date =  (Date.today + 1.months).strftime("03 %B %Y")
    if (1..14).member?(Date.today.strftime("%e").gsub(" ","").to_i)
     date = Time.now().strftime("18 %B %Y")
    end
    return date
  end



 def get_currency_sign
  symbol = "INR"
  case $region
   when  "US"
    symbol = "USD"
  when "SA"
    symbol = "SAR"
  when  'AE'
    symbol = "AED"
  when  'QA'
    symbol = "QR"
    when  'BH'
    symbol = "BD"
  when 'EG'
    symbol = "EGP"
  when 'CA'
    symbol = "CAD"
  when "FR"
    symbol = "EUR"
 when "ES"
    symbol = "EUR"
  end

 end

 def check_valid_region
  st = "false"
  if $region == "IN" || $region == "US" ||  $region == "BD" || $region == "MY" || $region == "CA"
    st = "true"
  end
  p st.inspect
  return st
end

def check_mnth_pak_status(title,category)
 st = true
 if $region == "IN" && title.downcase == "monthly" && category == "all_access_pack"
  st = false
 end
 return st
end

def get_mov_validity_till(item)
  dt =   (Time.parse(item["price_tag"]["release_date"])+item["price_tag"]["duration"].days).strftime("%d-%b-%y %H:%M %p")
  if (Date.parse(item["price_tag"]["release_date"]) <= Date.today())
   dt =  ((Time.now()+5.hours+30.minutes)+item["price_tag"]["duration"].days).strftime("%d-%b-%y %H:%M %p")
  end
  return dt
end


def get_mov_andrd_till(params)
  if params[:releaseDate].present? && params[:duration].present?
    dt = (Time.parse(params[:releaseDate])+params[:duration].to_i.days).strftime("%d-%b-%y %H:%M %p")
    if (Date.parse(params[:releaseDate]) <= Date.today())
      dt = ((Time.now()+5.hours+30.minutes)+params[:duration].to_i.days).strftime("%d-%b-%y %H:%M %p")
    end
  end
  return dt
end


def get_pln_desc(desc,reg)
  pln_desc = desc
  if reg != "IN"
    pln_desc = desc.gsub("Get One Movie Ticket of Kasaai FREE with ShemarooMe Premium Plan","EXCLUDES SHEMAROOME BOX OFFICE MOVIES")
  end
  return pln_desc
end

def get_ernings_cur_symbol(reg)
  cur_sym = "USD"
  if reg == "IN"
   cur_sym = "INR"
  end
  return cur_sym

end

def encrypt_dt(plain_text)
    key = "shemaroome_website"
    cipher = OpenSSL::Cipher.new('aes-256-cbc').encrypt
    cipher.key = Digest::SHA1.hexdigest(key).unpack('B32').first
    s = cipher.update(plain_text) + cipher.final
    s.unpack('H*')[0].upcase
  end

  def decrypt_dt(cipher_text)
    p cipher_text.inspect
    if cipher_text.present? 
    key = "shemaroome_website"
    cipher = OpenSSL::Cipher.new('aes-256-cbc').decrypt
    cipher.key = Digest::SHA1.hexdigest(key).unpack('B32').first
    s = [cipher_text].pack("H*").unpack("C*").pack("c*")
    cipher.update(s) + cipher.final
    end
  end

  def get_release_date(item)
    item.present? ? item['release_date_string'].split('T').first : ''
  end


  def get_the_lang(lg)
    language = ""
  case lg
   when  "en"
    language = "English"
  when "hi"
    language = "Hindi"
  when  'mr'
    language = "Marathi"
  when  'gu'
    language = "Gujarati"
  end
  return  language
  end
  

  def get_gmfc_time(end_contest_time)
    end_time = end_contest_time.to_time.strftime("%H:%M:%S") 
    start_time = Time.now.strftime("%H:%M:%S") 
    get_time  = TimeDifference.between(start_time, end_time).in_each_component 
    contest_time = Time.at(get_time[:seconds]).utc.strftime("%H:%M:%S")
    return  contest_time 
  end  


  def get_img_type(type)
    val = "vertical"
   if type == "t_16_9_big" || type == "t_16_9_small" || type == "t_16_9_small_meta" || type == "t_16_9_big_meta" || type == "shows"
    val = "horizontal"
   end
   return val
  end

  def get_time_per(pln)
    tot_days = 31
   if pln['plan_title'].downcase == "yearly"
    tot_days = 365
    end
    no_of_days = (Date.parse(pln["valid_till"]) - Date.parse(DateTime.now().to_s)).to_f
    days = no_of_days/tot_days.to_f
    return (days.round(2)).to_s+"%"
  end

  def get_browse_img(type)
    config_resp = Rails.cache.fetch("design_configuration_list_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_configuration      
       }
    brow_img_url = config_resp["data"]["params_hash2"]["config_params"]["layout_scheme"].collect{|x| x["image_url"] if (x["scheme"] == type.gsub("-","_"))}.compact.first
   return brow_img_url
  end

  def get_list_url(l_item)

   ct_name = params[:catalog_name]
   url = l_item['home_link'].present? ?  l_item['home_link'] : l_item['friendly_id']
    case ct_name
    when  "new-home-movies","movie"
      final_url = "/movie"
    when "new-home-shows","show"
      final_url = "/show"
    when  'new-home-livetv','new-livetv-1','live'
     final_url = "/livetv"
    when "new-home-videos","video"
      final_url = "/video"
    else
      final_url  = ""
    end
    if params[:lang].present? && params[:lang] != "en"
      final_url = "/#{params[:lang]}"+final_url
    end
    if url.present?
      list = final_url+"/#{url}/collection"
    else
      list = final_url+"/collection"
    end  
    return list
  end
  
  def get_brd_name(theme)
    th_name = ""
     if cookies[:user_profile_type] == "kids"
      url = "kids"
      th_name = "Movie"
    else
   case theme
  when  "movie"
   th_name = "Movie"
   #url = "new-home-movies"
   url = "movie"
   when  "show","episode"
   th_name = "Shows"
   #url = "new-home-shows"
   url = "show"
   when  "video"
   th_name = "Videos"
   #url = "new-home-videos"
   url = "video"
   when  "live"
   th_name = "Live Tv"
   #url = "new-home-livetv"
   url = "live"
   when "vod_playlist"
    th_name = "Channels"
    url = "channels"
  end
end
  th_name =  get_lang_string("#{th_name.downcase.gsub(' ','_')}") if params[:lang].present? && params[:lang] != "en"
  url = params[:lang]+"/"+url if params[:lang].present? && params[:lang] != "en"
  return "<a href=/#{url}>#{th_name}</a>"
  end


  def get_head_link(hlink,type)
   case hlink
  when  "new-home-movies","movie"
    final_url = "/movie"
    link  = "new-home-movies"
  when "new-home-shows","show"
    final_url = "/show"
    link = "new-home-shows"
  when  'new-home-livetv','new-livetv-1','live','livetv','live-tv-1'
   final_url = "/livetv"
   link = "new-home-livetv"
  when "new-home-videos","video"
    final_url = "/video"
    link = "new-home-videos"
  when "new-home-channels"
    final_url = "/channels"
    link = "new-home-channels"
  else
    final_url  = ""
    link = hlink
  end
   if type != "home"
    final_url = link
  else
    if cookies[:shm_sel_lang].present? && cookies[:shm_sel_lang] != "en"
      final_url = "/"+cookies[:shm_sel_lang]+final_url 
    elsif params[:lang].present? && params[:lang] != "en"
       final_url = "/"+params[:lang]+final_url 
    end
   end
  return final_url
  end


  def get_lang_string(itm)
    return @all_app_str.collect{|k,v| v if k == itm}.compact.first
   end

   def get_lang_popup(itm)
    return @all_app_popup.collect{|k,v| v if k == itm}.compact.first
   end



   def get_lang_url(link)
    if link.present? && link[0] == "/"
      link = link[1..-1]
    end
    new_lang = request.path.split("/")[1]
    if ["en","hi","ma","gu"].include? new_lang
      params[:lang] = new_lang
    end
     final_url = "/#{link}"
       if cookies[:shm_sel_lang].present? && cookies[:shm_sel_lang] != "en"
        final_url = "/#{cookies[:shm_sel_lang]}/#{link}" 
      elsif params[:lang].present? && params[:lang] != "en"
        final_url = "/#{params[:lang]}/#{link}"
      end
      if params[:lang].present? && params[:lang] == "all-channels"
        final_url = "/#{link}"
      end
     return final_url
   end


   def get_cln_url(link)
    if link[0] == "/"
      link = link[1..-1]
    end
    new_lang = request.path.split("/")[1]
    if ["en","hi","ma","gu"].include? new_lang
      params[:lang] = new_lang
    end
     final_url = "/#{link}"
     if link.present?
      if  cookies[:shm_sel_lang].present? && cookies[:shm_sel_lang] != "en"
        final_url = "/#{cookies[:shm_sel_lang]}/#{link}/collection" 
      elsif  params[:lang].present? && params[:lang] != "en"
        final_url = "/#{params[:lang]}/#{link}/collection" 
      else
        final_url = "/#{link}/collection" 
      end
    end
     return final_url
   end

   def get_tit(itm)
    title = itm["title"]
    title = itm["ml_title"] if itm.has_key?("ml_title")
    return title
  end

  def check_month_status(title,category,platform)
   status = "true"
    if $region == "IN" && title == "monthly" && category == "all_access_pack"
      status = "true"
    elsif $region == "MY" && title == "yearly" && category == "all_access_pack" &&  platform == "web"
      status = "false"
    end
     return status
  end

   def payment_success_page(success_page)
      status = false
      if success_page == "amazonpay" || success_page == "googlepay_response" || success_page == "kuwait_viva_billing_response" || success_page == "lazypay_response"  || success_page == "oman_response"  || success_page == "ooredoo_billing_response"  || success_page == "ooredoo_kwbilling_response"  || success_page == "operator_billing_response" || success_page == "payment_response" || success_page == "payment_success" || success_page == "paytm_response" || success_page == "payu_payment_response" || success_page == "razor_pay_response" || success_page == "unifi_response" || success_page == "zain_kwbilling_response"
        status = true
       end
      return status
    end  


  def get_ip_check 
    ip = request.remote_ip
    return ip
  end
end




# On the enter OTP page the due date is incorrect, 
# it should be 3rd Feb (if the payment is done on any date between 18th Jan - 2nd Feb, 
#   the repayment date should be 3rd Feb, and in case 
#   of transaction between 3rd Feb and 
#   17th Feb the repayment date should be 18th Feb).