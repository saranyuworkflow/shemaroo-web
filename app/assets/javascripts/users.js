$(document).ready(function(){
	function get_user_region_time_data(){
    var today = new Date();
  	var date = today.getDate()+'-'+(today.getMonth()+1)+"-"+today.getFullYear();
    var h =  today.getHours(), m = today.getMinutes();
    var time = (h > 12) ? (h-12 + ':' + m +' PM') : (h + ':' + m +' AM');
    var region = $(".user_region").val();
    var user_city = $(".user_city").val();
    return [region,user_city,date,time]
  }

  function user_signup(){
 		$("#signup_user_name_error,#signup_mobile_number_error,#signup_user_email_error,#signup_password_error,#signup_confirm_password_error,#terms_check").hide();
	 	var user_name = $("#user_name").val();
	 	var mobile_no = $("#mobile_number").val();
	 	var email_id = $("#user_email").val();
	 	var pwd = $("#password").val();
	 	var cf_pwd = $("#confirm_password").val();
	 	var terms_check = $("#agree_terms").is(':checked')
	 	var status = false
	 	var user_region = $(".user_region").val();
	 	var browser_details = browser_name();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var net_signup_type = ""
	 	if(user_region == "IN" || user_region == "BD"){
	       if(user_name.trim().length != 0 && mobile_no.length != 0 && mobile_no.length == 10 && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd) && terms_check == true){
	        status = true
	        signup_type = "msisdn"
	        net_signup_type = "phone number"
	 	  }
	 	}
	 	else{
	      if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd) && terms_check == true){
	        status = true
	        signup_type = "email"
	        net_signup_type = "email"
	 	  }
	 	}
	 	if(status == true){
			$("#user_register").text("Register..")
			$.ajax({
				url: "/users/sign_up",
				type: "POST",
				data: { 
				name: user_name, 
				mobileno: mobile_no,
				email_id: email_id,
			  type: signup_type,
				password: pwd,
				browser_det: browser_details
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
				 	var det = get_user_region_time_data()
				 	_paq.push(['trackEvent', 'Users', 'Activity', 'signup', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1] }]);
				   $("#user_register").text("Register")
				   if(signup_type == "msisdn"){
				   	 if(user_region == "IN"){
				   	 	$.cookie('user_registed_mobile_no',"91"+mobile_no, { expires: 14,path: '/'});
				   	 }
				   	 else{
				       $.cookie('user_registed_mobile_no',"880"+mobile_no, { expires: 14,path: '/'});
				       Netcore_Registration_Success(response,net_signup_type,"")
				   	 }
				     fb_pixel_signup_india_event()
				   	 $.cookie('user_registed_mobile_no',"91"+mobile_no, { expires: 14,path: '/'});
				   	 $("#user_name,#user_email,#mobile_number,#password,#confirm_password").val("");
				     window.location = "/users/verify_otp"
				   }
				   else{
				   	 branch.setIdentity(response.user_analytic_id);
				   	set_user_cookies(response);
				   	set_netcore_profile( email_id, mobile_no, user_name,"","","","","","")
				   	Netcore_Registration_Success(response,net_signup_type,"")

				   	//free_trail_fb_google_event()
				   	fb_pixel_signup_us_event()
				   	 gtag('event','Users',
			     {
			   	'dimension4': "Registered",
			   	'dimension11': response.analytics_user_id,
			   	'metric9': 1
			    })
				   	$("#user_name,#user_email,#password,#confirm_password").val("");
	             $("#reg_success").modal({
	                	backdrop: 'static'
	                });				   }
				 }
				else{
					Netcore_Registration_Failure(response.error_message,signup_type)

				 $("#user_register").text("Register")
	        //$("#signup_resp_error_msg").text(response.error_message).show().fadeOut(4500);
	        $("#signup_resp_error_msg").text(response.error_message).show();
				 }
				}
			});
		}
		else if(user_name.trim().length == 0){
      Netcore_Registration_Failure("Name mandatory",signup_type)
		  $("#signup_user_name_error").show();
		}
		else if(user_region == "IN" && mobile_no.length == 0){
      Netcore_Registration_Failure("Mobile number  mandatory",signup_type)
		  $("#signup_mobile_number_error").show();		  
		}
		else if(user_region == "IN" && mobile_no.length != 0 && mobile_no.length != 10){
      Netcore_Registration_Failure("Incorrect mobile number length",signup_type)
		  $("#signup_mobile_number_error").text("Please enter your 10 digit mobile number").show();		  
		}
		else if(user_region != "IN" && email_id.length == 0){
      Netcore_Registration_Failure("Email Mandatory",signup_type)
			$("#signup_user_email_error").show();			
		}
		else if(user_region != "IN" && !regex_email.test(email_id)){
      Netcore_Registration_Failure("Incorrect email format",signup_type)
		  $("#signup_user_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
      Netcore_Registration_Failure("Password Mandatory",signup_type)
		  $("#signup_password_error").show();
		  $("#hint_password").hide();
		}
		else if(pwd.length < 6){
		  $("#signup_password_error").text("Minimum length of 6 characters is mandatory for  Password").show();
		  $("#hint_password").hide();
		}
		else if(cf_pwd.length == 0){
      Netcore_Registration_Failure(" Confirm Password Mandatory",signup_type)
		  $("#signup_confirm_password_error").show();
		}
		else if(cf_pwd.length < 6){
      Netcore_Registration_Failure(" Minimum length of 6 characters is mandatory for Confirm Password",signup_type)
		  $("#signup_confirm_password_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
      Netcore_Registration_Failure(" Passwords do not match",signup_type)
	      $("#signup_confirm_password_error").text("Passwords do not match").show();
		}
		else if(terms_check == false){
		  $("#terms_check").show();
		}
  }

	$("#user_register").click(function(){
	 	user_signup();
	});


// code for  inline errors starts here 

//generic code starts here


	$("#user_name, #mobile_number, #user_email, #password, #confirm_password").focusin(function() {
	  var id = $(this).attr("id");
	  var user_region = $(".user_region").val();
	  var inline_array = ""
		if(user_region == "IN"){
			 inline_array = ["user_name","mobile_number","password","confirm_password"]
		}
		else{			
		  var inline_array = ["user_name","user_email","password","confirm_password",]
		}
			var index = inline_array.indexOf(id)
			elements = inline_array.slice(0,index+1)
			$("#"+id).on('keydown', function(e){
				//alert(e.which)
				var keycode = (window.event) ? event.keyCode : e.keyCode;
	      if (keycode == 9){
	        //alert('tab key pressed');
					for(var i=0; i<= index; i++){
		        var field_value = $("#"+elements[i]).val()
						if(field_value.length == 0){
							//alert("enter")
		      		$("#signup_"+ elements[i]+"_error").text("This field is mandatory").show();
		      		$("#hint_"+elements[i]).hide();
		    		}
		    		else{
		    			$("#signup_"+ elements[i]+"_error").hide();
		    			if(elements[i] == "mobile_number"){
	              if(field_value.length != 10){
	                $("#signup_"+ elements[i]+"_error").text("Please enter your 10 digit mobile number").show();
	                $("#hint_"+elements[i]).hide();
	              }
	              else{
	              	$("#signup_"+ elements[i]+"_error").hide();
	              	$("#hint_"+elements[i]).show();
	              }
		    			}
		    			if(elements[i] == "user_email"){
		    				var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	              if(!regex_email.test(field_value)){
	                $("#signup_"+ elements[i]+"_error").text("Please enter a valid email address").show();
	                $("#hint_"+elements[i]).hide();
	              }
	              else{
	              	$("#signup_"+ elements[i]+"_error").hide();
	              	$("#hint_"+elements[i]).show();
	              }
		    			}
		    			if(elements[i] == "password"){
		    				if(field_value.length < 6){
	                $("#signup_"+ elements[i]+"_error").text("Minimum length of 6 characters is mandatory").show();
	                $("#hint_"+elements[i]).hide();
		    				}
		    				else{
	              	$("#signup_"+ elements[i]+"_error").hide();
	              	$("#hint_"+elements[i]).show();
	              }
		    			}
		    			if(elements[i] == "confirm_password"){
		    				if(field_value.length < 6){
	                $("#signup_"+ elements[i]+"_error").text("Minimum length of 6 characters is mandatory").show();
	                $("#hint_"+elements[i]).hide();
		    				}
		    				else if($("#password").val() != field_value){
		    					("#signup_"+ elements[i]+"_error").text("Passwords do not match").show();
		    					$("#hint_"+elements[i]).hide();
		    				}
		    				else{
	              	$("#signup_"+ elements[i]+"_error").hide();
	              	$("#hint_"+elements[i]).show();
	              }
		    			}
		    		}
		    	}
	      }
	      
			})
	})


	$("#user_name, #mobile_number, #user_email, #password, #confirm_password").keypress(function(e) {
		$("#signup_resp_error_msg").hide();
		if(e.which == 13){
	    var user_region = $(".user_region").val();
	    //if(user_region == "IN"){
	    	var inline_array = ""
	    	if(user_region == "IN"){
	        inline_array = ["user_name","mobile_number","password","confirm_password"]
	    	}
	    	else{
	       inline_array = ["user_name","user_email","password","confirm_password"]
	    	}
				var index = inline_array.indexOf(id);
				var prev_error_status = "false";
				var next_error_status = "false";
				elements = inline_array.slice(0,index+1)
				next_elem = inline_array.slice(0,index+1)
	      var prev_elements = (inline_array.slice(0,index))
	      var next_elements = (inline_array.slice(index+1,4))
				for(i=0;i<prev_elements.length;i++){
					var elem = prev_elements[i];
					if($("#"+elem).val().length == 0){
						prev_error_status = "true"
						$("#"+elem).focus();
						break;
					}
					if($("#"+elem).val().length != 0 && elem == "mobile_number" && $("#signup_mobile_number_error").is(":visible")){
						prev_error_status = "true"
						$("#"+elem).focus();
						break;
					}
					if($("#"+elem).val().length != 0 && elem == "user_email" && $("#signup_user_email_error").is(":visible")){
						prev_error_status = "true"
						$("#"+elem).focus();
						break;
					}
					if($("#"+elem).val().length != 0 && $("#password").val().length != 0  && elem == "confirm_password" && $("#password").val() != $("#confirm_password").val()){
						$("#signup_confirm_password_error").text("Passwords do not match").show();
						prev_error_status = "true"
						$("#"+elem).focus();
						break;
					}
				}
				if(prev_error_status == "false"){
					for(i=0;i<next_elements.length;i++){
						var elem = next_elements[i];
						if($("#"+elem).val().length == 0){
							next_error_status = "true"
							$("#"+elem).focus();
							break;
						}
						if($("#"+elem).val().length != 0 && elem == "mobile_number" && $("#signup_mobile_number_error").is(":visible")){
							prev_error_status = "true"
							$("#"+elem).focus();
							break;
						}
						if($("#"+elem).val().length != 0 && elem == "user_email" && $("#signup_user_email_error").is(":visible")){
							prev_error_status = "true"
							$("#"+elem).focus();
							break;
						}
					}
				}
				if(prev_error_status == "false" && next_error_status == "false"){
					if($("#agree_terms").is(":checked")){
	         user_signup();
					} 
					else{
						$("#terms_check").show();
					}
				}
			}
	})


//generic code ends here


/* code for  inline errors ends here */
	function set_place_holder_position(name){
	  $("#country_code").show();
		$("#"+name).focus();
		$("#"+name).css({"padding":"5px 5px 5px 0px"});
	  $("#"+name).parents(".input-group").children('.fa-circle').css({"top":"-10px","left":"0px"});
	  $("#"+name).nextAll(".input-group .input-label").css({"top":"-14px","left":"0px"});  
	}
	$("#user_name").keypress(function(e){
	  if(e.which == 9){	
	  	var type = $(".user_region").val();
	  	if(type == 'IN'){
	  		set_place_holder_position("mobile_number") 		      	
	  	}
	    else {
	    	set_place_holder_position("user_email") 		      		
	    } 
	  }  
	});

	$("#mobile_number, #user_email").keypress(function(e){
	  if(e.which == 9){	
	    set_place_holder_position("password");
	  }
	});
	$("#password").keypress(function(e){
	  if(e.which == 9){	
	  	set_place_holder_position("confirm_password") 	
	  }
	}); 


	$("#agree_terms").click(function () {
		if ($("#agree_terms").is(":checked")) {
			
			$("#terms_check").hide();
		} else {
		 
			$("#terms_check").show();
		}
	});


	function set_user_cookies(resp){
	 $.cookie('user_id',resp.user_id, { expires: 14,path: '/'});
	 $.cookie('user_login_id',resp.login_id, { expires: 14,path: '/'});
	 $.cookie('user_name',resp.user_name, { expires: 14,path: '/'});
	 $.cookie('profile_id',resp.profile_id, { expires: 14,path: '/'});
	 $.cookie('user_profiles',resp.user_profiles, { expires: 14,path: '/'});
	 $.cookie('is_parent_control',resp.user_parental_control, { expires: 14,path: '/'});
	 $.cookie('pin',resp.pin, { expires: 14,path: '/'});
	 $.cookie('user_profile_type',resp.profile_type, { expires: 14,path: '/'});
	 $.cookie('user_analytical_id',resp.user_analytic_id, { expires: 14,path: '/'});
	 $.cookie('user_actv_sub_status',resp.user_status, { expires: 14,path: '/'});
	}

	$("#verify_otp").click(function(){
		branch_otp_event() 
	  $("#verify_otp_error,#resend_otp_msg").hide();
	  var first_no  = $("#first_digit").val();
	  var second_no = $("#second_digit").val();
	  var third_no  = $("#third_digit").val();
	  var fouth_no  = $("#fourth_digit").val();
	  var browser_details = browser_name();
	  var user_mobile_no = $.cookie("user_registed_mobile_no");
	  if(first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fouth_no.length != 0){
		  $("#verify_otp").text("Verifying..")
			$.ajax({
				url: "/users/validate_otp",
				type: "POST",
				data: { 
				 otp: first_no+second_no+third_no+fouth_no,
				 mobile_no: $.cookie("user_registed_mobile_no"),
				 browser_det: browser_details
				},
				success: function(response,status){
				 console.log(response);
				if(response.status == true){  
					branch_otp_success(response.user_id, response.user_analytic_id)
					localStorage.removeItem("welcome_message_text");
					localStorage.setItem("welcome_message_text", response.verify_otp_message)
					localStorage.removeItem("free_trail")
					localStorage.setItem("free_trail", response.free_trail)
				  set_user_cookies(response)
				  $.removeCookie('user_registed_mobile_no', { path: '/' });
				  localStorage.removeItem("new_user_registered_st")
				  localStorage.setItem("new_user_registered_st","true")
				 /* $("#otp_success").modal({
				  	backdrop: 'static'
				  });*/
				  fb_pixel_signup_success_free_trail_india_event()
				  window.location = "/plans"
				}
				else{
			     var otp_no = first_no+second_no+third_no+fouth_no
				 branch_otp_fail(user_mobile_no,otp_no)
				 $("#verify_otp").text("Verify")
				 $("#first_digit,#second_digit,#third_digit,#fourth_digit").val("");
				 $("#verify_otp_error").text(response.error_message).show();
				}
				}
			});
	  }
	  else if(first_no.length == 0){
		  $("#verify_otp_error").show();
		}
		else if(second_no.length == 0){
		  $("#verify_otp_error").show();
		}
		else if(third_no.length == 0){
		  $("#verify_otp_error").show();
		}
		else if(fouth_no.length == 0){
		  $("#verify_otp_error").show();
		}
	})

	$("#first_digit,#second_digit,#third_digit,#fourth_digit").focus(function(){
	   $("#verify_otp_error").hide();
	})

/*Resend Otp */

	$("#resend_otp").click(function(){
		var mobile_number = getShemarooCookies().user_registed_mobile_no;
		$.ajax({
			url: "/users/resend_otp",
			type: "POST",
			data: { 
			 mobile_no: mobile_number 
			},
			success: function(response,status){
			console.log(response);
			if(response.status == true){  
				$("#verify_otp_error").hide();
			  $("#resend_otp_msg").show();
			}
			else{
				$("#verify_otp_error").hide();
			 	$("#resend_otp_msg").text(response.error_message).show();
			}
			}
		});
	})



   //  function set_netcore_profile1(user_region, city){
   //  	smartech('contact', '60',{ 
   //  'pk^user_id': getShemarooCookies().user_analytical_id,
   //  'INACTIVE_PLANS': 'NA',
   //  'ACTIVE_PLANS': 'NA',
   //  'IS_SUBSCRIBED': '0',
   //  'IS_LOGGEDIN': '1',
   //  'COUNTRY': user_region,
   //  'CITY': city,
   //  'STATE': 'NA'
   // }); 
   //  }


	function log_out(){
	  localStorage.removeItem("no_passwd_chge")
	  var current_path = window.location.pathname;
	  $.ajax({
	    url: "/users/sign_out",
	    type: "POST",
	    data: { },
	    success: function(response,status){
		    if(response.status == true){ 
		    	if(localStorage.getItem("max_device_limit_reached") != undefined){
		    		localStorage.removeItem("max_device_limit_reached")
		    	}
		      delete_user_cookies();
		      window.location = "/";
		    }
	    }
	  });
	}


    function set_netcore_profile(user_email_id, mobile_no, first_name,sub_st,sub_pln,sub_pt,sub_expd,sub_dt,reg_mod){
    var city = $(".user_city").val()
    var user_region = $(".user_region").val()
    smartech('contact', '60',{ 
    "pk^user_id": getShemarooCookies().user_analytical_id,
    "email": user_email_id,
    "mobile": mobile_no,
    "FIRST_NAME": first_name,
    "COUNTRY": user_region,
    "CITY": city,
    "STATE": "NA",
    "REGISTRATION_STATUS": "registered",
    "SUBSCRIPTION_STATUS": sub_st,
    "SUBSCRIPTION_PLAN_NAME": sub_pln,
    "SUBSCRIPTION_PLAN_TYPE": sub_pt,
    "SUBSCRIPTION_EXPIRY_DATE": sub_expd,
    "SUBSCRIPTION_DATE": sub_dt,
    "REGISTRATION_MODE": reg_mod,
    "user_language": getShemarooCookies().shm_sel_lang,
    'user_state': $.cookie('user_actv_sub_status')
   }); 
    apxor_set_user_pro(user_email_id, mobile_no, first_name,sub_st,sub_pln,sub_pt,sub_expd,sub_dt,reg_mod)
    }

	function user_sign_in(){
	 $("#login_mobile_number_error,#login_password_error").hide();
	 var mobile_no = $("#login_mobile_number").val();
	 var user_email_id = $("#login_email").val();
	 var pwd = $("#login_password").val();
	 var user_region = $(".user_region").val();
	 var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 var error_status = false
	 if(user_region == "IN" || user_region == "BD"){
	 	login_type = "msisdn"
	 }
	 else{
	 	login_type = "email"
	 }
	 if(user_region == "IN" || user_region == "BD"){
	  if(mobile_no.length != 0 && mobile_no.length == 10 && pwd.length != 0 ){
	  	error_status = true
	  	login_type = "msisdn"
	  }
	 }
	 else{
	   if(user_email_id.length != 0 && regex_email.test(user_email_id) && pwd.length != 0){
	   	error_status = true
	   	login_type = "email"
	   }
	 } 
	 if(error_status == true){
		$("#user_login").text("Login...")
        var browser_details = browser_name();
		$.ajax({
			url: "/users/sign_in",
			type: "POST",
			data: { 
			mobile_no: mobile_no, 
			email_id: user_email_id,
			password: pwd,
			type: login_type,
			browser_det: browser_details
			},
			//<!-- Video ID(1), Title(2), Bitrate(3), User state(4), User plan(5), User plan type(6), Category(7), Content type(8), Genre(9), Language(10), User ID(11) -->
			success: function(response,status){
			 console.log(response)
			 $("#user_login").text("Login")
			 if(response.status == true){
			    set_user_cookies(response)
			    // d(user_region, response.city_of_user, response.email_id, response.mob_num, response.first_name)
			    branch.setIdentity(response.user_analytic_id);
                branch_user_login_click(response)
           		branch_login_success(login_type)

			    /*branch.logEvent("LOGIN",{'description': 'login',
                 'user_id': response.user_analytic_id },
                function(err) { console.log(err); }
                );*/
			   gtag('event','Users',
			   {
			   	'dimension4': response.user_status,
			   	'dimension11': response.user_analytic_id,
			   	'metric8': 1
			   });
			   if(response.device_limit == "true"){
					  localStorage.removeItem("max_device_limit_reached")
					 	localStorage.setItem("max_device_limit_reached", "true")
				 		$("#device-limit-pop").modal("show");
				 		$("#login_pop").modal("hide");
					 	$("#limit_device_cancel").click(function(){
					 		//$("#device-limit-pop").modal("hide")
					 		$(this).text($("#device_cancel").val() +"...")
					 		log_out();
					 	})
					 	$("#limit_device_confirm").click(function(){
					 		$(this).text($("#manage_devices").val()+"...")
					 		var device_url = "/users/registered_devices"
				      if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
			    			device_url = "/"+getShemarooCookies().shm_sel_lang+"/users/registered_devices"
			        }
			        window.location = device_url
					 	})
					}
			  else{
			   if(localStorage.getItem("activate_tv_path") != undefined){
			   	localStorage.removeItem("activate_tv_path")
			   	var tv_url = "/tv"
		      if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
	    			tv_url = "/"+getShemarooCookies().shm_sel_lang+"/tv"
	        }
	        window.location = tv_url
	             //window.location = "/users/activate_code"
			   }
			   else{
			   	  if(getShemarooCookies().is_parent_control == "true"){
			   	  	var watch_url = "/users/who_is_watching"
				      if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
			    			watch_url = "/"+getShemarooCookies().shm_sel_lang+"/users/who_is_watching"
			        }
			        window.location = watch_url
			   	  }
			   	  else{
			   	  	 _paq.push(['trackEvent', 'Users', 'Activity', 'user_active', 1]);
	                 _paq.push(['trackEvent', 'Users', 'Activity', 'registered_active', 1]);
	                 _paq.push(['resetUserId']);
			   	  	_paq.push(['setUserId', response.user_analytic_id]);
	                _paq.push(['trackPageView']);

	                var home_url = "/"
				      if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
			    			home_url = "/"+getShemarooCookies().shm_sel_lang+"/"
			        }
			        window.location = home_url
			   	  }
			   }
			
			 }
			  }
			else{
			 //$("#login_mobile_number,#login_password,#login_email").val("");
		     $("#bakend_user_errors").text(response.error_message).show();
			 }
			}
		});
	 }
	
	 else if(user_region == "IN" && mobile_no.length == 0){
	  $("#login_mobile_number_error").show();
	 }
	 else if(user_region == "IN" && mobile_no.length != 10){
	  $("#login_mobile_number_error").text("Please enter your 10 digit mobile number").show();
	 }
	 else if(user_region != "IN" && user_email_id.length == 0){
	  $("#login_email_error").text("Please enter your registered email address").show();
	 }
	 else if(user_region != "IN" && !regex_email.test(user_email_id)){
	  $("#login_email_error").text("Invalid email address").show();
	 }
	 else if(pwd.length == 0){
	  $("#login_password_error").show();
	 }
	}

	$("#user_login").click(function(){
	  user_sign_in();
	})

	$("#login_mobile_number,#login_password,#login_email").keypress(function(e){
		$("#bakend_user_errors").hide();
	  if(e.which == 13){
			var id = $(this).attr("id")
		  var val = $("#"+id).val()
		  var user_region = $(".user_region").val();
		  var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;  	
	      if(id == "login_email"){ 
	      	if(!val.length){
	      		$("#"+id+"_error").text("This field is mandatory").show();
	      	}   	
	      	else if(!regex_email.test(val)){
	          $("#login_email_error").text("Please enter your registered email address").show();
	          $("#login_password").focus();
	      	}
	      	else{
	      		$("#login_password").focus();
	      	}
	      }
	      if(id == "login_mobile_number"){
	      	if(!val.length){
	      		$("#"+id+"_error").text("This field is mandatory").show();
	      	}  
	      	else if(val.length != 10){
	      		$("#login_mobile_number_error").text("Please enter your 10 digit mobile number").show();
	      		$("#login_password").focus();
	      	}
	      	else{
	      		$("#login_password").focus();
	      	}
	      }
	      if(id == "login_password"){   	
			    if(user_region == "IN"){
			     	var num = $("#login_mobile_number").val()
			     	if(!num.length || num.length !=10){
			     		$("#login_mobile_number_error").hide();
			     		$("#login_mobile_number").focus();
			     	}
			     	else{
			     		if(!val.length){
	      				$("#"+id+"_error").text("This field is mandatory").show();
	      			}
	      			else{
			    			user_sign_in();  
			    		}	  	      		
			      }     			
			    } 
			    else{
			     	var mail_val = $("#login_email").val()
			    	if(!mail_val.length || (!regex_email.test(mail_val))){
			     		$("#login_email_error").hide();
			     		$("#login_email").focus();
			     	}  
			     	else{
			      	if(!val.length){
	      				$("#"+id+"_error").text("This field is mandatory").show();
	      			}
	      			else{
			    			user_sign_in();  
			    		}	    
			      }    			
			    } 		
	      }
	  }
	}); 

	function validate(){
		var id = $(this).attr("id")
	  var val = $("#"+id).val() 	 
	  if(!val.length){
	    $("#"+id+"_error").text("This field is mandatory").show();
	  }
	  else{
	    $("#"+id+"_error").hide();
	    if(id == "login_email"){
	  	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	    	if(!regex_email.test(val)){
	        $("#login_email_error").text("Please enter your registered email address").show();
	     	}
	    }
	    if(id == "login_mobile_number"){
	    	if(val.length != 10){
	     		$("#login_mobile_number_error").text("Please enter your 10 digit mobile number").show();
	     	}
	    }
	  }
	}

	$("#login_mobile_number,#login_password,#login_email").on('keydown', function(e){
		var keycode = (window.event) ? event.keyCode : e.keyCode;      
	  if(keycode == 9){
	  	validate()
	  }
	});


	$("#login_mobile_number,#login_password,#login_email").focus(function(){
		id = $(this).attr("id")
	 $("#"+id+"_error").hide();
	});
	$("#login_mobile_number,#login_password,#login_email").focusout(function(){
		validate()
	});


	$("#otp_success_close,#user_email_close,#user_email_process,#otp_success_process").click(function(){
		window.location = "/users/who_is_watching_register"
	})

	$("#processed_register_sucess").click(function(){
		window.location = "/users/welcome?t=true"
	})


	function delete_user_cookies(){
    localStorage.removeItem("offer_couponcode_message")
		var  keysToRemove = ["offer_purcahse_promo_flag","offer_coupon_message","offer_purcahse_promo_code","offer_purcahse_promo_id","razorpay_order_id","user_mob_map_st"]
     keysToRemove.forEach(k => localStorage.removeItem(k))
	  _paq.push(['resetUserId']);
	  _paq.push(['trackPageView']);
	  document.cookie = 'user_id' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'user_login_id' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'user_name' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'profile_id' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'user_profiles' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'is_parent_control' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'pin' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'user_profile_type' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'user_analytical_id' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'app_version' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  document.cookie = 'user_actv_sub_status' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}


  $("#user_mobile_signout, #user_signout,#user_logout").click(function(){
  	$("#logout-pop").modal("show");
  })
	$("#logout_cancel").click(function(){
		$("#logout-pop").modal("hide");
  })

	$("#logout_confirm").click(function(){
	 	$("#logout_confirm").val($("#logout_yes").val()+"...");
	 	log_out();
	 })


	function log_out(){
		localStorage.removeItem("no_passwd_chge")
		var current_path = window.location.pathname;
	  $.ajax({
	    url: "/users/sign_out",
	    type: "POST",
	    data: { },
	    success: function(response,status){
	    console.log(response);
	    if(response.status == true){ 
	    	if(firebase.auth().currentUser != null){
		      firebase_logout();
		    }
	      smartech('dispatch', 'Logout', {});
	      delete_user_cookies();
	      branch.logout();
	      var hm_url = "/"
	      if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
    			hm_url = "/"+getShemarooCookies().shm_sel_lang
        }
        window.location = hm_url
	    }
	    }
	  });
	
	}
	  


	$("input[type='text'], input[type='password'], input[type='number'],  input[type='email'], textarea").keyup(function() {
	    var inputlenth = $(this).val().length;
	    var id_val = $(this).attr("id");
	    if(inputlenth > 0) {
	    	
	    	if(id_val == 'mobile_number' || id_val == 'login_mobile_number' || id_val == 'forgot_pass_mobile_number' || 
	    		id_val == 'contact_mobile' || id_val == 'signin_mobile_number' || id_val == 'register_mobile') {
	    		//$(this).parents(".input-group").children('.fa-circle').hide();
	      	$("#country_code").show();
	      	$(this).css({"padding":"5px 5px 5px 0px"});
	      	$(this).parents(".input-group").children('.fa-circle').css({"top":"-10px","left":"0px"});
	      	$(this).parents(".input-group").children('.fa-star').css({"top":"-10px","left":"0px"});
	        $(this).nextAll(".input-group .input-label").css({"top":"-14px","left":"10px"});  
	      }
	      else if (id_val == 'user_email_address' || id_val == 'ro_user_mobile_number') {
	      	$(this).nextAll(".input-group .input-label").css({"top":"-14px","left":"0px !important"});	
	      }
	      else if (id_val == 'card_cvv') {
	      	$(this).nextAll(".input-group img.info").css({"top":"-13px"});	
	      	$(".cvv-label").css({"top":"-14px"});
	      	$(".cvv-circle").css({"top":"-10px"});
	      	
	      }
	      else {
	      	//$(this).parents(".input-group").children('.fa-circle').hide();
	      	$(this).css({"padding":"5px"});
	      	$(this).parents(".input-group").children('.fa-circle').css({"top":"-10px","left":"0px"});
	      	$(this).parents(".input-group").children('.fa-star').css({"top":"-10px","left":"0px"});
	        $(this).nextAll(".input-group .input-label").css({"top":"-14px","left":"10px"});    
	      }
	    }
	    else {
	      
	      if(id_val == 'mobile_number' || id_val == 'login_mobile_number' || id_val == 'forgot_pass_mobile_number' || id_val == 'contact_mobile') {
	      	//$("#country_code").hide();
	      	$(this).parents(".input-group").children('.fa-circle').css({"top":"14px","left":"0px"});    
	      	$(this).parents(".input-group").children('.fa-star').css({"top":"14px","left":"0px"});    
	        $(this).nextAll(".input-group .input-label").css({"top":"12px","left":"10px"});    
	      }
	      else if(id_val == 'user_email_address' || id_val == 'ro_user_mobile_number') {
	      	$(this).nextAll(".input-group .input-label").css({"top":"-14px","left":"10px"});	
	      }
	      else if (id_val == 'card_cvv') {
	      	$(this).nextAll(".input-group img.info").css({"top":"12px"});	
	      	$(".cvv-label").css({"top":"12px"});
	      	$(".cvv-circle").css({"top":"12px"});
	      }
	      else {
	        //$(this).parents(".input-group").children('.fa-circle').show();
	        $(this).parents(".input-group").children('.fa-circle').css({"top":"14px","left":"0px"});    
	        $(this).nextAll(".input-group .input-label").css({"top":"12px","left":"10px"});      
	      }
	    }
	});
	$("input[type='text'], input[type='password'], input[type='number'],  input[type='email']").focusout(function() { 
		var inputlenth = $(this).val().length;
		if(inputlenth > 0) {
			$(this).nextAll(".input-group .input-label").css({"font-size":"0.75em"}); 
		}
	});
	$("textarea").focusout(function() { 
		console.log("DFgdgfd");
		var inputlenth = $(this).val().length;
		if(inputlenth > 0) {
			$(".query_description .fa-circle").css({"top":"-17px"});
			$(".query_description .input-label").css({"top":"-20px","left":"10px", "font-size":"0.75em"});       
		}	
		else {
			$(".query_description .fa-circle").css({"top":"7px", "left": "3px"});
			$(".query_description .input-label").css({"top":"2px","left":"12px"});
		}
	});
	$(".input-group.zipcode input[type='text'], .input-group.zipcode input[type='number']").focusout(function() { 
		var inputlenth = $(this).val().length;
		if(inputlenth > 0) {
			$(this).nextAll(".input-group .input-label").css({"top":"-18px", "left": "0px"}); 
		}
		else {
			$(this).nextAll(".input-group .input-label").css({"top":"8px", "left": "30px"}); 
		}	
	});
	$('input#datepicker').blur(function(){
		var inputlenth = $(this).val().length;
		console.log(inputlenth+"inputlenth")
		if(inputlenth > 0) { 
			$(this).nextAll(".input-group .input-label").css({"top":"-14px","left":"0px"});
		}
		else {
	    $(this).nextAll(".input-group .input-label").css({"top":"12px","left":"10px"});      
	  }
	})
		
	$('textarea').keyup(function() {
    var inputlenth = $(this).val().length;
    if(inputlenth > 0) { 
      $(this).parents(".input-group").children('.fa-circle').css({"top":"-17px"});
      $(this).nextAll(".input-group .input-label").css({"top":"-20px","left":"10px", "font-size":"0.75em"});       
    }
    else {
      //$(this).parents(".input-group").children('.fa-circle').show();
      $(this).parents(".input-group").children('.fa-circle').css({"top":"7px", "left": "3px"});
      $(this).nextAll(".input-group .input-label").css({"top":"2px","left":"12px"});
    }
  }); 	

  function update_profile() {
 	  var profile_name = $("#edit_profile_name").val().trim();
 	  var profile_id = window.location.pathname.split("/users/edit_profile/")[1];
 	  var ischild = $(".material-switch-control-input").is(':checked')
 	  if(profile_name.length != 0){
 	  	$("#update_profile").text($("#edit_done").val()+"..");
     	$.ajax({
	 		  url: "/users/update_profile",
			  type: "POST",
				data: { 
					profileid: profile_id,
					name: profile_name,
					is_child: ischild
				},
				success: function(response,status){
			  		$("#update_profile").text($("#edit_done").val());
			  		$.cookie('user_profiles',response.user_profiles, { expires: 14,path: '/'});
			  		$.cookie('user_name',response.profile_name, { expires: 14,path: '/'});
			  		localStorage.setItem("edit_profile_toast","true");
			  		var url = "/users/manage_profiles";
			  		if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
	               url = "/"+getShemarooCookies().shm_sel_lang+"/users/manage_profiles";
	            }
      				window.location = url
				}
			});
 		}
 		else if(profile_name.length == 0){
    		$("#edit_profile_name_error").show();
 		}
  }

	$("#edit_profile_name").focusin(function(){
	   $("#edit_profile_name_error").hide();
	 });

	$("#delete_user_profile").click(function(){
		$("#delete_prof").modal("show");
	})

  $("#delet_prof_confirm").click(function(){
  	$("#delet_prof_confirm").text("Yes....");
 	 var profile_id = window.location.pathname.split("/users/edit_profile/")[1];
 	 $("#delete_user_profile").text("DELETE THIS PROFILE....");
    $.ajax({
			url: "/users/delete_profile",
			type: "POST",
			data: { 
			profileid: profile_id
			},
			success: function(response,status){
				$.cookie('user_profiles',response.user_profiles, { expires: 14,path: '/'})
				localStorage.setItem("delete_profile_toast","true");
				$("#delete_user_profile").text("DELETE THIS PROFILE....");
				$("#delet_prof_confirm").text("Yes");
				window.location = "/users/manage_profiles"
			 }
	  });
 	});

	function update_personal_details() {
  	$("#user_name_error, #user_email_error, #ro_user_mobile_error").hide();
	 	var status = true;
	 	var name = $("#user_profile_name").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var user_dob = $("#datepicker").val();
	  var user_region = $("#reg").val();
	 	if(user_region == "IN" || user_region == "BD"){
		  var mobile_no = $("#user_mobile_number").val();
		  var user_email = $("#user_email_address").val();
		  if(user_email.length != 0 && !regex_email.test(user_email)){
		  	status = false
		  }

		}
    else{
      var mobile_no = $("#ro_user_mobile_number").val();
      var user_email = $("#ro_user_email_address").val();
      if(mobile_no.length != 0 && mobile_no.length != 10){
	  	  status = false
	    }
    }
	 	 if(name.length != 0 && status == true){
	 	 	$("#update_personal_details").text("DONE...");
		    $.ajax({
					url: "/users/update_personal_details",
					type: "POST",
					data: { 
					 profile_name: name,
					 date_of_birth: user_dob,
					 email_id: user_email,
					 mobile_no: mobile_no  
					},
					success: function(response,status){
					console.log("User profile updated")
					/* clevertap.event.push("User profile updated", {
					    "User name": getShemarooCookies().user_name,
					    "User Email/Mobile": getShemarooCookies().user_login_id
					  });*/
					localStorage.removeItem("update_personal_details_toast");
					if(response.status == true) {
						$("#update_personal_details").text("DONE");		
						localStorage.setItem("update_personal_details_toast","true")
						window.location = "/users/account_details";
				}
			}
			  });
	 	 }
	 	 else if(name.length == 0){
	 	 	$("#user_name_error").show()
	 	 }
		else if(user_region == "IN" && user_email.length !=0 && !regex_email.test(user_email)){
		  $("#user_email_error").text("Invalid email address").show();
		}
		else if(user_region != "IN" && mobile_no.length != 0 && mobile_no.length != 10){
		  $("#ro_user_mobile_error").text("Invalid mobile number").show();
		}
  }

  var user_region = $(".user_region").val();
	var elem = "";
	console.log("user_region "+user_region)
 	if(user_region == "IN"){
 		elem = ".edit_profile_page input[type='text'], .edit_profile_page input[type='email']";		
	}
	else {
		elem = ".edit_profile_page input[type='text'], .edit_profile_page input[type='number']";		
	}
	console.log(elem)
	$(elem).focusin(function(){	
		if(!$("#edit_profile_name").val().length) {
	  	$("#edit_profile_name_error").show();
	  }
	  else {
	  	$("#edit_profile_name_error").hide();	
	  }	  
	});

	$("#edit_profile_name").focusin(function(){
    $("#edit_profile_name_error").hide();
 	});	

	$("#user_email_address").keypress(function(e){
		$("#user_email_error").text("Invalid email address").hide();		    		
		if(e.which == 13){
			if(user_region == "IN"){
		    if($(this).val().length){
		    	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	    		if(!regex_email.test($(this).val())){
	    			$("#user_email_error").text("Invalid email address").show();
	    			if(!$("#user_profile_name").val().length) {
	    				$("#user_profile_name").focus();
	    			}
	    		}
	    		else {
	    			$("#user_email_error").text("Invalid email address").hide();		    			
	    		}
		    }
		  }
		}
  }); 


	$("#update_personal_details").click(function(){
		update_personal_details(); 
	})
  $("#user_profile_name").focusin(function(){
   $("#user_name_error").hide();
  });
	$("#user_email_address").focusin(function(){
		$("#user_email_error").hide();
	});
	$("#ro_user_mobile_number").focusin(function(){
		$("#ro_user_mobile_error").hide();
	});

function add_profile() {
 		$("#profile_name_error").hide();
 	 var profile_name = $("#profile_name").val().trim();
 	 var is_kid_profile =  $("#add_profile_kids").is(':checked');
 	 if(profile_name.length != 0){
 	 	$("#add_profile").text("Adding...");
      $.ajax({
				url: "/users/add_profile",
				type: "POST",
				data: { 
				 profile_name: profile_name,
				 kids_profile: is_kid_profile
				},
				success: function(response,status){
					localStorage.removeItem("add_profile_toast");
					if(response.status == true) {
						$("#add_profile").text("Add");
			      $("#profile_name").val("");
			      $.cookie('user_profiles',response.user_profiles, { expires: 14,path: '/'})
			      localStorage.setItem("add_profile_toast", "true");
			      var url_new_profile  = localStorage.getItem("new_profile_at_register");
			      localStorage.removeItem("new_profile_at_register");
			      if(url_new_profile != undefined){
			      	window.location = url_new_profile;
			      }
			      else{
			      	window.location = '/users/manage_profiles'
			    	}
			    }
			    else {
			    	$("#bakend_addprofile_errors").text(response.error_message).show().fadeOut(4500);	
			    	$("#add_profile").text("Add");
			    }
				}
         
		  });
 	 }
 	 else if(profile_name.length == 0){
 	   $("#profile_name_error").show();
 	 }
 	}
 	 
	$("#add_profile").click(function(){
		add_profile();
	});

	$('#profile_name').keypress(function(e){
	  if(e.which == 13){
		 if($(this).val().length) {
		 	add_profile();
		 } 
	 	}
	});
	$("#update_profile").click(function(){
		update_profile();
	});
	$('#edit_profile_name').keypress(function(e){
	  if(e.which == 13){
		 if($(this).val().length) {
		 	update_profile();
		 } 
	 	}
	});

 $("#profile_name").focusin(function(){
   $("#profile_name_error").hide();
 });


	$("#verify_tv_code").click(function(){
		var first_no = $("#first_char").val();
	    var second_no = $("#second_char").val();
		var third_no = $("#third_char").val();
		var fourth_no = $("#fourth_char").val();
		var fifth_no = $("#fifth_char").val();
		var sixth_no = $("#sixth_char").val();
		if(first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fourth_no.length !=0 && fifth_no.length != 0 && sixth_no.length != 0){
	     $("#verify_tv_code").text("Verifying...");
	     $.ajax({
				url: "/users/activate_code",
				type: "POST",
				data: { code: first_no+second_no+third_no+fourth_no+fifth_no+sixth_no },
				success: function(response,status){
					console.log(response.status);
				 if(response.status == true){
	 	          window.location = "/"
				 }
				 else{
				 	$("#verify_code_error").text(response.message).show().fadeOut(6500);
				 }
	 	     $("#verify_tv_code").text("Verify");
				 }
		  });
		}
		else if(first_no.length == 0 || second_no.length == 0 || third_no.length == 0 || fourth_no.length == 0 || fifth_no.length == 0 || sixth_no.length == 0){
	     $("#verify_code_error").show().fadeOut(6500);
		}
	})


	$("#clear-watch-history").click(function(){
		$("#clear-watch-history-pop").modal("show");
	})
	$("#cancel-clear").click(function(){
		$("#clear-watch-history-pop").modal("hide");
	})

	$("#user_watch_history_clear").click(function(){
		$(this).text($("#clear_his_confirm").val()+"...")
		$.ajax({
			url: "/users/clear_watchhistory",
			type: "POST",
			data: {profile_id: getShemarooCookies().profile_id},
			success: function(response,status){
				$("#user_watch_history_clear").text($("#clear_his_confirm").val());
				$("#clear-watch-history-pop").modal("hide");
				$(".watch_history_toast").show().fadeOut(4500);
				$('html,body').animate({ scrollTop: 300}, 'slow');
			}
		});
	})
  


	$('body').on('click', '.delete_watch_item', function() {
	  var item_id = $(this).attr("id");
	  var item_info = $(this).data("watchlist-info");
	  $.ajax({
			url: "/users/watch_list",
			type: "POST",
			data: {playlist_id: item_id},
			success: function(response,status){
	            // netcore_profile_remove_watch_list(item_info)
				$("#item_"+item_id).hide();
				$("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
				$("#watch_list_delete_toast").show().fadeOut(4500);
				var item_cnt = response.items_count
				if(item_cnt == 0){
					window.location.reload();
				}
			}
		});
	})


/*Start of Parent Control settings */
	$(".parent_control_switch").click(function(){
	  var parent_switch = $(this).is(':checked');
	  if(parent_switch == false){
	  	localStorage.removeItem("parent_control_disable")
	  	localStorage.setItem("parent_control_disable","true")
	  }
	  	window.location = "/users/verify_password"
	})

	function verify_password(){
		 $("#password_verify_error").hide();
		 var pwd = $("#verify_password").val();
		 if(pwd.length != 0 && pwd.length >=6){
		   var parent_switch = localStorage.getItem("parent_control_disable");
		    //localStorage.removeItem("parent_control_disable");
		    var is_remove_pc = "false"
		    if(parent_switch !=  undefined && parent_switch == "true"){
	         is_remove_pc = "true"
		    }
		 	  $("#password_verify").text("Continue....");
		    $.ajax({
				url: "/users/verify_password",
				type: "POST",
				data: {password: pwd,parent_control: is_remove_pc},
				success: function(response,status){

					if(response.status == false){
				    $(".verify_password_message").text(response.error_message)
		        //$("#backend_verify_pwd_errors").text(response.error_message).show().fadeOut(4500);
		             $(".verify_passowrd_toast").show().fadeOut(4500);
					  $("#password_verify").text("Continue");
					}
					else if(response.status == true){
						if(is_remove_pc == "true"){
						  localStorage.removeItem("parent_control_disable")
						  localStorage.setItem("parent_control_toast","true")
	           	          window.location = "/users/settings"
						}
						else if(is_remove_pc == "false"){
							window.location = "/users/create_pin"
						}
					}
				}
			});
		 }
		 else if(pwd.length == 0){
		  $("#password_verify_error").show();
		 }
		 else if(pwd.length < 6){
		 	$("#password_verify_error").text("Password must contain at least 6 characters").show();
		 }
	}

  $("#password_verify").click(function(){
    verify_password()
  })

	$("#verify_password").focusin(function(){
	 $("#password_verify_error").hide();
	});

	$("#verify_password").keypress(function(e){
		if(e.which == 13){
			verify_password();
		}
	});


	function forgot_pin(){
		$("#pass_verify_error").hide();
		var pwd = $("#verify_pass").val();
		if(pwd.length != 0 && pwd.length >=6){
		 	$("#pass_verify").text("Continue....");
		  $.ajax({
				url: "/users/verify_password",
				type: "POST",
				data: {password: pwd},
				success: function(response,status){
					$("#pass_verify").text("Continue");
					if(response.status == false){
		      	$("#backend_error").text(response.error_message).show().fadeOut(4500);
				  }
				  else{
				  	var url = "/users/reset_pin" 
				  	if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
				  		url = "/"+getShemarooCookies().shm_sel_lang+"/users/reset_pin" 
				  	} 
				  	window.location = url         						
				  }				
				}
			});
	  }
		else if(pwd.length == 0){
		  $("#pass_verify_error").show();
		}
		else if(pwd.length < 6){
		 	$("#pass_verify_error").text("Password must contain at least 6 characters").show();
		}
	}

	$("#pass_verify").click(function(){
	  forgot_pin()
	})

	$("#verify_pass").focusin(function(){
	  $("#pass_verify_error").hide();
	});

	$("#verify_pass").keypress(function(e){
		if(e.which == 13){
			forgot_pin();
		}
	});


	function pin_create(){
	 $("#pin_error").hide();
	 var first_no  = $("#first_pwd_digit").val();
	 var second_no = $("#second_pwd_digit").val();
	 var third_no  = $("#third_pwd_digit").val();
	 var fourth_no  = $("#fourth_pwd_digit").val();
	 var user_pin = first_no+second_no+third_no+fourth_no
	 if(first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fourth_no.length !=0 ){
	   $("#create_pin").text($("#confirm").val()+"...")
	   $.ajax({
				url: "/users/create_pin",
				type: "POST",
				data: {pin: first_no+second_no+third_no+fourth_no},
				success: function(response,status){
					if(response.status == true){
					  $.cookie('is_parent_control',"true", { expires: 14,path: '/'})
	          $.cookie('pin',user_pin, { expires: 14,path: '/'})
					  window.location = "/users/settings?type=pin"
					}
				}
			});
	  }
	 else if(first_no.length == 0 || second_no.length == 0 || third_no.length == 0 || fourth_no.length == 0 || fifth_no.length == 0 || sixth_no.length == 0){
	    $("#pin_error").show().fadeOut(4500);
		}
	}

	$("#create_pin").click(function(){
	 new_pin_create();
	})
	$("#first_pwd_digit,#second_pwd_digit,#third_pwd_digit,#fouth_pwd_digit").focusin(function(){
	 $("#pin_error").hide();
	});

	$("#fourth_pwd_digit").keypress(function(e){
		if(e.which == 13){
			pin_create();
		}
	})

	function reset_pin(){
	  $("#pin_errors").hide();
	  var first_no  = $("#first_pn_digit").val();
	  var second_no = $("#second_pn_digit").val();
	  var third_no  = $("#third_pn_digit").val();
	  var fourth_no  = $("#fourth_pn_digit").val();
	  var user_pin = first_no+second_no+third_no+fourth_no
	  var profile = localStorage.getItem("switch_profile_id");
	  if(first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fourth_no.length !=0 ){
	    $("#forgot_pin_reset_pin").text($("#reset_confirm").val()+"....")
	    $.ajax({
				url: "/users/create_pin",
				type: "POST",
				data: {pin: first_no+second_no+third_no+fourth_no},
				success: function(response,status){
					if(response.status == true){
					  $.cookie('is_parent_control',"true", { expires: 14,path: '/'})
	          $.cookie('pin',user_pin, { expires: 14,path: '/'})
					  window.location = "/users/change_profile/"+profile
					}
					$("#forgot_pin_reset_pin").text($("#reset_confirm").val())

				}
			});
	  }
	  else if(first_no.length == 0 || second_no.length == 0 || third_no.length == 0 || fourth_no.length == 0){
	    $("#pin_errors").show().fadeOut(4500);
		}
	}

	$("#forgot_pin_reset_pin").click(function(){
	 new_reset_pin();
	})
	$("#first_pn_digit,#second_pn_digit,#third_pn_digit,#fouth_pn_digit").focusin(function(){
	 $("#pin_errors").hide();
	});

	$("#fourth_pn_digit").keypress(function(e){
		if(e.which == 13){
			reset_pin();
		}
	})

/*End of Parent Control settings */


	$(".profile_item_watching").click(function(){
	  var profile_id = $(this).attr("id");
	  localStorage.removeItem("switch_profile_id")
	  console.log("profile_id",profile_id)
	  localStorage.setItem("switch_profile_id",profile_id)
	  window.location = get_lng_web_url("/users/verify_pin")
	});

	$("#first_pin_digit,#second_pin_digit,#third_pin_digit,#fouth_pin_digit").focusin(function(){
	 $("#verify_pin_error").hide();
	});

	function pin_verify(){
		$("#verify_pin_error").hide();
		var first_no  = $("#first_pin_digit").val();
		var second_no = $("#second_pin_digit").val();
		var third_no  = $("#third_pin_digit").val();
		var fourth_no  = $("#fourth_pin_digit").val();
		if(first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fourth_no.length !=0 ){
		$("#verify_pin").text($("#pin_confirm").val()+"....")
		var user_pin = first_no+second_no+third_no+fourth_no
		var profile =  localStorage.getItem("switch_profile_id");
		if(getShemarooCookies().pin == user_pin ){
			localStorage.setItem("verified_pin", "true")
			localStorage.removeItem("switch_profile_id")
			localStorage.setItem("suc_parent_pin_verify","true")
			var profile_url = "/users/change_profile/"+profile
			if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
				profile_url = "/"+getShemarooCookies().shm_sel_lang+"/users/change_profile/"+profile
			}
			window.location = profile_url
		}
		else{
		$("#verify_pin_error").text($("#incorrect_pin").val()).show();
		$("#verify_pin").text($("#pin_confirm").val());
		$("#forgot_pin_div").show();
		}
		}
		else if(first_no.length == 0 || second_no.length == 0 || third_no.length == 0 || fourth_no.length == 0 || fifth_no.length == 0 || sixth_no.length == 0){
		$("#verify_pin_error").show().fadeOut(4500);
		}
	}


	$("#verify_pin").click(function(){
	 pin_verify();
	})


	$("#fourth_pin_digit").keypress(function(e){
	  if(e.which == 13){
		pin_verify();
	  }
	});
/*Start of Continue watching Remove Item */

	$("#cancel-pop").click(function(){
		$("#ctn_watch_delete_pop").modal("hide");
		$("body").removeClass("modal2_open")
	})

	$('body').on('click', '.remove_continue_item', function() {
	 var list_id = $(this).attr("id");
	 $("#user_playlist_id").val("");
	 $("#user_playlist_id").val(list_id)
	 $("body").addClass("modal2_open")
	 $("#ctn_watch_delete_pop").modal("show");
	});

	$("#user_continue_watch_clear").click(function(){
		var playlist_id = $("#user_playlist_id").val();
		localStorage.removeItem("ctn_watch_delete");
		localStorage.setItem("ctn_watch_delete","true")
		$(this).text($("#continue_confirm").val()+"....");
		$.ajax({
				url: "/users/continue_watching",
				type: "POST",
				data: {playlist_id: playlist_id},
				success: function(response,status){
	        window.location = "/"
				}
			}); 
	})

	$('body').on('click', '.delete_continue_watch_item', function() {
		var playlist_id = $(this).attr("id");
		$("#continue_item_"+playlist_id).hide();
		$.ajax({
				url: "/users/continue_watching",
				type: "POST",
				data: {playlist_id: playlist_id},
				success: function(response,status){
				$("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
	             $("#continue_watch_delete_toast").show().fadeOut(4500);
	              var cnt = response.items_count
	              if(cnt == 0){
	              	window.location.reload();              
	              }
				}
			}); 
	})

	function get_lng_web_url(url){
		var fin_url = url
    if(getShemarooCookies().shm_sel_lang != undefined && getShemarooCookies().shm_sel_lang != "en"){
			fin_url = "/"+getShemarooCookies().shm_sel_lang+url
	  }
	  return fin_url
	}

	function change_password(){
		var pwd = $("#change_password").val();
		var new_password = $("#new_change_password").val();
		var new_confirm_password = $("#new_change_confirm_password").val();
		if(pwd.length != 0 && pwd.length >= 6 && new_password.length != 0 && new_password.length >= 6 && new_confirm_password.length != 0 && new_confirm_password.length >= 6 && new_password == new_confirm_password && pwd != new_password){
			$("#confirm_pass").text($("#pwd_confirm").val()+"..")
			$.ajax({
				url: "/users/change_password",
				type: "POST",
				data: { 
					old_password: pwd,
					new_password: new_password,
					confirm_new_password: new_confirm_password
				},
				success: function(response,status){
					localStorage.removeItem("reset_password_toast")
				  if(response.status == true){
				  	localStorage.setItem("reset_password_toast","true")
				    $("#confirm_pass").text($("#pwd_done").val());
				    //delete_user_cookies();
			
				    window.location = get_lng_web_url("/users/account_details")
				  }
				  else{
				    //$("#change_password").val("");
				    //$("#cp_bakend_user_errors").text(response.error_message).show().fadeOut(2000);
				    $(".change_passowrd_toast").show().fadeOut(4500);
				    $(".change_password_message").text(response.error_message).show().fadeOut(4500);
      			$('html,body').animate({ scrollTop: 300}, 'slow');
				    $("#confirm_pass").text($("#pwd_confirm").val());
				  }
				}
			});
		}
		else if(pwd.length == 0){
		  $("#change_password_error").text($("#pwd_mandatory_info").val()).show();
		}
		else if(pwd.length < 6){
		  $("#change_password_error").text($("#pwd_min_info").val()).show();
		}
		else if(new_password.length == 0){
		  $("#new_change_password_error").text($("#pwd_mandatory_info").val()).show();
		}
		else if(new_password.length < 6){
		  $("#new_change_password_error").text($("#pwd_min_info").val()).show();
		}
		else if(new_confirm_password.length == 0){
		  $("#new_change_confirm_password_error").text($("#pwd_mandatory_info").val()).show();
		}
		else if(new_confirm_password.length < 6){
		  $("#new_change_confirm_password_error").text($("#pwd_min_info").val()).show();
		}
		else if(new_confirm_password != new_password){
			$("#new_change_confirm_password_error").text($("#password_match").val()).show();
		}
		else if(pwd == new_password){
      $(".change_passowrd_toast").show().fadeOut(4500);
      $('html,body').animate({ scrollTop: 300}, 'slow');
    }

	}

	$("#confirm_pass").click(function(){
	  change_password();
	});

	function forgot_password(){
		$("#forgot_pass_mobile_error, #forgot_pass_email_error").hide();
		var mobile_number = $("#forgot_pass_mobile_number").val();
		var email_id = $("#forgot_pass_email").val();
		var user_region = $(".user_region").val();
		var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var error_status = false
		if(user_region == "IN"){
			if(mobile_number.length != 0 && mobile_number.length == 10){
				error_status = true;
			}
		}
		else{
			if(email_id.length != 0 && regex_email.test(email_id)){
			  error_status = true;
			}
		}
		if(error_status == true){
		  $("#confirm_mob_no").text("confirm...")
		  $.ajax({
				url: "/users/forgot_password",
				type: "POST",
				data: { 
					mobile_number: mobile_number,
					email_id: email_id
				},
				success: function(response,status){
				  if(response.status == true){
				    $("#confirm_mob_no").text("confirm");
				  	if(user_region == "IN"){
					    $("#mob_number").val(mobile_number);
					    localStorage.setItem("user_id",mobile_number);
				       window.location = "/users/forgot_password_otp_verify"
					  }
					 else{
	                   $("#fp_bakend_user_errors").text("Please check your mail and follow the instructions to reset your password").show().fadeOut(2000);
					 }
					}
				  else{
					  $("#fp_bakend_user_errors").text(response.error_message).show();
					  $("#confirm_mob_no").text("confirm");
				  }
				}
			})
		}
		else{
		  if(user_region == "IN"){ 
			  if(mobile_number.length == 0){
			    $("#forgot_pass_mobile_error").text("Please enter the mobile number").show();
			    $("#forgot_pass_mobile_number").nextAll(".input-group .input-label").css({"top":"12px","left":"10px"});
			  }
			  else if(mobile_number.length > 0 && mobile_number.length != 10){
			    $("#forgot_pass_mobile_error").css('bottom', '0px').text("Enter the mobile number associated with your ShemarooMe account.").show();
			  }
		  }
		
		  else if(user_region != "IN"){
			  if(email_id.length == 0){
			    $("#forgot_pass_email_error").text($("#email_addr").val()).show();
			  }
			  else if(!regex_email.test(email_id)){
			  	$("#forgot_pass_email_error").text("Enter the email id associated with your ShemarooMe account.").show();
				}
		  }
		}
	}
	$("#forgot_pass_mobile_number").focusin(function(){
		$("#forgot_pass_mobile_error").hide();
	});
	$("#forgot_pass_email").focusin(function(){
		$("#forgot_pass_email_error").hide();
	});
	$("#confirm_mob_no,#confirm_email").click(function(){
		forgot_password();
	});

	$("#forgot_pass_mobile_number,#forgot_pass_email").keypress(function(e){
		$("#fp_bakend_user_errors").hide();
	  if(e.which == 13){
		  forgot_password();
		}
	});
	function forgot_password_verify_otp(){
		$("#fp_verify_otp_error,#fp_resend_otp_msg").hide();
	  var first_no  = $("#fp_first_digit").val();
	  var second_no = $("#fp_second_digit").val();
	  var third_no  = $("#fp_third_digit").val();
	  var fouth_no  = $("#fp_fourth_digit").val();
		if(first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fouth_no.length != 0){
		  $("#fp_verify_otp").text("Verify...")
		  var valid_otp = first_no+second_no+third_no+fouth_no
			$.ajax({
				url: "/users/forgot_password_otp_verify",
				type: "POST",
				data: { 
				  otp: valid_otp,
				  mobile_no: localStorage.getItem("user_id")
				},
				success: function(response,status){
				  if(response.status == true){
				    localStorage.setItem("user_otp",valid_otp);
				    window.location = "/users/reset_password"
				  }
				  else{
				    $("#fp_verify_otp").text("Verify")
				    $("#fp_first_digit,#fp_second_digit,#fp_third_digit,#fp_fourth_digit").val("");
				    $("#fp_verify_otp_error").text(response.error_message).show();
				  }
				}
			});
	  }
	  else if(first_no.length == 0){
		  $("#fp_verify_otp_error").show();
		}
		else if(second_no.length == 0){
		  $("#fp_verify_otp_error").show();
		}
		else if(third_no.length == 0){
		  $("#fp_verify_otp_error").show();
		}
		else if(fouth_no.length == 0){
		  $("#fp_verify_otp_error").show();
		}
	}

	$("#fp_verify_otp").click(function(){
	 forgot_password_verify_otp()
	})
	$("#fp_first_digit,#fp_second_digit,#fp_third_digit,#fp_fourth_digit").focus(function(){
	  $("#fp_verify_otp_error").hide();
	});

	$("#fp_first_digit,#fp_second_digit,#fp_third_digit,#fp_fourth_digit").keypress(function(e){
	 if(e.which == 13){
	 	forgot_password_verify_otp();
	 }
	})

/*Resend Otp */

/*Resend Otp */
	$("#fp_resend_otp").click(function(){
		var mobile_number = localStorage.getItem("user_id")
		$.ajax({
			url: "/users/resend_otp",
			type: "POST",
			data: { 
			  mobile_no: "91"+mobile_number 
			},
			success: function(response,status){
			  if(response.status == true){  
			    $("#fp_resend_otp_msg").show();
			    $("#fp_verify_otp_error").hide();
			  }
			  else{
			    $("#fp_resend_otp_msg").text(response.error_message).show();
			    $("#fp_verify_otp_error").hide();
			  }
			}
		});
	}) 
	function reset_password(){
		var pwd = $("#reset_password").val();
		var confirm_password = $("#new_reset_password").val();
		var otp = $("#reset_password_otp").val();
		if(pwd.length != 0 && pwd.length >= 6 && confirm_password.length != 0 && confirm_password.length >= 6 && pwd == confirm_password && otp.length != 0 ){
			$("#reset_pass").text("Reset..")
			$.ajax({
				url: "/users/reset_password",
				type: "POST",
				data: { 
					otp: otp,
					password: pwd,
					confirm_password: confirm_password				
				},
				success: function(response,status){
					localStorage.removeItem("reset_password_toast")
					localStorage.removeItem("password_reset_toast")
				  if(response.status == true){
				  	//localStorage.setItem("reset_password_toast","true")
				  	localStorage.setItem("password_reset_toast","true")
				    $("#reset_pass").text("Reset");
				    window.location = "/"
				    //$("#login_pop").modal("show")
				  }
				  else{
					  //$("#otp").val("");
					  $("#rp_bakend_user_errors").text(response.error_message).show().fadeOut(2000);
				  }
				}
			})
		}	
		else if(pwd.length == 0){
		  $("#reset_password_error").text("This field is mandatory").show();
		  $("#hint_password").hide();
		}
		else if(pwd.length < 6){
		  $("#reset_password_error").text("Minimum length of 6 characters is mandatory").show();
		  $("#hint_password").hide();
		}
		else if(confirm_password.length == 0){
		  $("#new_reset_password_error").text("This field is mandatory").show();
		}
		else if(confirm_password.length < 6){
		  $("#new_reset_password_error").text("Minimum length of 6 characters is mandatory").show();
		}	
		else if(pwd != confirm_password){
			$("#new_reset_password_error").text("Passwords do not match").show();
		}
	}


	$("#reset_pass").click(function(){
	  reset_password();
	});


	function reset_password_email(){
		$("#reset_pwd_error_email,#new_reset_pwd_email_error").hide();
		var pwd = $("#reset_password_email").val();
		var confirm_password = $("#new_reset_password_email").val();
		var forgot_key = window.location.href.split("?forgot_pin_secret_key=")[1].split("&reset_pwd=")[0]
		if(pwd.length != 0 && pwd.length >= 6 && confirm_password.length != 0 && confirm_password.length >= 6 && pwd == confirm_password){
			$("#reset_pass_email").text("Reset..")
			$.ajax({
				url: "/users/reset_password_email",
				type: "POST",
				data: { 
					key: forgot_key,
					password: pwd,
					confirm_password: confirm_password				
				},
				success: function(response,status){
					 $("#reset_pass_email").text("Reset");
				  if(response.status == true){
				  	console.log(window.location.href)
				  	//var mobile_check = window.location.href.split("?forgot_pin_secret_key=")[1].split("&reset_pwd=")[1].split("&mobile=")[1]
				  	var mobile_check = window.location.href.includes("mobile=true")
				  	console.log(mobile_check)
				  	console.log("Reset Password")
				  	if(mobile_check == true){
				  	  var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
				  	  if(iOS){
				  	  	window.webkit.messageHandlers.deviceStatus.postMessage("resetpwd=success");
				  	  }
				  	  else{
				  	  	console.log("SUCCESS Android")
				  	  	Android.reset_success("password reset successfully")	
				  	  }
				  	  
				  	}
				  	else{
				  		localStorage.setItem("reset_password_email_toast","true")
				  		window.location = "/"
				  	}
				  }
				  else{
					$("#rp_bakend_user_errors_email").text(response.error_message).show().fadeOut(2000);
				  }
				}
			})
		}	
		else if(pwd.length == 0){
		  $("#reset_pwd_error_email").text("Please enter the new password").show();
		}
		else if(pwd.length < 6){
		  $("#reset_pwd_error_email").text("Password must contain at least 6 characters").show();
		}
		else if(confirm_password.length == 0){
		  $("#new_reset_pwd_email_error").text("Please enter the confirm the password").show();
		}
		else if(confirm_password.length < 6){
		  $("#new_reset_pwd_email_error").text("Password must contain at least 6 characters").show();
		}	
		else if(pwd != confirm_password){
			$("#new_reset_pwd_email_error").text("Passwords do not match").show();
		}
	}
	$("#reset_password_email,#new_reset_password_email").focusin(function(){
	 $("#reset_pwd_error_email,#new_reset_pwd_email_error").hide();
	});

	$("#reset_pass_email").click(function(){
	  reset_password_email();
	});

	$("#reset_password_email,#new_reset_password_email").keypress(function(e){
		if(e.which == 13){
			reset_password_email();
		}
	})

/*End of Continue watching Remove Item */



/*Watch Later code */






function netcore_watchlater(){
	var det = get_user_region_time_data()
	var state_of_user = $("#user_state").val();
    var user_period = $("#user_period").val();
    var user_plan_type  = $("#user_plan_type").val();
    var user_plan = $("#user_plan").val();
	_paq.push(['trackEvent', 'Media', 'Activity', 'watch list', 1, {dimension1: 'shemaroo-web', dimension2: $('#language').val(), dimension3: $('#item_category').val(), dimension4: $('#theme_type').val(), dimension5: $("#genre").val(), dimension6: "NA", dimension7: "284p", dimension10: state_of_user, dimension11: user_period, dimension14: getShemarooCookies().user_analytical_id, dimension15: $("#content_id").val(), dimension16: $("#ma_title").val(), dimension12: user_plan_type, dimension13: user_plan }]);
	branch.logEvent(
     "ADD_TO_WISHLIST",
      {'video_id': $("#content_id").val(),
      'title': $("#ma_title").val(),
      'category': $('#item_category').val(),
      'content_type': $('#theme_type').val(),
      'genre': $("#genre").val(),
      'language': $('#language').val(),
      'user_id': getShemarooCookies().user_analytical_id,
      'user_state': state_of_user,
      'user_period': user_period,
      'user_plan_type': user_plan_type,
      'user_plan': user_plan
       },
     function(err) { console.log(err); }
     );
	/*smartech('dispatch', 'media watchlist', {
		'content_type': $('#theme_type').val(),
		'genre': $("#genre").val(),
		'language': $('#language').val(),
		'category': $('#item_category').val(),
		'title': $("#ma_title").val(),
		'user_plan_type': $("#user_plan_type").val(),
      	'user_period': $("#user_period").val()
	});*/
	
};

	  $(".watch_later").click(function(){
		 	var catalogid = $("#catalog_id").val();
		 	var contentid = $("#content_id").val();
		 	var listid = $('#watch_later').data('listitemid');
		 	if(getShemarooCookies().user_id){
		 		var img_src = $(".watch_later").attr("src").split("/assets/")[1];
	      	$.ajax({
					url: "/users/add_watch_later",
					type: "POST",
					data: {catalog_id: catalogid,content_id: contentid,playlist_id: listid},
					success: function(response){
						if(response.status == true){
							Netcore_Watch_Later(response)
           
							$('html,body').animate({ scrollTop: 130}, 'slow');
				       if(response.playlist_id.length == 0){
				      	// netcore_watchlist_remove()
				      	$("#watch_later_message").text($("#my_list_remove").val());
	             	$("#watch_later_pop").show().fadeOut(4500);
	             	$('#watch_later').data('listitemid',"");
	             	if (getShemarooCookies().theme_option == "light_theme"){
	             		$(".watch_later").attr("src","/assets/watch_later.svg");
	             	}else{
	             		$(".watch_later").attr("src","/assets/watch_later_dark.svg");
	             	}
	             	
				      }
				      else{
				      	netcore_watchlater();
				      	branch_watchlist_event()
				      	$("#watch_later_message").text($("#added_to_list").val());
				      	$("#watch_later").data("listitemid",response.playlist_id);
				        $("#watch_later_pop").show().fadeOut(4500);
				        $(".watch_later").attr("src","/assets/watch_list.svg");
				      }
						}
					  else{
					  	$("#watch_later_message").text($("something_wrong").val());
			        $("#watch_later_pop").show().fadeOut(4500);
					  }
					}
			  });
	    /* else{
	     	 $("#watch_later_message").text("Already added to watch later")
	     	 $('html,body').animate({ scrollTop: 130}, 'slow');
	     	 $("#watch_later_pop").show().fadeOut(4500);
	     }*/
		 	}
		 	else{
	      $("#add_watch_list").modal("show");
		 		 //window.location = "/users/login"
		 	}
	  })

	$("#watch_list_confirm").click(function(){
		// $("#watch_list_confirm").text("Proceed....");
		// window.location = "/users/login"
		$("#add_watch_list").modal("hide");
		//$("#sso_mobile_login_pop").modal("show");
		//$("#sso_mobile_login_pop").modal();
		if($(".user_region").val() == "IN"){
			$("#sso_mobile_login_pop").modal();
		}else{
			$("#fb_login_pop").modal("show");
		}



	})

/* End of Watch Later code */


	$("#load_more_continuewatch").click(function(){
	    $(this).text($("#load_more_cont").val()+"...");
	    var pageno = $("#page_no").val();
	    console.log(pageno);
	    $.ajax({
	    url: window.location.pathname,
	    type: "POST",
	    data: {
	    page_no: pageno,
	    },
	    success: function(response,status){
	      $("#load_more_continuewatch").text($("#load_more_cont").val());
	      $("#page_no").val(response.pageno);
	      var items = response.items
	      for(i=0 ; i <items.length; i++){
	        var item_det = items[i].split("$");
	        $("#all_items").append('<div class="item details_episode_horizontal_title" id="continue_item_'+item_det[3]+'"><a href="'+item_det[4]+'"><span class="premium-txt text-uppercase" style="display:'+(item_det[5] == "true" ? "none" : "")+'">premium</span><img src="'+item_det[1]+'" alt="'+item_det[0]+'" title="'+item_det[0]+'"></a><div class="item-feature"><a href="'+item_det[4]+'"><p class="list-tile-title pull-left w-100 margin-bottom-0 text-ellipsis">'+item_det[0]+'</p></a><span class="list-tile-duration pull-left">'+item_det[2]+'</span><span class="pull-right"><img src="/assets/remove.svg" class="delete_continue_watch_item" alt="Delete Watch List" title="Delete Watch List" id="'+item_det[3]+'"></span></div></div>')
	        
	      }
	      if(response.next_page == "false"){
	        $("#load_more_continuewatch_link").hide();
	      }
	      $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
	    }
	  });
	})

	$("#load_more_userrecommendation").click(function(){
	    $(this).text($("#load_more_cont").val()+"...");
	    var pageno = $("#page_no").val();
	    console.log(pageno);
	    $.ajax({
	    url: window.location.pathname,
	    type: "POST",
	    data: {
	    page_no: pageno,
	    },
	    success: function(response,status){
	      $("#load_more_userrecommendation").text($("#load_more_cont").val());
	      $("#page_no").val(response.pageno);
	      var items = response.items
	      for(i=0 ; i <items.length; i++){
	        var item_det = items[i].split("$");
	        $("#all_items").append('<div class="item details_episode_horizontal_title" id="continue_item_'+item_det[3]+'"><a href="'+item_det[4]+'"><span class="premium-img" style="display:'+(item_det[5] == "true" ? "none" : "")+'"><img src="/assets/premium_icon.svg" alt="" title=""></span><img src="'+item_det[1]+'" alt="'+item_det[0]+'" title="'+item_det[0]+'"></a><div class="item-feature"><a href="'+item_det[4]+'"><p class="list-tile-title pull-left w-100 margin-bottom-0 text-ellipsis">'+item_det[0]+'</br></br></p></a><span class="list-tile-duration pull-left">'+item_det[2]+'</span></div></div>')
	        
	      }
	      if(response.next_page == "false"){
	        $("#load_more_userrecommendation_link").hide();
	      }
	      $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
	    }
	  });
	})

	$("#load_more_watchlist").click(function(){
	    $(this).text("Load more...");
	    var pageno = $("#page_no").val();
	    $.ajax({
	    url: window.location.pathname,
	    type: "POST",
	    data: {
	    page_no: pageno,
	    },
	    success: function(response,status){
	      $("#load_more_watchlist").text("Load more");
	      $("#page_no").val(response.pageno);
	      var items = response.items
	      for(i=0 ; i <items.length; i++){
	        var item_det = items[i].split("$");
	        $("#all_episodes").append('<div class="item details_episode_horizontal_title" id="item_'+item_det[3]+'"><a href="'+item_det[4]+'"><span class="premium-img" style="display:'+(item_det[5] == "true" ? "none" : "")+'"><img src="/assets/premium_icon.svg" alt="" title=""></span><img src="'+item_det[1]+'" alt="'+item_det[0]+'" title="'+item_det[0]+'"></a><div class="item-feature"><a href="'+item_det[4]+'"><p class="list-tile-title pull-left w-100 margin-bottom-0 text-ellipsis">'+item_det[0]+'</p></a><span class="list-tile-duration pull-left">'+item_det[2]+'</span><span class="pull-right"><img src="/assets/remove.svg" class="remove delete_watch_item" alt="Delete Watch List" title="Delete Watch List" id="'+item_det[3]+'"></span></div>')        
	      } ;
	      if(response.next_page == "false"){
	        $("#load_more_watchlist_link").hide();
	      }
	      $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
	    }
	  });
	})

	$("#manage_user_profiles").click(function(e){
		if(getShemarooCookies().user_id && getShemarooCookies().user_profile_type == "kids"){
	    e.preventDefault();
	    $("#profile_disable").modal("show");
		}
	})


	$("#kids_cancel,#kids_ok").click(function(){
		$("#profile_disable").modal("hide");
	})

	$("#cancel_login").click(function(){
	  $("#plans_login_pop").modal("hide");
	})

  $(".reset_password_page input[type='password']").focusin(function() { 

  	var id = $(this).attr("id");
  	var inline_array = ["reset_password","new_reset_password"];
		var index = inline_array.indexOf(id);
		elements = inline_array.slice(0,index+1);
		$("#"+id).on('keydown', function(e){
			var keycode = (window.event) ? event.keyCode : e.keyCode;
      if (keycode == 9){
    		for(var i=0; i<= index; i++){
	        var field_value = $("#"+elements[i]).val();
	        if(field_value.length == 0){
						$("#"+elements[i]+"_error").text($("#pwd_mandatory_info").val()).show();
						if(elements[i] == "reset_password"){
							$("#hint_password").hide();
						}
	    		}
	    		else{
	    			$("#"+elements[i]+"_error").hide();
	    			$("#hint_password").show();
    				if(field_value.length < 6){
              $("#"+elements[i]+"_error").text("Minimum length of 6 characters is mandatory").show();
              if(elements[i] == "reset_password"){
								$("#hint_password").hide();
							}
							else{
								$("#hint_password").show();
							}
    				}
    				else{
            	$("#"+elements[i]+"_error").hide();
            	$("#hint_password").show();
            }	    			
	    		}
	    	}
	    }	
  	});
  	for(var i=0; i<= index; i++){
	        var field_value = $("#"+elements[i]).val();
	        if(field_value.length == 0){
						$("#"+elements[i]+"_error").text($("#pwd_mandatory_info").val()).show();
						if(elements[i] == "reset_password"){
							$("#hint_password").hide();
						}
	    		}
	    		else{
	    			$("#"+elements[i]+"_error").hide();
	    			$("#hint_password").show();
    				if(field_value.length < 6){
              $("#"+elements[i]+"_error").text("Minimum length of 6 characters is mandatory").show();
              if(elements[i] == "reset_password"){
								$("#hint_password").hide();
							}
							else{
								$("#hint_password").show();
							}
    				}
    				else{
            	$("#"+elements[i]+"_error").hide();
            	$("#hint_password").show();
            }	    			
	    		}
	    	}
  	$(this).nextAll("label.error").hide();
  	if($("#reset_password_error").is(":visible")){
  		$("#hint_password").hide();
    }
    else{
    	$("#hint_password").show();
    }
	});	



	$("#reset_password").keypress(function(e){
		if(e.which == 13){
			if(!$("#reset_password").val().length) {
				$("#reset_password_error").show();
				$("#hint_password").hide();
				$("#new_reset_password").focus();
			}
			else if($("#reset_password").val().length < 6 ) {
				$("#reset_password_error").text("Minimum length of 6 characters is mandatory").show();
				$("#hint_password").hide();
				$("#new_reset_password").focus();
			}
			else if($("#reset_password").val().length > 5) {										
				$("#new_reset_password").focus();
			}
			else if(($("#reset_password").val().length > 5 && $("#new_reset_password").val().length > 5) && ($("#reset_password").val() != $("#new_reset_password").val())) {						
				$("#new_reset_password_error").text("Passwords do not match").show();
				$("#reset_password").focus();
			}
			else if(($("#reset_password").val().length > 5) && ($("#reset_password").val() == $("#new_reset_password").val())) { 
				reset_password();
			}
			else {
				if(!$("#reset_password").val().length || $("#reset_password").val().length < 6) {
					$("#reset_password").focus();
				}
				else if(!$("#new_reset_password").val().length || $("#new_reset_password").val().length < 6) {
					$("#new_reset_confirm_password").focus();
				}
			}
		}	
	});

	$("#new_reset_password").keypress(function(e){
		if(e.which == 13){
			if(!$("#new_reset_password").val().length) {
				$("#new_reset_password_error").show();
				$("#reset_password").focus();
			}
			else if($("#new_reset_password").val().length < 6 ) {
				$("#new_reset_password_error").text("Minimum length of 6 characters is mandatory").show();
				$("#reset_password").focus();
			}
			else if($("#new_reset_password").val().length > 5 && !$("#reset_password").val().length) {										
				//alert("zdfdsdsf")
				$("#reset_password").focus();
			}
			else if(($("#reset_password").val().length > 5 && $("#new_reset_password").val().length > 5) && ($("#reset_password").val() != $("#new_reset_password").val())) {						
				//alert("DFgfdgfd")
				$("#new_reset_password_error").text("Passwords do not match").show();
				$("#reset_password").focus();
			}
			else if(($("#reset_password").val().length > 5 && $("#new_reset_password").val().length > 5) && ($("#reset_password").val() == $("#new_reset_password").val())) { 
				reset_password();
			}
			else {
				if(!$("#reset_password").val().length || $("#reset_password").val().length < 6) {
					$("#reset_password").focus();
				}
				else if(!$("#new_reset_password").val().length || $("#new_reset_password").val().length < 6) {
					$("#new_reset_password").focus();
				}
			}
		}	
	});




  $(".change_password_page input[type='password']").focusin(function() { 

  	var id = $(this).attr("id");
  	var inline_array = ["change_password","new_change_password","new_change_confirm_password"];
		var index = inline_array.indexOf(id);
		elements = inline_array.slice(0,index+1);
		$("#"+id).on('keydown', function(e){
			var keycode = (window.event) ? event.keyCode : e.keyCode;
      if (keycode == 9){
    		for(var i=0; i<= index; i++){
	        var field_value = $("#"+elements[i]).val();
	        if(field_value.length == 0){
						$("#"+elements[i]+"_error").text($("#pwd_mandatory_info").val()).show();
	    		}
	    		else{
	    			$("#"+elements[i]+"_error").hide();
    				if(field_value.length < 6){
              $("#"+elements[i]+"_error").text($("#pwd_min_info").val()).show();
    				}
    				else{
            	$("#"+elements[i]+"_error").hide();
            }	    			
	    		}
	    	}
	    }	
  	});
  	for(var i=0; i<= index; i++){
	        var field_value = $("#"+elements[i]).val();
	        if(field_value.length == 0){
						$("#"+elements[i]+"_error").text($("#pwd_mandatory_info").val()).show();
	    		}
	    		else{
	    			$("#"+elements[i]+"_error").hide();
    				if(field_value.length < 6){
              $("#"+elements[i]+"_error").text($("#pwd_min_info").val()).show();
    				}
    				else{
            	$("#"+elements[i]+"_error").hide();
            }	    			
	    		}
	    	}
  	$(this).nextAll("label.error").hide();
	});	
  
  $("#change_password").keypress(function(e){
		if(e.which == 13){
			var selected_id = $(this).attr("id");
			if(!$("#change_password").val().length) {
				$("#change_password_error").show();
				if(!$("#new_change_password").val().length || $("#new_change_password").val().length < 6) {
					$("#new_change_password").focus();
				}
				else if(!$("#new_change_confirm_password").val().length || $("#new_change_confirm_password").val().length < 6) {
					$("#new_change_confirm_password").focus();
				}
			}
			else if($("#change_password").val().length < 6) {
				$("#change_password_error").text($("#pwd_min_info").val()).show();
				if(!$("#new_change_password").val().length || $("#new_change_password").val().length < 6) {
					$("#new_change_password").focus();
				}
				else if(!$("#new_change_confirm_password").val().length || $("#new_change_confirm_password").val().length < 6) {
					$("#new_change_confirm_password").focus();
				}
			}
			else {
				if(!$("#new_change_password").val().length || $("#new_change_password").val().length < 6) {
					$("#new_change_password").focus();
				}
				else if(!$("#new_change_confirm_password").val().length || $("#new_change_confirm_password").val().length < 6) {
					$("#new_change_confirm_password").focus();
				}
			}
		}
	});
	$("#new_change_password").keypress(function(e){
		if(e.which == 13){
			if(!$("#new_change_password").val().length) {
				$("#new_change_password_error").text($("#pwd_mandatory_info").val()).show();
				if(!$("#change_password").val().length || $("#change_password").val().length < 6) {
					$("#change_password").focus();
				}
				else if(!$("#new_change_confirm_password").val().length || $("#new_change_confirm_password").val().length < 6) {
					$("#new_change_confirm_password").focus();
				}
			}
			else if($("#new_change_password").val().length < 6 ) {
				$("#new_change_password_error").text($("#pwd_min_info").val()).show();
				if(!$("#change_password").val().length || $("#change_password").val().length < 6) {
					$("#change_password").focus();
				}
				else if(!$("#new_change_confirm_password").val().length || $("#new_change_confirm_password").val().length < 6) {
					$("#new_change_confirm_password").focus();
				}
			}
			else if(($("#new_change_confirm_password").val().length > 5) && ($("#new_change_password").val() != $("#new_change_confirm_password").val())) {
						
				$("#new_change_password_error").text($("#password_match").val()).show();
				$("#new_change_password").focus();
			}
			else if(($("#new_change_password").val().length > 5) && ($("#new_change_password").val() == $("#new_change_confirm_password").val())) { 
				$("#new_change_password_error").hide();
				change_password();
			}
			else {
				if(!$("#change_password").val().length || $("#change_password").val().length < 6) {
					$("#change_password").focus();
				}
				else if(!$("#new_change_confirm_password").val().length || $("#new_change_confirm_password").val().length < 6) {
					$("#new_change_confirm_password").focus();
				}
			}
		}	
	});

	$("#new_change_confirm_password").keypress(function(e){
		if(e.which == 13){
			if(!$("#new_change_confirm_password").val().length) {
				$("#new_change_confirm_password_error").show();
				if(!$("#change_password").val().length || $("#change_password").val().length < 6) {
					$("#change_password").focus();
				}
				else if(!$("#new_change_password").val().length || $("#new_change_password").val().length < 6) {
					$("#new_change_password").focus();
				}
			}
			else if($("#new_change_confirm_password").val().length < 6) {
				$("#new_change_confirm_password_error").text("Minimum length of 6 characters is mandatory").show();
				if(!$("#change_password").val().length || $("#change_password").val().length < 6) {
					$("#change_password").focus();
				}
				else if(!$("#new_change_password").val().length || $("#new_change_password").val().length < 6) {
					$("#new_change_password").focus();
				}
			}
			else if(($("#new_change_password").val().length > 5) && ($("#new_change_password").val() != $("#new_change_confirm_password").val())) {
				$("#new_change_confirm_password_error").text("Passwords do not match").show();
				//$("#new_change_confirm_password").focus();
			}
			else if(($("#new_change_password").val().length > 5) && ($("#new_change_password").val() == $("#new_change_confirm_password").val())) { 
				$("#new_change_confirm_password_error").hide();
				change_password();
			}
			else {
				if(!$("#change_password").val().length || $("#change_password").val().length < 6) {
					$("#change_password").focus();
				}
				else if(!$("#new_change_password").val().length || $("#new_change_password").val().length < 6) {
					$("#new_change_password").focus();
				}
			}
		}

	});
   

   $("#otp_verify").click(function(){
	  $("#otp_verify_error,#resend_otp_msg").hide();
	  var first_no  = $("#first_digit").val();
	  var second_no = $("#second_digit").val();
	  var third_no  = $("#third_digit").val();
	  var fouth_no  = $("#fourth_digit").val();
	  var mobile_number = $.cookie('user_registed_mobile_no');
	  if(first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fouth_no.length != 0){
		  $("#otp_verify").text("Verifying..")
			$.ajax({
				url: "/users/validate_otp",
				type: "POST",
				data: { 
				 otp: first_no+second_no+third_no+fouth_no,
				 mobile_no: mobile_number
				},
				success: function(response,status){
				 console.log(response);
				if(response.status == true){  
				   window.location = "/thanks"
				}
				else{
				 $("#otp_verify").text("Verify")
				 $("#first_digit,#second_digit,#third_digit,#fourth_digit").val("");
				 $("#otp_verify_error").text(response.error_message).show();
				}
				}
			});
	  }
	  else if(first_no.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(second_no.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(third_no.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(fouth_no.length == 0){
		  $("#otp_verify_error").show();
		}
	})

	$("#first_digit,#second_digit,#third_digit,#fourth_digit").focus(function(){
	   $("#otp_verify_error").hide();
	})



  	 $("#offer_user_name,#offer_user_email,#offer_password,#offer_confirm_password,#offers_coupon_code").focusin(function(){
      $("#signup_offer_resp_error_msg").hide();
  	 });


  function set_promo_code_data(){
  	var  keysToRemove = ["offer_purcahse_promo_flag","offer_coupon_message","offer_purcahse_promo_code","offer_purcahse_promo_id"]
  	 keysToRemove.forEach(k => localStorage.removeItem(k))
  	 localStorage.setItem("offer_purcahse_promo_flag","true");
  	 localStorage.setItem("offer_coupon_message","30 days free trail");
     localStorage.setItem("offer_purcahse_promo_code",$("#offers_coupon_code").val());
     localStorage.setItem("offer_purcahse_promo_id",$("#offers_coupon_id").val());
  }

  function offer_user_signup(){
 		$("#signup_offer_user_name_error,#signup_offer_user_email_error,#signup_offer_password_error,#signup_offer_confirm_password_error,#offer_agree_terms, #signup_offers_coupon_code_error, #offers_signup_user_promo_error").hide();
	 	var user_name = $("#offer_user_name").val();
	 	var email_id = $("#offer_user_email").val();
	 	var pwd = $("#offer_password").val();
	 	var cf_pwd = $("#offer_confirm_password").val();
	 	var terms_check = $("#offer_agree_terms").is(':checked')
	 	var status = false
	 	var coupon_check = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var offers_coupon_code = $("#offers_coupon_code").val()
	 	var url = window.location.pathname.split("/")[1]
	 	//if(url == "8koffers"){
          //coupon_check = true
	 	//}
	 	//else{
         if(offers_coupon_code.length != 0 && $('#offers_apply_promo_code').is(':hidden'))
	 	 {
           coupon_check = true
	 	 }
	 	 if(offers_coupon_code.length == 0 && $('#offers_apply_promo_code').is(':visible')){
	 	 	coupon_check = true
	 	 }
	 	//}
    if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd) && terms_check == true && coupon_check == true){
      status = true
      signup_type = "email"
 	  }
	 	if(status == true){
			$("#offer_user_register").text("START FREE TRIAL..")
			$.ajax({
				url: "/users/sign_up",
				type: "POST",
				data: { 
				name: user_name, 
				email_id: email_id,
			    type: signup_type,
				password: pwd
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
				 	var couponcode_message = localStorage.getItem("coupon_message")
				 	localStorage.removeItem("coupon_message")
				 	if(url == "offers" && couponcode_message != undefined || couponcode_message != null){
				 		localStorage.removeItem("offer_couponcode_message")
				   		localStorage.setItem("offer_couponcode_message", couponcode_message)
				 	}
				   $("#offer_user_register").text("START FREE TRIAL")
				   	set_user_cookies(response);
				   	if(url == "offers"){
		               set_promo_code_data()
				   	}
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	$("#offer_user_name,#offer_user_email,#offer_password,#offer_confirm_password").val("");
             /*$("#reg_success").modal({
              	backdrop: 'static'
              });		*/
           
              window.location = get_lng_web_url("/plans")
				 }
				else{
				 $("#offer_user_register").text("START FREE TRIAL")
	        //$("#signup_resp_error_msg").text(response.error_message).show().fadeOut(4500);
	        $("#signup_offer_resp_error_msg").text(response.error_message).show();
				 }
				}
			});
		}
		else if(user_name.trim().length == 0){
		  $("#signup_offer_user_name_error").show();
		}
		else if(user_region != "IN" && email_id.length == 0){
			$("#signup_offer_user_email_error").show();			
		}
		else if(user_region != "IN" && !regex_email.test(email_id)){
		  $("#signup_offer_user_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#signup_offer_password_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#signup_offer_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#signup_offer_confirm_password_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#signup_offer_confirm_password_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	      $("#signup_offer_confirm_password_error").text("Passwords do not match").show();
		}
		else if(terms_check == false){
		  $("#offer_agree_terms").show();
		}
		else if(offers_coupon_code.length != 0 && $('#offers_apply_promo_code').is(':visible')){
			localStorage.removeItem("promo_apply")
			localStorage.setItem("promo_apply", "true")
			offer_promo_code_apply()
		}
  }

	$("#offer_user_register").click(function(){
	 	offer_user_signup();
	});


	function gifts_user_signup(){
 		$("#signup_gifts_user_name_error,#signup_gifts_user_email_error,#signup_gifts_password_error,#signup_gifts_confirm_password_error,#gifts_agree_terms").hide();
	 	var user_name = $("#gifts_user_name").val();
	 	var email_id = $("#gifts_user_email").val();
	 	var pwd = $("#gifts_password").val();
	 	var cf_pwd = $("#gifts_confirm_password").val();
	 	var terms_check = $("#gifts_agree_terms").is(':checked')
	 	var status = false
	 	var user_region = $(".user_region").val();
	 	var coupon_code = $("#gift_coupon_code").val();
	 	var coupon_id = $("#coupon_id").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
 	    if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd) && terms_check == true && coupon_code.trim().length != 0){
        status = true
        signup_type = "email"
 	  }
	 	if(status == true){
			$("#gifts_user_register").text("START STREAMING..")
			$.ajax({
				url: "/users/sign_up",
				type: "POST",
				data: { 
				name: user_name, 
				email_id: email_id,
			  type: signup_type,
				password: pwd,
				coupon: coupon_code,
				coupon_id: coupon_id
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
				   $("#gifts_user_register").text("START STREAMING")
				   	set_user_cookies(response);
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	$("#gifts_user_name,#gifts_user_email,#gifts_password,#gifts_confirm_password").val("");
            window.location = "/users/welcome_offer"			   
				 }
				else{
				 $("#gifts_user_register").text("START STREAMING")
	        $("#signup_gifts_resp_error_msg").text(response.error_message).show();
				 }
				}
			});
		}
		else if(user_name.trim().length == 0){
		  $("#signup_gifts_user_name_error").show();
		}
		else if(user_region != "IN" && email_id.length == 0){
			$("#signup_gifts_user_email_error").show();			
		}
		else if(user_region != "IN" && !regex_email.test(email_id)){
		  $("#signup_gifts_user_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#signup_gifts_password_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#signup_gifts_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#signup_gifts_confirm_password_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#signup_gifts_confirm_password_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	      $("#signup_gifts_confirm_password_error").text("Passwords do not match").show();
		}
		else if(terms_check == false){
		  $("#gifts_agree_terms").show();
		}
		else if (coupon_code.length == 0){
		  $("#signup_user_promo_error").text("please enter the gift card code").show();
		}
  }

  $("#gifts_user_register").click(function(){
	 	gifts_user_signup();
	});
 
 $("#apply_promo_code").click(function(){
 	$("#signup_user_promo_error").hide();
 	var coupon_code = $("#gift_coupon_code").val();
 	if(coupon_code.trim().length != 0){
 		$(this).text("apply..")
		$.ajax({
			url: "/users/validate_promocode",
			type: "POST",
			data: { 
			coupon: coupon_code
			},
			success: function(response,status){
			console.log(response);
			$("#apply_promo_code").text("apply")
			if(response.status == true){  
				$("#signup_user_promo_error").text("coupon applied successfully").show().fadeOut(4500);
				$("#apply_promo_code").hide();
				$("#coupon_id").val(response.coupon_id)
			}
			else{
			$("#apply_promo_code").text("apply")
			$("#signup_user_promo_error").text(response.error_message).show();
			}
		}
		});	
 	}
 });

 $("#gift_coupon_code").focusin(function(){
  $("#signup_user_promo_error").hide();
 });


/* function free_trail_fb_google_event(){
 	  $(".free_trail_fb_pixel script").append(`!function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '397219034188525');
      fbq('track', 'PageView');
      fbq('track', 'StartTrial', {
      value: 0,
      currency: 'USD',
       })`);
      $('.free_trail_fb_pixel').append(`<noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=397219034188525&ev=PageView&noscript=1"
      /></noscript>`);
     $(".free_trail_google_pixel script").append(`function gtag_report_conversion(url) {
      var callback = function () {
      if (typeof(url) != 'undefined') {
      window.location = url;
      }
      };
      gtag('event', 'conversion', {
      'send_to': 'AW-773430063/tUsOCNnRpZgBEK-25vAC',
      'transaction_id': '',
      'event_callback': callback
      });
      return false;
      }`
    )*/


  
 
  function offer_promo_code_apply(){
	 	$("#offers_signup_user_promo_error").hide();
	 	var coupon_code = $("#offers_coupon_code").val();
	 	if(coupon_code.trim().length != 0){
	 		$(this).text("apply..")
			$.ajax({
				url: "/users/validate_promocode",
				type: "POST",
				data: { 
				coupon: coupon_code
				},
				success: function(response,status){
					console.log(response);

					$("#offers_apply_promo_code").text("apply")
					if(response.status == true){  
						localStorage.removeItem("coupon_message")
						localStorage.setItem("coupon_message", response.coupon_message)
						var promo_click = localStorage.getItem("promo_apply")
						localStorage.removeItem("promo_apply")
						if((promo_click != undefined || promo_click != null) && promo_click == "true"){
							offer_user_signup()
						}
						
						$("#offers_signup_user_promo_error").text("coupon applied successfully").show().fadeOut(4500);
						$("#offers_apply_promo_code").hide();
						$("#offers_coupon_id").val(response.coupon_id)
					}
					else{
						$(".dont_have_coupon").show()
						$("#offers_coupon_code").val('')
						$("#offers_apply_promo_code").text("apply")
						$("#offers_signup_user_promo_error").text(response.error_message).show();
					}
				}
			});	
	 	}
  };

  $("#offers_coupon_code").focusin(function(){
  	$(".dont_have_coupon").hide()
  })

$("#offers_apply_promo_code").click(function(){
  offer_promo_code_apply()
})

 $("#offers_coupon_code").focusin(function(){
  $("#offers_signup_user_promo_error").hide();
 });


 $("#marketing_user_email,#marketing_password").focusin(function(){
      $("#signup_marketing_resp_error_msg").hide();
  	 });


  function marketing_user_signup(){
 		$("#signup_marketing_user_email_error,#signup_marketing_password_error").hide();
	 	var email_id = $("#marketing_user_email").val();
	 	var pwd = $("#marketing_password").val();
	 	var status = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if(email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 ){
      status = true
      signup_type = "email"
 	  }
	 	if(status == true){
			$("#marketing_user_register").text("START 7 DAY FREE TRIAL..")
			$.ajax({
				url: "/users/sign_up",
				type: "POST",
				data: { 
				name: "guest", 
				email_id: email_id,
			  type: signup_type,
				password: pwd
				},
				success: function(response,status){
				 //console.log(response);
				 if(response.status == true){
				   $("#marketing_user_register").text("START 7 DAY FREE TRIAL")
				   	set_user_cookies(response);
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	$("#marketing_user_email,#marketing_password").val("");
             /*$("#reg_success").modal({
              	backdrop: 'static'
              });		*/
                 window.location = "/plans/plans_summary"		   
				 }
				else{
				 $("#marketing_user_register").text("START 7 DAY FREE TRIAL")
	        //$("#signup_resp_error_msg").text(response.error_message).show().fadeOut(4500);
	        $("#signup_marketing_resp_error_msg").text(response.error_message).show();
				 }
				}
			});
		}
		else if(email_id.length == 0){
			$("#signup_marketing_user_email_error").show();			
		}
		else if(!regex_email.test(email_id)){
		  $("#signup_marketing_user_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#signup_marketing_password_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#signup_marketing_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
  }

	$("#marketing_user_register").click(function(){
	 	marketing_user_signup();
	});



	

	$(".search_icon").click(function(){
		smartech('dispatch', 'search', {});
	})	
	$(".view_plans").click(function(){
	})
	$("#header_tv_icon").click(function(){
		smartech('dispatch', 'Browse_Tv', {
			"user_id": getShemarooCookies().user_analytical_id,
            "channel_name": "" ,
            'user_state':$.cookie('user_actv_sub_status')
		});

		/*Apxor.logEvent('Browse_Tv', {
		"channel_name": channel_name,
		"COUNTRY": user_region
 		},"BROWSE");*/

	})
	
	$("#non_kids_tabs .nav-item a,#kids_tabs .nav-item a").click(function(){
	})
 


  function netcore_profile_remove_watch_list(item){
    	var det = item.split("|");
    	var state_of_user = $("#user_state").val();
    	var user_region = $(".user_region").val()
    	smartech('dispatch', 'remove from watchlist', {
    'user_id': getShemarooCookies().user_analytic_id,
	'content_type': det[0],
	'genre': det[1],
	'language': det[2],
	'category': det[3],
	'title': det[4],
	'COUNTRY':user_region,
	'user_state': $.cookie('user_actv_sub_status')
	});
    }
  
  function fb_pixel_signup_india_event(){
	  fbq('track', 'SignupInitiated', {
	    value: 0,
	    currency: 'INR',
	  });
  }

  function fb_pixel_signup_success_free_trail_india_event(){
  	fbq('track', 'SignupSuccess', {
    value: 0,
    currency: 'INR',
    });
    fbq('track', 'StartTrial');
  }
  
  function fb_pixel_signup_us_event(){
	  fbq('track', 'SignupInitiated', {
	    value: 0,
	    currency: 'USD',
	  });
	  /*fbq('track', 'SignupSuccess', {
    value: 0,
    currency: 'USD',
    });*/
  }

 function fb_pixel_signup_sucs_event(){
 	var user_region = $(".user_region").val();
 	var cur = 'INR'
 	if(user_region == "US"){
 		cur = 'USD'
 	}
	fbq('track', 'SignupSuccess', {
    value: 0,
    currency: cur,
    });
  }

  function hotstar_registration(){
  	$("#signup_hotstar_user_email_error, #signup_hotstar_password_error").hide()
  	var mail_id = $("#hotstar_user_email").val()
  	var password = $("#hotstar_password").val()
  	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
  	if(mail_id.length != 0 && regex_email.test(mail_id) && password.length != 0 && password.length >= 6){
  		$("#hotstar_user_register").text("PROCEED...")
  		var signup_type = "email"
  		$.ajax({
  			url: "/users/sign_up",
  			type: "POST",
  			data: {
  				name: "guest",
  				email_id: mail_id,
  				type: signup_type,
  				password: password
  			},
  			success: function(response,status){
  				if(response.status == true){
  					$("#hotstar_user_register").text("PROCEED")
  					set_user_cookies(response);
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	var promo_status = localStorage.getItem("coupon_message")
				   	set_pack_info()
				   	window.location = "/plans/plans_summary"
  				}
  				else{
  					$("#hotstar_user_register").text("PROCEED")
  					$("#signup_offer_resp_error_msg").text(response.error_message).show();
  				}
  			}
  		})
  	}
  	else if(mail_id.length == 0){
  		$("#signup_hotstar_user_email_error").show()
  	}
  	else if(!regex_email.test(mail_id)){
  		$("#signup_hotstar_user_email_error").text("Please enter a valid email address").show();		  
  	}
  	else if(password.length == 0){
  		$("#signup_hotstar_password_error").show()
  	}
  	else if(password.length < 6){
  		$("#signup_hotstar_password_error").text("Minimum length of 6 characters is mandatory").show()
  	}
  }

  $("#hotstar_user_register").click(function(){
  	hotstar_registration()
  })
  




	function user_signup(){
 		$("#register_name_error,#register_mobile_error,#register_email_error,#register_password_error,#terms_check").hide();
	 	var user_name = $("#register_name").val();
	 	var mobile_no = $("#register_mobile").val();
	 	var email_id = $("#register_email").val();
	 	var pwd = $("#register_password").val();
	 	var terms_check = $("#register_agree_terms").is(':checked');
	 	var status = false;
	 	var user_region = $(".user_region").val();
	 	var browser_details = browser_name();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	if(user_region == "IN"){
	       if(user_name.trim().length != 0 && mobile_no.length != 0 && mobile_no.length == 10 && pwd.length != 0 && pwd.length >=6 && terms_check == true){
	        status = true
	        signup_type = "msisdn"
	 	  }
	 	}
	 	else{
	      if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && terms_check == true){
	        status = true
	        signup_type = "email"
	 	  }
	 	}
	 	if(status == true){
			$("#user_register_bottom").text("Register..")
			$.ajax({
				url: "/users/sign_up",
				type: "POST",
				data: { 
				name: user_name, 
				mobileno: mobile_no,
				email_id: email_id,
			  type: signup_type,
				password: pwd,
				browser_det: browser_details
				},
				success: function(response,status){
				console.log(response);

					$("#hotstar_apply_promo").text("apply")
					if(response.status == true){  
    				$("#hotstar_promo_code").text(coupon_code);
    				$("#hotstar_promo_txt").show()
    				$("#hotstar_apply_promo_code").hide();
						localStorage.removeItem("coupon_message")
						localStorage.setItem("coupon_message", response.coupon_info.coupon_message)
						localStorage.setItem("promo_apply", "true")
						$("#promo_code_id").val(response.coupon_id)
						set_hotstar_promo_code_data(response.coupon_info.coupon_message,coupon_code)
						$("#promo_code_error").text("coupon applied successfully").show().fadeOut(4500);
	 					$('#hotstar_promocode_pop').modal('hide');
	 					$("#promo_code").val('')
					}
					else {
						$("#hotstar_apply_promo").text("apply")
						$("#promo_code").val('')
						$("#promo_code_backend_error").text(response.error_message).show();
					}
				 console.log(response);
				 if(response.status == true){
				 	
				 	_paq.push(['trackEvent', 'Users', 'Activity', 'signup', 1]);
				   $("#user_register_bottom").text("Register")
				   if(signup_type == "msisdn"){
				     fb_pixel_signup_india_event()
				   	 $.cookie('user_registed_mobile_no',"91"+mobile_no, { expires: 14,path: '/'});
				   	 $("#user_name,#user_email,#mobile_number,#password,#confirm_password").val("");
				     window.location = "/users/verify_otp"
				   }
				   else{
				   	 branch.setIdentity(response.user_analytic_id);
				   	set_user_cookies(response);
				   	//free_trail_fb_google_event()
				   	fb_pixel_signup_us_event()
				   	 gtag('event','Users',
			     {
			   	'dimension4': "Registered",
			   	'dimension11': response.analytics_user_id,
			   	'metric9': 1
			    })
				   	$("#user_name,#user_email,#password,#confirm_password").val("");
	             $("#reg_success").modal({
	                	backdrop: 'static'
	                });				   }
				 }
				else{
				 $("#user_register_bottom").text("Register")
	        //$("#signup_resp_error_msg").text(response.error_message).show().fadeOut(4500);
	        $("#register_resp_error_msg").text(response.error_message).show();
				 }
				}
			});
		}
		else if(user_name.trim().length == 0){
		  $("#register_name_error").show();
		}
		else if(user_region == "IN" && mobile_no.length == 0){
		  $("#register_mobile_error").show();		  
		}
		else if(user_region == "IN" && mobile_no.length != 0 && mobile_no.length != 10){
		  $("#register_mobile_error").text("Please enter your 10 digit mobile number").show();		  
		}
		else if(user_region != "IN" && email_id.length == 0){
			$("#register_email_error").show();			
		}
		else if(user_region != "IN" && !regex_email.test(email_id)){
		  $("#register_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#signup_password_error").show();
		  $("#hint_password").hide();
		}
		else if(pwd.length < 6){
		  $("#register_password_error").text("Minimum length of 6 characters is mandatory").show();		 
		}
		else if(terms_check == false){
		  $("#terms_check").show();
		}
  }

	/*$("#user_register_bottom").click(function(){
	 	user_signup();
	});*/

function pop_user_sign_in(){
	 $("#signin_mobile_number_error,#signin_email_error, #signin_password_error").hide();
	 var mobile_no = $("#signin_mobile_number").val();
	 var user_email_id = $("#signin_email").val();
	 var pwd = $("#signin_password").val();
	 var user_region = $(".user_region").val();
	 var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 var error_status = false
	 if(user_region == "IN" || user_region == "BD"){
	 	login_type = "msisdn"
	 }
	 else{
	 	login_type = "email"
	 }
	 if(user_region == "IN" || user_region == "BD"){
	  if(mobile_no.length != 0 && mobile_no.length == 10 && pwd.length != 0 ){
	  	error_status = true
	  	login_type = "msisdn"
	  }
	 }
	 else{
	   if(user_email_id.length != 0 && regex_email.test(user_email_id) && pwd.length != 0){
	   	error_status = true
	   	login_type = "email"
	   }
	 } 
	 if(error_status == true){
		$("#user_signin").text("Login...")
        var browser_details = browser_name();
		$.ajax({
			url: "/users/sign_in",
			type: "POST",
			data: { 
			mobile_no: mobile_no, 
			email_id: user_email_id,
			password: pwd,
			type: login_type,
			browser_det: browser_details
			},
			//<!-- Video ID(1), Title(2), Bitrate(3), User state(4), User plan(5), User plan type(6), Category(7), Content type(8), Genre(9), Language(10), User ID(11) -->
			success: function(response,status){
			 console.log(response)
			 $("#user_signin").text("Login")
			 if(response.status == true){
			    set_user_cookies(response)
			    d(user_region, response.city_of_user, response.email_id, response.mob_num, response.first_name)
			    branch.setIdentity(response.user_analytic_id);
			    /*branch.logEvent("LOGIN",{'description': 'login',
                 'user_id': response.user_analytic_id },
                function(err) { console.log(err); }
                );*/
               branch_user_login_click(response)
               branch_login_success(login_type)
			   gtag('event','Users',
			   {
			   	'dimension4': response.user_status,
			   	'dimension11': response.user_analytic_id,
			   	'metric8': 1
			   });
			   if(response.device_limit == "true"){
					 	$("#login_pop").modal("hide")
					  localStorage.removeItem("max_device_limit_reached")
					 	localStorage.setItem("max_device_limit_reached", "true")
				 		$("#device-limit-pop").modal("show")
					 	$("#limit_device_cancel").click(function(){
					 		//$("#device-limit-pop").modal("hide")
					 		$(this).text($("#device_cancel").val()+"...")
					 		log_out();
					 	})
					 	$("#limit_device_confirm").click(function(){
					 		$(this).text($("#manage_devices").val()+"...")
					 		var device_url = "/users/registered_devices"
				      if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
			    			device_url = "/"+getShemarooCookies().shm_sel_lang+"/users/registered_devices"
			        }
			        window.location = device_url
					 	})
					}
			  else{
			   if(localStorage.getItem("activate_tv_path") != undefined){
			   	localStorage.removeItem("activate_tv_path")
			   	window.location = "/tv"
	             //window.location = "/users/activate_code"
			   }
			   else{
			   	  if(getShemarooCookies().is_parent_control == "true"){
			   	  	 window.location = "/users/who_is_watching"
			   	  }
			   	  else{
			   	  	 _paq.push(['trackEvent', 'Users', 'Activity', 'user_active', 1]);
	                 _paq.push(['trackEvent', 'Users', 'Activity', 'registered_active', 1]);
	                 _paq.push(['resetUserId']);
			   	  	_paq.push(['setUserId', response.user_analytic_id]);
	                _paq.push(['trackPageView']);
			   	  	 //window.location = "/"
			   	  	 window.location.reload();
			   	  }
			   }
			
			 }
			  }
			else{
			 //$("#login_mobile_number,#login_password,#login_email").val("");
		     $("#bakend_user_errors_msg").text(response.error_message).show();
			 }
			}
		});
	 }
	 else if((user_region == "IN" ||  user_region == "BD") && mobile_no.length == 0){
	  $("#signin_mobile_number_error").text("This field is mandatory").show();
	 }
	 else if((user_region == "IN" ||  user_region == "BD") && mobile_no.length != 10){
	  $("#signin_mobile_number_error").text("Please enter your 10 digit mobile number").show();
	 }
	 else if(user_region == "US" && user_email_id.length == 0){
	  $("#signin_email_error").text("Please enter your registered email address").show();
	 }
	 else if(user_region == "US" && !regex_email.test(user_email_id)){
	  $("#signin_email_error").text("Invalid email address").show();
	 }
	 else if(pwd.length == 0){
	  $("#signin_password_error").text("This field is mandatory").show();
	 }
	}

	$("#user_signin").click(function(){
	  pop_user_sign_in();
	})


	$("#signin_mobile_number,#signin_password,#signin_email").keypress(function(e){
		$("#bakend_user_errors_msg").hide();
	  if(e.which == 13){
			var id = $(this).attr("id")
		  var val = $("#"+id).val()
		  var user_region = $(".user_region").val();
		  var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;  	
	      if(id == "signin_email"){ 
	      	if(!val.length){
	      		$("#"+id+"_error").text("This field is mandatory").show();
	      	}   	
	      	else if(!regex_email.test(val)){
	          $("#signin_email_error").text("Please enter your registered email address").show();
	          $("#signin_password").focus();
	      	}
	      	else{
	      		$("#signin_password").focus();
	      	}
	      }
	      if(id == "signin_mobile_number"){
	      	if(!val.length){
	      		$("#"+id+"_error").text("This field is mandatory").show();
	      	}  
	      	else if(val.length != 10){
	      		$("#signin_mobile_number_error").text("Please enter your 10 digit mobile number").show();
	      		$("#signin_password").focus();
	      	}
	      	else{
	      		$("#signin_password").focus();
	      	}
	      }
	      if(id == "signin_password"){   	
			    if(user_region == "IN"){
			     	var num = $("#signin_mobile_number").val()
			     	if(!num.length || num.length !=10){
			     		$("#signin_mobile_number_error").hide();
			     		$("#signin_mobile_number").focus();
			     	}
			     	else{
			     		if(!val.length){
	      				$("#"+id+"_error").text("This field is mandatory").show();
	      			}
	      			else{
			    			pop_user_sign_in();  
			    		}	  	      		
			      }     			
			    } 
			    else{
			     	var mail_val = $("#signin_email").val()
			    	if(!mail_val.length || (!regex_email.test(mail_val))){
			     		$("#signin_email_error").hide();
			     		$("#signin_email").focus();
			     	}  
			     	else{
			      	if(!val.length){
	      				$("#"+id+"_error").text("This field is mandatory").show();
	      			}
	      			else{
			    			pop_user_sign_in();  
			    		}	    
			      }    			
			    } 		
	      }
	  }
	}); 

	function validate_signin(){
		var id = $(this).attr("id")
	  var val = $("#"+id).val() 	 
	  if(!val.length){
	    $("#"+id+"_error").text("This field is mandatory").show();
	  }
	  else{
	    $("#"+id+"_error").hide();
	    if(id == "signin_email"){
	  	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	    	if(!regex_email.test(val)){
	        $("#signin_email_error").text("Please enter your registered email address").show();
	     	}
	    }
	    if(id == "signin_mobile_number"){
	    	if(val.length != 10){
	     		$("#signin_mobile_number_error").text("Please enter your 10 digit mobile number").show();
	     	}
	    }
	  }
	}

	$("#signin_mobile_number,#signin_password,#signin_email").on('keydown', function(e){
		var keycode = (window.event) ? event.keyCode : e.keyCode;      
	  if(keycode == 9){
	  	validate_signin()
	  }
	});


	$("#signin_mobile_number,#signin_password,#signin_email").focus(function(){
		id = $(this).attr("id")
	 $("#"+id+"_error").hide();
	});
	$("#signin_mobile_number,#signin_password,#signin_email").focusout(function(){
		validate_signin()
		if($(this).val().length > 0) {
			$(".country_code").show();
			$(this).parents('.input-group').find('.input-label').css({'top':'-14px','left': '0px'});	
		}
		else {
			$(".country_code").hide();
			$(this).parents('.input-group').find('.input-label').css({'top':'12px','left': '0px'});		
		}
	});

/* ----------------------- */
	function pop_user_signup(){
 		$("#register_name_error,#register_mobile_error,#register_email_error,#register_password_error,#terms_check_error").hide();
	 	var user_name = $("#register_name").val();
	 	var mobile_no = $("#register_mobile").val();
	 	var email_id = $("#register_email").val();
	 	var pwd = $("#register_password").val();
	 	var terms_check = $("#register_agree_terms").is(':checked')
	 	//var terms_check = true;
	 	var status = false
	 	var user_region = $(".user_region").val();
	 	var browser_details = browser_name();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var free_trail_status = $(".free_trail").val();
  	var free_trail_message = $(".free_trail_msg").val();
  	var net_signup_type = ""
	 	if(user_region == "IN" || user_region == "BD"){
	       if(user_name.trim().length != 0 && mobile_no.length != 0 && mobile_no.length == 10 && pwd.length != 0 && pwd.length >=6 && terms_check == true){
	        status = true
	        signup_type = "msisdn"
	        net_signup_type = "phone number"
	 	  }
	 	}
	 	else{
	      if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && terms_check == true){
	        status = true
	        signup_type = "email"
	        net_signup_type = "email"
	 	  }
	 	}
	 	if(status == true){
	 		if(free_trail_status == 'true'){
	 			$("#user_register_bottom").text(free_trail_message+"..")
	 		}
			else if(free_trail_status == 'false'){
				$("#user_register_bottom").text("Register..")
			}
			$.ajax({
				url: "/users/sign_up",
				type: "POST",
				data: { 
				name: user_name, 
				mobileno: mobile_no,
				email_id: email_id,
			    type: signup_type,
				password: pwd,
				browser_det: browser_details
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
				 	var user_id = ""
				 	if(signup_type == "email"){
                      user_id = response.user_id
				 	}
				 	branch_signup_event(response.status, mobile_no, email_id,user_id)
				 	
				 	var det = get_user_region_time_data()
				 	_paq.push(['trackEvent', 'Users', 'Activity', 'signup', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: response.user_id }]);
				   $("#user_register").text("Register")
				   if(signup_type == "msisdn"){
				     fb_pixel_signup_india_event()
				     var mobil_pre_fix = "91"
				     if(user_region == "BD"){
                       mobil_pre_fix = "880"
				     }
				   	 $.cookie('user_registed_mobile_no',mobil_pre_fix+mobile_no, { expires: 14,path: '/'});
				   	 $("#register_name,#register_mobile,#register_password").val("");
				   	 _paq.push(['trackEvent', 'Users', 'Activity', 'SignUp_OTP_Step', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: response.user_id }]);
				     window.location = "/users/verify_otp"
				   }
				   else{
				   	 branch.setIdentity(response.user_analytic_id);
				   	set_user_cookies(response);
				   	set_netcore_profile( email_id, mobile_no, user_name,"","","","","","")
				   	Netcore_Registration_Success(response,net_signup_type,"")

				   	//free_trail_fb_google_event()
				   	fb_pixel_signup_us_event()
				   	 gtag('event','Users',
			     {
			   	'dimension4': "Registered",
			   	'dimension11': response.analytics_user_id,
			   	'metric9': 1
			    })
				   	$("#register_name,#register_email,#register_password").val("");
	            /* $("#reg_success").modal({
	                	backdrop: 'static'
	                });	*/		
	                /*localStorage.removeItem("new_user_registered_st")
	                localStorage.setItem("new_user_registered_st","true")*/ 
	                window.location = get_lng_web_url("/plans")

	            }
				 }
				else{
					Netcore_Registration_Failure(response.error_message,signup_type)
		
					if(free_trail_status == 'true'){
					  $("#user_register_bottom").text(free_trail_message)
					}
					else if(free_trail_status == 'false'){
						$("#user_register_bottom").text("Register")
					}
	        //$("#signup_resp_error_msg").text(response.error_message).show().fadeOut(4500);
	        $("#register_resp_error_msg").text(response.error_message).show();
				 }
				}
			});
		}
		
		else if(user_name.trim().length == 0){
			Netcore_Registration_Failure("Name is mandatory",signup_type)
		  $("#register_name_error").text("This field is mandatory").show();
		}

		else if((user_region == "IN" || user_region == "BD")&& mobile_no.length == 0){
			 Netcore_Registration_Failure("Mobile number is mandatory",signup_type)
		  $("#register_mobile_error").text("This field is mandatory").show();
		}
		else if((user_region == "IN" || user_region == "BD") && mobile_no.length != 0 && mobile_no.length != 10){
			 Netcore_Registration_Failure("Incorrect phoneNumber length",signup_type)
		  $("#register_mobile_error").text("Please enter your 10 digit mobile number").show();		  
		}
		else if(user_region == "US" && email_id.length == 0){
			 Netcore_Registration_Failure("Email is mandatory",signup_type)
			$("#register_email_error").text("This field is mandatory").show();
		}
		else if(user_region == "US" && !regex_email.test(email_id)){
			 Netcore_Registration_Failure("Incorrect email format",signup_type)
		  $("#register_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
			 Netcore_Registration_Failure("Password is mandatory",signup_type)
		  $("#register_password_error").text("This field is mandatory").show();
		}
		else if(pwd.length < 6){
			 Netcore_Registration_Failure("Password is mandatory",signup_type)
		  $("#register_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(terms_check == false){
		  $("#terms_check_error").show();
		}
  }

	$(".user_register_bottom").click(function(){
	 	pop_user_signup();
	});


// code for  inline errors starts here 

//generic code starts here

	$("#register_name,#register_mobile,#register_password,#register_email").keypress(function(e){
		$("#register_resp_error_msg").hide();
	  if(e.which == 13){
			var id = $(this).attr("id")
		  var val = $("#"+id).val()
		  var user_region = $(".user_region").val();
		  var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;  	
	      if(id == "register_email"){ 
	      	if(!val.length){
	      		$("#"+id+"_error").text("This field is mandatory").show();
	      	}   	
	      	else if(!regex_email.test(val)){
	          $("#register_email_error").text("Please enter your registered email address").show();
	          $("#register_password").focus();
	      	}
	      	else{
	      		$("#register_password").focus();
	      	}
	      }
	      if(id == "register_mobile"){
	      	if(!val.length){
	      		$("#"+id+"_error").text("This field is mandatory").show();
	      	}  
	      	else if(val.length != 10){
	      		$("#register_mobile_error").text("Please enter your 10 digit mobile number").show();
	      		$("#register_password").focus();
	      	}
	      	else{
	      		$("#register_password").focus();
	      	}
	      }
	      if(id == "register_password"){   	
			    if(user_region == "IN"){
			     	var num = $("#register_mobile").val()
			     	if(!num.length || num.length !=10){
			     		$("#register_mobile_error").hide();
			     		$("#register_mobile").focus();
			     	}
			     	else{
			     		if(!val.length){
	      				$("#"+id+"_error").text("This field is mandatory").show();
	      			}
	      			else{
			    			pop_user_signup();  
			    		}	  	      		
			      }     			
			    } 
			    else{
			     	var mail_val = $("#signin_email").val()
			    	if(!mail_val.length || (!regex_email.test(mail_val))){
			     		$("#register_email_error").hide();
			     		$("#register_email").focus();
			     	}  
			     	else{
			      	if(!val.length){
	      				$("#"+id+"_error").text("This field is mandatory").show();
	      			}
	      			else{
			    			pop_user_signup();  
			    		}	    
			      }    			
			    } 		
	      }
	  }
	}); 

	function validate_signup(){
		var id = $(this).attr("id")
	  var val = $("#"+id).val() 	 
	  if(!val.length){
	    $("#"+id+"_error").text("This field is mandatory").show();
	  }
	  else{
	    $("#"+id+"_error").hide();
	    if(id == "register_email"){
	  	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	    	if(!regex_email.test(val)){
	        $("#register_email_error").text("Please enter your registered email address").show();
	     	}
	    }
	    if(id == "register_mobile"){
	    	if(val.length != 10){
	     		$("#register_mobile_error").text("Please enter your 10 digit mobile number").show();
	     	}
	    }
	  }
	}

	$("#register_name,#register_mobile,#register_password,#register_email").on('keydown', function(e){
		var keycode = (window.event) ? event.keyCode : e.keyCode;      
	  if(keycode == 9){
	  	validate_signup()
	  }
	});


	$("#register_name,#register_mobile,#register_password,#register_email").focus(function(){
		id = $(this).attr("id")
	 $("#"+id+"_error").hide();
	});
	$("#register_name,#register_mobile,#register_password,#register_email").focusout(function(){
		validate_signup();
		if($(this).val().length > 0) {
			//$(".country_code").show();
			$(this).parents('.input-group').find('.input-label').css({'top':'-14px','left': '10px'});	
		}
		else {
			$(".country_code").hide();
			$(this).parents('.input-group').find('.input-label').css({'top':'12px','left': '10px'});		
		}
	});




function browser_name() {
    var browser,
        os,
        device_type,
        browser_det,
        ua = window.navigator.userAgent,
        platform = window.navigator.platform,
        mobile = false;
    if ( /MSIE/.test(ua) || /Trident/.test(ua)) {        
        browser = 'Internet Explorer';        
        if (/IEMobile/.test(ua) ) {
            mobile = true;
        }
    }
    else if(/Edge/.test(ua)) {
      browser = 'Microsoft Edge';
    } else if (navigator.userAgent.indexOf(' UCBrowser/') >= 0) {
    	browser = 'UC Browser';
        mobile = true;

	} else if (/Chrome/.test(ua) ) {
    	// Platform override for Chromebooks
        if ( /CrOS/.test(ua) ) {
            platform = 'CrOS';
        }
        if ((/android/i.test(ua)) ||  (/iPhone/.test(ua)) || (/iPad/.test(ua)) || (/iPod/.test(ua)) ) { 
        	mobile = true;
        }
        browser = 'Chrome';

        
    } else if ( /Opera/.test(ua) ) {      
        browser = 'Opera';
        if ( /mini/.test(ua) || /Mobile/.test(ua) ) {
            mobile = true;
        }
        
    }  else if ( /Android/.test(ua) ) {        
        browser = 'Android Webkit Browser';
        mobile = true;
        os = /Android\s[\.\d]+/.exec(ua)[0];
        
    } else if ( /Firefox/.test(ua) ) {        
        browser = 'Firefox';
        
        if ( /Fennec/.test(ua) ) {
            mobile = true;
        }        
        
    } else if ( /Safari/.test(ua) ) {        
        browser = 'Safari';        
        if ( (/iPhone/.test(ua)) || (/iPad/.test(ua)) || (/iPod/.test(ua)) ) {
            os = 'iOS';
            mobile = true;
        }        
    }
    
    if ( platform === 'MacIntel' || platform === 'MacPPC' ) {
        os = 'Mac OS X';
        /*osversion = /10[\.\_\d]+/.exec(ua)[0];
        if ( /[\_]/.test(osversion) ) {
            osversion = osversion.split('_').join('.');
        }*/
    } else if ( platform === 'CrOS' ) {
        os = 'ChromeOS';
    } else if ( platform === 'Win32' || platform == 'Win64' ) {
        os = 'Windows';
        bit = platform.replace(/[^0-9]+/,'');
    } else if ( !os && /Android/.test(ua) ) {
        os = 'Android';
    } else if ( !os && /Linux/.test(platform) ) {
        os = 'Linux';
    } else if ( !os && /Windows/.test(ua) ) {
        os = 'Windows';
    }
    
    if(mobile == true) {
    	device_type = "Mobile";
    }
    else {
    	device_type = "Web";	
    }
    return device_type+"-"+browser
}


	$("#login_icon, #login_link, .login_link, #plans_signin_link, #player_login_link, #user_login_id, #profile_signin_link").click(function(){
		//branch_user_login_click()
	  $("#profile_pop").modal("hide");
		$("#plans_login_pop").modal('hide');
		$("#player_login_pop").modal('hide');
		$("#player_subscription_pop").modal("hide");
	    //$("#sso_mobile_login_pop").modal("show");
	    if($(".user_region").val() == "IN"){
	    	$("#sso_mobile_login_pop").modal("show");
	    }else{
	    	$("#fb_login_pop").modal("show");
	    }

	  $("#register_pop").modal("hide");
	  $("#signin_mobile_number, #signin_email, #signin_password").val('');
		$("#signin_country_code, #register_country_code").hide();
		$("#signin_mobile_number_error, #signin_email_error, #signin_password_error").text('');
		$('.input-group .input-label').css({"left": "10px", "top": "12px", "font-size": "1em"});
		$('.input-group .fa-circle').css({"left": "0px", "top": "14px"});
	});

	$("#register_icon, #register_link, #plans_register_link, #user_register").click(function(){		
		$("#plans_login_pop").modal('hide');
		$("#login_pop").modal("hide");
		$("#register_pop").modal("show");		
		$("#register_country_code, #signin_country_code").hide();
		$("#register_name, #register_mobile, #register_email, #register_password").val('');
		$("#register_name_error, #register_mobile_error, #register_email_error, #register_password_error, #register_resp_error_msg").text('');
		$('.input-group .input-label').css({"left": "10px", "top": "12px", "font-size": "1em"});
		$('.input-group .fa-circle').css({"left": "0px", "top": "14px"});
	});

	$("#login_close").click(function() {
		$("#login_pop").modal("hide");
		$("#signin_country_code, #register_country_code").hide();
		$("#signin_mobile_number, #signin_email, #signin_password").val('');	
		$("#signin_mobile_number_error, #signin_email_error, #signin_password_error").text('');
		$('.input-group .input-label').css({"left": "10px", "top": "12px", "font-size": "1em"});
		$('.input-group .fa-circle').css({"left": "0px", "top": "14px"});
	});
	$("#register_close").click(function() {
		$("#register_pop").modal("hide");
		$("#register_country_code, #signin_country_code").hide();
		$("#register_name, #register_mobile, #register_email, #register_password").val('');
		$("#register_name_error, #register_mobile_error, #register_email_error, #register_password_error, #register_resp_error_msg").text('');
		$('.input-group .input-label').css({"left": "10px", "top": "12px", "font-size": "1em"});
		$('.input-group .fa-circle').css({"left": "0px", "top": "14px"});
		
	});



  function hotstar_promo_code_apply(){
	 	$("#promo_code_error").hide();
	 	var coupon_code = $("#promo_code").val();
	 	if(coupon_code.trim().length != 0){
	 		$("#hotstar_apply_promo").text("apply..")
			$.ajax({
				url: "/users/validate_promocode",
				type: "POST",
				data: { 
				coupon: coupon_code
				},
				success: function(response,status){
					console.log(response);

					$("#hotstar_apply_promo").text("apply")
					if(response.status == true){  
    				$("#hotstar_promo_code").text(coupon_code);
    				$("#hotstar_promo_txt").show()
    				$("#hotstar_apply_promo_code").hide();
						localStorage.removeItem("coupon_message")
						localStorage.setItem("coupon_message", response.coupon_info.coupon_message)
						localStorage.setItem("promo_apply", "true")
						$("#promo_code_id").val(response.coupon_info.coupon_id)
						set_hotstar_promo_code_data(response.coupon_info.coupon_message,coupon_code)
						$("#promo_code_error").text("coupon applied successfully").show().fadeOut(4500);
	 					$('#hotstar_promocode_pop').modal('hide');
	 					$("#promo_code").val('')
					}
					else {
						$("#hotstar_apply_promo").text("apply")
						$("#promo_code").val('')
						$("#promo_code_backend_error").text(response.error_message).show();
					}
				}
			});	
	 	}
	 	else if(coupon_code.trim().length == 0){
	 		//$("#hotstar_apply_promo_code").show();
	 		$("#promo_code_error").show()
	 	}
  };
  $("#hotstar_apply_promo").click(function(){
  	hotstar_promo_code_apply()
  })

  function set_pack_info(){
  	var plan_det = $("#hotstar_plan_info").val();
  	var selected_plan_det = plan_det.split("|")
  	var plan_price = selected_plan_det[6]
  	var plan_currency = selected_plan_det[5]
    localStorage.setItem("first_purchase_plan_price",plan_price);
    localStorage.setItem("first_purchase_plans_details",plan_det);
   	localStorage.setItem("first_purchase_plan_currency",plan_currency)
   }

   function set_hotstar_promo_code_data(message,coupon_code){
  	var  keysToRemove = ["offer_purcahse_promo_flag","offer_coupon_message","offer_purcahse_promo_code","offer_purcahse_promo_id"]
  	 keysToRemove.forEach(k => localStorage.removeItem(k))
  	 var mesg = ""
  	 localStorage.setItem("offer_purcahse_promo_flag","true");
  	  if(window.location.pathname.split("/")[1] == "hotstar"){
       mesg = "6 months Free Trial"
  	  }
  	  else{
        mesg = "3 months Free Trial"
  	  }
  	 localStorage.removeItem("offer_couponcode_message");
  	 localStorage.setItem("offer_couponcode_message",mesg);
     localStorage.setItem("offer_purcahse_promo_code",coupon_code);
     localStorage.setItem("offer_purcahse_promo_id",$("#promo_code_id").val());
  }

	$("#user_login_id").click(function(){
		$("#login_pop").modal("show")
	})

	$("#user_register").click(function(){
		$("#register_pop").modal("show")
	})


   $(".register").click(function(){
   	  if(!getShemarooCookies().user_id){
       //$("#fb_login_pop").modal("show")
       $("#fb_login_pop").modal();
   	  }
   })
    $(".user_login").click(function(){
   	  if(!getShemarooCookies().user_id){
       //$("#fb_login_pop").modal("show")
       $("#fb_login_pop").modal();
   	  }
   })

  
  function branch_signup_event(resp, mobile, email,user_session_id){
  	var det = get_user_region_time_data()



  	branch.logEvent(
       "SignUp_Initiated",
        {"region": det[0],
	      "city": det[1],
		  "date_event": det[2],
		  "time_event": det[3],
		  "session_id": user_session_id
        },
       function(err) { console.log(err); }
      );
  }
  function branch_otp_event(){
  	var det = get_user_region_time_data()


  	branch.logEvent(
	  	"SignUp_OTP_Step",
	  	{"region": det[0],
		"city": det[1],
		"date_event": det[2],
		"time_event": det[3],
		"session_id": ""
		},
	  	function(err) { console.log(err); }
  	);
  }
  function branch_otp_success(user_session_id, user_analytic_id){
   var det = get_user_region_time_data()
   _paq.push(['trackEvent', 'Users', 'Activity', 'SignUp_OTP_Success', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: user_session_id }]);
   _paq.push(['trackEvent', 'Users', 'Activity', 'Free_Trial_India_Success_Users', 1, {dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: user_session_id }]);


  	branch.logEvent(
	  	"SignUp_OTP_Success",
	  	{"region": det[0],
			"city":  det[1],
			"date_event": det[2],
			"time_event": det[3],
			"session_id": user_session_id
			},
	  	function(err) { console.log(err); }
  	);

  	branch.logEvent(
       "Free_Trial_India_Success_Users",
        {"region": det[0],
				"city": det[1],
				"date_event": det[2],
				"time_event": det[3],
				"session_id": user_session_id
				},
       function(err) { console.log(err); }
      );



  }
  function branch_otp_fail(mobile_no,otp){
   var det = get_user_region_time_data()
   _paq.push(['trackEvent', 'Users', 'Activity', 'SignUp_OTP_Failure', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension34: "failure", dimension35: otp }]);

  	branch.logEvent(
	  	"SignUp_OTP_Failure",
	  	{  "user_mobile": mobile_no,
	  		"region": det[0],
			"city": det[1],
			"date_event": det[2],
			"time_event": det[3],
			"otp_status": "failure",
			"otp_received": otp
			},
	  	function(err) { console.log(err); }
  	);
  }
  $("#header_plans_icon, #view_plans").click(function(){
  	var det = get_user_region_time_data()
    var user_ses_id = ""
    if(getShemarooCookies().user_id != undefined){
      user_ses_id = getShemarooCookies().user_id
    }
    var user_id = ""
    if(getShemarooCookies().user_analytical_id != undefined){
    	user_id = getShemarooCookies().user_analytical_id
    }
   _paq.push(['trackEvent', 'Users', 'Activity', 'View_Plans', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: user_ses_id }]);

  	branch.logEvent(
       "View_Plans",
        {  "region": det[0],
			"city": det[1],
			"date_event": det[2],
			"time_event": det[3],
			"session_id": user_ses_id
        },
       function(err) { console.log(err); }
      );
  })

	$(".forgot_pass_tag").click(function(){
		var session_id = ""
  	   var det = get_user_region_time_data()
  	 _paq.push(['trackEvent', 'Users', 'Activity', 'Forgot_Password', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: session_id}]);
  // 	  smartech('dispatch', 'Forgot_Password', {
  //       'region': det[0],
		// 'city': det[1],
		// 'date_event': det[2],
		// 'time_event': det[3],
		// 'session_id': "NA"
  //      });

		branch.logEvent(
       "Forgot_Password",
        {"region": det[0],
				"city": det[1],
				"date_event": det[2],
				"time_event": det[3],
				"session_id": ""
				},
       function(err) { console.log(err); }
      );
	})

	function branch_user_login_click(response){
		var det = get_user_region_time_data()
		_paq.push(['trackEvent', 'Users', 'Activity', 'LOGIN', 1, {dimension14: getShemarooCookies().user_analytical_id, dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: getShemarooCookies().user_id}]);
		// smartech('dispatch', 'LOGIN', {
  //       'region': det[0],
		// 'city': det[1],
		// 'date_event': det[2],
		// 'time_event': det[3],
		// 'session_id': response.user_id,
		// 'userid': response.user_analytic_id
  //      });

		branch.logEvent(
       "LOGIN",
        {"region": det[0],
				"city": det[1],
				"date_event": det[2],
				"time_event": det[3],
				"session_id": response.user_id,
        "userid": response.user_analytic_id,
			  'user_state': $.cookie('user_actv_sub_status')
			  },
       function(err) { console.log(err); }
      );
	}

	function branch_login_success(login_type){
    var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
		branch.logEvent(
		"Login_Success",
		{
		"method": login_type,
		'user_state': user_state

		},
		function(err) { console.log(err); }
		);
	}


	function branch_watchlist_event(){
      var det = get_user_region_time_data()
      _paq.push(['trackEvent', 'Media', 'Activity', 'ADD_TO_WISHLIST', 1, {dimension2: $('#language').val(), dimension3: $('#item_category').val(), dimension4: $('#theme_type').val(), dimension5: $("#genre").val(), dimension10: $.cookie('user_state'), dimension11: $.cookie('user_period'), dimension14: getShemarooCookies().user_analytical_id, dimension15: $("#content_id").val(), dimension16: $("#ma_title").val(), dimension12: $.cookie('user_plan_type'), dimension13: $.cookie('user_plan'), dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: getShemarooCookies().user_id }]);
		branch.logEvent(
       "ADD_TO_WISHLIST",
			{   "region": det[0],
				"city": det[1],
				"date_event": det[2],
				"time_event": det[3],
				"session_id": getShemarooCookies().user_id,
				"userid": getShemarooCookies().user_analytic_id,
				'content_id': $("#content_id").val(),
				'title': $("#ma_title").val(),
				'category': $('#item_category').val(),
				'content_type': $('#theme_type').val(),
				'genre': $("#genre").val(),
				'language': $('#language').val(),
				'user_plan_type': $.cookie('user_plan_type'),
				'user_period': $.cookie('user_period'),
				'user_state': $.cookie('user_actv_sub_status'),
				'user_custom_plan': $.cookie('user_plan')
				},
       function(err) { console.log(err); }
      );
	}

  
  function set_user_pack(){
  var  keysToRemove = ["first_purchase_plan_price","first_purchase_plans_details","first_purchase_plan_currency"]
  	 keysToRemove.forEach(k => localStorage.removeItem(k))
   var plan_det = $(".pack-active").data("plan-info");
   var selected_plan_det = plan_det.split("|")
   var plan_price = selected_plan_det[6]
   var plan_currency = selected_plan_det[5]
   localStorage.setItem("first_purchase_plan_price",plan_price);
   localStorage.setItem("first_purchase_plans_details",plan_det);
   localStorage.setItem("first_purchase_plan_currency",plan_currency)

		document.cookie = 'plan_id' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  $.cookie('plan_id',selected_plan_det[0], { expires: 14,path: '/'});
	  document.cookie = 'sub_id' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  $.cookie('sub_id',selected_plan_det[9], { expires: 14,path: '/'});
	  var web = getShemarooCookies().mobile_browser_type === "android" ?  "android" : "web" 
	  document.cookie = 'app_type' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	  $.cookie('app_type',web, { expires: 14,path: '/'});
	  }

  function set_guj_promo_code(){
  	var  keysToRemove = ["offer_purcahse_promo_flag","offer_coupon_message","offer_purcahse_promo_code","offer_purcahse_promo_id","offer_couponcode_message","offer_purcahse_lohan_det","offer_purcahse_lohan_flag"]
  	 keysToRemove.forEach(k => localStorage.removeItem(k))
  	 localStorage.setItem("offer_purcahse_promo_flag","true");
  	 localStorage.setItem("offer_purcahse_lohan_flag","true");
  	 localStorage.setItem("offer_couponcode_message","Lohana Samaj");
     localStorage.setItem("offer_purcahse_promo_code",$("#lok_samaj_coupon_code").val());
     localStorage.setItem("offer_purcahse_promo_id",$("#lok_samaj_coupon_id").val());
     localStorage.setItem("offer_purcahse_lohan_det",$("#lok_samaj_promo_amt").val());
  }


    function set_blackday_promo_code(){
  	var  keysToRemove = ["offer_purcahse_promo_flag","offer_coupon_message","offer_purcahse_promo_code","offer_purcahse_promo_id","offer_couponcode_message","offer_purcahse_lohan_det","offer_purcahse_lohan_flag"]
  	 keysToRemove.forEach(k => localStorage.removeItem(k))
  	 localStorage.setItem("offer_purcahse_promo_flag","true");
  	 localStorage.setItem("offer_purcahse_black_day_flag","true");
  	 localStorage.setItem("offer_couponcode_message","Dewali special");
     localStorage.setItem("offer_purcahse_promo_code",$("#black_day_coupon_code").val());
     localStorage.setItem("offer_purcahse_promo_id",$("#black_day_coupon_id").val());
     localStorage.setItem("offer_purcahse_black_day_det",$("#black_day_promo_amt").val());
  }



  function gu_signup(){
  	var  keysToRemove = ["first_purchase_plan_price","first_purchase_plans_details","first_purchase_plan_currency"];
      keysToRemove.forEach(k => localStorage.removeItem(k))
		$("#gu_email_error,#gu_password_error").hide();
		var user_email = $("#gu_email").val();
		var user_pwd = $("#gu_pwd").val();
		var status = false;
		var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/; 
		if(user_email.length != 0 && regex_email.test(user_email) && user_pwd.length != 0 && user_pwd.length >=6 ){
			status = true
		}
		if(status == true){
			$("#gujarati_sub").text("Register...")
			$.ajax({
				url: "/users/sign_up",
				type: "POST",
				data: { 
				name: "guest", 
				email_id: user_email,
				type: "email",
				password: user_pwd
				},
				success: function(response,status){
			     console.log(response)
			     console.log(response.status)
				if(response.status == true){
					set_user_pack()
					branch.setIdentity(response.user_analytic_id);
					Apxor.setUserId(response.user_analytic_id)
					$("#gujarati_sub").text("Register")
					set_user_cookies(response);
					gtag('event','Users',
					{
					'dimension4': "Registered",
					'dimension11': response.analytics_user_id,
					'metric9': 1
					})
					$("#gu_email,#gu_pwd").val("");
					var cur_url = window.location.pathname.split("/")[1]
					if(cur_url == "lohana-samaj"){
					  set_guj_promo_code()
					}else if(cur_url == "blackfriday"){
						set_blackday_promo_code()
					}
					window.location = "/plans/plans_summary"		   
				}
				else{
					$("#gujarati_sub").text("Register")
					$("#signup_marketing_resp_error_msg").text(response.error_message).show();
				}
				}
			});
		}
		else if(user_email.length == 0){
			$("#gu_email_error").show();			
		}
		else if(!regex_email.test(user_email)){
			$("#gu_email_error").text("Please enter a valid email address").show();		  
		}
		else if(user_pwd.length == 0){
			$("#gu_password_error").show();		  
		}
		else if(user_pwd.length < 6){
			$("#gu_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
  }

  $("#gu_pwd").keypress(function(e) {
  	if(e.which == 13){
     gu_signup()
    }
  });

	$("#gujarati_sub").click(function(){
		 gu_signup()
	})
	 $("#gu_email,#gu_pwd").focusin(function(){
      $("#gu_email_error,#gu_password_error,#signup_marketing_resp_error_msg").hide();
  	 });

  $("#user_reg_pop").click(function(){
  	var user_region = $(".user_region").val()
  	var allow_regions = ["IN","US","BD"]
  	if ($.inArray(user_region, allow_regions) > -1){
     	$("#plans_login_pop").modal('hide');
			$("#login_pop").modal("hide");
			$("#register_pop").modal("show");		
			$("#register_country_code, #signin_country_code").hide();
			$("#register_name, #register_mobile, #register_email, #register_password").val('');
			$("#register_name_error, #register_mobile_error, #register_email_error, #register_password_error, #register_resp_error_msg").text('');
			$('.input-group .input-label').css({"left": "10px", "top": "12px", "font-size": "1em"});
			$('.input-group .fa-circle').css({"left": "0px", "top": "14px"});
    }
    else{
  	  //alert("Please register in App's")
  	  $("#user_reg_pop12").modal("show");
    }
    
    $("#confirm_app_login").click(function() {
    	$("#user_reg_pop12").modal("hide");
    });
  })

/*Start of firebase user flows*/

  $("#fbse_user_login,#usr_login_link,#profile_signin_btn").click(function(){
  	firebase.auth().signOut();
  	localStorage.removeItem("nc_sub_strt_src")
  	//$("#fb_login_pop").modal("show");
  	//$("#sso_mobile_login_pop").modal("show");
  	//$("#sso_mobile_login_pop").modal();
  	if($(".user_region").val() == "IN"){
  		$("#sso_mobile_login_pop").modal()
  	}else{
  		$("#fb_login_pop").modal("show");
  	}

  })

  $("#fb_mobile_login_btn").click(function(){
  	$("#fb_login_pop").modal("hide");
  	$("#fb_mobile_login_pop").modal("show");
  })

	setTimeout(function(){
		window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container',{
			'size': 'invisible',
			'callback': function(response){
			 console.log("success", response);
			},
			'expired-callback': function(){
			 console.log("expired-callback");
			}
		});
		recaptchaVerifier.render().then(function(widgetId) {
		window.recaptchaWidgetId = widgetId;
		});
	},2000);

	$(".fb_mobile_sub").click(function(){
		var user_reg = $(".user_region").val()
		var ext_country_code = $("#country_code").val()
		var max_digit = $("#max_phone_digit").val()
		var mob_st = false
		var user_mob = $("#fb_login_mobile_number").val();
		if(user_mob.length != 0 && user_mob.length == max_digit){
			mob_st = true
		}
		if (user_reg == "OM"){
		if(user_mob.length != 0 && (user_mob.length >= 8  &&  user_mob.length <= max_digit)){
			mob_st = true
		 }
		}
	  if(mob_st == true){
	  	var phoneNumber = ext_country_code+$("#fb_login_mobile_number").val();
			var appVerifier = window.recaptchaVerifier;
		/*if(user_reg == "IN"){
      user_mobile_signup(phoneNumber)

		}else{*/
			firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier) .then(function (confirmationResult){
				window.confirmationResult = confirmationResult;
				localStorage.setItem("fb_mobile_no",phoneNumber)
				$("#fb_mobile_login_pop").modal("hide");
				var mb_num_val = phoneNumber.substring(3,phoneNumber.length-4)
                $("#user_mobile_no").text(localStorage.getItem("fb_mobile_no").replace(mb_num_val, "******"));
				$("#fb_otp_pop").modal("show");
			}).catch(function (error){
			console.log(error + " error");
			$("#fb_mobile_msg").text(error).show().fadeOut(4500);
			});
		}
	  // }
	  else{
	  	if(user_mob.length == 0){
        $("#fb_mobile_error_msg").text($("#mobile_mandatory_info").val()).show().fadeOut(4500);
	  	}
	  	else if(user_mob.length != 0 && user_mob.length != max_digit){
         $("#fb_mobile_error_msg").text("Please enter your "+max_digit+" digit mobile number").show().fadeOut(4500);
	  	}
	  }	
	})


 $(".fb_mobile_back").click(function(){
 	 $("#fb_mobile_login_pop").modal("hide");
 	 //$("#fb_login_pop").modal("show");
 	 $("#fb_login_pop").modal();
 });
 $("#fb_mobile_back_login").click(function(){
 	clear_entered_data()
 	 $("#fb_email_login_pop").modal("hide");
 	 //$("#fb_login_pop").modal("show")
 	 $("#fb_login_pop").modal();
 });
 
$("#fb_mobile_back_register").click(function(){
	 clear_entered_data()
 	 $("#fb_email_register_pop").modal("hide");
 	 //$("#fb_email_login_pop").modal("show")
 	 $("#fb_email_login_pop").modal();
})

 $("#fb_otp_back").click(function(){
 	 $("#fb_otp_pop").modal("hide");
 	 //$("#sso_mobile_login_pop").modal("show");
 	 //$("#fb_mobile_login_pop").modal("show");
 	 //$("#sso_mobile_login_pop").modal();
 	 if($(".user_region").val() == "IN"){
 	 	$("#sso_mobile_login_pop").modal();
 	 }else{
 	 	$("#fb_login_pop").modal("show");
 	 }


 })

 $("#fb_mobile_later").click(function(){
 	$("#fb_mobile_login_pop").modal("hide");
 })

	$(".fb_otp_verify,.fb_otp_next").click(function(){
		var fb_otp = $("#fb_logn_otp").val();
		var user_region = $(".user_region").val()
    var fb_otp_st = false
    if(fb_otp.length != 0 && fb_otp.length == 6){
    	fb_otp_st = true
    }
   if(fb_otp_st == true){
   	Netcore_Registration_Start("Phone Number")
   	$('.fb_otp_verify').text($("#continue").val()+"...")
   	var mb_type = $("#mb_lgn_type").val();
   	if(mb_type == "netcore"){
     validate_user_otp(fb_otp)
   	}
   	else{
      confirmationResult.confirm(fb_otp).then(function (result) {
			var user = result.user;
			firebase_success_callback()
			$('.fb_otp_verify').text($("#continue").val());
			}).catch(function (error) {
			$(".fb_otp_verify").text($("#continue").val())
			if(error.code == "auth/invalid-verification-code"){
				$("#fb_otp_error_msg").text($('#valid_otp').val()).show()
			}else
			{
				$("#fb_otp_msg").text( error.message).show()
			}
			});
   	  }	
   }
   else{
    if(fb_otp.length == 0){
    	$("#fb_otp_error_msg").text($("#otp_mandatory_info").val()).show().fadeOut(4500);
    }
    else if(fb_otp.length != 0 && fb_otp.length != 6){
    	$("#fb_otp_error_msg").text($("#valid_6_digit_otp").val()).show().fadeOut(4500);
    }
   }
	})

	$("#fb_resend_otp").click(function(){
		$("#fb_logn_otp").val("")
		var phoneNumber = localStorage.getItem("fb_mobile_no");
		var user_reg = $(".user_region").val();
		var mb_type = $("#mb_lgn_type").val();
		if(mb_type == "netcore"){
			user_resend_otp(phoneNumber)
		}
		 else{
			var appVerifier = window.recaptchaVerifier;
			firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier) .then(function (confirmationResult){
				console.log(phoneNumber + " data " + appVerifier );
				window.confirmationResult = confirmationResult;
				localStorage.setItem("fb_mobile_no",phoneNumber)
				console.log("otp sent successfully");
				$("#fb_otp_msg").text($("#otp_sent").val()).show().fadeOut(4500);
			}).catch(function (error){
			console.log(error + " error");
			});
		 }
	})


 $("#fb_facebk_login").click(function(){
 	var user_region = $(".user_region").val()
 	if(user_region == "US"){
     fb_pixel_signup_us_event()
 	}
 	else{
 	 fb_pixel_signup_india_event()
 	}
 	$("#fb_login_pop").modal("hide");
 	var provider = new firebase.auth.FacebookAuthProvider()
 	provider.setCustomParameters({
  'display': 'popup'
	});
 	firebase_social_login(provider)
 })

 $("#fb_google_login").click(function(){
 	$("#fb_login_pop").modal("hide");
 	var provider = new firebase.auth.GoogleAuthProvider();
 	firebase_social_login(provider)
 })


	function user_mobile_signup(phoneNumber){
		var phone_num = phoneNumber;
		$.ajax({
			url: "/users/mobile_no_signup",
			type: "POST",
			data: {mobile_no: phone_num}, 
			success: function(response,status){
				if(response.status == true){
					localStorage.setItem("fb_mobile_no",phoneNumber)
					//$("#fb_mobile_login_pop").modal("hide");
					if($(".user_region").val() == "IN"){
						$("#sso_mobile_login_pop").modal("hide");	
					}else{
						$("#fb_login_pop").modal("hide")
					}
					
					var mb_num_val = phoneNumber.substring(3,phoneNumber.length-4)
					$("#user_mobile_no").text(localStorage.getItem("fb_mobile_no").replace(mb_num_val, "******"));
					$("#fb_otp_pop").modal("show");
				}
			},
			error: function(status,error){
				console.log(error + " error");
				$("#fb_mobile_msg").text(error).show().fadeOut(4500);
			}
		});
	}

	function validate_user_otp(user_otp){
		var ph_no = localStorage.getItem("fb_mobile_no");
		var browser_details = browser_name()
		var cur_url = window.location.pathname.split("/")[1]
		$.ajax({
			url: "/users/validate_otp",
			type: "POST",
			data: {mobile_no: ph_no,otp:user_otp,browser_det: browser_details}, 
			success: function(response,status){
	      $('.fb_otp_verify').text($("#continue").val());
	 			if(response.status == true){
          set_user_cookies(response)
					branch.setIdentity(response.user_analytic_id);
					Apxor.setUserId(response.user_analytic_id)
					var ap_login_type = "Phone Number"
					var prov_login_type = "Phone Number"
					var phone_num = getShemarooCookies().user_registed_mobile_no
					var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
					Apxor.logEvent("login_success",{
					"method": ap_login_type,
					"user_state": user_state, 
					"phone_number": ph_no
					}, "USER_EVENTS");
					branch_login_success(ap_login_type)
					branch_user_login_click(response)
					gtag('event','Users',{
					'dimension4': response.user_status,
					'dimension11': response.user_analytic_id,
					'metric8': 1
					});
					_paq.push(['trackEvent', 'Users', 'Activity', 'user_active', 1]);
					_paq.push(['trackEvent', 'Users', 'Activity', 'registered_active', 1]);
					_paq.push(['resetUserId']);
					_paq.push(['setUserId', response.user_analytic_id]);
					_paq.push(['trackPageView']);
					set_netcore_profile("",response.login_id, response.user_name,response.user_ac_pln,response.sub_pl_ty,response.sub_expi_date,response.sub_date,prov_login_type) 
					if(cur_url == "allaccessoffer" || cur_url == "blackfriday"){
						if(response.user_all_ac_pk == "false"){
							window.location = "/plans/plans_summary"
						}
						else if(response.user_all_ac_pk == "true"){
							$("#fb_email_login_pop").modal("hide");
							$("#exist_user").modal("show");
						}
						else if(response.user_all_mnth_pck == "true"){
							$("#fb_email_login_pop").modal("hide");
							$("#all_access_mon_user").modal("show");
						}
					}
					else{
							set_netcore_profile("", response.login_id, response.user_name,response.user_status,response.user_ac_pln,
							response.sub_pl_ty,response.sub_expi_date,response.sub_date,prov_login_type)
						if(localStorage.getItem("activate_tv_path") != undefined){
							localStorage.removeItem("activate_tv_path")
							var url = "/tv"
							if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
          				url  = "/"+getShemarooCookies().shm_sel_lang+"/tv"
	            }
	            window.location = url
						}
						else if(response.first_time_user == "true"){
							Netcore_Registration_Success(response,prov_login_type,ph_no)
							branch_signup_event(response.status, "", response.login_id,response.user_analytic_id)
							var det = get_user_region_time_data()
							_paq.push(['trackEvent', 'Users', 'Activity', 'signup', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: response.user_id }])
                window.location = get_lng_web_url("/plans")
						}
						else{
							var user_region = $(".user_region").val();
							smartech('dispatch', 'Login success', {});
							if(getShemarooCookies().is_parent_control == "true"){
								var url = "/users/who_is_watching"
								if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
            				url = "/"+getShemarooCookies().shm_sel_lang+"/users/who_is_watching"
		            }
		            window.location = url								
							}
							else{
								if (window.location.pathname == "/users/sign_in"){
									var home_url = "/"
									if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
              				home_url = "/"+getShemarooCookies().shm_sel_lang
			            }
			             window.location =  home_url
               	}
               else{
               		window.location.reload();
               	}
							}
						}
					}
				}
				else{
	      	$("#fb_otp_msg").text($("#invalid_otp").val()).show().fadeOut(4500);
				}
			}
		});
	}
 


  function user_resend_otp(phoneNumber){
	 var ph_num = phoneNumber
		$.ajax({
			url: "/users/resend_otp",
			type: "POST",
			data: {mobile_no: ph_num}, 
		  success: function(response,status){
		  	console.log(response)
			if(response.status == true){
				$("#fb_otp_msg").text($("#otp_sent").val()).show().fadeOut(4500);
				console.log("otp sent successfully");
			}
		},
		error: function(status,error){
			console.log(error + " error");
		}
		});
  }

 function firebase_social_login(provider){
 	firebase.auth().signInWithPopup(provider).then(function(result){
  	var token = result.credential.accessToken;
  	var user = result.user;
  	firebase_success_callback()
  }).catch(function(error){
		console.log(error)
		if (provider['providerId'] == "google.com"){
		Netcore_Registration_Failure(error.message,"Google")
	}else{
		Netcore_Registration_Failure(error.message,"FB")
	}
	  var errorCode = error.code;
	  var errorMessage = error.message;
	  var email = error.email;
	  var credential = error.credential;
	  console.log(errorMessage);
	  var error_msg = decodeURI(errorMessage)
	  $("#fb_login_pop").modal("hide");
	  $("#fb_aut_error_msg").text(error_msg)
	  $("#fb_autherror_pop").modal("show");
	});
 }


 function firebase_logout(){
 	localStorage.removeItem("no_passwd_chge")
 	 firebase.auth().signOut().then(function() {}).catch(function(error){});
 }
	

function firebase_success_callback(){
	var cur_url = window.location.pathname.split("/")[1]
	$("#fb_login_pop,#fb_otp_pop").modal("hide");
	$(".loader").show();
	var user = firebase.auth().currentUser.providerData[0]
	var unq_id = firebase.auth().currentUser.uid
	var mobileno = ""
	var prov_login_type = ""
	var email = user.email
	var first_name = user.displayName
	var browser_details = browser_name();
	if(user.providerId == "phone"){
	 prov_login_type = "phone"
     unq_id = firebase.auth().currentUser.uid
     mobileno = firebase.auth().currentUser.providerData[0].uid.replace("+","")
     email = ""
     first_name = ""
	}
	if(user.providerId == "google.com"){
     prov_login_type = "Google"
	}
	if(user.providerId == "facebook.com"){
     prov_login_type = "Facebook"
	}
	$.ajax({
		url: "/users/firebase_success",
		type: "POST",
		data: { 
		 user_name: user.displayName,
		 user_email: user.email,
		 user_id: unq_id,
		 provider_type: user.providerId,
		 mobile_no: mobileno,
		 browser_det: browser_details
		},
		success: function(response,status){
		$(".loader").hide();
		fb_pixel_signup_sucs_event()
		if(response.status == true){ 
			set_user_cookies(response)
			branch.setIdentity(response.user_analytic_id);
			Apxor.setUserId(response.user_analytic_id)
			//Apxor.setCustomUserId(response.user_analytic_id)
			var ap_login_type = prov_login_type
			if(prov_login_type == "phone"){
			  ap_login_type = "Phone Number"
			}
			else if(prov_login_type == "facebook"){
			  ap_login_type = "FB"
			}else if(prov_login_type == "email"){
				ap_login_type = "Email"
			}
			var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
			Apxor.logEvent("login_success", {
               "method": ap_login_type,
               "user_state": user_state,
               "phone_number": response.ext_mob_number
            }, "USER_EVENTS");
            branch_login_success(ap_login_type)
			branch_user_login_click(response)
				gtag('event','Users',{
				'dimension4': response.user_status,
				'dimension11': response.user_analytic_id,
				'metric8': 1
			});
			_paq.push(['trackEvent', 'Users', 'Activity', 'user_active', 1]);
      		_paq.push(['trackEvent', 'Users', 'Activity', 'registered_active', 1]);
      		_paq.push(['resetUserId']);
      		_paq.push(['setUserId', response.user_analytic_id]);
      		_paq.push(['trackPageView']);
      		 Apxor.setUserId(response.user_analytic_id);
      	   set_netcore_profile(user.email, mobileno, user.displayName,response.user_ac_pln,response.sub_pl_ty,response.sub_expi_date,response.sub_date,prov_login_type) 
          if(cur_url == "allaccessoffer" || cur_url == "blackfriday"){
         	if(response.user_all_ac_pk == "false"){
         	  window.location = "/plans/plans_summary"
         	}
         	else if(response.user_all_ac_pk == "true") {
         	  $("#fb_email_login_pop").modal("hide");
         	  $("#exist_user").modal("show");
         	}
         	else if(response.user_all_mnth_pck == "true") {
         	  $("#fb_email_login_pop").modal("hide");
         	  $("#all_access_mon_user").modal("show");
         	}
         }
         else{
      	   set_netcore_profile(email, mobileno, first_name,response.user_status,response.user_ac_pln,
           response.sub_pl_ty,response.sub_expi_date,response.sub_date,prov_login_type)
		  if(localStorage.getItem("activate_tv_path") != undefined){
            localStorage.removeItem("activate_tv_path")
            window.location = "/tv"
           }
			else if(response.first_time_user == "true"){
				 	Netcore_Registration_Start(ap_login_type)
 					localStorage.setItem("shm_user_login",ap_login_type)
				 								

				//debugger
				// netcore_signup_success(response)
				if(prov_login_type == "phone"){
					prov_login_type = "Phone Number"
				}
				Netcore_Registration_Success(response,prov_login_type,response.ext_mob_number)

				Netcore_Registration_Success(response,prov_login_type,response.ext_mob_number)
				branch_signup_event(response.status, "", response.login_id,response.user_analytic_id)
				var det = get_user_region_time_data()
				_paq.push(['trackEvent', 'Users', 'Activity', 'signup', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: response.user_id }]);
					if(response.mob_map_st == "false"){
			 		localStorage.removeItem("user_mob_map_st")
        	localStorage.setItem("user_mob_map_st","false")
        	//$("#sso_mobile_login_pop2").modal("show");
        	$("#sso_mobile_login_pop2").modal({backdrop: 'static', keyboard: false});
        }
        else{
        	Netcore_Registration_Success(response,prov_login_type,response.ext_mob_number)
					window.location = get_lng_web_url("/plans") 
        }
			}
			else{

				var user_region = $(".user_region").val();
			  if(getShemarooCookies().is_parent_control == "true"){
       var watch_url = "/users/who_is_watching"
			  	if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
      				watch_url =  "/"+getShemarooCookies().shm_sel_lang+"/users/who_is_watching"
          }       
         	window.location = watch_url
			 }
			 else if(response.mob_map_st == "false"){
			 		localStorage.removeItem("user_mob_map_st")
        	localStorage.setItem("user_mob_map_st","false")
        	//$("#sso_mobile_login_pop2").modal("show");
        	$("#sso_mobile_login_pop2").modal({backdrop: 'static', keyboard: false});
			 }
			else{
				 if (window.location.pathname == "/users/sign_in"){
         	window.location = "/"
         }else if(window.location.pathname == "/"+getShemarooCookies().shm_sel_lang+"/users/sign_in"){
         	window.location = "/"+getShemarooCookies().shm_sel_lang
         }
         else{
         		window.location.reload();
         	}
			}
			}
			

		}
	}
		else{
			Netcore_Registration_Failure(response.error_message,"mobile")
			if(response.merg_ac_st == true){
				var fb_login_type = firebase.auth().currentUser.providerData[0].providerId
				if(fb_login_type == "google.com"){
					var type_login = "Google"
					$("#social_img").attr("src","/assets/firebase/google.png")
					$(".login_type").text(type_login)
				}
				$("#merge_user_email").text(user.email)
				$("#acnt_merge_pop").modal("show");
			}
			else if(response.region_pop_up == true){
				$("#regional_popup").modal("show")
			}
			else if(response.existing_user == true || response.status == false){
				$("#social_error_popup_text").text(response["error_message"])
				$("#social_error_popup").modal("show")
			}
			else{
			  window.location.reload()    
			}
}
		}
  });
}

 $("#fb_email_login"). click(function(){
 	$("#fb_login_pop").modal("hide");
 	grecaptcha.reset();
 	//$("#fb_email_login_pop").modal("show");
 	$("#fb_email_login_pop").modal();
 })


$("#fgt_pwd_later").click(function(){
	clear_entered_data()
	$("#fb_forgot_pwd_pop").modal("hide");
})


$(".fb_fgt_back").click(function(){
	clear_entered_data()
	$("#fb_forgot_pwd_pop").modal("hide");
	//$("#fb_email_login_pop").modal("show");
	$("#fb_email_login_pop").modal();
})

 var verifyRecaptcha = function(evt) {
  var response = grecaptcha.getResponse();
  if(response.length == 0) 
  { 
    //reCaptcha not verified
   $("#captcha_error").show()
    evt.preventDefault();
    return false;
  }else{
   $("#captcha_error").hide()
  	return true;
  }


      };

function firebase_email_sign_in(evt){
	var cur_url = window.location.pathname.split("/")[1]
   $("#firebase_user_email_error,#firebase_password_error,#firebase_user_errors_msg,#fb_email_error,#captcha_error").hide();
   var user_email_id = $("#firebase_user_email").val();
   var user_region = $(".user_region").val();
   var captcha_res = true;
   var allowed_region = ["IN","US"]
   if(!allowed_region.includes(user_region)){
   		captcha_res = verifyRecaptcha(evt);
   }

   var pwd = $("#firebase_password").val();
   var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
   var error_status = false
  	var captcha = grecaptcha.getResponse()
    if(user_email_id.length != 0 && regex_email.test(user_email_id) && captcha.length != 0){
        error_status = true;
      }
     if(pwd.length < 6){
       error_status = false;
     }
   login_type = "email"
   if(error_status == true && captcha_res == true){
    $("#social_login_btn_text").text($("#sign_in").val()+"...")
        var browser_details = browser_name();
    $.ajax({
      url: "/users/sign_in",
      type: "POST",
      data: { 
      email_id: user_email_id,
      password: pwd,
      type: login_type,
      browser_det: browser_details,
      login_type: "firebase",
      "g-recaptcha-response": captcha
      },
      success: function(response,status){
      	grecaptcha.reset();
       // console.log(response12)
       if(response.status == true){
          set_user_cookies(response)
          branch.setIdentity(response.user_analytic_id);
          //Apxor.setCustomUserId(response.user_analytic_id)
          Apxor.setUserId(response.user_analytic_id)
          smartech('dispatch', 'Login success', {});
          var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
          Apxor.logEvent("login_success", {
               "method": "Email",
               "user_state": user_state,
               "phone_number": response.ext_mob_number
            }, "USER_EVENTS");
          branch_user_login_click(response)
           branch_login_success(login_type)
          set_netcore_profile( user_email_id, response.mob_num, response.user_name,response.user_status,response.user_ac_pln,response.sub_pl_ty,response.sub_expi_date,response.sub_date,"email")
	         gtag('event','Users',
	         {
	          'dimension4': response.user_status,
	          'dimension11': response.user_analytic_id,
	          'metric8': 1
	         });
         if(cur_url == "allaccessoffer" || cur_url == "blackfriday"){
         	if(response.user_all_ac_pk == "false"){
         	  window.location = "/plans/plans_summary"
         	}
         	else if(response.user_all_yr_pck == "true") {
         	  $("#fb_email_login_pop").modal("hide");
         	  $("#exist_user").modal("show");
         	}
         	else if(response.user_all_mnth_pck == "true") {
         	  $("#fb_email_login_pop").modal("hide");
         	  $("#all_access_mon_user").modal("show");
         	}
         }
        else{
         if(response.device_limit == "true"){
            $("#fb_email_login_pop").modal("hide")
            localStorage.removeItem("max_device_limit_reached")
            localStorage.setItem("max_device_limit_reached", "true")
            $("#device-limit-pop").modal("show")
            $("#limit_device_cancel").click(function(){
              $(this).text($("#device_cancel").val()+"...")
              log_out();
            })
            $("#limit_device_confirm").click(function(){
              $(this).text($("#manage_devices").val()+"...")
              var device_url = "/users/registered_devices"
              if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
          			device_url = "/"+getShemarooCookies().shm_sel_lang+"/users/registered_devices"
	            }
	              window.location = device_url
            })
          }
         else if(response.mob_map_st == "false"){
           $("#fb_email_login_pop").modal("hide")
           localStorage.removeItem("user_mob_map_st")
           localStorage.setItem("user_mob_map_st","false")
           //$("#sso_mobile_login_pop2").modal("show");
           $("#sso_mobile_login_pop2").modal({backdrop: 'static', keyboard: false});
          }
        else{
         if(localStorage.getItem("activate_tv_path") != undefined){
          localStorage.removeItem("activate_tv_path")
           var tv_url = "/tv"
	          if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
	      				tv_url = "/"+getShemarooCookies().shm_sel_lang+"/tv"
	          }
	            window.location = tv_url
         }
         else{
            if(getShemarooCookies().is_parent_control == "true"){
            	var watching_url = "/users/who_is_watching"
            	if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
          				window.location = "/"+getShemarooCookies().shm_sel_lang+"/users/who_is_watching"
	            }
               window.location = watching_url
            }
            else{
               _paq.push(['trackEvent', 'Users', 'Activity', 'user_active', 1]);
                   _paq.push(['trackEvent', 'Users', 'Activity', 'registered_active', 1]);
                   _paq.push(['resetUserId']);
              _paq.push(['setUserId', response.user_analytic_id]);
                  _paq.push(['trackPageView']);
               //window.location = "/"
			   	  	 if(localStorage.getItem("check_setting_or_referal")=== "profile_setting_pop"){
			   	  	 		localStorage.removeItem("check_setting_or_referal")
			            window.location = get_lng_web_url("/users/settings")

			   	  	 }else if(localStorage.getItem("check_setting_or_referal")=== "profile_earn_pop"){
			   	  	 		localStorage.removeItem("check_setting_or_referal")
			             window.location = get_lng_web_url("/users/referral")
			   	  	 		
			   	  	 }else if (window.location.pathname == "/users/sign_in"){
               		window.location = "/"
               
               }else if (window.location.pathname == "/"+getShemarooCookies().shm_sel_lang+"/users/sign_in"){
               		window.location = "/"+getShemarooCookies().shm_sel_lang
               }
               else{
               		window.location.reload();
               	}
               
            }
         }
      
       }
        }
         }
      else{   
      	$("#social_login_btn_text").text($("#sign_in").val())
        smartech('dispatch', 'login failure', {});
        if(response.user_region_key == true ){
        	$("#fb_email_login_pop").modal("hide")
        	$("#regional_popup").modal("show")
        }
      else{
      	//grecaptcha.reset();
   	    $("#firebase_user_errors_msg").text(response.error_message).show();
       }
     }
     

        }
    });
   }
   else if(user_email_id.length == 0){
   	//grecaptcha.reset();
    $("#firebase_user_email_error").text($("#registered_email").val()).show().fadeOut(2000);
   }
   else if(!regex_email.test(user_email_id)){
   	//grecaptcha.reset();
    $("#firebase_user_email_error").text($("#invalid_email").val()).show().fadeOut(2000);
   }
   else if(pwd.length == 0){
   	//grecaptcha.reset();
    $("#firebase_password_error").text($("#mandatory_info").val()).show();
   }
   else if(pwd.length < 6){
   	//grecaptcha.reset();
	$("#firebase_password_error").text($("#min_info").val()).show();
   }
   else if(captcha.length == 0){
	$("#firebase_captcha_errors_msg").text($("#not_robot").val()).show().fadeOut(4000);
   }
}


$("#social_login_btn_text").on('click', function(evt){
    firebase_email_sign_in(evt);
  })


$("#forgot_fb_password").click(function(){
  $("#fb_email_login_pop").modal("hide")
  $("#fb_forgot_pwd_pop").modal("show")
})



function forgot_firebase_password(){
    $("#fb_email_error,#fb_forgot_errors_msg").hide();
    var email_id = $("#fb_email").val();
    var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var error_status = false
      if(email_id.length != 0 && regex_email.test(email_id)){
        error_status = true;
      }

    if(error_status == true){
      $("#fb_confirm_text").text($("#send_text").val()+"...")
      $.ajax({
        url: "/users/forgot_password",
        type: "POST",
        data: { 
          email_id: email_id
        },
        success: function(response,status){
          console.log(response,"response",status,"status")
           $("#fb_confirm_text").text($("#send_text").val())
          if(response.status == true){
          	 $("#fb_forgot_errors_msg").text($("#pass_reset_lang").val()).show().fadeOut(6000);
            }
          else{
         	$("#fb_forgot_errors_msg").text(response.error_message).show();    
          }
        }
      })
    }
    else{
        if(email_id.length == 0){
          $("#fb_email_error").text($("#email_addr").val()).show();
        }
        else if(!regex_email.test(email_id)){
          $("#fb_email_error").text($("#associated_email_err").val()).show();
        }
      }
    }
  

  $("#fb_email").focusin(function(){
    $("#fb_email_error").hide();
  });
  $("#fb_confirm_text").click(function(){
    forgot_firebase_password();
  });

  $("#fb_email").keypress(function(e){
    $("#fb_forgot_errors_msg").hide();
    if(e.which == 13){
      forgot_firebase_password();
    }
  });

  $("#fb_email_reg_later").click(function(){
  	clear_entered_data()
  	$("#fb_email_reg_name,#fb_email_reg_email,#fb_email_reg_pwd,#fb_email_reg_con_pwd").val("")
  	$("#fb_email_register_pop").modal("hide");
  });

  $("#fb_user_login").click(function(){
     $("#fb_email_register_pop").modal("hide");
     //$("#fb_email_login_pop").modal("show");
     $("#fb_email_login_pop").modal();
	});


  	function fb_email_signup(){
        $("#fb_email_reg_name_error,#fb_backend_error_msg,#fb_email_reg_email_error,#fb_email_reg_pwd_error,#fb_terms_check_error,#terms_check_error,#fb_email_reg_con_pwd_error").hide();
	 	var user_name = $("#fb_email_reg_name").val();
	 	var email_id = $("#fb_email_reg_email").val();
	 	var pwd = $("#fb_email_reg_pwd").val();
	 	var confirm_pwd = $("#fb_email_reg_con_pwd").val();

	 	var terms_check = $("#fb_register_agree_terms").is(':checked')
	 	// var terms_check = true;
	 	var status = false
	 	var signup_type = true;
	 	var user_region = $(".user_region").val();
	 	var browser_details = browser_name();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var free_trail_status = $(".free_trail").val();
  	   var free_trail_message = $(".free_trail_msg").val();
	      if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && terms_check == true  && confirm_pwd.length != 0 && confirm_pwd.length >= 6 && (pwd == confirm_pwd) ){
	        status = true
	        // signup_type = "email"
	 	  }

	 	if(status == true){
	 		if(free_trail_status == 'true'){
	 			// $("#fb_email_sub").text(free_trail_message+"..")
	 			$("#fb_email_sub").text("Sign up..")
	 		}
			else if(free_trail_status == 'false'){
				$("#fb_email_sub").text("Sign up..")
			}
			Netcore_Registration_Start("email")
			$.ajax({
				url: "/users/sign_up",
				type: "POST",
				data: { 
				name: user_name, 
				email_id: email_id,
			    type: "email",
				password: pwd,
				login_type: "firebase",
				browser_det: browser_details
				},
				success: function(response,status){
				 console.log(response);
				 localStorage.setItem("shm_user_login","email")
				 if(response.status == true){
				 	var user_id = ""
				 	if(signup_type == "email"){
            user_id = response.user_id
            
				 	}
				 	branch_signup_event(response.status, "", email_id,user_id)
				 	fb_pixel_signup_sucs_event()
				 
				 	var det = get_user_region_time_data()
				 	_paq.push(['trackEvent', 'Users', 'Activity', 'signup', 1, { dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: response.user_id }]);
				   $("#user_register").text("Sign up")
				   	set_user_cookies(response)
				    set_netcore_profile(email_id, "", user_name,"free","NA","NA","NA","NA","email")
				   	Netcore_Registration_Success(response,"email","")

				   	 branch.setIdentity(response.user_analytic_id);
				   	 Apxor.setUserId(response.user_analytic_id)
				   	//free_trail_fb_google_event()
				   	fb_pixel_signup_us_event()
				   	 gtag('event','Users',
			     {
			   	'dimension4': "Registered",
			   	'dimension11': response.analytics_user_id,
			   	'metric9': 1
			    })
				   	$("#fb_email_reg_name,#fb_email_reg_email,#fb_email_reg_email").val("");
	            /* $("#reg_success").modal({
	                	backdrop: 'static'
	                });	*/		
	                /*localStorage.removeItem("new_user_registered_st")
	                localStorage.setItem("new_user_registered_st","true")*/
	                 if(localStorage.getItem("activate_tv_path") != undefined){
			          localStorage.removeItem("activate_tv_path")
			          window.location = "/tv"
			         }
			         else{

			         	if(localStorage.getItem("tvod_loc") == "true"){
  		         		$("#sso_mobile_login_pop2").modal({backdrop: 'static', keyboard: false});
			         		$("#fb_email_register_pop").modal("hide");
			         		//window.location.reload();
			         	}
			         	else{
	                if(user_region == "IN"){
                    localStorage.setItem("user_mob_map_st","false")
                    $("#fb_email_register_pop").modal("hide");
        	          //$("#sso_mobile_login_pop2").modal("show");
        	          $("#sso_mobile_login_pop2").modal({backdrop: 'static', keyboard: false});
			         		}
			         		else{
			         			window.location = get_lng_web_url("/plans")
			         		}
			         	}
			          localStorage.removeItem("tvod_loc")
			         }
				                
	           
				 }
				else{
					Netcore_Registration_Failure(response.error_message,"email")
				
					if(free_trail_status == 'true'){
					  // $("#fb_email_sub").text(free_trail_message)
					  $("#fb_email_sub").text("Sign up")

					}
					else if(free_trail_status == 'false'){
						$("#fb_email_sub").text("Sign up")
					}
	        //$("#signup_resp_error_msg").text(response.error_message).show().fadeOut(4500);
	        $("#fb_backend_error_msg").text(response.error_message).show();
				 }
				}
			});
		}
		
		else if(user_name.trim().length == 0){
					Netcore_Registration_Failure("Name is mandatory","email")
		  $("#fb_email_reg_name_error").text($("#reg_mandatory_info").val()).show();
		}
		else if(email_id.length == 0){
					Netcore_Registration_Failure("Email is mandatory","email")
			$("#fb_email_reg_email_error").text($("#reg_mandatory_info").val()).show();
		}
		else if(!regex_email.test(email_id)){
					Netcore_Registration_Failure("Incorrect email format","email")
		  $("#fb_email_reg_email_error").text($("#reg_valid_email").val()).show();		  
		}
		else if(pwd.length == 0){
					Netcore_Registration_Failure("Password is mandatory","email")
		  $("#fb_email_reg_pwd_error").text($("#reg_mandatory_info").val()).show();
		}
		else if(pwd.length < 6){
					Netcore_Registration_Failure(" Password is mandatory","email")
		  $("#fb_email_reg_pwd_error").text($("#reg_min_info").val()).show();		  
		}
		else if(confirm_pwd.length == 0){
					Netcore_Registration_Failure("Confirm Password is mandatory","email")
		  $("#fb_email_reg_con_pwd_error").text($("#reg_mandatory_info").val()).show();		
		}
		else if(confirm_pwd != pwd ){
					Netcore_Registration_Failure("Passwords do not match","email")
		  $("#fb_email_reg_con_pwd_error").text($("#reg_password_match").val()).show();		
		}
		else if(terms_check == false){
		 $("#fb_terms_check_error").show();
		}
		/*else if(terms_check == false){
		  $("#terms_check_error").show();
		}*/
  }

	$("#fb_email_sub").click(function(){	
   	 fb_email_signup();
	});


	$("#fb_user_reg").click(function(){
      var user_region = $(".user_region").val()
 	if(user_region == "US"){
     fb_pixel_signup_us_event()
 	}
 	else{
 	 fb_pixel_signup_india_event()
 	}
	 $("#fb_email_login_pop").modal("hide");
     //$("#fb_email_register_pop").modal("show");
     $("#fb_email_register_pop").modal();
	})

/*End of firebase user flows*/
$("#fb_email_login_later").click(function(){
	clear_entered_data()
	$("#firebase_user_email,#firebase_password").val("")
  $("#fb_email_login_pop").modal("hide")
})

var ext_country_code = $("#ext_country_code").val();
$("#country_code,#ss_country_code").val("+"+ext_country_code)

$("#fb_otp_later").click(function(){
	clear_entered_data()
	$("#fb_otp_pop").modal("hide")
})
  
/*Start of Firebase email account merge */

$("#fb_acnt_merge").click(function(){
	fb_email_merge_sign()
})

function fb_email_merge_sign(){
	var acnt_email = $("#merge_user_email").text();
	var acnt_pwd = $("#fb_acnt_pwd").val();
  var browser_details = browser_name();
  var unq_id = firebase.auth().currentUser.uid
  var login_type = "email"
	if(acnt_pwd.length != 0){
		$("#fb_acnt_merge").text($("#merge_connect").val()+"...")
      $.ajax({
      url: "/users/account_merge",
      type: "POST",
      data: { 
      email_id: acnt_email,
      password: acnt_pwd,
      type: login_type,
      browser_det: browser_details,
      login_type: "firebase",
      uniq_id: unq_id,
      provider_type: firebase.auth().currentUser.providerData[0].providerId
      },
      success: function(response,status){

       $("#fb_acnt_merge").text($("#merge_connect").val())
       if(response.status == true){
          set_user_cookies(response)
          branch.setIdentity(response.user_analytic_id);
          Apxor.setUserId(response.user_analytic_id);
          branch_user_login_click(response)
           branch_login_success(login_type)
         gtag('event','Users',
         {
          'dimension4': response.user_status,
          'dimension11': response.user_analytic_id,
          'metric8': 1
         });
         if(response.device_limit == "true"){
            $("#fb_email_login_pop").modal("hide")
            localStorage.removeItem("max_device_limit_reached")
            localStorage.setItem("max_device_limit_reached", "true")
            $("#device-limit-pop").modal("show")
            $("#limit_device_cancel").click(function(){
              $(this).text($("#device_cancel").val()+"...")
              log_out();
            })
            $("#limit_device_confirm").click(function(){
              $(this).text($("#manage_devices").val()+"...")
              window.location = get_lng_web_url("/users/registered_devices")
            })
          }
        else{
         if(localStorage.getItem("activate_tv_path") != undefined){
          localStorage.removeItem("activate_tv_path")
          window.location = get_lng_web_url("/tv")
         }
         else{
            if(getShemarooCookies().is_parent_control == "true"){
               window.location = get_lng_web_url("/users/who_is_watching")
            }
            else{
           /* _paq.push(['trackEvent', 'Users', 'Activity', 'user_active', 1]);
            _paq.push(['trackEvent', 'Users', 'Activity', 'registered_active', 1]);
            _paq.push(['resetUserId']);
            _paq.push(['setUserId', response.user_analytic_id]);
            _paq.push(['trackPageView']);*/
               //window.location = "/"
               window.location.reload();
            }
         }
      
       }
      _paq.push(['trackEvent', 'Users', 'Activity', 'user_active', 1]);
      _paq.push(['trackEvent', 'Users', 'Activity', 'registered_active', 1]);
      _paq.push(['resetUserId']);
      _paq.push(['setUserId', response.user_analytic_id]);
      _paq.push(['trackPageView']);
       
    }
      else{
   	   $("#fb_acnt_error_msg").text(response.error_message).show();
  
     }
      }
    });
	}
	else{
		if(acnt_pwd.length == 0){
     $("#fb_acnt_pwd_error").text($("#merge_mandatory_info").val()).show();
		}
		/*else if(acnt_pwd.length != 0 && acnt_pwd.length < 6){
     $("#fb_acnt_pwd_error").text("Minimum length of 6 characters is mandatory").show();
		}*/
	}
}

$("#fgt_acnt_fb_pwd").click(function(){
	$("#acnt_merge_pop").modal("hide");
	$("#fb_forgot_pwd_pop").modal("show");

})


$("#fb_acnt_later").click(function(){
	$("#acnt_merge_pop").modal("hide");
})

/*End of Firebase email account merge */

$("#fb_acnt_pwd").focus(function(){
	$("#fb_acnt_pwd_error").hide()
})
	
$("#forgot_firebase_later").click(function(){
	clear_entered_data()
	$("#fb_forgot_pwd_pop").modal("hide")
	$("#fb_email").val("")
})

$("#fb_mobile_later,#fb_otp_later").click(function(){
	$("#fb_login_mobile_number,#fb_logn_otp").val("")
})


//new parental pin flow
$(".new_parent_cntrl_switch").click(function(){
	var parent_switch = $(this).is(':checked');
	console.log(parent_switch,"new_parent_cntrl_switch")
	if(parent_switch == false){
		$("#parental-pin-disable-pop").modal("show");
		/*localStorage.removeItem("parent_control_disable")
		// localStorage.setItem("parent_control_disable","true")
		remove_parental_pin_directly()*/
	}
	else{
		var pin_url = "/users/create_pin"
		if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
			pin_url = "/"+getShemarooCookies().shm_sel_lang+"/users/create_pin"
		}
			window.location = pin_url
	}
})

$("#user_disable_parental_pin").click(function(){
		$("#parental-pin-disable-pop").modal("hide");
		localStorage.removeItem("parent_control_disable")
		// localStorage.setItem("parent_control_disable","true")
		remove_parental_pin_directly()
	})

$("#cancel-disable").click(function(){
	$("#parental-pin-disable-pop").modal("hide");
	$('.new_parent_cntrl_switch').prop('checked', true);
	})


function new_pin_create(){
	console.log("new_pin_create")
	$("#pin_error,#cnfrm_pin_error").hide();
	var first_no  = $("#first_pwd_digit").val();
	var second_no = $("#second_pwd_digit").val();
	var third_no  = $("#third_pwd_digit").val();
	var fourth_no  = $("#fourth_pwd_digit").val();
	var user_pin = first_no+second_no+third_no+fourth_no
	var cnfrm_first_no  = $("#cnfrm_first_pwd_digit").val();
	var cnfrm_second_no = $("#cnfrm_second_pwd_digit").val();
	var cnfrm_third_no  = $("#cnfrm_third_pwd_digit").val();
	var cnfrm_fourth_no  = $("#cnfrm_fourth_pwd_digit").val();
	var cnfrm_user_pin = cnfrm_first_no+cnfrm_second_no+cnfrm_third_no+cnfrm_fourth_no
	if(first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fourth_no.length !=0 && user_pin == cnfrm_user_pin){
		//$("#create_pin").text(localStorage.getItem("confirm")+"....")
		$("#create_pin").text($("#create_confirm").val()+"....")
		$.ajax({
			url: "/users/create_pin",
			type: "POST",
			data: {pin: first_no+second_no+third_no+fourth_no},
			success: function(response,status){
				if(response.status == true){
					$.cookie('is_parent_control',"true", { expires: 14,path: '/'})
					$.cookie('pin',user_pin, { expires: 14,path: '/'})
					var set_url = "/users/settings?type=pin"
					if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
						set_url = "/"+getShemarooCookies().shm_sel_lang+"/users/settings?type=pin"
					}
					window.location = set_url
				}
			}
		});
	}
	else if(user_pin.length == 0){
		$("#pin_error").text($("#mandatory_info").val()).show().fadeOut(4500);
	}
	else if(user_pin.length < 4){
		$("#pin_error").text($("#4_digit_valid_pin_err").val()).show().fadeOut(4500);
	}
	else if(cnfrm_user_pin.length == 0){
		$("#cnfrm_pin_error").text($("#mandatory_info").val()).show().fadeOut(4500);
	}
	else if(cnfrm_user_pin.length < 4){
	    $("#cnfrm_pin_error").text($("#4_digit_valid_pin_err").val()).show().fadeOut(4500);
	}
	else if(user_pin != cnfrm_user_pin){
		$("#cnfrm_pin_error").text($("#pin_match").val()).show().fadeOut(4500);
	}
}



function new_reset_pin(){
	$("#pin_errors").hide();
	var  parental_pin = $("#parental_pin").val();
	console.log(parental_pin,"parental_pin")
	var parental_control = $("#parental_control").val()
	var old_first_no  = $("#first_pn_digit").val();
	var old_second_no = $("#second_pn_digit").val();
	var old_third_no  = $("#third_pn_digit").val();
	var old_fourth_no  = $("#fourth_pn_digit").val();
	var old_user_pin = old_first_no+old_second_no+old_third_no+old_fourth_no

	var new_first_no  = $("#new_first_pn_digit").val();
	var new_second_no = $("#new_second_pn_digit").val();
	var new_third_no  = $("#new_third_pn_digit").val();
	var new_fourth_no  = $("#new_fourth_pn_digit").val();
	var new_user_pin = new_first_no+new_second_no+new_third_no+new_fourth_no

	var cnfrm_first_no  = $("#cnfrm_first_pn_digit").val();
	var cnfrm_second_no = $("#cnfrm_second_pn_digit").val();
	var cnfrm_third_no  = $("#cnfrm_third_pn_digit").val();
	var cnfrm_fourth_no  = $("#cnfrm_fourth_pn_digit").val();
	var cnfrm_user_pin = cnfrm_first_no+cnfrm_second_no+cnfrm_third_no+cnfrm_fourth_no
	var profile = localStorage.getItem("switch_profile_id");
	if(old_first_no.length != 0 && old_second_no.length != 0 && old_third_no.length != 0 && old_fourth_no.length !=0 && parental_pin == old_user_pin && new_user_pin == cnfrm_user_pin && new_user_pin.length == 4 &&
	cnfrm_user_pin.length == 4 && new_user_pin != parental_pin){
		$("#forgot_pin_reset_pin").text($("#reset_confirm").val()+"....")
		$.ajax({
			url: "/users/create_pin",
			type: "POST",
			data: {pin: new_user_pin},
			success: function(response,status){
				if(response.status == true){
					console.log(new_user_pin,"new_user_pin")
					$.cookie('is_parent_control',"true", { expires: 14,path: '/'})
					$.cookie('pin',new_user_pin, { expires: 14,path: '/'})
					var sett_url = "/users/settings?type=pin"
					if (getShemarooCookies().shm_sel_lang != undefined  && getShemarooCookies().shm_sel_lang != "en"){
						sett_url = "/"+getShemarooCookies().shm_sel_lang+"/users/settings?type=pin"
					}
					window.location = sett_url
				}
				$("#forgot_pin_reset_pin").text($("#reset_confirm").val())
			}
		});
	}
	else if(old_user_pin.length == 0){
		$("#pin_errors").show().fadeOut(4500);
	}
	else if(old_user_pin.length < 4){
		$("#pin_errors").text($("#enter_4_digit_pin_1").val()).show().fadeOut(4500);
	}
	else if(new_user_pin.length == 0){
		$("#new_pin_errors").show().fadeOut(4500);
	}
	else if(new_user_pin.length < 4){
		$("#new_pin_errors").text($("#enter_4_digit_pin_1").val()).show().fadeOut(4500);
	}
	else if(cnfrm_user_pin.length == 0){
		$("#cnfrm_pin_errors").show().fadeOut(4500);
	}
	else if(cnfrm_user_pin.length < 4){
	$("#cnfrm_pin_errors").text($("#enter_4_digit_pin_1").val()).show().fadeOut(4500);
	}
	else if(parental_pin != old_user_pin){
		$("#parental_pin_errors_msg").text($("#invalid_old_pin").val()).show()
	}
	else if(new_user_pin != cnfrm_user_pin){
		$("#parental_pin_errors_msg").text($("#pin_not_matching").val()).show().fadeOut(4500);
	}
	else if(new_user_pin == parental_pin){
		$("#parental_pin_errors_msg").text($("#not_as_old_pin_1").val()).show().fadeOut(4500);
	}sent


}

$('#confirm_pin_email').click(function(){
	forgot_pin_new();
})


function forgot_pin_new(){
	$(" #forgot_pin_email_error").hide();
	var email_id = $("#forgot_pin_email").val();
	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var error_status = false

	if(email_id.length != 0 && regex_email.test(email_id)){
		error_status = true;
	}

	if(error_status == true){
		$("#confirm_pin_email").text($("#pin_confirm").val()+"...")
		$.ajax({
			url: "/users/forgot_pin_email",
			type: "POST",
			data: { 
				email_id: email_id
			},
			success: function(response,status){
				if(response.status == true){
					$("#confirm_pin_email").text($("#pin_confirm").val());
					$("#forgot_pin_email").val("")
					$("#fpin_bakend_user_errors").text($("#pin_has_mailed").val()).show();	
				    window.location = "/users/verify_pin"		 
				}
				else{
					$("#fpin_bakend_user_errors").text(response.error_message).show();
					$("#confirm_pin_email").text($("#pin_confirm").val());
				}
			}
		})
	}
	else{
		if(email_id.length == 0){
			$("#forgot_pin_email_error").text($("#pin_mandatory_info").val()).show();
		}
		else if(email_id.length != 0 && !regex_email.test(email_id)){
			$("#forgot_pin_email_error").text($("#pin_vaild_email").val()).show();
		}

	}
}


$("#otp_pin_verify").click(function() {
	$("#otp_pin_verify_error,#resend_otp_pin_msg").hide();
	var first_no = $("#first_digit").val();
	var second_no = $("#second_digit").val();
	var third_no = $("#third_digit").val();
	var fouth_no = $("#fourth_digit").val();
	var mobile_number = $.cookie('user_registed_mobile_no');
	if (first_no.length != 0 && second_no.length != 0 && third_no.length != 0 && fouth_no.length != 0) {
		$("#otp_pin_verify").text("Verifying..")
		$.ajax({
			url: "/users/validate_otp",
			type: "POST",
			data: {
				pin: first_no + second_no + third_no + fouth_no,
				email_id: emai_id
			},
			success: function(response, status) {
				console.log(response);
				if (response.status == true) {
					document.cookie = 'is_parent_control' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					document.cookie = 'pin' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
					window.location = "/thanks"
				} else {
					$("#otp_pin_verify").text("Verify")
					$("#first_digit,#second_digit,#third_digit,#fourth_digit").val("");
					$("#otp_pin_verify_error").text(response.error_message).show();
				}
			}
		});
	} else if (first_no.length == 0) {
			$("#otp_pin_verify_error").show();
	} else if (second_no.length == 0) {
			$("#otp_pin_verify_error").show();
	} else if (third_no.length == 0) {
			$("#otp_pin_verify_error").show();
	} else if (fouth_no.length == 0) {
			$("#otp_pin_verify_error").show();
	}
})

$("#remove_pin").click(function(){
		remove_parental_pin()
})


function remove_parental_pin(){
	$("#verify_pin_error").hide();
	var rmv_parental_pin = $("#rmv_parental_pin").val()
	console.log(rmv_parental_pin,"rmv_parental_pin")
	var first_pin_digit  = $("#first_pin_digit").val();
	var second_pin_digit = $("#second_pin_digit").val();
	var third_pin_digit  = $("#third_pin_digit").val();
	var fourth_pin_digit  = $("#fourth_pin_digit").val();
	var parental_pin = first_pin_digit+second_pin_digit+third_pin_digit+fourth_pin_digit
	console.log(parental_pin,"parental_pin")
	if(parental_pin.length != 0 && parental_pin.length >=4){
		var parent_switch = localStorage.getItem("parent_control_disable");
	}
	//localStorage.removeItem("parent_control_disable");
	var is_remove_pc = "false"
	$("#remove_pin").text("Continue....");
	if(first_pin_digit.length != 0 && second_pin_digit.length != 0 && third_pin_digit.length != 0 && fourth_pin_digit.length != 0 && rmv_parental_pin == parental_pin){
	$.ajax({
	url: "/users/remove_pin",
	type: "POST",
	data: {password: parental_pin,parent_control: is_remove_pc},
	success: function(response,status){

		if(response.status == false){
			console.log("inside false")
			$("#remove_pin_error").text(response.error_message).show()
			//$("#backend_verify_pwd_errors").text(response.error_message).show().fadeOut(4500);
			$(".verify_passowrd_toast").show().fadeOut(4500);
			$("#remove_pin").text("Continue");
		}
		else if(response.status == true){
			localStorage.removeItem("parent_control_disable")
			localStorage.setItem("parent_control_toast","true")
			document.cookie = 'is_parent_control' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			document.cookie = 'pin' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';  
			window.location = "/users/settings"						
		}
		}
	});
	}
	else{
		$("#remove_pin").text("Continue");
		if(parental_pin.length == 0){
			$("#remove_pin_error").text($("#mandatory_pin_err").val()).show();
		}
		else if(parental_pin.length < 4){
			$("#remove_pin_error").text($("#pin_length_err").val()).show();
		}
		else if(parental_pin != rmv_parental_pin){
			$("#forgot_pin_div").show()
			$("#remove_pin_error").text($("#invalid_pin_err").val()).show();
		}
	}
}


  $("#firebase_user_email,#firebase_password").focusin(function(){
    $("#firebase_user_errors_msg,#firebase_user_email_error,#firebase_password_error").hide();
  });


  $("#fb_email_reg_name,#fb_email_reg_email,#fb_email_reg_pwd,#fb_email_reg_con_pwd").focusin(function(){
    $("#fb_backend_error_msg,#fb_email_reg_name_error,#fb_email_reg_email_error,#fb_email_reg_pwd_error,#fb_email_reg_con_pwd_error").hide();
  });


  $("#fb_email").focusin(function(){
    $("#fb_forgot_errors_msg,#fb_email_error").hide();
  });

function  remove_parental_pin_directly(){
	$(".loader").show();
	$.ajax({
	url: "/users/remove_pin",
	type: "POST",
	data: {password: "",parent_control: "false"},
	success: function(response,status){
		if(response.status == false){
			console.log("inside false")		
			$(".verify_passowrd_toast").show().fadeOut(4500);
		}
		else if(response.status == true){
			$(".loader").hide();
			$("#change_pin_text").hide()
			localStorage.removeItem("parent_control_disable")
			localStorage.removeItem("parent_control_toast");
			$(".settings_page_message").text($("#parental_pin_disabled").val());
			$.cookie('is_parent_control',"false", { expires: 14,path: '/'})
			$.cookie('pin',"", { expires: 14,path: '/'})
			$(".parental_pin_toast").show().fadeOut(4500);
			$('html,body').animate({ scrollTop: 300}, 'slow');
			document.cookie = 'is_parent_control' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
			document.cookie = 'pin' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';  
		}
	}
	});
}
$("#social_error_popup_ok").click(function(){
	$("#social_error_popup").modal("hide")
})

function clear_entered_data(){
	$("#firebase_user_errors_msg,#firebase_password_error,#firebase_user_email_error,#fb_backend_error_msg,#fb_email_reg_name_error,#fb_email_reg_email_error,#fb_email_reg_pwd_error,#fb_email_reg_con_pwd_error,#fb_terms_check_error,#fb_forgot_errors_msg,#fb_email_error,#fb_otp_error_msg").hide()
}  $("#firebase_user_email,#firebase_password,#fb_email_reg_name,#fb_email_reg_email,#fb_email_reg_pwd,#fb_logn_otp,#fb_email_reg_con_pwd").val("")


$("#priv_user_sign_in").click(function(){
	$(".priv_email_error,.priv_password_error").hide();
	var user_email_id = $("#priv_email").val()
	 var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var pwd = $("#priv_pwd").val()
	error_status = false
	if(user_email_id.length != 0 && regex_email.test(user_email_id) && pwd.length != 0){
		error_status = true
	}
	if(error_status == true){
		$("#priv_user_sign_in").val("SIGN IN...")
		var browser_details = browser_name();
		$.ajax({
			url: "/users/sign_in",
			type: "POST",
			data: {
				email_id: user_email_id,
				password: pwd,
				private_check: true,
				type: "email",
				browser_det: browser_details
		},
		success: function(response,status){
			 if(response.status == true){
			 	$("#priv_user_sign_in").val("SIGN IN")
			 	// smartech('dispatch', 'Login success', {});
			    set_private_user_cookies(response)
			    window.location =  "/users/view_content"
			    // d(user_region, response.city_of_user, response.email_id, response.mob_num, response.first_name)
			    // branch.setIdentity(response.user_analytic_id);
                // branch_private_user_login_click(response)
			   // gtag('event','Users',
			   // {
			   // 	'dimension4': response.user_status,
			   // 	'dimension11': response.user_analytic_id,
			   // 	'metric8': 1
			   // });
			 
			  }
	  	}
		})
	}
	 else if(user_email_id.length == 0){
	  $(".priv_email_error").text("Please enter your registered email address").show();
	 }
	 else if( !regex_email.test(user_email_id)){
	  $(".priv_email_error").text("Invalid email address").show();
	 }
	 else if(pwd.length == 0){
	  $(".priv_password_error").text("This field is mandatory").show();
	 }
})

function set_private_user_cookies(resp){
	 $.cookie('priv_view_user_id',resp.user_id, { expires: 14,path: '/'});
	 $.cookie('priv_view_user_login_id',resp.login_id, { expires: 14,path: '/'});
	 $.cookie('priv_view_user_name',resp.user_name, { expires: 14,path: '/'});
	 $.cookie('priv_view_profile_id',resp.profile_id, { expires: 14,path: '/'});
	 $.cookie('priv_view_user_profiles',resp.user_profiles, { expires: 14,path: '/'})
	 $.cookie('priv_view_is_parent_control',resp.user_parental_control, { expires: 14,path: '/'})
	 $.cookie('priv_view_pin',resp.pin, { expires: 14,path: '/'})
	 $.cookie('priv_view_user_profile_type',resp.profile_type, { expires: 14,path: '/'})
	 $.cookie('priv_view_user_analytical_id',resp.user_analytic_id, { expires: 14,path: '/'})
	}

	



		function branch_private_user_login_click(response){
		var det = get_user_region_time_data()
		_paq.push(['trackEvent', 'Users', 'Activity', 'LOGIN', 1, {dimension14: getShemarooCookies().priv_view_user_analytical_id, dimension29: det[2], dimension30: det[3], dimension31: det[0], dimension32: det[1], dimension33: getShemarooCookies().priv_view_user_id}]);
		smartech('dispatch', 'LOGIN', {
        'region': det[0],
		'city': det[1],
		'date_event': det[2],
		'time_event': det[3],
		'session_id': response.user_id,
		'userid': response.user_analytic_id
       });

		branch.logEvent(
       "LOGIN",
        {"region": det[0],
				"city": det[1],
				"date_event": det[2],
				"time_event": det[3],
				"session_id": response.user_id,
                 "userid": response.user_analytic_id
			    },
       function(err) { console.log(err); }
      );
	}







	$("#lazypay_confirm").click(function(){
		lazypay_sub()
	})

	$("#lzypy_mob_no,#lzypy_email").focusin(function(){
    $("#lazypay_mobile_number_error,#lazypay_email_error,#lazy_pay_error").hide();
  });

  function otp_lzpay_pymt_success(){
  	$("#lazy_pay_otp_bkd_err").hide();
  	var first_otp  = $("#first_otp_digit").val();
	  var second_otp = $("#second_otp_digit").val();
	  var third_otp  = $("#third_otp_digit").val();
	  var fourth_otp  = $("#fourth_otp_digit").val();
	  var lzpy_otp = first_otp+second_otp+third_otp+fourth_otp;
	  var price = localStorage.getItem("first_purchase_plan_price")
	  if(price == undefined){
     price = localStorage.getItem("new_plan_price")
	  }
	  if(first_otp.length != 0 && second_otp.length != 0 && third_otp.length != 0 && fourth_otp.length != 0){
		  $("#lazypay_otp_confirm").text("Pay...");
		  $.ajax({
	       url: "/users/lazpay_otp_valid",
				 type: "POST",
				 data: {
					lzpyotp: lzpy_otp,
					trans_id: "",
					sub_id: localStorage.getItem("lazy_pay_sub_req_id"),
					order_id: localStorage.getItem("first_purchase_plan_order_id"),
					mobileno: localStorage.getItem("lazy_pay_mobile_no")
				},
				success: function(response, status){
					$("#lazypay_otp_confirm").text("Pay");
					if(response.status == "true"){
						var order_id = localStorage.getItem("first_purchase_plan_order_id") 
						window.location = "/payment/lazypay_response?order_id="+order_id+"&payment_gateway=lazypay&suscription_id="+getShemarooCookies().lazy_pay_sub_req_id+"&status="+response.trans_status+"&price="+price
					}
					else{
						$("#lazy_pay_otp_bkd_err").text("Invalid OTP").show();
					}
				}
		  });
  	}
  	else{
  		if(lzpy_otp.length ==  0){
  			$("#lazy_pay_otp_bkd_err").text("Please enter the OTP").show();
  		}
  		else{
  			 $("#lazy_pay_otp_bkd_err").text("Please enter the 4 digit OTP").show();
  		}
  	}
}

$("#lazypay_cancel").click(function(){
	$("#lazypay-pop").modal("hide");
	$("#new_plan_summary_proceed").text("Proceed");
})

$("#lazypay_otp_cancel").click(function(){
	$("#lazypay-otp-pop").modal("hide");
	delete_lazy_lcstge();
	$("#new_plan_summary_proceed").text("Proceed");
})

function set_timer_lzypay_otp(){
	var counter = 60;
	var interval = setInterval(function() {
		counter--;
		if (counter <= 0) {
		clearInterval(interval);
		//$("#timer").hide();
		$('#timer').text("00:00");
		return;
		}else{
		$('#timer').text("00:"+counter);
		// console.log("Timer --> " + counter);
		if(counter == 1){
			$('#timer').hide();
			$(".resend_lzy_sec").show();
		}
		}
	}, 1000);
	function checkSecond(sec) {
		if (sec < 10 && sec >= 0) {sec = "0" + sec}; // add zero in front of numbers < 10
		if (sec < 0) {sec = "59"};
		return sec;
	}
}

$("#lazypay_otp_confirm_yes").click(function(){
	$(this).text("CONFIRM WITH OTP...");
	$("#lazypay_confirm_pop").modal("hide");
	$("#lazypay-otp-pop").modal("show");
	set_timer_lzypay_otp()
})

function lzypay_resend_otp(){
 $("#resend_otp_lazypay").text("Resend...");
 var price = localStorage.getItem("first_purchase_plan_price")
 if(price == undefined){
       price = localStorage.getItem("new_plan_price")
        plan_category = localStorage.getItem("new_plans_details").split("|")[3]
        var promo_dis_amt = localStorage.getItem("new_plan_promo_flag")
      if(promo_dis_amt != undefined && promo_dis_amt == "true"){
        price = localStorage.getItem("new_plan_purcahse_promo_disc_amt")
       }
	  }
	  else{
	  	plan_category  = localStorage.getItem("first_purchase_plans_details").split("|")[3]
	  	 var promo_dis_amt = localStorage.getItem("first_purcahse_promo_flag")
        if(promo_dis_amt != undefined && promo_dis_amt == "true"){
         price  = localStorage.getItem("first_purcahse_promo_disc_amt")
       }
	  }
  $.ajax({
   url: "/users/lzypay_rsd_otp",
	 type: "POST",
	 data: {
		elg_id:  localStorage.getItem("lazy_pay_elgi_id"),
		mobileno: localStorage.getItem("lazy_pay_mobile_no"),
        email_id: localStorage.getItem("lazy_pay_email_id"),
        amt: price
	},
	success: function(response,status){
		console.log(response);
		$(".resend_lzy_sec").hide();
		set_lazy_pay_cookies(response);
		if(response.status == "true"){
      $("#lazy_pay_otp_bkd_err").text("Otp Sent successfully").show().fadeOut(4500);
		}
		else{
      $("#lazy_pay_otp_bkd_err").text("Sorry something went wrong").show().fadeOut(4500);
		}
	}
	});
}

$("#resend_otp_lazypay").click(function(){
	lzypay_resend_otp()
})

$("#worldmeday_user_register").click(function(){
	 	worldmeday_user_register();
	});


function worldmeday_user_register(){
 		$("#signup_worldmeday_user_name_error,#signup_worldmeday_user_email_error,#signup_worldmeday_password_error,#signup_worldmeday_confirm_password_error,#worldmeday_agree_terms, #signup_worldmeday_coupon_code_error, #worldmeday_signup_user_promo_error").hide();
	 	var user_name = $("#worldmeday_user_name").val();
	 	var email_id = $("#worldmeday_user_email").val();
	 	var pwd = $("#worldmeday_password").val();
	 	var cf_pwd = $("#worldmeday_confirm_password").val();
	 	var terms_check = $("#worldmeday_agree_terms").is(':checked')
	 	var status = false
	 	var coupon_check = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var worldmeday_coupon_code = $("#worldmeday_coupon_code").val()
	 	var url = window.location.pathname.split("/")[1]
       
    if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd) && terms_check == true){
      status = true
      signup_type = "email"
 	  }

	 	if(status == true){
			$("#worldmeday_user_register").text("START FREE TRIAL..")
			$.ajax({
				url: "/users/coupon_register",
				type: "POST",
				data: { 
				name: user_name, 
				email_id: email_id,
			    type: signup_type,
				password: pwd,
				browser_det: browser_name(),
				coupon: worldmeday_coupon_code
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
				   $("#worldmeday_user_register").text("START FREE TRIAL")
				   	set_user_cookies(response);
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	$("#worldmeday_user_name,#worldmeday_user_email,#worldmeday_password,#worldmeday_confirm_password").val("");
                    if(response.pack_st == "true"){
                     $("#plan_act_pop").modal("show")
                    }
                    else{
                    	 window.location = "/"	
                    } 	   
				 }
				else{
				 $("#worldmeday_user_register").text("START FREE TRIAL")
				 $("#signup_worldmeday_resp_error_msg").text(response.error_message).show()
				 $('html,body').animate({ scrollTop: 300}, 'slow');
				 }
				}
			});
		}
		else if(user_name.trim().length == 0){
		  $("#signup_worldmeday_user_name_error").show();
		}
		else if( email_id.length == 0){
			$("#signup_worldmeday_user_email_error").show();			
		}
		else if( !regex_email.test(email_id)){
		  $("#signup_worldmeday_user_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#signup_worldmeday_password_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#signup_worldmeday_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#signup_worldmeday_confirm_password_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#signup_worldmeday_confirm_password_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	      $("#signup_worldmeday_confirm_password_error").text("Passwords do not match").show();
		}
		else if(terms_check == false){
		  $("#worldmeday_agree_terms").show();
		}
		else if(worldmeday_coupon_code.length != 0 && $('#offers_apply_promo_code').is(':visible')){
			localStorage.removeItem("promo_apply")
			localStorage.setItem("promo_apply", "true")
			offer_promo_code_apply()
		}
  }

 $("#worldmeday_apply_promo_code").click(function(){
 	$("#worldmeday_signup_user_promo_error").hide();
 	var coupon_code = $("#worldmeday_coupon_code").val();
 	if(coupon_code.trim().length != 0){
 		$(this).text("apply..")
		$.ajax({
			url: "/users/validate_promocode",
			type: "POST",
			data: { 
			coupon: coupon_code,
			region: "IN"
			},
			success: function(response,status){
			console.log(response);
			$("#worldmeday_apply_promo_code").text("apply")
			if(response.status == true){  
				$("#worldmeday_signup_user_promo_error").text("coupon applied successfully").show().fadeOut(4500);
				$("#worldmeday_apply_promo_code").hide();
				$("#coupon_id").val(response.coupon_id)
			}
			else{
			$("#worldmeday_apply_promo_code").text("apply")
			$("#worldmeday_signup_user_promo_error").text(response.error_message).show();
			}
		  }
		});	
 	}
 });


 //------------- netcore events starts---------------

 function Netcore_Registration_Start(reg_mode){
    	var user_region = $(".user_region").val()
    var source = localStorage.getItem("nc_sub_strt_src") == "true" ? "Payment Page" : "Registration Pop Up"
    var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
	 smartech('dispatch', 'Registration_Start', {
	"source":source,
	"method": reg_mode,
	"COUNTRY": user_region,
	'user_state': user_state

   });
	 Apxor_Registration_Start(reg_mode, user_state)
	 if(reg_mode == "facebook"){
		reg_mode = "FB"
	}else if(reg_mode == "Phone number"){
		reg_mode = "Phone Number"
	}else{	
		reg_mode = reg_mode.charAt(0).toUpperCase() + reg_mode.slice(1)
	}

	 branch.logEvent(
	"Registration_Start",
		{
	"source":source,
	"method": reg_mode,
	"user_state": user_state

	},
	function(err) { console.log(err); }
);
}


function Netcore_Registration_Success(response,reg_mode,mob_no){
	var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
    	var user_region = $(".user_region").val()
	 console.log("netcore Registration_Success----------------------------")
	  smartech('identify', response.user_analytic_id); 
      smartech('dispatch',1,{});
    var det = get_user_region_time_data()
    smartech('dispatch', 'Registration_Success', {
  'region': det[0],
  'city': det[1],
  'date_event': det[2],
  'time_event': det[3],
  'session_id': response.user_id,
  'user_id': response.user_analytic_id,
  'method': reg_mode,
  'COUNTRY':user_region,
  'phone_number': mob_no,
  'user_state': user_state
    });

	if(reg_mode == "facebook"){
		reg_mode = "FB"
	}else if(reg_mode == "phone number"){
		reg_mode = "Phone Number"
	}else{
		reg_mode = reg_mode.charAt(0).toUpperCase() + reg_mode.slice(1)
	}

    branch.logEvent(
	"Registration_Success",
		{
	"method": reg_mode,
	'region': det[0],
	'user_id': response.user_analytic_id,
	'phone_number': mob_no,
	'user_state': user_state
	},
	function(err) { console.log(err); }
);
    Apxor_Registration_Success(response,reg_mode,mob_no, user_state)
}

function Netcore_Registration_Failure(error_message,reg_mode){
	var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
console.log("netcore Registration_Failure----------------------------")
    	var user_region = $(".user_region").val()
smartech('dispatch', 'Registration_Failure', {
"error_type":error_message,
"method":reg_mode,
"COUNTRY":user_region,
"user_state": user_state

    });

	if(reg_mode == "facebook"){
		reg_mode = "FB"
	}else if(reg_mode == "phone number"){
		reg_mode = "Phone Number"
	}else{
		reg_mode = reg_mode.charAt(0).toUpperCase() + reg_mode.slice(1)
	}
	branch.logEvent(
	"Registration_Failure",
		{
	"error_type":error_message,
	"method": reg_mode,
	"user_state": user_state
	},
	function(err) { console.log(err); }
	);

Apxor_Registration_Failure(error_message,reg_mode, user_state)
}



// ------------------------- apxor events 

function apxor_set_user_pro( user_email_id, mobile_no, first_name,sub_st,sub_pln,sub_pt,sub_expd,sub_dt,reg_mod){
        var city = $(".user_city").val()
        var user_region = $(".user_region").val()
        var user_state = $("#user_state").val();
        console.log("apxor_set_user_pro---------------------------------------")
    $.ajax({
		url: "/users/user_prop",
		type: "POST",
		data: {user_id: getShemarooCookies().user_analytical_id},
		success: function(response){
			console.log(response);
			if(response.status){
        Apxor.setUserProperties({
	    "user_id": getShemarooCookies().user_analytical_id,
	    "country": response.dt.country,
	    "email": user_email_id,
	    "city": city,
	    "user_language" : getShemarooCookies().shm_sel_lang,
	    "user_type": response.dt.user_type,
	    "user_state": user_state,
      "registration_status": response.dt.user_type

	    })
       send_apxor_plans_info(response.dt)
			}
		}
	})
                     /* Apxor.setUserProperties({
          "email": user_email_id,
          "mobile": mobile_no,
          "first_name": first_name,
          "country": user_region,
          "city": city,
          "state": "NA",
          "registration_status": "registered",
          "subscription_status": sub_st,
          "subscription_plan_name": sub_pln,
          "subscription_plan_type": sub_pt,
          "subscription_expiry_date": sub_expd,
          "subscription_date": sub_dt,
          "registration_mode": reg_mod
                      });*/
}

function Apxor_Registration_Start(reg_mode, user_state){
      var user_region = $(".user_region").val()
        var source = localStorage.getItem("nc_sub_strt_src") == true ? "Payment Page" : "Registration Pop Up"
       console.log("Apxor Registration_Success----------------------------")
   var det = get_user_region_time_data()
   Apxor.logEvent("Registration_Start", {
 "source":source,
      "method": reg_mode,
      "COUNTRY": user_region,
      "user_state": user_state
   }, "REGISTRATION");
}



function Apxor_Registration_Success(response,reg_mode,mobileno, user_state){
      var user_region = $(".user_region").val()
       console.log("Apxor Registration_Success----------------------------")
   var det = get_user_region_time_data()
   Apxor.logEvent("Registration_Success", {
 'region': det[0],
 'city': det[1],
 'date_event': det[2],
 'time_event': det[3],
 'method': reg_mode,
 'COUNTRY':user_region,
 'phone_number': mobileno,
 'user_state': user_state
   }, "REGISTRATION");
}


function Apxor_Registration_Failure(error_message,reg_mode, user_state){
      var user_region = $(".user_region").val()
      console.log("Apxor Registration_Failure----------------------------")
      var det = get_user_region_time_data()
      Apxor.logEvent("Registration_Failure", {
              "error_type":error_message,
              "method":reg_mode,
              "COUNTRY":user_region,
              "user_state": user_state
      }, "REGISTRATION");
}

function delete_lazy_lcstge(){
	var  keysToRemove = ["lazy_pay_elgi_id","lazy_pay_sub_req_id","lazy_pay_mobile_no","lazy_pay_email_id"];
  keysToRemove.forEach(k => localStorage.removeItem(k))
}

function set_lazy_lcstge(det){
	var mob_no = $("#user_mob_no").val();
	var email_id = $("#lzypy_email").val();
	localStorage.setItem("lazy_pay_elgi_id",det.elg_id);
	localStorage.setItem("lazy_pay_sub_req_id",det.sub_req_id)
  localStorage.setItem("lazy_pay_mobile_no",mob_no)
  localStorage.setItem("lazy_pay_email_id",email_id)
}

 function set_lazy_pay_cookies(lz_dt){
 	delete_lazy_lcstge()
  set_lazy_lcstge(lz_dt)
 }

	function lazypay_sub(){
		if(getShemarooCookies().mobile_browser_type == "android"){
      var user_anal_id = getShemarooCookies().user_analytical_id;
      branch.logout();
      branch.setIdentity(user_anal_id);
    }
		$("#lazypay_mobile_number_error,#lazypay_email_error,#lazy_pay_error").hide();
	  var lz_mobile_no = $("#lzypy_mob_no").val();
	  var lz_email = $("#lzypy_email").val();
	  var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	  var price = localStorage.getItem("first_purchase_plan_price")
	  var plan_category = ""
	  if(price == undefined){
       price = localStorage.getItem("new_plan_price")
        plan_category = localStorage.getItem("new_plans_details").split("|")[3]
        var promo_dis_amt = localStorage.getItem("new_plan_promo_flag")
      if(promo_dis_amt != undefined && promo_dis_amt == "true"){
        price = localStorage.getItem("new_plan_purcahse_promo_disc_amt")
       }
	  }
	  else{
	  	plan_category  = localStorage.getItem("first_purchase_plans_details").split("|")[3]
	  	 var promo_dis_amt = localStorage.getItem("first_purcahse_promo_flag")
        if(promo_dis_amt != undefined && promo_dis_amt == "true"){
         price  = localStorage.getItem("first_purcahse_promo_disc_amt")
       }
	  }
	  if(lz_mobile_no.trim().length != 0 && lz_mobile_no.length == 10 && lz_email.length != 0 && regex_email.test(lz_email)){
	  	$("#lazypay_confirm").text("Proceed...");
	  	$(".loader").show();
      $.ajax({
			url: "/users/validate_lzy_mobile",
			type: "POST",
			data: {
				mobile: lz_mobile_no,
				email: lz_email,
				amt: price,
				plan_catgry: plan_category
			},
			success: function(response, status) {
				console.log(response);
				$(".loader").hide();
				$("#lazypay_confirm").text("Proceed");
				if (response.status == "true") {
				  set_lazy_pay_cookies(response);
				  $("#lazypay-pop").modal("hide");
				  //$("#lazypay-otp-pop").modal("show");
				  //$(".lzypy_plan_amt").text(localStorage.getItem("first_purchase_plan_price"));
				  $(".lzypy_plan_amt").text(price)
				  $("#lazypay_confirm_pop").modal("show");
				}else{
					$("#lazy_pay_error").text(response.error_msg).show();
				}
			}
		});
	  }
	  else{
	  	if(lz_mobile_no.trim().length == 0){
	  	  $("#lazypay_mobile_number_error").text("This field is mandatory").show();
	  	}
	  	else if(lz_mobile_no.trim().length != 0 && lz_mobile_no.length < 10){
       $("#lazypay_mobile_number_error").text("Please enter your 10 digit mobile number").show();
	  	}
	  	else if(lz_mobile_no.trim().length != 0 && lz_mobile_no.length > 10){
       $("#lazypay_mobile_number_error").text("Please enter your 10 digit mobile number").show();
	  	}
	  	else if(lz_email.trim().length == 0){
       $("#lazypay_email_error").text("This field is mandatory").show();
	  	}
	  	else if(lz_email.trim().length != 0 && !regex_email.test(lz_email)){
       $("#lazypay_email_error").text("Please enter a valid email address").show();
	  	}
	  }
	}

	$("#lazypay_confirm").click(function(){
		lazypay_sub()
	})

	$("#lzypy_mob_no,#lzypy_email").focusin(function(){
    $("#lazypay_mobile_number_error,#lazypay_email_error,#lazy_pay_error").hide();
  });

  function otp_lzpay_pymt_success(){
  	$("#lazy_pay_otp_bkd_err").hide();
  	var first_otp  = $("#first_otp_digit").val();
	  var second_otp = $("#second_otp_digit").val();
	  var third_otp  = $("#third_otp_digit").val();
	  var fourth_otp  = $("#fourth_otp_digit").val();
	  var lzpy_otp = first_otp+second_otp+third_otp+fourth_otp;
	  var price = localStorage.getItem("first_purchase_plan_price")
	  if(price == undefined){
       price = localStorage.getItem("new_plan_price")
        var promo_dis_amt = localStorage.getItem("new_plan_promo_flag")
      if(promo_dis_amt != undefined && promo_dis_amt == "true"){
        price = localStorage.getItem("new_plan_purcahse_promo_disc_amt")
       }
	  }
	  else{
	  	 var promo_dis_amt = localStorage.getItem("first_purcahse_promo_flag")
        if(promo_dis_amt != undefined && promo_dis_amt == "true"){
         price  = localStorage.getItem("first_purcahse_promo_disc_amt")
       }
	  }
	  if(first_otp.length != 0 && second_otp.length != 0 && third_otp.length != 0 && fourth_otp.length != 0){
		  $("#lazypay_otp_confirm").text("Pay...");
		  $.ajax({
	       url: "/users/lazpay_otp_valid",
				 type: "POST",
				 data: {
					lzpyotp: lzpy_otp,
					trans_id: "",
					sub_id: localStorage.getItem("lazy_pay_sub_req_id"),
					order_id: localStorage.getItem("first_purchase_plan_order_id"),
					mobileno: localStorage.getItem("lazy_pay_mobile_no"),
					plan_price: price
				},
				success: function(response, status){
					$("#lazypay_otp_confirm").text("Pay");
					if(response.status == "true"){
						var order_id = localStorage.getItem("first_purchase_plan_order_id") 
						window.location = "/payment/lazypay_response?order_id="+order_id+"&payment_gateway=lazypay&suscription_id="+getShemarooCookies().lazy_pay_sub_req_id+"&status="+response.trans_status+"&price="+price+"&title="+response.plan_title+"&pln_exp_dt="+response.pln_exp_dt
					}
					else{
						$("#lazy_pay_otp_bkd_err").text("Invalid OTP").show();
					}
				}
		  });
  	}
  	else{
  		if(lzpy_otp.length ==  0){
  			$("#lazy_pay_otp_bkd_err").text("Please enter the OTP").show();
  		}
  		else{
  			 $("#lazy_pay_otp_bkd_err").text("Please enter the 4 digit OTP").show();
  		}
  	}
}

  $("#lazypay_otp_confirm").click(function(){
  	otp_lzpay_pymt_success()
  })



$("#lazypay_cancel").click(function(){
	$("#lazypay-pop").modal("hide");
	$("#new_plan_summary_proceed").text("Proceed");
})

$("#lazypay_otp_cancel").click(function(){
	$("#lazypay-otp-pop").modal("hide");
	delete_lazy_lcstge();
	$("#new_plan_summary_proceed").text("Proceed");
})

function set_timer_lzypay_otp(){
	var counter = 60;
	var interval = setInterval(function() {
		counter--;
		if (counter <= 0) {
		clearInterval(interval);
		//$("#timer").hide();
		$('#timer').text("00:00");
		return;
		}else{
		$('#timer').text("00:"+counter);
		// console.log("Timer --> " + counter);
		if(counter == 1){
			$('#timer').hide();
			$(".resend_lzy_sec").show();
		}
		}
	}, 1000);
	function checkSecond(sec) {
		if (sec < 10 && sec >= 0) {sec = "0" + sec}; // add zero in front of numbers < 10
		if (sec < 0) {sec = "59"};
		return sec;
	}
}

$("#lazypay_otp_confirm_yes").click(function(){
	$(this).text("CONFIRM WITH OTP...");
	$("#lazypay_confirm_pop").modal("hide");
	$("#lazypay-otp-pop").modal("show");
	set_timer_lzypay_otp()
})

function lzypay_resend_otp(){
 var plan_category = localStorage.getItem("first_purchase_plans_details").split("|")[3]
 $("#resend_otp_lazypay").text("Resend...");
  $.ajax({
   url: "/users/lzypay_rsd_otp",
	 type: "POST",
	 data: {
		elg_id:  localStorage.getItem("lazy_pay_elgi_id"),
		mobileno: localStorage.getItem("lazy_pay_mobile_no"),
    email_id: localStorage.getItem("lazy_pay_email_id"),
    amt: localStorage.getItem("first_purchase_plan_price"),
    category: plan_category
	},
	success: function(response,status){
		console.log(response);
		$(".resend_lzy_sec").hide();
		set_lazy_pay_cookies(response);
		if(response.status == "true"){
      $("#lazy_pay_otp_bkd_err").text("Otp Sent successfully").show().fadeOut(4500);
		}
		else{
      $("#lazy_pay_otp_bkd_err").text("Sorry something went wrong").show().fadeOut(4500);
		}
	}
	});
}

/*$("#resend_otp_lazypay").click(function(){
	lzypay_resend_otp()
})*/


function Netcore_Watch_Later(response){
console.log("netcore Netcore_Watch_Later----------------------------",response)
var user_region = $(".user_region").val()
var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status').toLowerCase() : "anonymous"
var release_date = parseInt($("#release_date").val().split("T")[0].split("-")[0])
smartech('dispatch', 'Watch_Later', {
		'content_type': $('#theme_type').val(),
		'genre': $("#genre").val(),
		'language': $('#language').val(),
		'category': $('#item_category').val(),
		'content_name': $("#ma_title").val(),
		'session_id': getShemarooCookies().user_id,
		'user_id': getShemarooCookies().user_analytic_id,
    	'release_year_new1':release_date,
    	'COUNTRY':user_region,
    	"user_state": user_state,
    	'video_name' : $("#ma_title").val(),
		'Parent_Video_name' : $("#ma_title").val()
	});
 
 Apxor.logEvent('Watch_Later', {
    'content_type': $('#theme_type').val(),
		'genre': $("#genre").val(),
		'language': $('#language').val(),
		'content_name': $("#ma_title").val(),
		'video_name' : $("#ma_title").val(),
		'Parent_Video_name' : $("#ma_title").val(),
		'user_id': getShemarooCookies().user_analytic_id,
		"user_state": user_state
	}, "USER_EVENTS")

 bt_cnt_type = $('#theme_type').val()
if(bt_cnt_type == "linear"){
  bt_cnt_type = "tv channel"
}else if(bt_cnt_type == "show"){
  bt_cnt_type = "episode"
}

	 branch.logEvent(
	"Watch_Later",
		{
	"content_type": bt_cnt_type,
	"content_name":  $("#branch_title").val(),
	"genre":  $("#genre").val(),
	'video_name' : $("#ma_title").val(),
	'Parent_Video_name' : $("#ma_title").val(),
	"language": $('#language').val(),
	"user_state": user_state
		},
	function(err) { console.log(err); }
	);


}

$(".see-more-episodes").click(function(){
	var parent_category = localStorage.getItem("parent_category_src")
	var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
	if(parent_category == undefined || parent_category.length == 0 ){
		parent_category  = "home page"
	}
	var catalog_name = $(this).data("cat-name")
	Apxor.logEvent("Catalogue_Browse",{
  "all_episodes": catalog_name,
  "see_more": catalog_name,
  "parent_category": parent_category,
  "user_state": user_state
  }, "Catalogue_Browse");

  smartech('dispatch', 'Catalogue_Browse', {
    "all_episodes": catalog_name,
    "see_more": catalog_name,
    "parent_category": parent_category,
    "user_state": user_state
  });

  branch.logEvent(
  "Catalogue_Browse",
    {"all_episodes": catalog_name,
    "see_more": catalog_name,
    "parent_category": parent_category,
    "user_state": user_state
  },
  function(err) { console.log(err); }
  );
})

$(".see-more").click(function(){
	var catalog_name = $(this).data("cat-name")
	 localStorage.removeItem("nc_stream_src")
    localStorage.setItem("nc_stream_src","More in")
Netcore_Browse_More(catalog_name)
})

function Netcore_Browse_More(catalog_name){
var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
var site_name= $("#host_site").val()
var user_region = $(".user_region").val()
console.log("netcore Netcore_Browse_More----------------------------")
//var parent_category = $('ul.nav').find('li a.active').text()
var parent_category = localStorage.getItem("parent_category_src")

if(parent_category == undefined || parent_category.length == 0 ){
	parent_category  = "home page"
}
smartech('dispatch', 'Browse_More', {
"user_id": getShemarooCookies().user_analytic_id,
"user_state": user_state,
"parent_category": parent_category,
"see_more": catalog_name,
"COUNTRY":user_region		
	});
	Apxor.logEvent("Catalogue_Browse",{
  "see_more": catalog_name,
  "parent_category": parent_category,
  "user_state": user_state
  }, "Catalogue_Browse");

  smartech('dispatch', 'Catalogue_Browse', {
    "see_more": catalog_name,
    "parent_category": parent_category,
    "user_state": user_state
  });
  branch.logEvent(
  "Catalogue_Browse",
    {"see_more": catalog_name,
    "parent_category": parent_category,
    "user_state": user_state
  },
  function(err) { console.log(err); }
  );
}



 $("#rehalt_user_register").click(function(){
	 	rehalt_offer_user_register();
	});

 function rehalt_offer_user_register(){
 		$("#signup_rehalt_user_name_error,#signup_rehalt_user_email_error,#signup_rehalt_password_error,#signup_rehalt_confirm_password_error,#rehalt_agree_terms, #signup_rehalt_coupon_code_error, #rehalt_signup_user_promo_error").hide();
	 	var user_name = $("#rehalt_user_name").val();
	 	var email_id = $("#rehalt_user_email").val();
	 	var pwd = $("#rehalt_password").val();
	 	var cf_pwd = $("#rehalt_confirm_password").val();
	 	var terms_check = $("#rehalt_agree_terms").is(':checked')
	 	var status = false
	 	var coupon_check = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var rehalt_coupon_code = $("#rehalt_coupon_code").val()
	 	var url = window.location.pathname.split("/")[1]
       
    if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd) && terms_check == true){
      status = true
      signup_type = "email"
 	  }

	 	if(status == true){
			$("#rehalt_user_register").text("START FREE TRIAL..")
			$.ajax({
				url: "/users/coupon_register",
				type: "POST",
				data: { 
				name: user_name, 
				email_id: email_id,
			    type: signup_type,
				password: pwd,
				browser_det: browser_name(),
				coupon: rehalt_coupon_code,
				coupon_type: "rehaltoffer"
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
				 	
				   $("#rehalt_user_register").text("START FREE TRIAL")
				   	/*set_user_cookies(response);
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })*/
				   	$("#rehalt_user_name,#rehalt_user_email,#rehalt_password,#rehalt_confirm_password").val("");
                    if(response.pack_st == "true"){
                      /* if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ){
                       $("#rehal_link").show();
                       $("#rehal_cong").text("Congratulations you have successfully availed 3 months free offer for ShemarooMe. To watch free content please download the ShemarooMe Android/iOS app from below link")
                       }
                      else{
                      $("#rehal_cong").text("Congratulations you have successfully availed 3 months free offer for ShemarooMe. To watch free content please download the ShemarooMe Android/iOS app");
                      }*/
                     $("#download_app_pop").modal("show");
                    }
                    else{
                    	$(".normal_txt").text("Thank you Registering on ShemarooMe.To watch free content please download the ShemarooMe app from the links below")
                    	$("#download_app_pop").modal("show");
                    	 // window.location = "/"	
                    } 	   
				 }
				else{
				 $("#rehalt_user_register").text("START FREE TRIAL")
				 $("#signup_rehalt_resp_error_msg").text(response.error_message).show()
				 $('html,body').animate({ scrollTop: 300}, 'slow');
				 }
				}
			});
		}
		else if(user_name.trim().length == 0){
		  $("#signup_rehalt_user_name_error").show();
		}
		else if( email_id.length == 0){
			$("#signup_rehalt_user_email_error").show();			
		}
		else if( !regex_email.test(email_id)){
		  $("#signup_rehalt_user_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#signup_rehalt_password_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#signup_rehalt_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#signup_rehalt_confirm_password_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#signup_rehalt_confirm_password_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	      $("#signup_rehalt_confirm_password_error").text("Passwords do not match").show();
		}
		else if(terms_check == false){
		  $("#rehalt_agree_terms").show();
		}
		else if(rehalt_coupon_code.length != 0 && $('#offers_apply_promo_code').is(':visible')){
			localStorage.removeItem("promo_apply")
			localStorage.setItem("promo_apply", "true")
			offer_promo_code_apply()
		}
  }


 $("#rehalt_apply_promo_code").click(function(){
 	$("#rehalt_signup_user_promo_error").hide();
 	var coupon_code = $("#rehalt_coupon_code").val();
 	if(coupon_code.trim().length != 0){
 		$(this).text("apply..")
		$.ajax({
			url: "/users/validate_promocode",
			type: "POST",
			data: { 
			coupon: coupon_code
			// region: "AE"
			},
			success: function(response,status){
			console.log(response);
			$("#rehalt_apply_promo_code").text("apply")
			if(response.status == true){  
				$("#rehalt_signup_user_promo_error").text("coupon applied successfully").show().fadeOut(4500);
				$("#rehalt_apply_promo_code").hide();
				$("#coupon_id").val(response.coupon_id)
			}
			else{
			$("#rehalt_apply_promo_code").text("apply")
			$("#rehalt_signup_user_promo_error").text(response.error_message).show();
			}
		  }
		});	
 	}
 });

 
$("#cons_gen_user_register").click(function(){
	 	cont_gen_user_register();
	});


function cont_gen_user_register(){
 		$("#signup_cons_gen_user_name_error,#signup_cons_gen_user_email_error,#signup_cons_gen_password_error,#signup_cons_gen_confirm_password_error,#cons_gen_agree_terms, #signup_cons_gen_coupon_code_error, #cons_gen_signup_user_promo_error").hide();
	 	var user_name = $("#cons_gen_user_name").val();
	 	var email_id = $("#cons_gen_user_email").val();
	 	var pwd = $("#cons_gen_password").val();
	 	var cf_pwd = $("#cons_gen_confirm_password").val();
	 	var terms_check = $("#cons_gen_agree_terms").is(':checked')
	 	var status = false
	 	var coupon_check = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var cons_gen_coupon_code = $("#cons_gen_coupon_code").val()
	 	var url = window.location.pathname.split("/")[1]
       
    if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd) && terms_check == true){
      status = true
      signup_type = "email"
 	  }

	 	if(status == true){
			$("#cons_gen_user_register").text("Register now..")
			$.ajax({
				url: "/users/coupon_register",
				type: "POST",
				data: { 
				name: user_name, 
				email_id: email_id,
			    type: signup_type,
				password: pwd,
				browser_det: browser_name(),
				coupon: $("#cons_gen_coupon_code").val(),
				//coupon: "INDIANCONSULATENY",
				coupon_type: "consult"
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
                  var region = $("#con_user_region").val();				 	
				   $("#cons_gen_user_register").text("Register now")
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	$("#cons_gen_user_name,#cons_gen_user_email,#cons_gen_password,#cons_gen_confirm_password").val("");
                   
                   if((region !=undefined && region == "CA" ) || (region !=undefined && region == "FR" ) || (region !=undefined && region == "ES")){
                    $("#download_app_pop").modal("show");
                   }
                   else{
                    set_user_cookies(response);
                     window.location = "/"	
                   }			   	
                       
				 }
				else{
				 $("#cons_gen_user_register").text("START FREE TRIAL")
				 $("#signup_cons_gen_resp_error_msg").text(response.error_message).show()
				 $('html,body').animate({ scrollTop: 300}, 'slow');
				 }
				}
			});
		}
		else if(user_name.trim().length == 0){
		  $("#signup_cons_gen_user_name_error").show();
		}
		else if( email_id.length == 0){
			$("#signup_cons_gen_user_email_error").show();			
		}
		else if( !regex_email.test(email_id)){
		  $("#signup_cons_gen_user_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#signup_cons_gen_password_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#signup_cons_gen_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#signup_cons_gen_confirm_password_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#signup_cons_gen_confirm_password_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	      $("#signup_cons_gen_confirm_password_error").text("Passwords do not match").show();
		}
		else if(terms_check == false){
		  $("#cons_gen_agree_terms").show();
		}
		/*else if(cons_gen_coupon_code.length != 0 && $('#offers_apply_promo_code').is(':visible')){
			localStorage.removeItem("promo_apply")
			localStorage.setItem("promo_apply", "true")
			offer_promo_code_apply()
		}*/
  }

  
 $("#homeis_gen_apply_promo_code").click(function(){
 	$("#cons_gen_signup_user_promo_error").hide();
 	var coupon_code = $("#cons_gen_coupon_code").val();
 	if(coupon_code.trim().length != 0){
 		$(this).text("apply..")
		$.ajax({
			url: "/users/validate_promocode",
			type: "POST",
			data: { 
			coupon: coupon_code
			// region: "AE"
			},
			success: function(response,status){
			console.log(response);
			$("#homeis_gen_apply_promo_code").text("apply")
			if(response.status == true){  
				$("#cons_gen_signup_user_promo_error").text("coupon applied successfully").show().fadeOut(4500);
				$("#homeis_gen_apply_promo_code").hide();
				$("#coupon_id").val(response.coupon_id)
			}
			else{
			$("#homeis_gen_apply_promo_code").text("apply")
			$("#cons_gen_signup_user_promo_error").text(response.error_message).show();
			}
		  }
		});	
 	}
 });


$("#cons_gen_coupon_code").focusin(function(){
 $("#cons_gen_signup_user_promo_error").hide();
});


function get_os_det(){
  var osname = "Linux";
  if (navigator.appVersion.indexOf("Win")!=-1) osname="Windows";
 if (navigator.appVersion.indexOf("Mac")!=-1) osname="MacOS";
 if (navigator.appVersion.indexOf("X11")!=-1) osname="UNIX";
 if (navigator.appVersion.indexOf("Linux")!=-1) osname="Linux";
 return osname
}

	$("#redm_tckt_apy").click(function(){
		$(".promocode_error").hide();
		var cpn_code = $("#promo_code").val()
		if(cpn_code.trim().length != 0){
		$(this).text($("#rdm_apply").val()+"..")
		localStorage.removeItem("brnch_finger_id")
		branch.getBrowserFingerprintId(function(err, data) { localStorage.setItem("brnch_finger_id",data) });
		if(getShemarooCookies().mobile_browser_type == "android"){
	  	var data = $.cookie('android_branch_set').split('|')
	  	var branch_data = {
          "aaid": data[0],
          "city": data[1],
          "device_id" : data[2],
          "device_make": data[3],
          "user_ip": data[4],
          "os":data[5],
           "user_agent": data[6]
        }
					
		}else{
			var osname = get_os_det()
			var user_id = getShemarooCookies().user_analytical_id;
			var user_city = $(".user_city").val();
			var branch_data = {
        "os": osname,
        "user_agent": navigator.userAgent,
        "browser_fingerprint_id": localStorage.getItem("brnch_finger_id"),
        "city": user_city
      }
    }
		$.ajax({
			url: "/users/redeem_ticket",
			type: "POST",
			data: { 
			coupon: cpn_code,
			lang : getShemarooCookies().shm_sel_lang,
			branch_data: branch_data
			},
			success: function(response,status){
				console.log(response)
				$("#redm_tckt_apy").text($("#rdm_apply").val())
				if(response.status == true){  
					window.location = get_lng_web_url("/payment/tvod_success?order_id="+response.order_id+"&msg="+response.message+"&pln_tit="+response.title+"&exp_dt="+response.exp_dt+"&cupn_code="+cpn_code+"&cupn_prc=") 
				}
				else{
					$("#redm_tckt_apy").text($("#rdm_apply").val())
					if(response.error_message == "Invalid code entered"){
						$(".promocode_error").text(response.error_message).show().fadeOut(5500);
					}else{
						$("#redeem_tkt_bakend_user_errors_msg").text(response.error_message).show().fadeOut(5500);
					}
						redeem_failure(cpn_code);
				}
			}
		});	
	}
	else{
		$(".promocode_error").text($("#enter_code").val()).show();
	}


})

function redeem_failure(cpn_code) {
  var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
	var user_region = $(".user_region").val();
	smartech('dispatch', 'Redeem_Ticket', {
    'user_id': getShemarooCookies().user_analytical_id,
    'status': "failure",
    'ticket_name': cpn_code,
    'ticket_value': 100,
    'COUNTRY':user_region,
    "user_state": user_state
  });
    
  Apxor.logEvent("Redeem_Ticket", {
    'user_id': getShemarooCookies().user_analytical_id,
    'status': "failure",
    'ticket_name': cpn_code,
    "user_state": user_state
   }, "REDEEM_TICKET");


 	branch.logEvent(
  	"Redeem_Ticket",
    {'user_id': getShemarooCookies().user_analytical_id,
    'status': "failure",
    'ticket_name': cpn_code,
    'COUNTRY':user_region,
    "user_state": user_state
  });

}



function set_tvod_plan_data(){
 var  keysToRemove = ["first_purchase_plan_selected_plan_ids","first_purchase_plan_price","first_purchase_plans_details","first_purchase_plan_currency","tvod_plan_title"]
  keysToRemove.forEach(k => localStorage.removeItem(k))
 var curncy = $("#tvd_curncy").val();
 var plan_id = $("#tvd_plan_id").val();
 var price = $("#tvd_plan_prc").val();
 var plan_info = $("#tvd_plan_info").val();
 var tvod_title = $("#tvd_pln_tit").val();
 localStorage.setItem("first_purchase_plan_selected_plan_ids",plan_id);
 localStorage.setItem("first_purchase_plan_price",price);
 localStorage.setItem("first_purchase_plans_details",plan_info);
 localStorage.setItem("first_purchase_plan_currency",curncy)
 localStorage.setItem("tvod_plan_title",tvod_title)
}

$("#buy_movie,#tvod_subscribe").click(function(){
	if(getShemarooCookies().user_id){
	 set_tvod_plan_data()
	 var url = window.location.pathname;
	 var  keysToRemove = ["mv_cnt_type","mv_tvod_url","mv_rel_dt","mv_dur","mv_pre_rel","mv_valid_till","mv_title"]
     keysToRemove.forEach(k => localStorage.removeItem(k))
	 localStorage.setItem("mv_cnt_type","TVOD")
	 localStorage.setItem("mv_tvod_url",url)
	 localStorage.setItem("mv_rel_dt",$("#tvod_mv_rel_dt").val())
	 localStorage.setItem("mv_dur",$("#tvod_mv_dur").val())
	 localStorage.setItem("mv_pre_rel",$("#pre_rel_chk").val())
	 localStorage.setItem("mv_valid_till",$("#tvod_mv_valid_till").val())
	 localStorage.setItem("mv_title",$("#tvod_mv_title").val());
	 window.location =  get_lng_web_url("/plans/plans_summary")
	}
	else{
	 localStorage.setItem("tvod_loc","true")
	 $("#tvod_login_pop").modal("show");
	}
	
})

$("#cancel_login_tvod").click(function(){
	$("#tvod_login_pop").modal("hide");
})


$("#can_login").click(function(){
  $("#tvod_login_pop").modal("hide")
  //$("#fb_login_pop").modal("show")
  $("#fb_login_pop").modal();
});


$("#profile_setting_pop,#profile_earn_pop").click(function(){
  if(!getShemarooCookies().user_id){
		if($(this).attr('id')=== "profile_setting_pop"){
			localStorage.setItem("check_setting_or_referal","profile_setting_pop")
		}else if($(this).attr('id')=== "profile_earn_pop"){
			localStorage.setItem("check_setting_or_referal","profile_earn_pop")
		}
    $("#profile_pop").modal("show");      
  }
});

$("#cancel_profile_login").click(function(){
  $("#profile_pop").modal("hide");
})


$("#redm_tkt").click(function(){
	if(getShemarooCookies().user_id){
		var url = window.location.pathname;
		 localStorage.removeItem("mv_cnt_type")
		 localStorage.removeItem("mv_tvod_url")
		 localStorage.setItem("mv_cnt_type","TVOD")
		 localStorage.setItem("mv_tvod_url",url);
     window.location = get_lng_web_url("/users/redeem_ticket")
	}
	else{
	    localStorage.setItem("tvod_loc","true")
		$("#tvod_pop_txt").text($("#redeem_login").val())
	  $("#tvod_login_pop").modal("show");
	}
})


$("#promo_code").focusin(function(){
    $(".promocode_error").hide();
  });


/*  Start of 1  lakh offer page */
$(".cntst_url").click(function(){
 if(getShemarooCookies().user_id){
 	$.ajax({
		url: "/participate",
		type: "POST",
		data: {user_id: getShemarooCookies().user_id},
		success: function(response){
			console.log(response);
			console.log(response.feedback_check)
			if(response.status){
				if(response.feedback_check){
				  window.location = "/contest_questions"
				}
				else if(response.yr_pln_st == false){
                  $("#yr_pk_pop").modal("show");
				}
				else if(response.mnt_pln_st){
                  $("#mnth_pk_pop").modal("show");
				}
				else{
				 window.location = "/plans"
				}
			}
		}
	})
   //$(".cntst_url").attr("href","/contest")
 }
 else{
 	$("#fb_login_pop").modal("show");
 }
})



var x = 0;
$(".frst_qus").click(function(){
	var ans = $(this).attr("id");
	if(ans == "amitabh_bachchan"){
		if(x == 0) {
		$(".frst_qus").css({"background-color": "#FFC83A", "color": "#000"})
		$(this).css({"background-color": "#4F8301", "color": "#fff"});
		}
	 else {
		$(this).css({"background-color": "#EE2A00", "color": "#fff"});	
	}
	}
	else{
	 $("#amitabh_bachchan").css({"background-color": "#FFC83A", "color": "#000"});
     $("#wrng_ans").modal("show");
	}
	
});
$(".scnd_qus").click(function(){
	var ans = $(this).attr("id");
	if(ans == "aditya"){
		if(x == 0) {
			$(".scnd_qus").css({"background-color": "#FFC83A", "color": "#000"})
			$(this).css({"background-color": "#4F8301", "color": "#fff"});
		}
		else {
			$(this).css({"background-color": "#EE2A00", "color": "#fff"});	
		}
	}
	else{
    $("#aditya").css({"background-color": "#FFC83A", "color": "#000"});
    $("#wrng_ans").modal("show");
	}
});
$("#ok_cns_btn").click(function(){
	$("#wrng_ans").modal("hide");
})

$("#cnts_sub").click(function(){
 if($("#amitabh_bachchan").css("background-color") == "rgb(79, 131, 1)" && $("#aditya").css("background-color") == "rgb(79, 131, 1)"){
   $("#cnts_sub").text("Submit...")
   $.ajax({
		url: "/contest_questions",
		type: "POST",
		data: {user_id: getShemarooCookies().user_analytical_id},
		success: function(response){
			if(response.status){
			  $("#sucs_parct").modal("show");
	    }
     }
   })
 }
 else{
 	$("#sel_ans").modal("show");
 }
})

$("#sel_an_ok").click(function(){
	$("#sel_ans").modal("hide");
})

$("#can_mnth").click(function(){
	$("#mnth_pk_pop").modal("hide");
})


function send_apxor_plans_info(dt){
	var user_state = $("#user_state").val();
 	if(parseInt(dt.plans_count) > 0)
 		if(parseInt(dt.plans_count) == 1){
 			Apxor.setUserProperties({
			"user_id": getShemarooCookies().user_analytic_id,
			"plan_1_subscription_status":  dt.plan_1_subscription_status,
			"plan_name_1":   dt.plan_name_1,
			"plan_duration_1": dt.plan_duration_1,
			"plan_start_date_1": dt.plan_start_date_1,
			"plan_end_date_1": dt.plan_end_date_1,
			"payment_mode_1": dt.payment_mode_1,
			"plan_name_1_coupon_code": dt.plan_name_1_coupon_code,
			"plan_1_autorenewal_count": dt.plan_1_autorenewal_count,
			"user_language": getShemarooCookies().shm_sel_lang,
			"user_type": dt.user_type,
			"user_state": user_state,
			"registration_status": dt.user_type
	    })
 		}
 		else if(parseInt(dt.plans_count) == 2){
 			Apxor.setUserProperties({
			"user_id": getShemarooCookies().user_analytic_id,
			"plan_1_subscription_status":  dt.plan_1_subscription_status,
			"plan_name_1":   dt.plan_name_1,
			"plan_duration_1": dt.plan_duration_1,
			"plan_start_date_1": dt.plan_start_date_1,
			"plan_end_date_1": dt.plan_end_date_1,
			"payment_mode_1": dt.payment_mode_1,
			"plan_name_1_coupon_code": dt.plan_name_1_coupon_code,
			"plan_1_autorenewal_count": dt.plan_1_autorenewal_count,
			"plan_2_subscription_status":  dt.plan_2_subscription_status,
			"plan_name_2":   dt.plan_name_2,
			"plan_duration_2": dt.plan_duration_2,
			"plan_start_date_2": dt.plan_start_date_2,
			"plan_end_date_2": dt.plan_end_date_2,
			"payment_mode_2": dt.payment_mode_2,
			"plan_name_2_coupon_code": dt.plan_name_2_coupon_code,
			"plan_2_autorenewal_count": dt.plan_2_autorenewal_count,
			"user_language": getShemarooCookies().shm_sel_lang,
			"user_type": dt.user_type,
			"user_state": user_state,
			"registration_status": dt.user_type
	    })
 		}
 }

 /*Apxor User profile & plans setting*/

/* End  of 1  lakh offer page */


/*Private screening code starts here*/

$("#red_login,#non_log_usr,#pv_redm_tkt").click(function(){
  pv_signout();
  $("#rdm_user_login").modal("show");
})

$("#red_can").click(function(){
  $("#rdm_user_login").modal("hide");
})

function set_prv_scrn_cookies(resp){
	var date = new Date();
  var exp_dt = date.setTime(date.getTime() + 24 * 60 * 60 * 1000);
  $.cookie('shm_pvs_user_id',resp.user_id, { expires: exp_dt,path: '/'});
	$.cookie('shm_pvs_user_login_id',resp.login_id, { expires: exp_dt,path: '/'});
	$.cookie('shm_pvs_user_name',resp.user_name, { expires: exp_dt,path: '/'});
	$.cookie('shm_pvs_profile_id',resp.profile_id, { expires: exp_dt,path: '/'});
	$.cookie('shm_pvs_user_analytical_id',resp.user_analytic_id, { expires: exp_dt,path: '/'});
}

$("#red_logout").click(function(){
	pv_signout();
})

function pv_signout(){
 //$().text("Logout...")
	$.ajax({
    url: "/users/sign_out",
    type: "POST",
    data: {},
    success: function(response,status){
	    if(response.status == true){ 
	    	rm_pvt_scn_cookies()
	      //window.location.reload();
	    }
    }
	});
}

function rm_pvt_scn_cookies(){
	var dt = ["shm_pvs_user_id","shm_pvs_user_login_id","shm_pvs_user_name","shm_pvs_profile_id","shm_pvs_user_analytical_id"]
	for(i=0;i<dt.length;i++){
	 document.cookie = dt[i] + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	}
}

$("#prv_c_code").focusin(function(){
	$("#prv_cupn_error").hide();
});

$("#red_confirm").click(function(){
	var c_code = $("#prv_c_code").val();
	if(c_code.trim().length == 0){
     $("#prv_cupn_error").text("This field is mandatory").show();
	}
	else{
	  $(this).text("Proceed...")
		var browser_details = browser_name();
	  $.ajax({
		url: "/users/prv_redem_tket",
		type: "POST",
		data: {coupon_code: c_code,browser_det:  browser_details},
		success: function(resp){
			if(resp.status == "true"){
				set_prv_scrn_cookies(resp)
				if(resp.ex_pk_st == "yes"){
          window.location.reload();
				}
				else{
          window.location = "/payment/tvod_success?order_id="+resp.order_id+"&msg="+resp.message+"&pln_tit="+resp.title+"&exp_dt="+resp.exp_dt+"&prv_sc=true"
				}	
	    }
	    else{
	    	$("#prv_cupn_error").text("Invalid coupon code").show();
	    	$("#red_confirm").text("Proceed")
	    }
	  }
	 })
	}
})


$("#accen_user_register").click(function(){
  accentive_user_register();
});


function accentive_user_register(){
	  var cpn_st = $("#acntv_cupn_st").val();
	  if(cpn_st == "false"){
	  	actv_cupn_aply();
	   }
 		$("#signup_cons_gen_user_name_error,#signup_cons_gen_user_email_error,#signup_cons_gen_password_error,#signup_cons_gen_confirm_password_error,#cons_gen_agree_terms, #signup_cons_gen_coupon_code_error, #cons_gen_signup_user_promo_error").hide();
	 	var user_name = $("#cons_gen_user_name").val();
	 	var email_id = $("#cons_gen_user_email").val().toLowerCase();
	 	var pwd = $("#cons_gen_password").val();
	 	var cf_pwd = $("#cons_gen_confirm_password").val();
	 	var terms_check = $("#cons_gen_agree_terms").is(':checked')
	 	var status = false
	 	var coupon_check = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var cons_gen_coupon_code = $("#cons_gen_coupon_code").val()
	 	var url = window.location.pathname.split("/")[1]
       
    if(user_name.trim().length != 0 && email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd) && terms_check == true && cpn_st == "true"){
      status = true
      signup_type = "email"
 	  }
	 	if(status == true){
	 		$(".loader").show();
			$("#accen_user_register").text("Register now..")
			$.ajax({
				url: "/users/coupon_register",
				type: "POST",
				data: { 
				name: user_name, 
				email_id: email_id,
			    type: signup_type,
				password: pwd,
				browser_det: browser_name(),
				coupon: $("#cons_gen_coupon_code").val(),
				pln_type: $("#acntv_cupn_type").val(),
				//coupon: "INDIANCONSULATENY",
				coupon_type: "accentive"
				},
				success: function(response,status){
					$(".loader").hide();
				 console.log(response);
				 if(response.status == true){
                  var region = $("#con_user_region").val();				 	
				   $("#cons_gen_user_register").text("Register now")
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	$("#cons_gen_user_name,#cons_gen_user_email,#cons_gen_password,#cons_gen_confirm_password").val("");
                   
             $("#acntv_suc_pop").modal("show");
             set_user_cookies(response);
               
				 }
				else{
				 $(".loader").hide();
				 $("#accen_user_register").text("Register now")
				 $("#signup_cons_gen_resp_error_msg").text(response.error_message).show()
				 $('html,body').animate({ scrollTop: 300}, 'slow');
				 }
				}
			});
		}
		else if(cpn_st == "false"){
		  $("#cons_gen_signup_user_promo_error").text("please enter the coupon code").show();

		}
		else if(user_name.trim().length == 0){
		  $("#signup_cons_gen_user_name_error").show();
		}
		else if( email_id.length == 0){
			$("#signup_cons_gen_user_email_error").show();			
		}
		else if( !regex_email.test(email_id)){
		  $("#signup_cons_gen_user_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#signup_cons_gen_password_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#signup_cons_gen_password_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#signup_cons_gen_confirm_password_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#signup_cons_gen_confirm_password_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	      $("#signup_cons_gen_confirm_password_error").text("Passwords do not match").show();
		}
		else if(terms_check == false){
		  $("#cons_gen_agree_terms").show();
		}
		/*else if(cons_gen_coupon_code.length != 0 && $('#offers_apply_promo_code').is(':visible')){
			localStorage.removeItem("promo_apply")
			localStorage.setItem("promo_apply", "true")
			offer_promo_code_apply()
		}*/
  }


$("#cons_gen_apply_promo_code").click(function(){
	actv_cupn_aply();
});

function actv_cupn_aply(){
	$("#cons_gen_signup_user_promo_error").hide();
	$("#acntv_cupn_st").val("false");
	var acen_code = $("#cons_gen_coupon_code").val()
	if(acen_code.trim().length != 0){
		$("#cons_gen_apply_promo_code").text("apply...")
		$.ajax({
			url: "/users/acntv_coupon",
			type: "POST",
			data: {"acntv_cupn": acen_code},
			success: function(response,status){
				$("#cons_gen_apply_promo_code").text("apply")
				if(response.status == "true"){  
					$("#acntv_cupn_type").val(response.cupn_type)
					$("#acntv_cupn_st").val("true");
					$("#cons_gen_signup_user_promo_error").text("coupon applied successfully").show().fadeOut(4500);
				}
				else{
					$("#acntv_cupn_st").val("false");
					$("#homeis_gen_apply_promo_code").text("apply")
					$("#cons_gen_signup_user_promo_error").text(response.error_message).show();
				}
		  }
		});
	}
	 else if(acen_code.trim().length == 0){
		  $("#cons_gen_signup_user_promo_error").text("please enter the coupon code").show();
		}
}

/* Refferal Implementation */
function send_brn_apxr_ref_start(ref_code){
	var plt_frm = getShemarooCookies().mobile_browser_type 
	var user_state = $.cookie('user_actv_sub_status') ? $.cookie('user_actv_sub_status').toLowerCase() : "registered"
	if(plt_frm ==  undefined){
		plt_frm = "web"
	}
	Apxor.logEvent("Referral_Start",{
	"user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"generate_code": "yes",
	"user_state": user_state
	}, "REFERRAL");
	branch.logEvent("Referral_Start",{
	"user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"generate_code": "yes",
	"user_state": user_state
	},
	function(err) { console.log(err); });
    smartech('dispatch', 'Referral_Start', {
    "user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"generate_code": "yes",
	"user_state": user_state
    });
}

$(".ref_shr").click(function(){
	var det =  $(this).attr("id").split("$");
	var plt_frm = getShemarooCookies().mobile_browser_type 
	if(plt_frm ==  undefined){
		plt_frm = "web"
	}
	Apxor.logEvent("Referral_Share",{
	"user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"share_code": "yes",
	"share_method": det[0],
	"user_state": $.cookie('user_actv_sub_status').toLowerCase()
  }, "REFERRAL");
	branch.logEvent("Referral_Share",{
	"user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"share_code": "yes",
	"share_method": det[0],
	"user_state": $.cookie('user_actv_sub_status').toLowerCase()
  },
	function(err) { console.log(err); });
	smartech('dispatch', 'Referral_Share', {
    "user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"share_code": "yes",
	"share_method": det[0],
	"user_state": $.cookie('user_actv_sub_status').toLowerCase()
    });
})

$("#user_earnings").click(function(){
 var plt_frm = getShemarooCookies().mobile_browser_type 
	if(plt_frm ==  undefined){
		plt_frm = "web"
	}
	Apxor.logEvent("Earnings",{
	"user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"user_state": $.cookie('user_actv_sub_status').toLowerCase()
   }, "REFERRAL");
	branch.logEvent("Earnings",{
	"user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"user_state": $.cookie('user_actv_sub_status').toLowerCase()
  },
	function(err) { console.log(err); });
	smartech('dispatch', 'Earnings', {
     "user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"user_state": $.cookie('user_actv_sub_status').toLowerCase()
    });
});

function send_brn_apxr_bank_add(){
 var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status').toLowerCase() : "Registered"
 var plt_frm = getShemarooCookies().mobile_browser_type 
	if(plt_frm ==  undefined){
		plt_frm = "web"
	}
	Apxor.logEvent("Bank_Details",{
	"user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"added": "yes",
	"user_state": user_state
   }, "REFERRAL");
	branch.logEvent("Bank_Details",{
	"user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"added": "yes",
	"user_state": user_state
  },
	function(err) { console.log(err); });
   smartech('dispatch', 'Bank_Details', {
    "user_id": getShemarooCookies().user_analytical_id,
	"platform": plt_frm,
	"added": "yes",
	"user_state": user_state
    });
 }


/*OTM  code starts here*/

 function get_query_dt(){
    var queries = {};
    $.each(document.location.search.substr(1).split('&'),function(c,q){
     var i = q.split('=');
     queries[i[0].toString()] = i[1].toString();
    });
   return queries
   }

	$("#otm_verfy").click(function(){
    var price = ""
		if(localStorage.getItem("first_purchase_plans_details")){
         price =  localStorage.getItem("first_purchase_plans_details").split(",").map(function(t){ return t.split("|")[6] })[0]
		    var typ_pln  = localStorage.getItem("first_purchase_plans_details").split(",")[0].split("|")[3].toLocaleLowerCase()
		   if(typ_pln == "monthly"){
		   	price = "8"
		   }
		}
		else{
         price =  localStorage.getItem("new_plans_details").split(",").map(function(t){ return t.split("|")[6] })[0]
		     var typ_pln  = localStorage.getItem("new_plans_details").split(",")[0].split("|")[3].toLocaleLowerCase()
		    if(typ_pln == "monthly"){
		   	 price = "8"
		    }
		}
		var tr_id = window.location.search.split("&")[2].split("tr_id=")[1]
	 	$("#otp_verify_error").hide();
	 	var all_dt = get_query_dt()
	 	var fr_otm = $("#otm_first_digit").val();
	 	var secnd_otm = $("#otm_second_digit").val();
	 	var thrd_otm = $("#otm_third_digit").val();
	 	var four_otm = $("#otm_fourth_digit").val();
	 	var fifth_otm = $("#otm_fifth_digit").val();
	 	var sixth_otm = $("#otm_sixth_digit").val();
	  var otp_otm = $("#otm_first_digit").val()+$("#otm_second_digit").val()+$("#otm_third_digit").val()+$("#otm_fourth_digit").val()+$("#otm_fifth_digit").val()+$("#otm_sixth_digit").val()
	 	all_dt["otm_otp"] =  otp_otm
	 	if(fr_otm.length != 0 && secnd_otm.length != 0 && thrd_otm.length != 0 && four_otm.length != 0 && fifth_otm.length != 0 && sixth_otm.length != 0){
	 		$("#otm_verfy").text("Verify...")
			$.ajax({
				url: "/plans/otm_otp_valid",
				type: "POST",
				data: all_dt,
				async: false,
				success: function(response,status){
					$("#otm_verfy").text("Verify");
					console.log(response);
					if(response.status == "true"){
						window.location = "/payment/unifi_response?order_id="+tr_id+"&payment_gateway=unifi&status="+response.trans_status+"&price="+price+"&title="+response.plan_title+"&pln_exp_dt="+response.pln_exp_dt
					}
					else{
					 $("#otp_verify_error").text(response.mesg).show();
					}
			  }
			});
		}
		else if(fr_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(secnd_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(thrd_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(four_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(fifth_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(sixth_otm.length == 0){
		  $("#otp_verify_error").show();
		}
	})

$("#ref_sub").click(function(e){
	$(".error").hide();
	var f_name = $("#ref_first_name").val();
	var l_name = $("#ref_last_name").val();
	var email_id = $("#ref_email").val();
	var phone_no = $("#ref_mobile_no").val();
	var terms_check = $("#get_start_agree_terms").is(':checked')
	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	//checking if the enter val is a number:
    res = isNumber(phone_no)
	if(res == false){
		 phone_no = '';
		$("#ref_mobile_no").val('');
	}
	if(f_name.trim().length != 0 && l_name.trim().length != 0 &&  f_name.length >= 2 && l_name.length >= 2 && email_id.trim().length != 0 && phone_no.trim().length != 0 && phone_no.trim().length == 10 && terms_check==true){
		$("#ref_sub").text($("#generate_code").val())
		$.ajax({
			url: "/users/referral",
			type: "POST",
			data: {"first_name": f_name,"last_name": l_name,"email": email_id,"phone": phone_no},
			success: function(response,status){
				send_brn_apxr_ref_start(response.ref_code)
				if(response.status == "true"){  
          window.location  = get_lng_web_url("/users/referral_code?name="+response.u_name+"&ref_code="+response.ref_code)
				}
				else{
				}
		  }
		});
	}
	else if(terms_check == false){
		$("#term_n_cond").text($("#accept_terms").val()).show();
	}
	else if(f_name.trim().length == 0){
   $("#fname_error").text($("#first_name_blank").val()).show();
	}
	else if(f_name.trim().length != 0 && f_name.trim().length < 2){
   $("#fname_error").text($("#first_name_length").val()).show();
	}
	else if(l_name.trim().length == 0){
		$("#lname_error").text($("#last_name_blank").val()).show();
	}
	else if(l_name.trim().length != 0 && l_name.trim().length < 2){
   $("#lname_error").text($("#first_name_length").val()).show();
	}
	else if(email_id.trim().length == 0){
		$("#email_error").text($("#email_blank").val()).show();
	}
	else if(email_id.trim().length != 0 &&  !regex_email.test(email_id)){
		$("#email_error").text("Please enter the valid email address").show();
	}
	else if(phone_no.trim().length == 0){
		$("#phone_error").text($("#mobile_blank").val()).show();
	}
	else if(phone_no.trim().length > 0 && phone_no.length != 10){
		$("#phone_error").text($("#mobile_length").val()).show();
	}
})

function isNumber(n) { return /^-?[\d.]+(?:e-?\d+)?$/.test(n); } 



$("#bnk_add").click(function(){
	$(".error").hide();
	var acnt_no = $("#acc_no").val();
	var re_acnt_no = $("#re_acc_no").val();
	var ifsc = $("#ifsc").val();
	var rec_name = $("#recipient_name").val();
	var pan_url = $("#pan_url").val();
	var txn_chk = $("#tax_disclaimer_check").is(':checked')
	var file_url = $("#s3_file_name").val();
	if(acnt_no.trim().length != 0 && re_acnt_no.trim().length != 0 && acnt_no == re_acnt_no && ifsc.trim().length != 0 && rec_name.trim().length != 0 && acnt_no.length >= 6 && re_acnt_no.length >= 6 && file_url.length != 0 && pan_url.length != 0 &&  txn_chk == true){
		$("#bank_add").text("ADD...");
		send_brn_apxr_bank_add()
		$.ajax({
			url: "/users/add_bank_account",
			type: "POST",
			data: {"account_no": acnt_no,"ifsc_code": ifsc,"recp_name": rec_name,"pan_url": $("#pan_url").val()},
			success: function(response,status){
				if(response.status == "true"){  
          $("#bnk_success_pop").modal("show");
         //window.location = "/users/my_earnings"
				}
				else{
				}
		  }
		});
	}
	else if(txn_chk == false){
		$("#txn_error").text("Please accept the taxation disclaimer").show();
	}
	else if(acnt_no.trim().length == 0){
	 $("#acc_no_error").text("Account number can't be blank").show();
	}
	else if(acnt_no.trim().length < 6 ){
	 $("#acc_no_error").text("Account number minimum should be 6 digits").show();
	}
	else if(re_acnt_no.trim().length == 0){
	 $("#re_acc_no_error").text("Re-enter Account number can't be blank").show();
	}
	else if(re_acnt_no.trim().length < 6){
	 $("#re_acc_no_error").text("Re-enter Account minimum should be 6 digits").show();
	}
	else if(acnt_no != re_acnt_no ){
	 $("#re_acc_no_error").text("Account no and Re-enter account number are not matching").show();
	}
	else if(ifsc.trim().length == 0){
		$("#ifsc_error").text("IFSC code can't be blank").show();
	}
	else if(rec_name.trim().length == 0){
	   $("#recpt_error").text("Recipient name can't be blank").show();
	}
	else if(pan_url.length == 0){
		$("#pan_error").text("Please upload the pan card").show();
	}
})

$("#ref_first_name").focusin(function(){
	$("#fname_error").hide();
})
$("#ref_last_name").focusin(function(){
	$("#lname_error").hide();
})
$("#ref_email").focusin(function(){
	$("#email_error").hide();
})
$("#ref_mobile_no").focusin(function(){
	$("#phone_error").hide();
})
$("#tax_disclaimer_check").change(function(){
	$(".error").hide();
})


$("#acc_no").focusin(function(){
	$("#acc_no_error").hide();
})
$("#re_acc_no").focusin(function(){
	$("#re_acc_no_error").hide();
})
$("#ifsc").focusin(function(){
	$("#ifsc_error").hide();
})
$("#recipient_name").focusin(function(){
	$("#recpt_error").hide();
})

$("#next_pg").click(function(){
	var pg_st = $("#next_pg_st").val();
	if(pg_st == "true"){
		$("#pre_pg").css("cursor","pointer");
		$("#prev_pg_st").val("true");
		$(".loader").show();
		var cur_pg = parseInt($("#cur_page").val());
		var next_sl = cur_pg+1+5;
		$.ajax({
			url: "/users/my_earnings",
			type: "POST",
			data: {page_no: cur_pg+1},
			success: function(response,status){
				console.log(response)
				if(response.status == "true"){  
					$("#earnings_tab tbody").html("");
					$("#cur_page").val(cur_pg+1)
					var list_data = ""
					var ref_ear = response.earnings_dt['user_referrals']
					if(ref_ear.length > 0){
						for(i=0;i<ref_ear.length;i++){
							list_data+= '<tr><td class="shemaroo-color9 font4">'+(parseInt(next_sl)+i)+'.</td><td class="shemaroo-color9 font4">'+ref_ear[i]['created_at'].split("T")[0]+'</td><td class="shemaroo-color9 font4">'+ref_ear[i]['user_details']+'</td><td class="shemaroo-color9 font4">'+ref_ear[i]['plan_title']+'</td><td class="shemaroo-color9 font4">'+ref_ear[i]['plan_type']+'</td><td class="shemaroo-color9 font4">'+response.cur+' '+ref_ear[i]['referral_amount']+'</td></tr>'
						}
						list_data = list_data+'<tr><td class="shemaroo-color9 font-rcb font4" align="right"colspan="6">Total Earnings&nbsp;&nbsp;&nbsp;<span class="total_price">'+response.cur+'  '+response.ref_amt+'</span></td></tr>'
						$("#earnings_tab tbody").append(list_data)
					}
					if(response.next_pg_st == "false"){
						$("#next_pg_st").val("false")
						$("#next_pg").css("cursor","default");
					}
					$("#pre_pg").show();
					$(".loader").hide();
				}
			}
		});
	}
})

$("#pre_pg").click(function(){
	var pg_st = $("#prev_pg_st").val();
	if(pg_st == "true"){
		$(".loader").show();
		var cur_pg = parseInt($("#cur_page").val());
		$.ajax({
			url: "/users/my_earnings",
			type: "POST",
			data: {page_no: cur_pg-1},
			success: function(response,status){
				console.log(response)
				if(response.status == "true"){  
				$("#earnings_tab tbody").html("");
				$("#cur_page").val(cur_pg-1)
				var list_data = ""
				var ref_ear = response.earnings_dt['user_referrals']
				if(ref_ear.length > 0){
					for(i=0;i<ref_ear.length;i++){
						list_data+= '<tr><td class="shemaroo-color9 font4">'+(parseInt(i)+1)+'.</td><td class="shemaroo-color9 font4">'+ref_ear[i]['created_at'].split("T")[0]+'</td><td class="shemaroo-color9 font4">'+ref_ear[i]['user_details']+'</td><td class="shemaroo-color9 font4">'+ref_ear[i]['plan_title']+'</td><td class="shemaroo-color9 font4">'+ref_ear[i]['plan_type']+'</td><td class="shemaroo-color9 font4">'+response.cur+' '+ref_ear[i]['referral_amount']+'</td></tr>'
					}
					list_data = list_data+'<tr><td class="shemaroo-color9 font-rcb font4" align="right"colspan="6">Total Earnings&nbsp;&nbsp;&nbsp;<span class="total_price">'+response.cur+'  '+response.ref_amt+'</span></td></tr>'
					$("#earnings_tab tbody").append(list_data)
				}
				if(response.next_pg_st == "true"){
					$("#next_pg_st").val("true")
					$("#next_pg").css("cursor","pointer");
				}
				$(".loader").hide();
				}
			}
		});
		if(cur_pg == 1){
			$("#pre_pg").css("cursor","default");
			$("#prev_pg_st").val("false")
		}
	}
})

function call_claim_post(){
  var u_name = $("#ref_usr_name").val();
  $("#claim_btn").text("CLAIM NOW...")
	$.ajax({
			url: "/users/claim_earnings",
			type: "POST",
			data: {},
			success: function(response,status){
				$("#claim_btn").text("CLAIM NOW")
				if(response.status == "true"){
					$("#claim_suc_pop").modal("show");
				 // $("#clm_txt").text("Congratulations "+u_name+" your withdrawal request is placed successfully. Note: Your request will be processed by the end of the month. It may take up to 12 days for the reward money to reflect in your bank account.");
	 	      //$("#claim_pop").modal("show");
				}
		  }
		});
}


$("#claim_btn").click(function(){
 var bnk_st = $("#usr_bnk_st").val();
 if(bnk_st == "false"){
 	var u_reg =  $(".user_region").val();
 	var pm_msg = "Please update your bank details"
 	if(u_reg == "US"){
    pm_msg = "Please update your paypal details"
 	}
 	$("#clm_txt").text(pm_msg);
 	$("#claim_pop").modal("show");
 }
 else if(bnk_st == "true"){
  var redem_amt = parseFloat($("#user_red_amt").val());
  var u_name = $("#ref_usr_name").val();
  var u_reg = $(".user_region").val();
  if(u_reg == "IN" && redem_amt > 100){
  	call_claim_post()
  }
  else if(u_reg == "US" && redem_amt > 20){
  	call_claim_post()
  }
  else{
  	var amt = "INR 100"
  	if(u_reg == "US"){
      amt = "USD 20"
  	}
    $("#clm_txt").text("Sorry! A user must earn a minimum of "+amt+" as reward money to be able to raise a claim money request.");
 	  $("#claim_pop").modal("show");
  }
 }
})

$("#clm_cls").click(function(){
	//$("#claim_pop").modal("hide");
})

/*OTM  code starts here*/

 function get_query_dt(){
    var queries = {};
    $.each(document.location.search.substr(1).split('&'),function(c,q){
     var i = q.split('=');
     queries[i[0].toString()] = i[1].toString();
    });
   return queries
   }


$("#ad_paypal").click(function(){
	$("#pypl_error").hide();
	var py_id = $("#paypal_id").val()
	if(py_id.trim().length != 0 && py_id.length >= 17){
		$("#ad_paypal").text("ADD...");
		send_brn_apxr_bank_add()
		$.ajax({
			url: "/users/add_bank_account",
			type: "POST",
			data: {"paypal_id": py_id},
			success: function(response,status){
				$("#ad_paypal").text("ADD");
				if(response.status == "true"){  
          $("#pypl_su_pop").modal("show");
				}
		  }
		});
	}
	else if(py_id.trim().length == 0){
   $("#pypl_error").text("Please enter the PayPal Id").show();
	}
	else if(py_id.trim().length < 17){
   $("#pypl_error").text("Please enter 17 digits of PayPal Id").show();
	}
})

$("#paypal_id").focusin(function(){
	$("#pypl_error").hide();
})

$("#bnk_cnt").click(function(){
	/*$("#pypl_su_pop").modal("hide");
	$("#us_bnk_sec").hide();*/
})
/* Refferal Implementation */

	$("#otm_verfy").click(function(){
    var price = ""
		if(localStorage.getItem("first_purchase_plans_details")){
    price =  localStorage.getItem("first_purchase_plans_details").split(",").map(function(t){ return t.split("|")[6] })[0]
		}
		else{
     price =  localStorage.getItem("new_plans_details").split(",").map(function(t){ return t.split("|")[6] })[0]
		}
		var tr_id = window.location.search.split("&")[2].split("tr_id=")[1]
	 	$("#otp_verify_error").hide();
	 	var all_dt = get_query_dt()
	 	var fr_otm = $("#otm_first_digit").val();
	 	var secnd_otm = $("#otm_second_digit").val();
	 	var thrd_otm = $("#otm_third_digit").val();
	 	var four_otm = $("#otm_fourth_digit").val();
	 	var fifth_otm = $("#otm_fifth_digit").val();
	 	var sixth_otm = $("#otm_sixth_digit").val();
	  var otp_otm = $("#otm_first_digit").val()+$("#otm_second_digit").val()+$("#otm_third_digit").val()+$("#otm_fourth_digit").val()+$("#otm_fifth_digit").val()+$("#otm_sixth_digit").val()
	 	all_dt["otm_otp"] =  otp_otm
	 	if(fr_otm.length != 0 && secnd_otm.length != 0 && thrd_otm.length != 0 && four_otm.length != 0 && fifth_otm.length != 0 && sixth_otm.length != 0){
	 		$("#otm_verfy").text("Verify...")
			$.ajax({
				url: "/plans/otm_otp_valid",
				type: "POST",
				data: all_dt,
				async: false,
				success: function(response,status){
					$("#otm_verfy").text("Verify");
					console.log(response);
					if(response.status == "true"){
						window.location = "/payment/unifi_response?order_id="+tr_id+"&payment_gateway=unifi&status="+response.trans_status+"&price="+price+"&title="+response.plan_title+"&pln_exp_dt="+response.pln_exp_dt
					}
					else{
					 $("#otp_verify_error").text(response.mesg).show();
					}
			  }
			});
		}
		else if(fr_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(secnd_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(thrd_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(four_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(fifth_otm.length == 0){
		  $("#otp_verify_error").show();
		}
		else if(sixth_otm.length == 0){
		  $("#otp_verify_error").show();
		}
	})


	$("#otm_resnd").click(function(){
	 $(this).text("Resend...")
   var url = window.location.search.split("&")
   var prod_id = url[3].split("pr_id=")[1];
   var tr_id = url[2].split("tr_id=")[1];
   var user_name =  url[1].split("u_name=")[1]
   $.ajax({
			url: "/plans/otm_resnd_otp",
			type: "POST",
			data: {u_name: user_name,pr_id: prod_id,trans_id:  tr_id},
			success: function(response,status){
				$("#otm_resnd").text("Resend");
				console.log(response);
				if(response.status == "true"){
					$("#iv_code").text(response.iv_code);
					$("#resend_otp_msg").show().fadeOut(40000);
				}
				else{
					alert("Sorry something went wrong");
				}
		  }
		});
	})

/*OTM  code end here*/

$("#sub_ans").click(function(){
  var qtns = $("#gmfc_qtns").val().split("|");
  var cnt_name = $("#gmfc_cntst_name").val();
  var anws = []
  for(i=0;i<qtns.length;i++){
   var q_ans = $(".q_"+qtns[i]+":checked").val()
   if(q_ans != undefined){
   	 anws.push(q_ans)
   }
  }
  console.log(anws);
  var cnt_img_url = $("#gmfc_cntst_img_url").val();
  if(qtns.length == anws.length){
  	var win_dt = $("#gmfc_win_dt").val();
		$.ajax({
			url:"/users/gmfc_answers",
			type: "POST",
			data: {"answers": anws,"language": $("#gmfc_lang").val(),"contest_id": $("#gmfc_cntst_id").val(),"slot_id": $("#gmfc_slot_id").val()},
			success: function(response,status){
				send_cntst_sucs_evnt()
				window.location = get_lng_web_url("/users/gmfc_thanks?dt="+win_dt+"&contest_name="+cnt_name+"&contest_img="+cnt_img_url)
      }
		});
   }
  else{
  	$("#gmfc_all_an_chck").modal("show");
  	//alert("Please select the answers")
  }
})

function send_cntst_sucs_evnt(){
	 var user_region = $(".user_region").val();
  var cnt_name = $("#gmfc_cntst_name").val();
  var cnt_id = $("#gmfc_cntst_id").val();
  var user_region = $(".user_region").val()
  var plat_form = "ios"
  var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status').toLowerCase() : "registered"
  if(getShemarooCookies().mobile_browser_type == "android"){
    plat_form = "android"
  }
  Apxor.logEvent('contest_complete', {
    "user_id": getShemarooCookies().user_analytical_id,
    "contest_name": cnt_name,
    "contest_id": cnt_id,
    "platform": plat_form,
    "region": user_region,
    "user_state": user_state
  }, "USER_EVENTS");
  smartech('dispatch', 'contest_complete', {
    "user_id": getShemarooCookies().user_analytical_id,
    "contest_name": cnt_name,
    "contest_id": cnt_id,
    "platform": plat_form,
    "region": user_region ,
    "user_state": user_state
  });
  branch.logEvent('contest_complete',
    { "user_id": getShemarooCookies().user_analytical_id,
      "contest_name": cnt_name,
      "contest_id": cnt_id,
      "platform": plat_form,
      "region": user_region ,
      "user_state": user_state
    },
   function(err) { console.log(err); }
  );
}

$("#gmfc_otp").click(function(){
	$("#gmfc_mob_error").hide();
	var mobile_no = $("#gmfc_mob_no").val();
	var mob_chck = isNumber(mobile_no)
	if(mobile_no.length != 0 && mobile_no.trim().length != 0 && mobile_no.trim().length == 10 && mob_chck == true){
		$("#gmfc_otp").text($("#gmfc_get_otp").val()+"...");
    $.ajax({
			url:"/users/gmfc_user_signup",
			type: "POST",
			data: {"phone_no": mobile_no},
			success: function(response,status){
				$("#gmfc_otp").text($("#gmfc_get_otp").val());
				if(response.status == "true"){
         $("#gmfc_reg").hide();
         var as_phno = mobile_no.substr(6)
         $("#gmfc_phone_no").text("+91XXXXX"+as_phno)
         $("#gmfc_otp_ver").show();
				}
				else{
					$("#gmfc_mob_error").text($("#gmfc_user_exists").val()).show().fadeOut(4500);
          // alert("Sorry something went wrong");
				}
		  }
		});
	}
 else{
   if(mobile_no.trim().length == 0){
   	$("#gmfc_mob_error").text($("#gmfc_mandatory_info").val()).show();
   }
   else if(mobile_no.trim().length != 0 && mobile_no.trim().length != 10){
     $("#gmfc_mob_error").text($("#gmfc_mobile_length").val()).show();
   }
 }
})


$("#gmfc_email_reg").click(function(){
 	var email = $("#gmfc_email").val();
  $.ajax({
    url: "/users/gmfc_user_signup",
    type: "POST",
    data: { 
     email: email 
    },
    success: function(response){
      if(response.status == "true"){
      	window.location = get_lng_web_url("/users/gmfc_contest?email="+$("#gmfc_email").val());
      }
      else{
        $("#gmfc_email_error").text("Invalid Email").show().fadeOut(4500);
      }
    }
  })
});


$("#gmfc_sub_otp").click(function(){
	$("#gmfc_otp_error_msg").hide();
	var gmfc_otp = $("#gmfc_mob_otp").val();
	if(gmfc_otp.trim().length != 0 && gmfc_otp.trim().length == 6){
	 $("#gmfc_sub_otp").text($("#gmfc_submit_otp").val()+"...")
   $.ajax({
			url:"/users/gmfc_otp_verify",
			type: "POST",
			data: {"otp": gmfc_otp},
			success: function(response,status){
				$("#gmfc_sub_otp").text($("#gmfc_submit_otp").val())
				if(response.status == "true"){
					gmfc_user_suc_register_events()
					window.location = get_lng_web_url("/users/gmfc_contest?mobile_no=91"+$("#gmfc_mob_no").val())
				}
				else{
         $("#gmfc_otp_error_msg").text($("#gmfc_invalid_otp").val()).show().fadeOut(4500);
				}
		  }
		});
	}
	else{
		if(gmfc_otp.trim().length == 0){
   	$("#gmfc_otp_error_msg").text($("#gmfc_mandatory_info").val()).show();
   }
   else if(gmfc_otp.trim().length != 0 && gmfc_otp.trim().length != 6){
     $("#gmfc_otp_error_msg").text($("#gmfc_enter_6_otp").val()).show();
   }
	}
})


/*start of SSO Implementation written by kotesh*/

  $(".ss_mobile_sub").click(function(){
		var user_reg = $(".user_region").val()
		var ext_country_code = $("#country_code").val()
		var max_digit = $("#max_phone_digit").val()
		var mob_st = false
		var user_mob = $("#sso_login_mobile_number").val();
		if(user_mob.length != 0 && user_mob.length == max_digit){
			mob_st = true
		}
	  if(mob_st == true){
	  	var phoneNumber = ext_country_code+$("#sso_login_mobile_number").val();
			var appVerifier = window.recaptchaVerifier;
		 var mob_type = $("#mb_lgn_type").val()
		if(mob_type == "netcore"){
      user_mobile_signup(phoneNumber)
    }else{
			firebase.auth().signInWithPhoneNumber(phoneNumber, appVerifier) .then(function (confirmationResult){
				window.confirmationResult = confirmationResult;
				localStorage.setItem("fb_mobile_no",phoneNumber)
				$("#sso_mobile_login_pop").modal("hide");
				var mb_num_val = phoneNumber.substring(3,phoneNumber.length-4)
        $("#user_mobile_no").text(localStorage.getItem("fb_mobile_no").replace(mb_num_val, "******"));
				$("#fb_otp_pop").modal("show");
			}).catch(function (error){
			console.log(error + " error");
			$("#sso_usr_bknd_msg").text(error).show().fadeOut(4500);
			});
		  }
	  }
	  else{
	  	if(user_mob.length == 0){
        $("#sso_usr_mobile_error_msg").text($("#ext_mandatory_info").val()).show().fadeOut(4500);
	  	}
	  	else if(user_mob.length != 0 && user_mob.length != max_digit){
         //$("#sso_usr_mobile_error_msg").text("Please enter your "+max_digit+" digit mobile number").show().fadeOut(4500);
         $("#sso_usr_mobile_error_msg").text($("#ext_mobile_length").val()).show().fadeOut(4500);
	  	}
	  }	
	})

	$("#sso_mob_otp").click(function(){
		$("#sso_mobile_error_msg").hide();
	 var ext_mobile = $("#sso_user_mob_num").val();
	 var mob_st = false;
	 if(ext_mobile.length != 0 && ext_mobile.length == 10){
			mob_st = true
		}
	  if(ext_mobile.trim().length != 0 && mob_st == true){
	 	 $("#sso_mob_otp").text($("#ext_get_otp").val()+"...");
	 	 $.ajax({
				url:"/users/ext_mobile",
				type: "POST",
				data: {"mob_no": ext_mobile },
				success: function(response,status){
				 console.log(response);
				 $("#sso_mob_otp").text($("#ext_get_otp").val());
			   if(response.status == "true"){
			   	$("#ext_user_mob_no").val("91"+ext_mobile)
	      	$("#sso_mobile_msg").text($("#ext_otp_sent").val()).show().fadeOut(4500);
	      	var digit_no = ext_mobile.substr(6)
	        $("#sso_mob_no").text("+91XXXXX "+digit_no)
	        $("#sso_mobile_login_pop2").modal("hide");
	        //$("#sso_mobile_login_otp_pop").modal("show");
	        $("#sso_mobile_login_otp_pop").modal({backdrop: 'static', keyboard: false});
			   }
			   else{
			   	$("#sso_mobile_error_msg").text(response.msg).show().fadeOut(4500);
			   }
			  }
			});
	  }
	  else{
	  	if(ext_mobile.length == 0){
        $("#sso_mobile_error_msg").text($("#ext_mandatory_info").val()).show().fadeOut(4500);
	  	}
	  	else if(ext_mobile.length != 0 && ext_mobile.length != 10){
         $("#sso_mobile_error_msg").text($("#ext_mobile_length").val()).show().fadeOut(4500);
	  	}
	  }

	});


  function registration_success_evnt(ev_name,u_id,method,phone_no,reg){
  	var fing_dt = "";
  	branch.getBrowserFingerprintId(function(err, data) {fing_dt = data});
   $.ajax({
      url:"/users/sub_events",
      type: "POST",
      data: {"evnt_dt": {"event_name": ev_name, "method": method, "phone_number":phone_no, "platform": "web","user_id": u_id,"browser_fingerprint_id": fing_dt, "region": reg}},
      success: function(response){
      }
    });
  }

  function mobileno_update(mob_no){
  	 $.ajax({
      url:"/users/sub_events",
      type: "POST",
      data: {"evnt_dt": {
      "event_name": "phonenumber_update", 
      "phone_number": mob_no, 
      "user_email_id": getShemarooCookies().user_login_id,
      "platform": "web",
      "user_id": getShemarooCookies().user_analytical_id,
      "region": $(".user_region").val()
       }},
      success: function(response){
      }
    });
  	Apxor.logEvent('phonenumber_update', {
			"phone_number": mob_no, 
      "user_email_id": getShemarooCookies().user_login_id,
      "platform": "web",
      "user_id": getShemarooCookies().user_analytical_id,
      "region": $(".user_region").val(),
      "user_state": $.cookie('user_actv_sub_status')
		}, "USER_EVENTS");
		smartech('dispatch', 'phonenumber_update', {
			"phone_number": mob_no, 
      "user_email_id": getShemarooCookies().user_login_id,
      "platform": "web",
      "user_id": getShemarooCookies().user_analytical_id,
      "region": $(".user_region").val(),
      "user_state": $.cookie('user_actv_sub_status')
		});
		 branch.logEvent(
       'phonenumber_update',
        { "phone_number": mob_no, 
      "user_email_id": getShemarooCookies().user_login_id,
      "platform": "web",
      "user_id": getShemarooCookies().user_analytical_id,
      "region": $(".user_region").val(),
      "user_state": $.cookie('user_actv_sub_status')
			  },
       function(err) { console.log(err); }
      );

  }

	$("#sso_otp_verfy").click(function(){
	 $("#sso_otp_msg").hide();
	 var otp = $("#sso_otp").val();
	 if(otp.trim().length != 0){
	 	 $("#sso_otp_verfy").text($("#ext_verify_otp").val()+"...");
	 	 $.ajax({
				url:"/users/ext_valid_otp",
				type: "POST",
				data: {"mob_no": $("#ext_user_mob_no").val(),"otp": otp },
				success: function(response,status){
				 $("#sso_otp_verfy").text($("#ext_verify_otp").val());
			   if(response.status == "true"){
			   	var  login_type = localStorage.getItem("shm_user_login")
			   	var region = $(".user_region").val() 
		   			if(login_type != undefined && region == "IN"){
		   				registration_success_evnt('Registration_Success',getShemarooCookies().user_analytical_id,login_type,$("#sso_user_mob_num").val(),region)
		   				localStorage.removeItem("shm_user_login")
			   		}
			   	 mobileno_update($("#ext_user_mob_no").val())
			   	 localStorage.removeItem("user_mob_map_st")
			     $("#mobile_suc_up").modal("show");
			   }
			   else{
			   	$("#sso_otp_msg").text($("#ext_invalid_otp").val()).show().fadeOut(4500);
			   }
			  }
			});
	 }
	 else{
	 	if(otp.trim().length == 0){
        $("#sso_otp_error_msg").text($("#ext_mandatory_info").val()).show().fadeOut(4500);
	  	}
	 }
	});

	$("#sso_resend_otp").click(function(){
		var mobile_no = $("#ext_user_mob_no").val().substr(2);
		 $.ajax({
				url:"/users/ext_mobile",
				type: "POST",
				data: {"mob_no": mobile_no },
				success: function(response,status){
			   if(response.status == "true"){
	      	$("#sso_otp_msg").text($("#ext_otp_sent").val()).show().fadeOut(4500);
			   }
			   else{
			   	$("#sso_otp_msg").text(response.msg)
			   }
			  }
			});
	})
	$("#sso_mob_later").click(function(){
	  $("#sso_mobile_login_pop2").modal("hide");
	})
	$("#sso_otp_later").click(function(){
		$("#sso_mobile_login_otp_pop").modal("hide");
	})
	$("#sso_user_mob_later").click(function(){
		$("#sso_mobile_login_pop").modal("hide");
	})
	$(".login_fb_google").click(function(){
		$("#sso_mobile_login_pop").modal("hide");
		//$("#fb_login_pop").modal("show");
		$("#fb_login_pop").modal();
	});

	$("#sso_user_mobile_later").click(function(){
   $("#sso_mobile_login_pop2").modal("hide");
	});

	$("#sso_login_later").click(function(){
   $("#sso_mobile_login_pop").modal("hide");
	});

/*end of SSO Implementation writtenby kotesh*/


/*Language Selection code written by kotesh on 1-Mar-2021*/

$(".hd_lg_box").click(function(){
  	$("#en,#hi,#gu").css('color','#212529');
    //document.cookie = 'shm_sel_lang' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    $(".hd_lg_box").removeClass("selected");
    var lang = $(this).attr("id");
    var lg_color = $(this).data("color");
    $("#"+lang).css('color', "#"+lg_color);
    //$.cookie('shm_sel_lang',lang, { expires: 14,path: '/'});
    $(this).addClass("selected");
  })

  $("#prf_lng").click(function(){
  	$(this).text($("#lang_continue").val()+"...")
  	document.cookie = 'shm_sel_lang' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    var user_lang = $(".prof_lang_box.selected").attr("id");
    if(user_lang == undefined){
      user_lang =  $(".user_lang_box.selected").attr("id");
    }
    $.cookie('shm_sel_lang',user_lang, { expires: 14,path: '/'});
    var lang = getShemarooCookies().shm_sel_lang
    if(lang != "en"){
    	update_user_lang(lang)
      //window.location = "/"+lang
    }
    else{
    	window.location = "/"
    }
  })
  
  function update_user_lang(lng){
		$.ajax({
			url:"/users/update_lang",
			type: "POST",
			data: {"usr_anly_id": getShemarooCookies().user_analytical_id,"lang": lng},
			success: function(response){
				window.location = "/"+lng
			}
		});
  }



  $("#cls_lang").click(function(){
  	$.cookie('shm_sel_lang',"en", { expires: 14,path: '/'});
    $("#lang_selection_pop").modal("hide");
  })

  $("#prof_lang_sel").click(function(){
  	var get_lang = getShemarooCookies().shm_sel_lang
  	if(get_lang != undefined){
     $("#"+get_lang).addClass("selected");
     $("#"+get_lang).css('color', "#D51F53");
  	}
  	else{
     $("#en").addClass("selected");
      $("#en").css('color', "#D51F53");
  	}
   $("#small_selection_pop").modal("show");
  });

 $(".lang_box").click(function(){
 	 $("#en,#hi,#gu").css('color','#212529');
    //document.cookie = 'shm_sel_lang' + '=;path=/;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    $(".lang_box").removeClass("selected");
    var lang = $(this).attr("id");
    var lg_color = $(this).data("color");
    $("#"+lang).css('color', "#"+lg_color);
    //$.cookie('shm_sel_lang',lang, { expires: 14,path: '/'});
    $(this).addClass("selected");
 })

 $("#prf_lg_cls").click(function(){
  $("#small_selection_pop").modal("hide");
  $(".prof_lang_box").removeClass("selected");
 })


 $("#swagat_sub").click(function(){
    $("#swagat_email_error,#swagat_pwd_error,#swagat_cpwd_error").hide();
	 	var email_id = $("#swagat_email").val();
	 	var pwd = $("#swagat_password").val();
	 	var cf_pwd = $("#swagat_cpassword").val();
	 	var status = false
	 	var coupon_check = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var url = window.location.pathname.split("/")[1] 
	 	 if(email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd)){
      status = true
      signup_type = "email"
 	  }
	 	if(status == true){
			$("#swagat_sub").text("Register..")
			$.ajax({
				url: "/users/coupon_register",
				type: "POST",
				data: { 
				name: "Guest",
				email_id: email_id,
			  type: signup_type,
				password: pwd,
				browser_det: browser_name()
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
				   $("#swagat_sub").text("Register")
				   	set_user_cookies(response);
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	 $("#swagat_cpwd_error").text("User Registered Successfully").show()
				   	 $('#overlay_disable').removeClass('form-group-overlay-layer');
				 }
				else{
				 $("#swagat_sub").text("Register")
				 $("#swagat_cpwd_error").text(response.error_message).show()
				 $('html,body').animate({ scrollTop: 300}, 'slow');
				 }
				}
			});
		}
		else if( email_id.length == 0){
			$("#swagat_email_error").show();			
		}
		else if( !regex_email.test(email_id)){
		  $("#swagat_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#swagat_pwd_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#swagat_pwd_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#swagat_cpwd_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#swagat_cpwd_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	      $("#swagat_cpwd_error").text("Passwords do not match").show();
		}

  });


 	$("#swagat_email,#swagat_password,#swagat_cpassword").focus(function(){
	   $("#swagat_email_error,#swagat_pwd_error,#swagat_cpwd_error").hide();
	})


 $("#gcs_sub").click(function(){
    $("#swagat_email_error,#swagat_pwd_error,#swagat_cpwd_error").hide();
	 	var email_id = $("#swagat_email").val();
	 	var pwd = $("#swagat_password").val();
	 	var cf_pwd = $("#swagat_cpassword").val();
	 	var status = false
	 	var coupon_check = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 	var url = window.location.pathname.split("/")[1] 
	 	 if(email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd)){
      status = true
      signup_type = "email"
 	  }
 	  var coupon_check = valid_coupon_chk()
	 	if(status == true && coupon_check == true){
			$("#gcs_sub").text("Register..")
			$.ajax({
				url: "/users/coupon_register",
				type: "POST",
				data: { 
				name: "Guest",
				email_id: email_id,
			    type: signup_type,
				password: pwd,
				browser_det: browser_name(),
				coupon: $("#gcs_cupn_code").val()
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
			 	  $.cookie('user_actv_sub_status',"Subscribed", { expires: 14,path: '/'});
				   $("#swagat_sub").text("Register")
				   	set_user_cookies(response);
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	 $("#plan_act_pop_swagat").modal("show");
				 }
				else{
				 $("#gcs_sub").text("Register")
				 $("#swagat_cpwd_error").text(response.error_message).show()
				 $('html,body').animate({ scrollTop: 300}, 'slow');
				 }
				}
			});
		}
		else if( email_id.length == 0){
			$("#swagat_email_error").show();			
		}
		else if( !regex_email.test(email_id)){
		  $("#swagat_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#swagat_pwd_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#swagat_pwd_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#swagat_cpwd_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#swagat_cpwd_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	     $("#swagat_cpwd_error").text("Passwords do not match").show();
		}
		else {
			$("#gcs_sub").text("Register")
	     $("#cupn_limit_excds").modal("show");
	    }
  });

	function valid_coupon_chk(){
		$("#gcs_sub").text("Register...")
		var coupon_chk = false
		$.ajax({
			url: "/users/check_coupon_validity",
			type: "POST",
			async: false,
			data: { 
			coupon: $("#gcs_cupn_code").val()
			},
			success: function(response,status){
				if (response.status == true){
					coupon_chk =  true
				}else{
					//coupon_chk = response.valid_res.error.message
				}
			}
		});
		return coupon_chk
	}

	$("#swagat_apply_coupon").click(function(){
		$("#valid_coupon_error").hide();
		var cpn_code = $("#swagat_coupon").val()
		localStorage.removeItem("brnch_finger_id")
		branch.getBrowserFingerprintId(function(err, data) { localStorage.setItem("brnch_finger_id",data) });
		var osname = get_os_det()
		var user_id = getShemarooCookies().user_analytical_id;
		var user_city = $(".user_city").val();
		var branch_data = {
      "os": osname,
      "user_agent": navigator.userAgent,
      "browser_fingerprint_id": localStorage.getItem("brnch_finger_id"),
      "city": user_city
    }
		if(cpn_code.trim().length != 0){
			$(this).text("Apply..")
			$.ajax({
				url: "/users/redeem_ticket",
				type: "POST",
				data: { 
				coupon: cpn_code,
				lang : getShemarooCookies().shm_sel_lang,
				branch_data: branch_data
				},
				success: function(response,status){
					$("#swagat_apply_coupon").text("Apply")
					if(response.status == true){  
						$("#valid_coupon_error").text("Coupon Applied Successfully").show();
						$("#plan_act_pop_swagat").modal("show")
					}else {
						$("#valid_coupon_error").text(response.error_message).show();
					}
					
				}
			});	
		}
		else{
			$("#valid_coupon_error").text("Invalid Coupon").show();
		}
	})	


  $("#hung_sub").click(function(){
    $("#hunga_email_error,#hunga_pwd_error,#hunga_cpwd_error").hide();
	 	var email_id = $("#hung_email").val();
	 	var pwd = $("#hung_password").val();
	 	var cf_pwd = $("#hung_cpassword").val();
	 	var status = false
	 	var coupon_check = false
	 	var user_region = $(".user_region").val();
	 	var regex_email = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
 		//var coupon_code = $("#hung_coupon").val();
	 	var url = window.location.pathname.split("/")[1] 

	 	 if(email_id.length != 0 && regex_email.test(email_id) && pwd.length != 0 && pwd.length >=6 && cf_pwd.length != 0 && cf_pwd.length >= 6 && (pwd == cf_pwd)){
      status = true
      signup_type = "email"
 	  }
 	  //get_coupon_check()
 	 //	coupon_check =  $("#cup_valid_chk").val()
	 	if(status == true){
			$("#hung_sub").text("Register..")
			$.ajax({
				url: "/users/coupon_register",
				type: "POST",
				data: { 
				name: "Guest",
				email_id: email_id,
			  type: signup_type,
				password: pwd,
				browser_det: browser_name()
				},
				success: function(response,status){
				 console.log(response);
				 if(response.status == true){
				   $("#hung_sub").text("Register")
				   	set_user_cookies(response);
				   	 gtag('event','Users',
				     {
				   	'dimension4': "Registered",
				   	'dimension11': response.analytics_user_id,
				   	'metric9': 1
				    })
				   	 $("#hunga_cpwd_error").text("User Registered Successfully").show()
				   	 $('#overlay_disable').removeClass('form-group-overlay-layer');
				 }
				else{
				 $("#hung_sub").text("Register")
				 $("#hunga_cpwd_error").text(response.error_message).show()
				 $('html,body').animate({ scrollTop: 300}, 'slow');
				 }
				}
			});
		}
		else if( email_id.length == 0){
			$("#hunga_email_error").show();			
		}
		else if( !regex_email.test(email_id)){
		  $("#hunga_email_error").text("Please enter a valid email address").show();		  
		}
		else if(pwd.length == 0){
		  $("#hunga_pwd_error").show();		  
		}
		else if(pwd.length < 6){
		  $("#hunga_pwd_error").text("Minimum length of 6 characters is mandatory").show();		  
		}
		else if(cf_pwd.length == 0){
		  $("#hunga_cpwd_error").show();
		}
		else if(cf_pwd.length < 6){
		  $("#hunga_cpwd_error").text("Minimum length of 6 characters is mandatory").show();
		}
		else if(pwd != cf_pwd){
	      $("#hunga_cpwd_error").text("Passwords do not match").show();
		}

  });





   $("#hung_email,#hung_password,#hung_cpassword").focus(function(){
	   $("#hunga_email_error,#hunga_pwd_error,#hunga_cpwd_error").hide();
	})
 /*Language Selection code written by kotesh on 1-Mar-2021*/



});
/*Private screening code ends here*/


  $("#hung_apply_coupon").click(function(){
 		get_coupon_check()
	});


function get_coupon_check(){
	$("#worldmeday_signup_user_promo_error").hide();
 	var coupon_code = $("#hung_coupon").val();
 	/*var location = window.location.pathname
  if ((location == "/sulekha" && coupon_code.slice(0, 3) != "SUL") || (location == "/store" && coupon_code.slice(0, 5) != "STORE") || (location == "/hungamacity" && coupon_code.slice(0, 5) != "HCITY")){
		$("#valid_coupon_error").text("Invalid Coupon").show().fadeOut(3500);
  }else{*/
	 	if(coupon_code.trim().length != 0){
	 		$(this).text("Apply..")
			$.ajax({
				url: "/users/validate_promocode",
				type: "POST",
				data: { 
				coupon: coupon_code,
				region: "US"
				},
				success: function(response,status){
					console.log(response);
					$("#hung_apply_coupon").text("Apply")
					if(response.status == true){  
						$("#cup_valid_chk").val("true")
						$("#valid_coupon_error").text("coupon applied successfully").show().fadeOut(4500);
						$("#coupon_id").val(response.coupon_id)
					}
					else{
					$("#hung_apply_coupon").text("Apply")
					$("#valid_coupon_error").text(response.error_message).show();
					}
			  }
			});	
	 	}
 	//}
}


$("#hungama_apply_coupon").click(function(){
		$("#valid_coupon_error").hide();
		var cpn_code = $("#hung_coupon").val()
		// if (cpn_code.toLocaleLowerCase().slice(0, 5) != "hcity"){
		// 	$("#valid_coupon_error").text("Invalid Coupon").show();
		// }
		// else 
		localStorage.removeItem("brnch_finger_id")
		branch.getBrowserFingerprintId(function(err, data) { localStorage.setItem("brnch_finger_id",data) });
		var osname = get_os_det()
		var user_id = getShemarooCookies().user_analytical_id;
		var user_city = $(".user_city").val();
		var branch_data = {
      "os": osname,
      "user_agent": navigator.userAgent,
      "browser_fingerprint_id": localStorage.getItem("brnch_finger_id"),
      "city": user_city
    }
		if(cpn_code.trim().length != 0){
		$(this).text("Apply..")
		$.ajax({
			url: "/users/redeem_ticket",
			type: "POST",
			data: { 
			coupon: cpn_code,
			lang : getShemarooCookies().shm_sel_lang,
			branch_data: branch_data
			},
			success: function(response,status){
				$("#hungama_apply_coupon").text("Apply")
				if(response.status == true){  
					$("#valid_coupon_error").text("Coupon Applied Successfully").show();
					$("#plan_act_pop_hungama").modal("show")
				}else {
					$("#valid_coupon_error").text(response.error_message).show();
				}
				
			}
		});	
	}
	else{
		$("#valid_coupon_error").text("Invalid Coupon").show();
	}


function gmfc_user_suc_register_events(){
  var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status') : "anonymous"
	  var user_region = $(".user_region").val()
		var plat_form = "ios"
		if(getShemarooCookies().mobile_browser_type == "android"){
			plat_form = "android"
		}
		Apxor.logEvent('Game_registerationcompleted', {
			"user_id": getShemarooCookies().user_analytical_id,
			"platform": plat_form,
			"user_status": "not_subscribed",
			"user_state": user_state
		}, "USER_EVENTS");
		smartech('dispatch', 'Game_registerationcompleted', {
			"user_id": getShemarooCookies().user_analytical_id,
			"platform": plat_form,
			"user_status": "not_subscribed",
			"user_state": user_state
		});
		 branch.logEvent(
       'Game_registerationcompleted',
        { "user_id": getShemarooCookies().user_analytical_id,
					"platform": plat_form,
					"user_status": "not_subscribed",
					"user_state": user_state
			  },
       function(err) { console.log(err); }
      );
}


  function encrypt_string(val){
  	var token = "92a7d1a410d84495fcf4c29e2a674a3c"
    var data = CryptoJS.AES.encrypt(val,token);
    console.log(data);
    return data;
  }

  function decrypt_string(val){
  	var token = "92a7d1a410d84495fcf4c29e2a674a3c"
    var data  = CryptoJS.AES.decrypt(val.toString(), 'secret key 123').toString(CryptoJS.enc.Utf8);
    return data;
  }

});