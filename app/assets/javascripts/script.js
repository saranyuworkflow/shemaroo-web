$(document).ready(function(){

	function Netcore_Search(search_val){
	console.log("netcore Search-----------------------------------")
	var user_state = $.cookie('user_actv_sub_status') != undefined ? $.cookie('user_actv_sub_status').toLowerCase() : "anonymous"
	var user_region = $(".user_region").val()
	smartech('dispatch', 'Search_Result_Clicked', {
		"session_id": getShemarooCookies().user_id,
		"user_id": getShemarooCookies().user_analytic_id,
		"keyword":search_val,
		'user_state': user_state,
		"clicked":"false",
		"COUNTRY":user_region
		
  });
  Apxor.logEvent('Search_Result_Clicked', {
		"keyword": search_val,
		"user_id": getShemarooCookies().user_analytic_id,
		"clicked":"false",
		'user_state': user_state,
 		},"SEARCH");

  branch.logEvent(
    "Search_Result_Clicked",
    	{
    		"user_id": getShemarooCookies().user_analytic_id,
        "keyword": search_val,
        'user_state': user_state,
        "clicked": "false"
    	},
    function(err) { console.log(err); }
    );

}

	if($(".main-menu ul li").length == 1){
    $(".main_logo_link").attr("href","/kids");
	}
	if(getShemarooCookies().user_id){
		$(".with_login_item").show();
		$(".with_out_login_item").hide();
		$(".mobile_with_login").show();
    $(".mobile_with_out_login_item").hide();
	}
	else{
		$(".with_login_item").hide();
		$(".with_out_login_item").show();
	}


	$('.header .navbar-toggler').on('click', function() {
    $("body").addClass("noscroll");
  });
  $(".close-hambergmenu img").on('click', function(){
    $('.fixed-top .navbar-collapse').removeClass('show');
    $("body").removeClass("noscroll");
  });

  $(".modal.show").on("show", function () {
  $("body").addClass("modal-open");
}).on("hidden", function () {
  $("body").removeClass("modal-open")
});

	$("#search").keyup(function(e){
    var search_val = $(this).val();
	  $("#search_heading,#recent_search,#trending_search,#trend_search,#popular_search,.search_icon").hide();
	  $('.search-page input[type="search"]').css("padding", "15px 20px 15px 20px");
	  $(".cancel_search").show();
	  $("#search_all_results").show();
	  $("#search_all_results,.search_count").html("");
	  var sel_lang = getShemarooCookies().shm_sel_lang;
	  var get_type = "all"
    if(getShemarooCookies().user_profile_type == "kids"){
      get_type = "kids"
   	}
	  if(search_val.trim().length != 0){
	    search_val = search_val.replace(/%/g, "%25").replace(/#/g, "%23").replace(/&/g, "%26").replace(/;/g, "%3B");
		  if(e.which == 13){
		  Netcore_Search(search_val)
		  var search_url = "/search?q="+search_val;
		  if(getShemarooCookies().shm_sel_lang != undefined && getShemarooCookies().shm_sel_lang != "en"){
				  search_url = "/"+getShemarooCookies().shm_sel_lang+"/search?q="+search_val
					}
	      window.location = search_url
	    }
	    
	    else{
		  	$.ajax({ 
		      type: 'POST', 
		      url: '/search', 
		      data: {'search' : search_val,
		      'lang': sel_lang }, 
		      success: function(response){
		        $("#search_heading").show();
		        var search_results = response.results;
		        if(search_results.length != 0){
		          $(".srch_tbs").show();
		          $("#trending_search").hide();         	
		          $('.search_all_results').html('');
		          for (var i = 0; i < search_results.length; i++) {
		          	var item_det = search_results[i][1].split("$");
		          	var list_data = "";
            		var list_items = "";
 								for (var m=0; m< search_results[i][0].length; m++){
              		var list = search_results[i][0][m].split("$");
              		list_items+='<div><div class="item"><div class="float-left w-100 search-thumb-section"><div class="search-thumb-img-section"><a href="'+list[0]+'"><img src="'+list[1]+'" class="img-loader lazy-img-loader loaded searched_item" alt="" title=""></a><input type="hidden" id="content_type"  value="'+list[6]+'"><input type="hidden" id="category_name"  value="'+list[7]+'"><input type="hidden" id="title_value"  value="'+list[8]+'"></div><div class=" float-left w-100 search-thumb-desc-section"><h2 class="app-color6 font-black float-left w-100">'+list[2]+'</h2><p class="app-color2 font-regular float-left w-100">'+list[10]+''+list[11]+''+list[9]+'</p></div></div></div></div>'
              	}
              	var dis_val = search_results[i][0].length;
              	console.log("display_valueee",dis_val)
              	list_data = '<div class="row"><div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col- app-d-flex v-center"><h2 class="search-results-sec-title font-black app-color1 mr-auto">'+item_det[0]+'</h2><a href="/search/all?q='+search_val+'&type='+ get_type +'&theme='+item_det[1]+'&lang='+sel_lang+'" class="search-see-all" style="display: '+(dis_val > 5 ? "" : "none;")+'" id=""><span>See all</span><img src="/assets/see_all.svg"></a></div></div><div class="row"><div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-"><div class="search-slicks-container"><div class="float-left w-100 slicks" ><div class="float-left w-100"><div class="float-left w-100 slick-container slick-gap"><div class="float-left w-100 slick-wrapper"><div class="float-left w-100 app-slick-carousel"><div class="float-left w-100 app-slick-slider-container"><div class="slider search-carousel'+ i +'">'+list_items+'</div></div></div></div></div></div></div></div></div></div>'
              	$(".search_all_results").append(list_data)
              	search_carousel(i)
	           	  // $(".search_count").text(search_results.length)
	              e.stopPropagation();
		          }
                 Netcore_Search(search_val)
		        }
		        else if(search_results.length == 0 && response.type == "single") {
		         	$(".search_all_results").html("");
		         	$("#search_heading,#recent_search,#trending_search").hide();
		        }
		        else {
		         	$("#search_heading").hide();
		         	$(".search_all_results").html("");
		         	$(".search_all_results").append('<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col- margin-bottom-30 no-content-wrap text-center"><img src="/assets/no_result.svg" class="search-no-results-image" alt="no search results" title="no search results"><p class="margin-bottom-0 font-bold text-ellipsis"><strong>'+$("#no_res_msg").val()+'</strong></p><p class="font-regular margin-bottom-0"><p>'+$("#no_match").val()+' &nbsp;<strong>"'+decodeURIComponent(search_val)+'"</strong></p><p class="font-regular margin-bottom-0"><p>'+$("#no_res_title").val()+'</p></div></div>')
		          $(".searched_text").text(search_val.toUpperCase().replace(/%23/g, "#").replace(/%26/g, "&").replace(/%25/g, "%").replace(/%3B/g, ";"));
		        }
		      } 
		    });
	    }
		}
	  else{
  	 	//$(".search_errors").show().fadeOut(2000);
  	 	setTimeout(function(){$("#search_all_results").html("");$("#search_heading,#recent_search,#trending_search").hide(); },800);
	    $(".search_all_results").html("");
		  $("#search").val("");
		  $(".cancel_search,#search_heading").hide();

		  var srh_url  = "/search"
		if(getShemarooCookies().shm_sel_lang != undefined && getShemarooCookies().shm_sel_lang != "en"){
			srh_url = "/"+getShemarooCookies().shm_sel_lang+srh_url
		}
		window.location = srh_url
	  }
	});

function search_carousel(i) {
$('.search-carousel'+i).slick({
  infinite: false,
  dots: false,
  speed: 300,
  slidesToShow: 3,
  slidesToScroll: 1,
  draggable: false,
  responsive: [      
    {
    breakpoint: 991,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
    breakpoint: 440,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});  
}
	$(".cancel_search").click(function(){
		$("#search_all_results").html("");
		$("#search").val("");
		$(".cancel_search,#search_heading").hide();
		$("#recent_search,#trend_search,#popular_search,.search_icon").show();
		$('.search-page input[type="search"]').css("padding", "15px 20px 15px 60px");
	})

	$(".cancel_search").click(function(){
		$("#search_all_results").html("");
		$("#search").val("");
		$(".cancel_search,#search_heading").hide();
		//$("#recent_search").show();
		//location.reload();
		var srh_url  = "/search"
		if(getShemarooCookies().shm_sel_lang != undefined && getShemarooCookies().shm_sel_lang != "en"){
			srh_url = "/"+getShemarooCookies().shm_sel_lang+srh_url
		}
		window.location = srh_url
	})

	$("#clear_search").click(function(){
	  $.removeCookie('recent_search_data', { path: '/' });
	  $(this).hide();
	  $("#user_recent_search").html('<span class="tag searchtag app-color1">You have not searched anything recently!</span>');
  });

 $("#load_more").click(function() {
 	
 })
  
  $(".input-group img.password_toggle").click(function() {
    $(this).parent(".input-group").toggleClass("hide_password_group");
    //$(this).attr('src', '')
    $(this).prevAll().attr('type', function(index, attr){
      return attr == "password" ? "text" : "password";
    });
  });

	$(window).scroll(function() {
	    if ($(window).scrollTop() > 80) {
	        //$('.content-separator').show();
	        $("section.header-section").addClass("shadow-separator");	        
	    }
	    else {
	        //$('.content-separator').hide();
	        $("section.header-section").removeClass("shadow-separator");
	    }
	});
	
	$('.share_social_icons').click(function(e) {
		$('.share_feature').toggle();
		e.stopPropagation();
	});
	$(document).click(function(e) {
		$('.share_feature').hide();	
		
	});


});

	function downloadLink() {
    var isMobile = {
      android: function() {
          return navigator.userAgent.match(/Android/i);
      },
      iphone: function() {
          return navigator.userAgent.match(/iPhone/i);
      },
      ipad: function() {
          return navigator.userAgent.match(/iPad/i);
      }
    };
    if ((isMobile.iphone() || isMobile.ipad())) {
        url ="https://apps.apple.com/mz/app/shemaroome/id1439041310"
    }
    else {
      url = "https://play.google.com/store/apps/details?id=com.saranyu.shemarooworld"
    }
   // window.location = url;
      window.open(url, '_blank');

  }

function copyLink() {

	/* get the text field */
	var copyText = document.getElementById('copy_link');

	/* Select the text field */
	copyText.select();

	/* Copy the text inside the text field */
	document.execCommand("copy");

	$("#copy_link_pop").show().fadeOut(4500);

	/* Alert the copied text */
	//alert("Copied the text: " + copyText.value);
} 

$(document).ready(function(){
	$('body').on('click', '.searched_item', function() {
		var user_state = $.cookie('user_actv_sub_status');
	    var searched_data = $("#search").val();
	    if(searched_data.length != 0){
      		if(getShemarooCookies().recent_search_data){
	 	    	searched_data = getShemarooCookies().recent_search_data+","+searched_data
	 		}
	 	  $.cookie('recent_search_data',searched_data, { expires: 14,path: '/'});
	  	 branch.logEvent(
       "SEARCH",
      {"search_term": '',
      'user_state': user_state,
	    "title": $("#search").val(),
	    "content_id": '',
	    "category": '',
	    "content_type": '',
	    "genre": '',
	    "language": ''
       },
     function(err) { console.log(err); }
     );
	  	}

	});

  $('#banner-carousel img').hover(function() {
  	var title = $(this).attr("title");
    $(this).attr('title', '');
  }, function() {
    //$(this).attr('title', title);
  })

})


/* Profile Menu Script starts */

$(document).ready(function() { 
	$('body').on('click', '.profile_item_watching', function() {
	    var profile_id = $(this).attr("id");
			localStorage.removeItem("switch_profile_id")
			localStorage.setItem("switch_profile_id",profile_id)
			var pin_url = "/users/verify_pin";
	  if( getShemarooCookies().shm_sel_lang != undefined && getShemarooCookies().shm_sel_lang != "en"){
	  	pin_url = "/"+getShemarooCookies().shm_sel_lang+"/users/verify_pin";
	  }
	  window.location = pin_url
	});
  
  /* close the menu section for both desktop and mobile script starts */
  
  $("#right-sidebar-menu .close-hambergmenu img").click(function() {
  	var window_height = window.outerHeight;
  	$("body").removeClass("noscroll");
    $('#right-sidebar-menu').animate({
	      width: "0px",
	      opacity: "0",
	      height: "0px"
	  },50, 'linear');
	  $('.user_menu').css("z-index", "1111");
	  $('.head-right ul li').css("z-index", "1111");
        
  });   

  /* close the menu section for both desktop and mobile script ends */

  /* Build slick carousel html of profile for both desktop and mobile script starts */

  function set_profiles(device_type) {
  	var user_profiles = getShemarooCookies().user_profiles.split(",");
  	var class_name = "";
  	var user_data = "";
  	var device = device_type
  	var profile_logo = "/assets/profile.svg"

    if(device == "desktop"){
    	user_data+= '<div id="divSlickSimilarItems">';
    }
    else {
    	user_data+= '<div id="divSlickSimilarItemsMobile">';	
    }
  	for(i=0;i< user_profiles.length;i++){
			var user_name = user_profiles[i].split("$")
			var username_acronym = user_name[1]
			//.match(/\b(\w)/g).join('').substr(0,2);
			if(user_name[1].replace(/%20/g, "").replace(/\s+/g, "").toLowerCase() == getShemarooCookies().user_name.replace(/%20/g, '').replace(/\s+/g, '').toLowerCase()) {													
	           if(user_name[2] == "true"){
                profile_logo = "/assets/profile_kids.svg"
	           }
	           user_data+= '<div class="item active"><img src="'+profile_logo+'" alt="profile" title="profile"><p>'+username_acronym.toUpperCase()+'</p><span class="text-uppercase text-ellipsis">'+user_name[1]+'</span></div>'
			}
			else {
				if(user_name[2] == "true") {
				 	class_name = "kids-profile"
				 	profile_url = "/users/change_profile/"+user_name[0]
			 	}
			 	else {
				 	class_name = ""
				 	profile_url = "/users/change_profile/"+user_name[0]
				 	if(getShemarooCookies().is_parent_control == "true") {
	          profile_url = "javascript:void(0)"
	          class_name = "profile_item_watching"
				 	}
			  }
			  user_data+= '<div class="item"><a href="'+profile_url+'" class="'+class_name+'" id="'+user_name[0]+'"><p class="'+class_name+'">'+username_acronym.toUpperCase()+'</p><span class="text-uppercase  text-ellipsis">'+user_name[1]+'</span></div>'
	  	}
		}
		user_data+= '</div>';
		return user_data;
  }

/* Build slick carousel html of profile for both desktop and mobile script ends */

/* Desktop Profile section scripts starts */

  function load_profiles () {
	    
		$('#divSlickSimilarItems').slick({
	    dots: false,
	    arrows: true,
	    prevArrow: '<span class="slick-prev" aria-label="' + 'Previous' + '"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
	    nextArrow: '<span class="slick-next" aria-label="' + 'Next' + '"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
		  infinite: true,
		  speed: 300,
		  slidesToShow: 3,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 3,
		        slidesToScroll: 1,
		        infinite: true,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 1
		      }
		    }
		  ]
		});
	}

  function check_user_parental_control(){
  	$.ajax({
				url: "/users/account_details",
				type: "POST",
				data: {},
				success: function(response){
					if(response.status == true){
						console.log("set cookies");
						$.cookie('is_parent_control',response.parental_control, { expires: 14,path: '/'})
            $.cookie('pin',response.pin, { expires: 14,path: '/'})
					}
				}
		  });
  }
	$('.user_menu').click(function(event) {
		if(getShemarooCookies().user_id){
			var class_name = "";
			$("#header_user_profiles .panel-body").html('');
			var user_profiles = getShemarooCookies().user_profiles.split(",");
			check_user_parental_control();
			$("#header_user_profiles .panel-body").html(set_profiles('desktop'));
		  $(".with_login_item").show();
		  if(getShemarooCookies().user_profile_type == "kids"){
			 $(".account_deatils,.view_plans,.settings,.logout,.manage-profile,.non_kid_tab").hide();
		 }
        else{
        var profiles = getShemarooCookies().user_profiles.split(",")
        var all_profile_names = profiles.map(function(t){ return t.split("$")[1].replace(/%20/g, "").replace(/\s+/g, "").toLowerCase(); })
        var profile_position = all_profile_names.indexOf(getShemarooCookies().user_name.replace(/%20/g, "").replace(/\s+/g, "").toLowerCase())
         var selected_profile = profiles[profile_position].split("$")[3]
          if(selected_profile.length != 0 && selected_profile == "false"){
            $(".account_deatils,.view_plans,.logout").hide();
         }
         
        }

	    $(".with_out_login_item").hide();
	  }
	  else {
	  	$("#right-sidebar-menu .profile-list").hide();
	  }

	  $('.user_menu').css("z-index", "1");
	  $('.head-right ul li').css("z-index", "1");
		var window_height = window.outerHeight;
		$("body").addClass("noscroll");
		$('#right-sidebar-menu, #right-sidebar-menu .panel-body').show();
    
    $('#right-sidebar-menu').animate({
        width: '250px',
				height: window_height, 
				opacity: 1                  
    }, 10, 'linear');
		
		setTimeout(load_profiles, 250);
    $(".panel-body").show();    
	});

/* Desktop Profile section scripts ends */

/* Mobile Profile section scripts starts */

  function load_profiles_mobile() {
		$('#divSlickSimilarItemsMobile').slick({
	    dots: false,
	    arrows: true,
	    prevArrow: '<span class="slick-prev" aria-label="' + 'Previous' + '"><i class="fa fa-angle-left" aria-hidden="true"></i></span>',
	    nextArrow: '<span class="slick-next" aria-label="' + 'Next' + '"><i class="fa fa-angle-right" aria-hidden="true"></i></span>',
		  infinite: false,
		  speed: 300,
		  slidesToShow: 5,
		  slidesToScroll: 1,
		  responsive: [
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 1,
		        infinite: false,
		        dots: false
		      }
		    },
		    {
		      breakpoint: 600,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 1
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 5,
		        slidesToScroll: 1
		      }
		    } 
		  ]
		});

	}

	$(".navbar-toggler-icon").click(function(){
		if(getShemarooCookies().user_id){
		 	check_user_parental_control();
		$("#mobile_user_profiles .panel-body").html(set_profiles('mobile'));
		$('iframe').contents().find('video').each(function (){
			if(!(this.paused)){
				this.pause();
			}
		});
		$("#kids_tabs").hide();
		$(".panel-body").show();
		$("#mobile_user_profiles .item.active").css({"width" : "100px"});
		setTimeout(load_profiles_mobile, 250);
	  $(".mobile_with_login").show();
	  if(getShemarooCookies().user_profile_type == "kids"){
			 $(".account_details,.view_plans,.manage_payments,.settings,.logout,.manage-profile,.non_kid_tab").hide();
		}
		else{
	    var profiles = getShemarooCookies().user_profiles.split(",")
        var all_profile_names = profiles.map(function(t){ return t.split("$")[1].replace(/%20/g, "").replace(/\s+/g, "").toLowerCase(); })
        
        var profile_position = all_profile_names.indexOf(getShemarooCookies().user_name.replace(/%20/g, "").replace(/\s+/g, "").toLowerCase())
         var selected_profile = profiles[profile_position].split("$")[3]
          if(selected_profile.length != 0 && selected_profile == "false"){
            $(".account_details,.view_plans,.logout").hide();
         }
        }
	 	$(".mobile_with_out_login_item").hide();
	   }
	})





});

/* Mobile Profile section scripts ends */

/* Profile Menu Script ends */