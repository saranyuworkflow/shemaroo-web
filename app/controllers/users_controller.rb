class UsersController < ApplicationController
 #skip_before_action  :verify_authenticity_token
include UsersHelper
include LazyPay
 before_action :check_valid_user,:only => ["edit_profile","manage_profiles","account_details","add_profile","create_pin","verify_pin","profile","settings","update_personal_details"]
 def sign_up
	 #begin
	  signup_params = {
	    :user => {
	    :firstname=> params[:name],
	    :password => params[:password],
	    :region => $region
	    }
	  }
	  if $region == "IN"
      if params[:login_type] == "firebase"
       signup_params[:user][:email_id] = params[:email_id]
     else
	     signup_params[:user][:user_id] = "91"+params[:mobileno]
	     signup_params[:user][:type] = "msisdn"
     end
   # elsif $region == "BD"
   #    signup_params[:user][:user_id] = "880"+params[:mobileno]
   #    signup_params[:user][:type] = "msisdn"
	  else
	    signup_params[:user][:email_id] = params[:email_id]
      end
      signup_params[:user][:device_data] = {
        :device_name => params[:browser_det],
        :device_type => "web"
      }
             
	 user_st = "Registered"
	  response = User.sign_up(signup_params)
    p response.inspect
    if  (($region == "US" || params[:login_type] == "firebase") && response.has_key?("data"))
    user_profiles = User.get_all_user_profiles(response["data"]["session_id"])
    account_resp = User.get_user_account_details(response["data"]["session_id"])
    all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
    first_profile = all_profiles.flatten.first.split("$")
    if params[:coupon].present?
      coupon_params = {
      :coupon_code => params[:coupon],
      :region => "US",
      :plans_info => "true"
      }
      coupon_resp = User.validate_coupon(coupon_params)
      if coupon_resp.has_key?("data")
        plan_info = coupon_resp["data"]
        p plan_info.inspect
        payment_data = plan_info["payment_info"]
        plan_ids = payment_data["packs"][0]["plan_id"]
        browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
        platform_type = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
        
        plan_init_params = {
        :us => get_user_payment_signature_key(plan_ids,response["data"]["session_id"]),
        :payment_gateway => "coupon",
        :platform => platform_type,
        :payment_info => payment_data,
        # :payment_info => {
        #   :net_amount => payment_data['net_amount'], 
        #   :price_charged => payment_data['price_charged'],
        #   :currency => payment_data['currency'], 
        #   :packs => [{
        #     :plan_categories => payment_data["packs"][0]['plan_categories'], 
        #     :category_type =>"individual",
        #     :category_pack_id => payment_data["packs"][0][''],
        #     :subscription_catalog_id => params[:new_plan_catalog_id], 
        #     :plan_id => params[:new_plan_id]
        #     }
        #   ] 
        #   :coupon_code => plan_info['coupon_info']['coupon_code'],
        #   :coupon_code_id => plan_info['coupon_info']['coupon_id']
        # },
        :transaction_info => {
          :app_txn_id => "", 
          :txn_message => "One Day Pack_10.00", 
          :txn_status => "init", 
          :order_id => "", 
          :pg_transaction_id => ""  
         },
        :user_info => {
         :email => account_resp["data"]["email_id"],
         :mobile_number => ""
        },
        :miscellaneous => {
         :browser => browser_name,
         :device_brand =>  "NA",
         :device_IMEI => "NA",
         :device_model =>  "NA", 
         :device_OS =>  "NA" ,
         :device_type =>  "NA" ,
         :inet => "NA",
         :isp =>  "NA",
         :operator =>  "NA"
        }
      }
      p plan_init_params.inspect
      p "@@@@@@@@@@@@@@@2"
      transaction_resp = User.send_init_transaction(response["data"]["session_id"],plan_init_params)
      
      p transaction_resp.inspect
         sucess_plan_params = {
          :us => get_user_payment_signature_key(plan_ids,response["data"]["session_id"]),
          :payment_gateway => "coupon",
          # :payment_info => {
          #   :net_amount => payment_data['net_amount'],
          #   :price_charged => payment_data['price_charged'],
          #   :currency => payment_data['currency'], 
          #   :packs => payment_data['packs'],
          #   :coupon_code => plan_info['coupon_info']['coupon_code'],
          #   :coupon_code_id => plan_info['coupon_info']['coupon_id']
          # },
        :payment_info => payment_data,
        :transaction_info => {
          :order_id => transaction_resp["data"]["transaction_id"],
          :txn_status => "success",
          :txn_message => "One Day Pack_10.00",
          :pg_transaction_id => SecureRandom.urlsafe_base64(5)
         },
        :user_info => {
         :email => account_resp["data"]["email_id"],
         :mobile_number => ""
        },
        :miscellaneous => {
         :browser => browser_name,
         :device_brand =>  "NA",
         :device_IMEI => "NA",
         :device_model =>  "NA", 
         :device_OS =>  "NA" ,
         :device_type =>  "NA" ,
         :inet => "NA",
         :isp =>  "NA",
         :operator =>  "NA"
        }
      }
       success_transaction_resp = User.send_init_transaction(response["data"]["session_id"],sucess_plan_params)
     end
    end
    render json: {status: true,user_id: "#{response["data"]["session_id"]}",user_name: first_profile[1],user_profiles: all_profiles,profile_id: first_profile[0],:login_id => params[:email_id],user_parental_control: account_resp["data"]["parental_control"],pin: account_resp["data"]["parental_pin"],:profile_type => "non_kids",:user_analytic_id => account_resp["data"]["user_id"], :user_status => user_st}
   else
    p "@@@@@@@@@@@@@@@@@@@@@@@@@@@2"
    set_response(response)
   end
	# rescue Exception => e
 #    render json: {status: false,error_message: "sorry something went wrong" }
 #    logger.info e.message
	# end
 end

 def pre_register
   begin
    if $region == "IN"
      pre_register_params = {
        :user => {
        :firstname=> params[:name],
        :password => params[:password],
        :region => $region,
        :pre_registered => "true"
        }
      }
      if $region == "IN"
        pre_register_params[:user][:user_id] = "91"+params[:mobileno]
        pre_register_params[:user][:type] = "msisdn"

      else
        pre_register_params[:user][:email_id] = params[:email_id]
      end
      response = User.sign_up(pre_register_params)
    else
     non_india_register_params = {
       :content => {
        :title => "Other user region",
        :email_id => params[:email_id],
        :name => params[:name],
        :status => "published",
        :user_region => $region
       }
     }
     response = User.pre_register(non_india_register_params)
     p response.inspect
    end
    p response.inspect
    if response.has_key?("data")
      render json: {status: true}
    else
      render json: {status: false,:error_message => response["error"]["message"]}
   end
  rescue Exception => e
    render json: {status: false,error_message: "sorry something went wrong" }
    logger.info e.message
  end
 end


 def sign_in
  begin
    if request.post?
  unless verify_recaptcha
    render json: {status: false,error_message: "Captcha failed" } and return
    logger.info e.message
  end
  p params.inspect
   signin_params = {
    :user => {
      :password => params[:password],
      :region => $region,
       :current_sign_in_ip => $user_ip,
      :device_data => {
        device_name: params[:browser_det],
        device_type: "web"
      }
    }
   }
   if $region == "IN"
     if params[:login_type] == "firebase"
      signin_params[:user][:email_id] = params[:email_id]
      signin_params[:user][:type] = "email"
      user_login_id = params[:email_id]
      mobile_num = ""
      email_id = user_login_id
     else
      signin_params[:user][:user_id] = "91"+params[:mobile_no]
      signin_params[:user][:type] = "msisdn"
      user_login_id = "91"+params[:mobile_no]
    end
    # elsif $region == "BD"
    #   signin_params[:user][:user_id] = "880"+params[:mobile_no]
    #   signin_params[:user][:type] = "msisdn"
    #   user_login_id = "880"+params[:mobile_no]  
   else
      signin_params[:user][:email_id] = params[:email_id]
      signin_params[:user][:type] = "email"
      user_login_id = params[:email_id]
      mobile_num = ""
      email_id = user_login_id
   end
   p signin_params.inspect
   sign_in_response = User.sign_in(signin_params)
  if sign_in_response.has_key?("data")
     user_region_key = sign_in_response["data"]["user_region_key"]
     if user_region_key == true
     user_region_key = sign_in_response["data"]["user_region_key"]
     render json: {user_region_key: user_region_key }
    else
    user_profiles = User.get_all_user_profiles(sign_in_response["data"]["session"])
    account_resp = User.get_user_account_details(sign_in_response["data"]["session"])
    all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
    first_profile = all_profiles.flatten.first.split("$")
    default_profile = user_profiles['data']['profiles'].collect{|x|x if x['default_profile'] == "true"}.compact.first
    user_profile_data  = {
      :user_profile => {
        :profile_id => default_profile['profile_id']
      }
    }
    user_st = "Registered"
    active_plan = "NA"
    all_pck_st = "false"
    all_mnt_st = "false"
    all_yr_st = "false"
    if account_resp["data"]["is_subscribed"] == true
      user_st = "Subscribed"
      active_plan = account_resp["data"]["active_plans"].join(",")
      lang = get_user_lang(params)
      all_user_plans = Ott.user_plans(sign_in_response["data"]["session"],lang)
      first_plan_det = all_user_plans["current_active_plans"].first
      sub_plan_type = first_plan_det["plan_title"]
      sub_exp_dt = first_plan_det["valid_till"].split("T")[0]
      sub_dt = first_plan_det["start_date"].split("T")[0]
       if account_resp["data"]["active_plans"].include?("all_access_pack")
         user_plans = Ott.user_plans(sign_in_response["data"]["session"],lang)
        act_pln = user_plans["current_active_plans"].collect{|x|x["plan_title"].downcase if x["plan_categories"].include?("all_access_pack")}.compact
        if !act_pln.empty? && (act_pln.include?("monthly") || act_pln.include?("yearly"))
          all_pck_st = "true" 
          if act_pln.include?("monthly")
            all_mnt_st = "true"
          end
           if act_pln.include?("yearly")
            all_yr_st = "true"
          end
       end
     end
    else
      check_expired_plans = Ott.user_plans(sign_in_response["data"]["session"], get_user_lang(params))
      if check_expired_plans.present? && check_expired_plans['current_active_plans'].blank? && check_expired_plans['previous_plans'].present?
        user_st = "Expired"
      end
    end

    device_limit = sign_in_response["data"]["is_device_limit_status"] 
    mobil_map_st = sign_in_response["data"]["ext_mobile_number_key"].to_s
    profile_assign  = User.assign_profile(sign_in_response["data"]["session"],user_profile_data)
    ext_mob_number =  sign_in_response["data"]["ext_mobile_number"]
    #render json: {status: true,user_id: "#{sign_in_response["data"]["session"]}",user_name: default_profile['firstname'],user_profiles: all_profiles,profile_id: default_profile["profile_id"],login_id: user_login_id,user_parental_control: account_resp["data"]["parental_control"],pin: account_resp["data"]["parental_pin"],:profile_type => "non_kids",:user_analytic_id => account_resp["data"]["user_id"],:user_status => user_st}
    city = $city
    first_name = sign_in_response["data"]["firstname"]
    render json: {status: true,user_id: "#{sign_in_response["data"]["session"]}",user_name: default_profile['firstname'],user_profiles: all_profiles,profile_id: default_profile["profile_id"],login_id: user_login_id,user_parental_control: account_resp["data"]["parental_control"],pin: account_resp["data"]["parental_pin"],:profile_type => "non_kids",:user_analytic_id => account_resp["data"]["user_id"],:user_status => user_st, city_of_user: city,device_limit: device_limit.to_s, first_name: first_name.to_s, mob_num: mobile_num, email_id: email_id,user_ac_pln: active_plan,sub_pl_ty: sub_plan_type,sub_expi_date: sub_exp_dt,sub_date:  sub_dt,user_all_ac_pk: all_pck_st,user_all_mnth_pck: all_mnt_st,user_all_yr_pck: all_yr_st,mob_map_st: mobil_map_st, ext_mob_number: ext_mob_number}
    end
   else
    p "!!!!!!!!!!!!!!!!!!!!!!!!"
    set_response(sign_in_response)
   end
 end
  
  rescue Exception => e
    render json: {status: false,error_message: "sorry something went wrong" }
    logger.info e.message
  end

 end

 def account_merge
  if params[:provider_type] == "google.com"
    provide_name = "google"
  elsif params[:provider_type] == "facebook.com"
    provide_name = "facebook"
  end
  acunt_dt = {
    :user => {
      :provider => provide_name, 
      :uid => params[:uniq_id], 
      :firstname => "Guest", 
      :ext_account_email_id => params[:email_id],
      :region => $region,
      :password => params[:password],
      :device_data => {
        :device_name => params[:browser_det],
        :device_type => "WEB"
      }
    }
  }
  p acunt_dt.inspect
  email_id = params[:email_id]
  user_login_id = params[:email_id]
  acunt_resp = User.merge_social_acc(acunt_dt)
  if acunt_resp.has_key?("data")
    user_profiles = User.get_all_user_profiles(acunt_resp["data"]["session"])
    account_resp = User.get_user_account_details(acunt_resp["data"]["session"])
    all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
    first_profile = all_profiles.flatten.first.split("$")
    default_profile = user_profiles['data']['profiles'].collect{|x|x if x['default_profile'] == "true"}.compact.first
    user_profile_data  = {
      :user_profile => {
        :profile_id => default_profile['profile_id']
      }
    }
    user_st = "Registered"
    if account_resp["data"]["is_subscribed"] == true
      user_st = "Subscribed"
    end
    device_limit = acunt_resp["data"]["is_device_limit_status"] 
    profile_assign  = User.assign_profile(acunt_resp["data"]["session"],user_profile_data)
    city = $city
    first_name = acunt_resp["data"]["firstname"]
    render json: {status: true,user_id: "#{acunt_resp["data"]["session"]}",user_name: default_profile['firstname'],user_profiles: all_profiles,profile_id: default_profile["profile_id"],login_id: user_login_id,user_parental_control: account_resp["data"]["parental_control"],pin: account_resp["data"]["parental_pin"],:profile_type => "non_kids",:user_analytic_id => account_resp["data"]["user_id"],:user_status => user_st, city_of_user: city,device_limit: device_limit.to_s, first_name: first_name.to_s, mob_num: "", email_id: email_id}

  else
    set_response(acunt_resp)
  end
 end



 def verify_otp
 
 end

 def welcome
  unless params[:t].present?
    redirect_to "#{SITE}"
  end
 end

 def firebase_success
  p params.inspect
  begin
    p "@@@@@@@@@"
    mobileno = ""
    if params[:provider_type] == "google.com"
      provide_name = "google"
    elsif params[:provider_type] == "facebook.com"
      provide_name = "facebook"
    else
      provide_name = "msisdn"
      mobileno = params[:mobile_no]
    end
      
     social_user_dt = {
      :user => {
        :provider => provide_name,
        :uid => params[:user_id],
        :firstname => params[:user_name].present? ? params[:user_name] : "Guest",
        :ext_account_email_id => params[:user_email],
        :mobile_number => mobileno,
        :region =>  $region,
        :device_data => {
          :device_name => params[:browser_det],
          :device_type => "WEB"
        }
      }
     }
    p social_user_dt.inspect
     social_sign_res = User.external_signin(social_user_dt)
     p social_sign_res.inspect
     p "@@@@@@@@@@@@@@@@@@@@@@@@@"
     if social_sign_res.has_key?("data") && social_sign_res["data"]["merge_account"] == true
      req_dt = {status: false,merg_ac_st: social_sign_res["data"]["merge_account"]}
    elsif social_sign_res.has_key?("data") && social_sign_res["data"]["user_region_key"] == true
      req_dt = {status: false, error_message: social_sign_res["data"]["message"], region_pop_up:  true}  
     elsif social_sign_res.has_key?("error")
      req_dt = {status: false, error_message: social_sign_res["error"]["message"], existing_user: true }  
     else
       p social_sign_res.inspect
       user_id = social_sign_res["data"]["session"]
       user_name = social_sign_res["data"]["firstname"]
       user_profiles = User.get_all_user_profiles(user_id)
       account_resp = User.get_user_account_details(user_id)
       all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
       default_profile = user_profiles['data']['profiles'].collect{|x|x if x['default_profile'] == "true"}.compact.first
      city = $city
      device_limit = account_resp['data']['is_device_limit_status']
      mobile_num = ""
      email_id = params[:user_email]
      user_log_id = social_sign_res['data']["branch_user_id"]
      user_login_id = social_sign_res["data"]["profile_obj"]["profile_id"]
      user_st = "Registered"
      all_pck_st = "false"
      active_plan = ""
      sub_plan_type = ""
      sub_exp_dt = ""
      sub_dt = ""
      all_mnt_st = "false"
      all_yr_st = "false"
      if account_resp["data"]["is_subscribed"] == true
        user_st = "Subscribed"
        lang = get_user_lang(params)
        active_plan = account_resp["data"]["active_plans"].join(",")
        all_user_plans = Ott.user_plans(social_sign_res["data"]["session"],lang)
        first_plan_det = all_user_plans["current_active_plans"].first
        sub_plan_type = first_plan_det["plan_title"]
        sub_exp_dt = first_plan_det["valid_till"].split("T")[0]
        sub_dt = first_plan_det["start_date"].split("T")[0]
        if account_resp["data"]["active_plans"].include?("all_access_pack")
          user_plans = Ott.user_plans(social_sign_res["data"]["session"],lang)
          act_pln = user_plans["current_active_plans"].collect{|x|x["plan_title"].downcase if x["plan_categories"].include?("all_access_pack")}.compact
          if !act_pln.empty? && (act_pln.include?("monthly") || act_pln.include?("yearly"))
            all_pck_st = "true" 
             if act_pln.include?("monthly")
              all_mnt_st = "true"
             end
             if act_pln.include?("yearly")
              all_yr_st = "true"
             end
          end
        end
      end
      req_dt = {
      status: true,
      user_id: user_id,
      user_name: default_profile['firstname'],
      user_profiles: all_profiles,
      profile_id: user_login_id,
      login_id: user_log_id,
      user_parental_control: account_resp["data"]["parental_control"],
      pin: account_resp["data"]["parental_pin"],
      profile_type: "non_kids",
      user_analytic_id: account_resp["data"]["user_id"],
      user_status: user_st, 
      city_of_user: city,
      device_limit: device_limit.to_s, 
      first_name: user_name, 
      mob_num: mobile_num, 
      email_id: email_id,
      first_time_user: social_sign_res['data']['first_time_login'],
      sub_pl_ty: sub_plan_type,
      sub_expi_date: sub_exp_dt,
      sub_date:  sub_dt,
      user_all_ac_pk: all_pck_st,
      user_all_mnth_pck: all_mnt_st,
      user_all_yr_pck: all_yr_st,
      mob_map_st: social_sign_res['data']['ext_mobile_number_key'].to_s,
      ext_mob_number: social_sign_res['data']['ext_mobile_number']
    }
    end
  rescue
    req_dt =  {status: false, error_message: "sorry something went wrong"}
  end
  render json: req_dt
 end


def welcome_offer
  # unless params[:t].present?
  #   redirect_to "#{SITE}"
  # end
 end
 def validate_otp
 	begin
   response = User.verify_otp(params[:otp],params[:mobile_no].gsub("+",""),"web",params[:browser_det])
   p response.inspect
   if response.has_key?("data")
      user_id = response["data"]["session"] 
      user_name = response["data"]["firstname"]
      user_profiles = User.get_all_user_profiles(user_id)
      account_resp = User.get_user_account_details(user_id)
      all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
      default_profile = user_profiles['data']['profiles'].collect{|x|x if x['default_profile'] == "true"}.compact.first
       city = $city
      device_limit = account_resp['data']['is_device_limit_status']
      mobile_num = ""
      email_id = params[:user_email]
      user_log_id = response['data']["branch_user_id"]
      user_login_id = default_profile["profile_id"]
      user_st = "Registered"
      all_pck_st = "false"
      active_plan = ""
      sub_plan_type = ""
      sub_exp_dt = ""
      sub_dt = ""
      all_mnt_st = "false"
      all_yr_st = "false"
      if account_resp["data"]["is_subscribed"] == true
        user_st = "Subscribed"
        lang = get_user_lang(params)
        active_plan = account_resp["data"]["active_plans"].join(",")
        all_user_plans = Ott.user_plans(user_id,lang)
        first_plan_det = all_user_plans["current_active_plans"].first
        sub_plan_type = first_plan_det["plan_title"]
        sub_exp_dt = first_plan_det["valid_till"].split("T")[0]
        sub_dt = first_plan_det["start_date"].split("T")[0]
        if account_resp["data"]["active_plans"].include?("all_access_pack")
          user_plans = Ott.user_plans(user_id,lang)
          act_pln = user_plans["current_active_plans"].collect{|x|x["plan_title"].downcase if x["plan_categories"].include?("all_access_pack")}.compact
          if !act_pln.empty? && (act_pln.include?("monthly") || act_pln.include?("yearly"))
            all_pck_st = "true" 
             if act_pln.include?("monthly")
              all_mnt_st = "true"
             end
             if act_pln.include?("yearly")
              all_yr_st = "true"
             end
          end
        end
      end
      req_dt = {
      status: true,
      user_id: user_id,
      user_name: default_profile['firstname'],
      user_profiles: all_profiles,
      profile_id: user_login_id,
      login_id: user_log_id,
      user_parental_control: account_resp["data"]["parental_control"],
      pin: account_resp["data"]["parental_pin"],
      profile_type: "non_kids",
      user_analytic_id: account_resp["data"]["user_id"],
      user_status: user_st, 
      city_of_user: city,
      device_limit: device_limit.to_s, 
      first_name: user_name, 
      mob_num: mobile_num, 
      email_id: email_id,
      first_time_user: response['data']['first_time_login'],
      sub_pl_ty: sub_plan_type,
      sub_expi_date: sub_exp_dt,
      sub_date:  sub_dt,
      user_all_ac_pk: all_pck_st,
      user_all_mnth_pck: all_mnt_st,
      user_all_yr_pck: all_yr_st
    }
    render json: req_dt
   	# render json: {status: true,user_id: "#{response["data"]["messages"][0]["session"]}",user_name: first_profile[1],user_profiles: all_profiles,profile_id: first_profile[0],:login_id => login_id,user_parental_control: user_login_profile["data"]["parental_control"],pin: user_login_profile["data"]["parental_pin"],:profile_type => "non_kids",:analytics_user_id => user_login_profile['data']['user_id'], :verify_otp_message => response['welcome_msg'], :free_trail=> response['free_trail'],:user_analytic_id => user_login_profile['data']['user_id']}
   else
    set_response(response)
   end
    rescue Exception => e
     render json: {status: false,error_message: "sorry something went wrong" }
	   logger.info e.message
	 end
 end

 def mobile_no_signup
    begin
      data_params = {
        user_info: {
          user_id: params[:mobile_no].gsub("+",""),
          firstname: "Guest",
          region: "IN",
          type: "msisdn"
        }
      }
      p data_params.inspect
      response = User.mobile_signup(data_params)
      render json: {status: true,response: response}
    rescue Exception => e
      render json: {status: false,error: "sorry something went wrong" }
      logger.info e.message   
    end
 end

 def resend_otp
  begin
  	 resend_params = {
	    :user => {
	    :email_id => params[:mobile_no].gsub("+",""),
	    :type => "msisdn"
	    }
	  }
    p resend_params.inspect
   response = User.resend_otp(resend_params) 
   set_response(response)
   rescue Exception => e
    render json: {status: false,error: "sorry something went wrong" }
	  logger.info e.message
	end
 end

 def sign_out
 	begin
    response = User.sign_out(cookies[:user_id])
    rescue Exception => e
      logger.info e.message
    end
    render json: {error_message: "",status: true }
 end


def login
 if cookies[:user_id].present?
    home_url = "#{SITE}"
    if cookies[:shm_sel_lang].present? && cookies[:shm_sel_lang] != "en"
      home_url = "#{SITE}/#{cookies[:shm_sel_lang]}"
    end
    redirect_to home_url
 end
end

def register
  if cookies[:user_id].present?
   redirect_to "#{SITE}"
  end
end


 def my_account

 end

 def subscriptions

 end

 def edit_profile
  user_profile = User.get_user_profile(params[:profile_id],cookies[:user_id])
  @edit_profile = user_profile["data"]["profile"]
 end

def manage_profiles
  config_resp = Rails.cache.fetch("design_configuration_list", expires_in: CACHE_EXPIRY_TIME) {
    Ott.get_configuration      
  }
  @profiles_limit = config_resp["data"]["params_hash2"]["config_params"]["profile_limit"]
  if @profiles_limit.nil? 
    @profiles_limit = 5
  end
  profile_response = User.get_all_user_profiles(cookies[:user_id])
  @user_profiles = profile_response["data"]["profiles"]

end

def account_details
  if request.post?
    user_details_resp = User.get_user_account_details(cookies[:user_id])
    render :json => {:status => true,:parental_control => user_details_resp["data"]["parental_control"],:pin => user_details_resp["data"]["parental_pin"]}
  else
    user_details_resp = User.get_user_account_details(cookies[:user_id])
    @user_details = user_details_resp["data"]
    @parental_pin = @user_details["parental_pin"]
    @parental_control = @user_details["parental_control"]
    logger.info @user_details
  end
end

def update_profile
  begin
    update_profile_params = {
      :user_profile => {
      :firstname => params[:name],
      :child => params[:is_child]
     }
  }
  update_profile_response = User.update_profile(cookies[:user_id],params[:profileid],update_profile_params)
  all_profiles,default_profile_name = get_user_all_profiles(cookies[:user_id])
  render json: {:status => true,:user_profiles => all_profiles,:profile_name => default_profile_name}
  rescue
    render json: {:status => false,:error_message => "sorry something went wrong"}
  end
end

def delete_profile
  begin
    delete_profile_response = User.delete_profile(cookies[:user_id],params[:profileid])
    all_profiles,default_profile_name = get_user_all_profiles(cookies[:user_id])
    render json: {:status => true,:user_profiles => all_profiles}
  rescue
    render json: {:status => false,:error_message => "sorry something went wrong"}
  end
end

def update_personal_details
  if request.post?
    user_params = {
      :user => {
        :firstname => params[:profile_name], 
        :birthdate => params[:date_of_birth],
      }
    }
    if $region == "IN" || $region == "BD"
      user_params[:user][:user_email_id] = params[:email_id]
    else 
      user_params[:user][:mobile_number] = params[:mobile_no]
    end
    p cookies[:email_id].inspect
    p user_params.inspect
    update_user_resp = User.update_account_details(cookies[:user_id],user_params)
    p update_user_resp.inspect
    @user_profile_details =update_user_resp["data"]
    render json: {:status => true}
  else
    lang = get_user_lang(params)
    user_plans = Ott.user_plans(cookies[:user_id],lang)
    current_plans = user_plans['current_active_plans']
    @plan_categories = current_plans.collect{|x|x['plan_categories'][0]}.join(",")
    expired_plans = user_plans['previous_plans']
    @expired_categories = expired_plans.collect{|x|x['plan_categories'][0]}.join(",")
    if @plan_categories.present?
      @subscribed_status = "1"
    else
      @subscribed_status = "0"
      @plan_categories = "NA"
    end
    if @expired_categories.empty?
      @expired_categories = "NA"
    end
    user_details_resp = User.get_user_account_details(cookies[:user_id])
    @user_profile_details = user_details_resp["data"]
 end
end

def change_password
  if request.post?
    password_params = {
      :user => {
        :current_password => params[:old_password],
        :password => params[:new_password],
        :confirm_password => params[:confirm_new_password],
        :region => $region
      }
    }
    update_user_password = User.change_password(cookies[:user_id],password_params)
    if update_user_password.has_key?("data")
      render json: {error_message: "",status: true }
    else
      set_response(update_user_password)
    end
  end
end
def forgot_password
  if request.post?
    forgot_password_params = {
      :user => {
        :region => $region,
      }
    }
    # if $region == "IN"
    #   forgot_password_params[:user][:user_id] = "91"+params[:mobile_number]
    #   forgot_password_params[:user][:type] = "msisdn"
    # else
      forgot_password_params[:user][:user_id] = params[:email_id]
      forgot_password_params[:user][:type] = "email"
    # end
    forgot_pass_response = User.forgot_pass(forgot_password_params)
    if !forgot_pass_response.has_key?("error")
      render json: {:status => true}
    else
     set_response(forgot_pass_response) 
    end
  end
end
def forgot_password_otp_verify
  if request.post?
    forgot_password_otp_params = {
      :user => {
        :key => params[:otp],
        :region => $region,
        :mobile_number => "91"+params[:mobile_no]
      }
    }
    p forgot_password_otp_params.inspect
    begin
      fp_response = User.fp_verify_otp(forgot_password_otp_params)
      p fp_response.inspect
      p "@@@@@@@@@@@@@@@@@@@@@2"
      p fp_response.inspect
      if fp_response.has_key?("data")
        render json: {status: true}
      else
        set_response(fp_response)
      end
      rescue Exception => e
      render json: {status: false,error_message: "sorry something went wrong" }
      logger.info e.message
    end
  end
end


def reset_password
  if request.post?
    reset_password_params = {
      :user => {
        :key => params[:otp],
        :password => params[:password],
        :confirm_password => params[:confirm_password],
        :region => $region,
      }
    }
    if $region == "IN"
      reset_password_params[:user][:type] = "msisdn" 
    else
      reset_password_params[:user][:type] = "email"
    end
    reset_password = User.reset_pass(reset_password_params)
    p reset_password.inspect
    if reset_password.has_key?("data")
      render json: {:status => true}
    else
      set_response(reset_password)
    end
  end
end


def reset_password_email
  if request.post?
    reset_params = {
        :user => {
        :key => params[:key],
        :password => params[:password],
        :confirm_password => params[:confirm_password],
        :region => $region
        }
      }
     reset_password = User.reset_password_email(reset_params)
    p reset_password.inspect
    if reset_password.has_key?("data")
      render json: {:status => true}
    else
      set_response(reset_password)
    end
  end
end


def add_profile
  lang = get_user_lang(params)
  user_plans = Ott.user_plans(cookies[:user_id],lang)
  current_plans = user_plans['current_active_plans']
  @plan_categories = current_plans.collect{|x|x['plan_categories'][0]}.join(",")
  expired_plans = user_plans['previous_plans']
  @expired_categories = expired_plans.collect{|x|x['plan_categories'][0]}.join(",")
  if @plan_categories.present?
    @subscribed_status = "1"
  else
    @subscribed_status = "0"
    @plan_categories = "NA"
  end
  if @expired_categories.empty?
    @expired_categories = "NA"
  end
  if request.post?
    profile_params = {
      :user_profile => {
        :firstname => params[:profile_name],
        :child => params[:kids_profile]
      }
    }
    profile_resp = User.add_profile(cookies[:user_id],profile_params)
    p profile_resp.inspect
    all_profiles,default_profile_name = get_user_all_profiles(cookies[:user_id])
     if profile_resp.has_key?("data")
      render json: {:status => true,:user_profiles => all_profiles}
    else
      set_response(profile_resp)
    end
    
  end
end

def remote_profile
  begin
    profile_resp = User.get_user_profile(params[:profile_id],cookies[:user_id])
     remote_profile_data  = {
      :user_profile => {
        :profile_id => params['profile_id']
      }
    }
    assign_profile_resp = User.assign_profile(cookies[:user_id],remote_profile_data)
    p assign_profile_resp.inspect
  rescue
  end
  if (profile_resp["data"]["profile"].present? && profile_resp["data"]["profile"]["child"] == false)

    url = "#{SITE}/?profile_id=#{params[:profile_id]}&name=#{profile_resp["data"]["profile"]["firstname"]}&profile_type=non_kids"
    url = "#{SITE}/#{params[:lang]}?profile_id=#{params[:profile_id]}&name=#{profile_resp["data"]["profile"]["firstname"]}&profile_type=non_kids" if params[:lang]

    #url = "/?profile_id=#{params[:profile_id]}&name=#{profile_resp["data"]["profile"]["firstname"]}&profile_type=non_kids"
  else
    url = "#{SITE}/kids?profile_id=#{params[:profile_id]}&name=#{profile_resp["data"]["profile"]["firstname"]}&profile_type=kids"
    url = "#{SITE}/#{params[:lang]}/kids?profile_id=#{params[:profile_id]}&name=#{profile_resp["data"]["profile"]["firstname"]}&profile_type=kids" if params[:lang]

    #url = "/kids?profile_id=#{params[:profile_id]}&name=#{profile_resp["data"]["profile"]["firstname"]}&profile_type=kids"
  end


  redirect_to url
end

def activate_code
  if request.post?
    p params[:code].inspect
    code_params = {
      :user => {
        :token => params[:code].downcase,
        :session_id => cookies[:user_id]
      }
    }
    p code_params.inspect
    response = User.activate_tv_code(code_params)
    p response.inspect
    if response.has_key?("message")
      render :json => {:status => false,:message => response["message"]}
    else
      render :json => {:status => true}
   end
  else
    p cookies[:user_id].inspect
    if !cookies[:user_id].present?
      tv_url = "#{SITE}/users/login?q=tv"
      if cookies[:shm_sel_lang].present? && cookies[:shm_sel_lang] != "en"
       tv_url = "#{SITE}/#{cookies[:shm_sel_lang]}/users/login?q=tv"
      end
      redirect_to tv_url
    end
  end
end

def clear_watchhistory
  clear_watchhistory_resp = User.delete_watch_history(cookies[:user_id])
  render :json => {:status => true}
end

def verify_password
  if request.post?
    begin
      pwd_params = {
         :user => {
            :password => params[:password]
         }
      }
      #pwd_resp = User.verify_password(cookies[:user_id],params[:password])
      pwd_resp = User.user_verify_password(cookies[:user_id],pwd_params)
      p pwd_resp.inspect
      if pwd_resp.has_key?("data") && params[:parent_control] == "true"
        pin_data_params = {
          :user => {
            :parental_control => "false",
            :parental_pin => ""
          }
        }
        pin_resp = User.create_pin(cookies[:user_id],pin_data_params)
        p pin_resp.inspect
      end
      set_response(pwd_resp)
    rescue
      render json: {status: false,error_message: "sorry something went wrong" }
    end
  end
end

def create_pin
  if request.post?
  begin
    pin_data_params = {
      :user => {
        :parental_control => "true",
        :parental_pin => params[:pin]
      }
    }
    pin_resp = User.create_pin(cookies[:user_id],pin_data_params)
    p pin_resp.inspect
    set_response(pin_resp)
  rescue
    render json: {status: false,error_message: "sorry something went wrong" }
  end
else
  user_account = User.get_user_account_details(cookies[:user_id])
  p user_account.inspect
  end
end

def who_is_watching
  unless cookies[:user_id].present?
    redirect_to "#{SITE}/users/login"
  else
  profile_response = User.get_all_user_profiles(cookies[:user_id])
  @user_profiles = profile_response["data"]["profiles"]
end
end

def who_is_watching_register
  profile_response = User.get_all_user_profiles(cookies[:user_id])
  @user_profiles = profile_response["data"]["profiles"]
end

def verify_pin
  user_account_resp = User.get_user_account_details(cookies[:user_id])
  @user_details = user_account_resp["data"]
end

def player_persistant
  begin
    time = params[:time].split(":")
    time1 = 
    persistant_params = {
      :listitem => {
        :catalog_id => params[:catalog_id],
        :content_id => params[:content_id],
        :play_back_time => params[:time]

      }
    }
    persistnat_resp = User.send_persistant_playbacktime(cookies[:user_id],persistant_params)
    p persistnat_resp.inspect
    render :json => {:status => true}
  rescue
    render :json => {:status => false}
  end
end

def add_watch_later
  begin
    playlist_id = ""
    if params[:playlist_id].present?
      resp = User.delete_user_watchlater(cookies[:user_id],params[:playlist_id])
    else
      watchlater_params = {
        :listitem => {
          :catalog_id => params[:catalog_id],
          :content_id => params[:content_id]
        }
      }
      watchlater_resp = User.add_watch_later(cookies[:user_id],watchlater_params)
      playlist_id = watchlater_resp["data"].first["listitem_id"]
    end
    status = true
  rescue
    status = false
  end
   render :json => {:status => status,:playlist_id => playlist_id}
end

def continue_watching_list
  lang = get_user_lang(params)
  user_plans = Ott.user_plans(cookies[:user_id],lang)
  current_plans = user_plans['current_active_plans']
  if current_plans.empty?
    subscribed_status = "0"
    plan_categories = "NA"
  else
    subscribed_status = "1"
    plan_categories = current_plans.collect{|x|x['plan_categories'][0]}.join(",")
  end
  expired_plans = user_plans['previous_plans']
  expired_plans.empty? ? expired_categories = "NA" : expired_categories = expired_plans.collect{|x|x['plan_categories'][0]}.join(",") 
  continue_watching_resp = User.get_continue_watch_list(cookies[:user_id])
  continue_watching_list = continue_watching_resp["data"]["items"]
  unless continue_watching_list.empty?
     all_items = continue_watching_list.collect{|x|(x["ml_title"].present? ? x["ml_title"]:x["title"])+"$"+(x["ml_item_caption"].present? ? x["ml_item_caption"] : x["item_caption"])+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+(get_lang_url(x['seo_web_url'])+"?time="+(x["play_back_time"].split(':').map{ |a| a.to_i }.inject(0) { |a, b| a * 60 + b}).to_s)+"$"+x["listitem_id"]+"$"+x["total_percentage"].to_s+"$"+get_premium_tag(x)+"$"+(get_premium_tag(x) == "false" ? "display:none" : "")+"$"+x["content_definition"]+"$"+x["access_control"]["premium_tag"].to_s}
  else
    all_items = []
  end
  render :json => {:status => true,:items => all_items, :sub_status => subscribed_status, :present_plans => plan_categories, :exp_plans => expired_categories}
end

def continue_watching
  page_size = 20
  if params[:page_no].present?
    page_number = params[:page_no].to_i
  else
    page_number = 0
  end
  p page_number.inspect
  next_page = "true"
  if request.post?
    if params[:page_no].present?
      continue_watching_response = User.get_continue_watch_with_pagination(cookies[:user_id], page_number,page_size)
      continue_watching_list = continue_watching_response["data"]["items"].collect{|x|x["title"]+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+get_duration_time(x['duration_string'])+"$"+x["listitem_id"]+"$"+(x['seo_web_url']+"?time="+(x["play_back_time"].split(':').map{ |a| a.to_i }.inject(0) { |a, b| a * 60 + b}).to_s)+"$"+x['access_control']['is_free'].to_s}
      total_items = continue_watching_response["data"]["total_items_count"]
      p total_items.inspect
      total_pages = (total_items/page_size).round
      if page_number+1 > total_pages
        next_page = "false"
      end
      render :json => {:status => true, :items => continue_watching_list, :next_page => next_page, :pageno => page_number+1}
    else
      user_delete_watch_resp = User.delete_user_continue_watching(cookies[:user_id],params[:playlist_id])
      p user_delete_watch_resp.inspect
      continue_watching_resp = User.get_continue_watch_list(cookies[:user_id])
      item_cnt = continue_watching_resp["data"]["items"].count.to_i
      render :json => {:status => true,:items_count => item_cnt}
    end
  else
    begin 
      continue_watching_responce = User.get_continue_watch_with_pagination(cookies[:user_id], page_number,page_size)
      @continue_watching_list = continue_watching_responce["data"]["items"]
      @total_items = continue_watching_responce["data"]["total_items_count"]
      if @continue_watching_list.count.to_i > 20
        next_page = "true"
      else
        next_page = "false"
      end

      #all_playlists_resp = User.get_all_playlists_v2(cookies[:user_id])
      #profile_watch_history_playlist_id = all_playlists_resp["data"]["items"].collect{|x|x['playlist_id'] if (x["playlist_type"].downcase.gsub(" ","") == "watchhistory" && x["profile_id"] == cookies[:profile_id] )}.compact
      #profile_playlist_items_response = User.get_profile_playlist_items(cookies[:userid], profile_my_favourites_playlist_id[0].to_i)
    rescue Exception => e
      @continue_watching_list = []
    end
 end
end


def user_recommendation_list
  lang = get_user_lang(params)
    user_plans = Ott.user_plans(cookies[:user_id],lang)
    current_plans = user_plans['current_active_plans']
    if current_plans.empty?
      subscribed_status = "0"
      plan_categories = "NA"
    else
      subscribed_status = "1"
      plan_categories = current_plans.collect{|x|x['plan_categories'][0]}.join(",")
    end
    expired_plans = user_plans['previous_plans']
    expired_plans.empty? ? expired_categories = "NA" : expired_categories = expired_plans.collect{|x|x['plan_categories'][0]}.join(",") 
    user_recommend_resp = User.get_user_recommendation_list(cookies[:user_id])
    if user_recommend_resp["data"].present?
      user_recommend_list = user_recommend_resp["data"]["items"]
      unless user_recommend_list.empty?
         all_items = user_recommend_list.collect{|x|(x["ml_title"].present? ? x["ml_title"]:x["title"])+"$"+(x["ml_item_caption"].present? ? x["ml_item_caption"] : x["item_caption"])+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+ get_lang_url(x['seo_web_url'])+"$"+get_premium_tag(x)+"$"+(get_premium_tag(x) == "false" ? "display:none" : "")+"$"+(x["content_definition"].present? ? x["content_definition"]: "") +"$"+x["access_control"]["premium_tag"].to_s}
      else
        all_items = []
      end
    else
      all_items = []
    end
    render :json => {:status => true,:items => all_items, :sub_status => subscribed_status, :present_plans => plan_categories, :exp_plans => expired_categories}
end

def user_recommendation
  page_size = 20
  if params[:page_no].present?
    page_number = params[:page_no].to_i
  else
    page_number = 0
  end
  p page_number.inspect
  next_page = "true"
  if request.post?
    if params[:page_no].present?
      user_recommend_resp = User.get_user_recommendation_list_with_pagination(cookies[:user_id], page_number,page_size)
      user_recommend_list = user_recommend_resp["data"]["items"].collect{|x|x["title"]+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+(x['duration_string'].present? ? get_duration_time(x['duration_string']): "") +"$"+get_lang_url(x['seo_web_url'])+"$"+x['access_control']['is_free'].to_s}
      total_items = user_recommend_resp["data"]["total_items_count"]
      p total_items.inspect
      total_pages = (total_items/page_size).round
      if page_number+1 > total_pages
        next_page = "false"
      end
      render :json => {:status => true, :items => user_recommend_list, :next_page => next_page, :pageno => page_number+1}
    end
  else
    begin 
        user_recommend_resp = User.get_user_recommendation_list_with_pagination(cookies[:user_id], page_number,page_size)
        @user_recommended_list = user_recommend_resp["data"]["items"]
        @total_items = user_recommend_resp["data"]["total_items_count"]
        if @user_recommended_list.count.to_i > 20
          next_page = "true"
        else
          next_page = "false"
        end
    rescue Exception => e
        @user_recommended_list = []
    end
  end

end



def user_all_lists
  watch_later_status = false
  listitem_id = ""
  play_back_time = "00:00:00"
  watched_video = false
    @md5_key,@time = get_md5_key(params[:catalog_id],params[:content_id],params[:content_def])
    list_params = {
      :catalog_id => params[:catalog_id],
      :content_id  => params[:content_id],
      :category  =>  params[:category],
      :content_definition  =>  params[:content_def],
      :md5  =>   @md5_key,
      :ts  =>  @time.to_s,
      :user_agent  =>  Browser.new(request.env["HTTP_USER_AGENT"], accept_language: "en-us").platform.send(:ua),
      :ip  =>  $user_ip,
      :id => cookies[:user_id]? cookies[:user_id] : "",
      :platform_type => "web"
      }
      logger.info "--user params-----#{list_params}"
      p list_params.inspect

      p '$$$$$$$$$'
    watchlist_resp = User.get_user_watch_details(list_params)
    logger.info "--user details-----#{watchlist_resp}"
    if watchlist_resp["data"]
      all_watchlist_resp = watchlist_resp["data"]["playlists"] 
      unless all_watchlist_resp.empty? 
        watch_later_resp = all_watchlist_resp.collect{|w|w if(w["name"].gsub(" ","").downcase == "watchlater")}.compact
        unless watch_later_resp.empty?
          watch_later_status = true
          listitem_id = watch_later_resp.first["listitem_id"]
        end
        watchhistory_resp = all_watchlist_resp.collect{|w|w if(w["name"].gsub(" ","").downcase == "watchhistory")}.compact
        unless watchhistory_resp.empty?
          play_back_time = watchhistory_resp.first["pos"].split(':').map{ |a| a.to_i }.inject(0) { |a, b| a * 60 + b}
        end
      end
    
      p watchlist_resp.inspect
      p "@@@@@@@@@@@@@@@"
      p play_back_time.inspect
      user_info = watchlist_resp["data"]["user_info"]
      pack_name = user_info["user_pack_name"]
      plan_type = user_info["user_plan_type"]
      # if user_info['user_plan_info'].blank?
      #   pack_name = "NA"
      #   plan_type = "NA"
      # else
      #   pack_name = user_info['user_plan_info'][0]['pack name']
      #   plan_type = user_info['user_plan_info'][0]['plan_type']
      #  end
      p user_info["analytics_user_id"]
      p "!!!!!!!!!!!!!!!!!!!!!!!"

      
        sub_tit_url = ""
    if (watchlist_resp['data'].has_key?("subtitles") && watchlist_resp['data']["subtitles"].length != 0)
     sub_tit_url = watchlist_resp['data']["subtitles"].collect{|x| x["url"] if x["tag"] == "without logo" && x["language"] == "english"}.compact.first
    end
      @new_play_url,@key = ""
      @new_play_url,@key = encrypt_play_url(watchlist_resp['data']['adaptive_url'],"") if watchlist_resp['data']['adaptive_url'].present?
      @ios_play_url,@ios_key = ""
      @ios_play_url,@ios_key = encrypt_play_url(watchlist_resp['data']['ios_play_url'],"") if watchlist_resp['data']['adaptive_url'].present?
      render :json => {:watch_later_st => watch_later_status,:status => true,:list_id => listitem_id,:user_play_back_time => play_back_time.to_i,:user_sub_status => watchlist_resp['data']['is_subscribed'].to_s,:age => user_info["age"],:gender => user_info["gender"],:user_state => user_info["user_state"],:user_period => user_info['user_period'],:user_pack_name => pack_name,:user_plan_type => plan_type,:analytic_user_id => user_info["analytics_user_id"], :new_play_url => @new_play_url, :key => @key, :stream_key => watchlist_resp['data']['stream_key'], :subtitle => sub_tit_url, :ios_play_url => @ios_play_url, :ios_key => @ios_key,:subscribe_chk => watchlist_resp['data']['subscribe_button'].to_s, :subscribe_text => watchlist_resp['data']['subscribe_text'].to_s, :last_episode => watchlist_resp['data']['last_episode'].to_s}
    else
      render :json => {:status => false}
    end
  end

def watch_list
  page_size = 20
    if params[:page_no].present?
      page_number = params[:page_no].to_i
    else
      page_number = 0
    end
    p page_number.inspect
    next_page = "true" 

    user_profile_details = User.get_user_profile(cookies[:profile_id],cookies[:user_id])
    @is_child_profile = user_profile_details['data']['profile']['child']
  if request.post?
    if params[:page_no].present?         
      watch_list = User.get_user_watch_list_with_pagination(cookies[:user_id],page_number,page_size)     
      watch_list_items = watch_list["data"]["items"].collect{|x|(x["ml_title"].present? ? x["ml_title"]: x["title"])+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+get_duration_time(x['duration_string'])+"$"+x["listitem_id"]+"$"+get_continue_item_url(x)+"$"+x['access_control']['is_free'].to_s}
      total_items_count = watch_list["data"]["total_items_count"] 
      no_of_pages = (total_items_count/page_size).round 
      p no_of_pages.inspect
      if page_number+1 > no_of_pages
        next_page = "false"
      end 
      render :json => {:status => true,:items => watch_list_items,:next_page => next_page,:pageno => page_number+1}
    else 
      resp = User.delete_user_watchlater(cookies[:user_id],params[:playlist_id])
      wathlater_list = User.get_user_watch_list(cookies[:user_id])
      items = wathlater_list["data"]["items"]
      render :json => {:status => true,:items_count => items.count.to_i}
    end
  else
    begin
      watch_list = User.get_user_watch_list_with_pagination(cookies[:user_id],page_number,page_size)   
      @watch_list_items = watch_list["data"]["items"]
      @total_items_count = watch_list["data"]["total_items_count"] 
      no_of_pages = (@total_items_count/page_size).round       
      if @total_items_count > 20
        next_page = "true"
      end
    rescue Exception => e
      @watch_list_items = []
    end
  end
end

def encrypt_play_url(url,sub_url)
    new_url = ""
    aes = OpenSSL::Cipher::Cipher.new('AES-128-CBC')
    aes.encrypt
    key = aes.random_key
    aes.key = key
    e_key = Base64.encode64(key).gsub(/\n/, '')
    encrypted = aes.update(url) + aes.final
    new_url = Base64.encode64(encrypted).gsub(/\n/, '')
    return new_url,e_key,sub_url
  end

def settings
  user_profile = User.get_user_account_details(cookies[:user_id])
  user_profile_details = User.get_user_profile(cookies[:profile_id],cookies[:user_id])
   @is_child_profile = user_profile_details['data']['profile']['child']
  p "2222222222222"
  @parent_control = user_profile["data"]["parental_control"]
end


def check_valid_user
  unless cookies[:user_id].present?
    redirect_to "#{SITE}/users/login"
  else
    response = User.get_user_account_details(cookies[:user_id])
    if response.has_key?("error")
      redirect_to "#{SITE}?session_error=true"
    end
  end
end

def check_valid_user_session
    stat = "true"
    check_user_region = "true"
    begin
      response = User.get_user_account_details(cookies[:user_id])
      p response["data"]["user_region"],$region,"$"*100
      if response["data"]["user_region"] != $region
        check_user_region = "false"
         User.sign_out(cookies[:user_id])
      end
      if response.has_key?("error") && response["message"] = "Invalid Session Id"
        response1 = User.sign_out(cookies[:user_id])
         stat = "false"
      end
    rescue
       stat = "false"
    end
    render :json => {:status => stat,:reg_loc_use=> check_user_region}
end

def otp_verify
  render :layout => false
end
def registered_devices
  @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
  user_id = params[:user_id].present? ? params[:user_id] : cookies[:user_id]
  @registered_devices = User.get_registered_devices(user_id)
  p @registered_devices['data'].inspect
  p "..........................."    
  if request.post?
    if params[:device_session_id].present?
      device_params = {
       :user_session_id => params[:device_session_id]
      }
    end
    response = User.registered_devices_delete(user_id,device_params)
    p response['data']['message'].inspect
    render :json => {status: true, device_count: response['data']['session_count'].to_s}
  end
end

def validate_promocode
  coupon_params = {
    :coupon_code => params[:coupon].upcase,
    :region =>   params[:region].blank? ? "US" : params[:region],
    :plans_info => "false"
  }
  coupon_resp = User.validate_coupon(coupon_params)
  logger.info "======================Coupon data here=======#{coupon_params.inspect}==================="
  logger.info "==== UAE Region Backend response ========================#{coupon_resp.inspect}"
  logger.info "^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^"
  # p coupon_resp.inspect
  # p message.inspect
  if coupon_resp.has_key?("data")
    message = coupon_resp["data"]["coupon_info"]["message"]
    render :json => {status: true,:coupon_id => coupon_resp['data']['coupon_info']['coupon_id'],:coupon_info => coupon_resp['data'], coupon_message: message}
  else
    render :json => {status: false,:error_message => coupon_resp['error']['message']}
  end
end

def get_location_details

end

def reset_pin
  account_details
end

def remove_pin
   if request.post?
  begin
      pin_data_params = {
          :user => {
            :parental_control => "false",
            :parental_pin => ""
          }
        }
        pin_resp = User.create_pin(cookies[:user_id],pin_data_params)
        p pin_resp.inspect
      render json: {status: true,data: pin_resp["data"]["message"]}
    rescue
      render json: {status: false,error_message: "sorry something went wrong" }
    end
 else
    account_details
 end
end

 def forgot_pin_email
   if request.post?
    begin
     pin_params = {
         :user => {
            :email_id => params[:email_id],
            :session_id  => cookies[:user_id],
            :region  =>$region
         }
      }
      pin_resp = User.forgot_pin_mail(pin_params)
      p pin_resp.inspect
      render json: {status: true}
    rescue
      render json: {status: false,error_message: "sorry something went wrong" }
    end
   end
end



def validate_lzy_mobile
  # params[:mobile] = "9136920201"
  # params[:email] = "revenue@shemaroome.com"
  # params[:amt] = "251.00"
  tarns_elg_ref_id = ""
  lzpy_resp = lazpy_eligibil_check(params[:mobile],params[:email],params[:amt],params[:plan_catgry].upcase,LZYPY_BASE_URL,LZYPY_ACCESS_KEY,LZYPY_SECRET_KEY,"true")
 p lzpy_resp.inspect
  if lzpy_resp["code"] == "LP_ELIGIBLE"
     if lzpy_resp["sub_err_code"].present?
      dt = {:status => "false",:error_msg => lzpy_resp["sub_req_er_msg"]}
    else
     #:elgres_id=>\"a05147bf-4054-d9fc-068a-87b8a4749861\", :subreq_id=>\"79dd994e-e85b-457d-81a5-906fde9898ae\", :tranref_id=>\"TFBUWDEwMDk1NTU2Ng==\", :code=>\"LP_ELIGIBLE\"
    dt = {:status => "true",:elg_id => lzpy_resp["elgres_id"],:sub_req_id => lzpy_resp["subreq_id"],:trans_ref_id => lzpy_resp["tranref_id"]}
    end
  else
    if lzpy_resp.has_key?("reason")
      msg = lzpy_resp["reason"]
      p msg.inspect
      if lzpy_resp.has_key?("userEligibility") && lzpy_resp["userEligibility"] == false
        msg = "ops!! We don't seem to have LazyPay open for you as of now. But we promise to throw it open to you soon"
       end
      dt = {:status => "false",:error_msg => msg}
    else
       dt = {:status => "false",:error_msg => lzpy_resp["message"]}
    end
  end
   render :json => dt
end

def lazpay_otp_valid
  #lp_sub_id,mobile_no,mob_otp,base_url,acc_key,sec_key
  lazy_pay_req = sub_otp_valid_lazypay(params[:sub_id],params[:mobileno],params[:plan_price],params[:lzpyotp],LZYPY_BASE_URL,LZYPY_ACCESS_KEY,LZYPY_SECRET_KEY)
  #p lazy_pay_req.inspect
  p "@@@@@@@@@@@@@@@@@@@"
  #lazy_pay_req = otp_valid_lazypy(params[:trans_id],params[:lzpyotp],LZYPY_BASE_URL,LZYPY_ACCESS_KEY,LZYPY_SECRET_KEY)
  if !lazy_pay_req.has_key?("errorCode") 
    #&& lazy_pay_req["responseData"]["OTP"]["status"] == "SUCCESS"
   lazy_pay_req["suscription_id"] = params[:sub_id]
   lazy_pay_req["order_id"] = params[:order_id]
   lazy_pay_req['status'] = "SUCCESS"
   p lazy_pay_req.inspect
    response =  User.secure_payment("lazypay",lazy_pay_req)
    otp_resp = {:status => "true",:trans_status => "success" ,:plan_title => response["data"]["title"],:pln_exp_dt => response["data"]["expiry_date"]}
  else
    otp_resp = {:status => "false",:trans_status => "failure"}
  end
  render json: otp_resp
end

def lzypay_rsd_otp
  lazypay_resend_otp = sub_create_lazypy(params[:elg_id],params[:mobileno],params[:email_id],params[:amt],params[:category].upcase,LZYPY_BASE_URL,LZYPY_ACCESS_KEY,LZYPY_SECRET_KEY)
  if !lazypay_resend_otp.has_key?("error")
    re_st = {:status => "true",:elg_id => params[:elg_id],:sub_req_id => lazypay_resend_otp["subscriptionId"],:trans_ref_id => ""}
  else
   re_st = {:status => "false"}
  end
  render json: re_st
end


  def branch_s2s
    ActionController::Parameters.permit_all_parameters = true
    ord_params = {
      :order_id => params["data"]["event_data"]["transaction_id"]
    }
    if Rails.env.production? 
        branch_log = Logger.new('/mnt/branch_events.log')
        branch_log.info("=====================Start of Branch s2s call parameters logs=======================")
        branch_log.info("Server Parameters from Branch data ----------------------> #{params.inspect} <--------------------------------")
        branch_log.info("=====================End of Branch s2s call parameters logs=======================")
    end
    #ord_resp = User.validate_order_id(ord_params)
     http_headers = { "Content-Type" => "application/json", "Accept"=>"application/json" }
     ord_check = Typhoeus::Request.post("http://18.209.25.40/validate_order_id?region=#{$region}&auth_token=#{AUTH_TOKEN}", :body => ord_params.to_json,:headers => http_headers, :ssl_verifypeer => false)
     ord_resp = JSON.parse(ord_check.body)
     p ord_resp.inspect
    if ord_resp.has_key?"data"
      if params["data"]["platform"] == "web" || params["data"]["user_data"]["os"] == "Ios"
        p "!!!!!!!!!!!!!"
       params["data"]["event_data"]["revenue"] = params["data"]["event_data"]["revenue"].to_f
      end
      if params["data"]["user_data"]["os"] == "Android"
        unless params["data"]["custom_data"].has_key?("productName")
          params["data"]["custom_data"]["productName"] = params["data"]["event_data"]["pack_name"]
        end
      end
      #BranchJob.set(wait: 1.second).perform_now(params)
      #br_bkd_resp =   User.branch_bkd_dt(params)
      logger.info "headers :: #{request.headers["HTTP_X_FORWARDED_FOR"].inspect}}"
      user_ips = request.headers["HTTP_X_FORWARDED_FOR"]
      user_ip = ""
      user_ip = user_ips.split(",").first unless user_ips.nil?
      #BranchJob.set(wait: 1.second).perform_now(params, user_ip)
       #br_bkd_resp =   User.branch_bkd_dt(params)
     response =  branch_post_reponse(params, user_ip)

     if (JSON.parse(response).has_key?("error"))
      if Rails.env.production? 
        branch_log = Logger.new('/mnt/branch_events.log')
        branch_log.info("=====================Start of Branch event Logs=======================")
        branch_log.info("Parameters from Branch data ----------------------> #{params.inspect} <--------------------------------")
       
      end
     else
      params["user_ip"] = user_ip
      bkd_pos_url = Typhoeus::Request.post("http://18.209.25.40/post_event_data?region=#{$region}&auth_token=#{AUTH_TOKEN}", :body => params.to_json,:headers => http_headers, :ssl_verifypeer => false)
      br_bkd_resp = JSON.parse(bkd_pos_url.body)
      logger.info "Post event data :: #{br_bkd_resp.inspect}"
       if Rails.env.production? 
        branch_log = Logger.new('/mnt/branch_events.log')
        branch_log.info("=====================Branch Back end response here #{br_bkd_resp.inspect}=======================")
        branch_log.info("=====================End of Branch event Logs=======================")
    end
    end
    end
    logger.info "=====================#{ord_resp.inspect}============================"
    render :json => ord_resp
  end


  def branch_post_reponse(params,user_ip)
     logger.info "-----------------Branch Api Call----------------"
     logger.info "data :: #{params.inspect}, user_ip :: #{user_ip}"
     data = params["data"]
     if Rails.env.production?
      data["branch_key"] = "key_live_flJg9QCXB43vk5MY1qhmfahmqFfYLPFF"
     else
       data["branch_key"] = "key_test_ilJk8Kq7A52Ai8J09DkMolidCujWPMsR"
     end
     
     data["user_data"]["http_origin"] = "c65f433f-a2fe-4321-a494-f19d80603b21"
     data["user_data"]["mac_address"] = "83a54d80-b87c-459f-88b0-3e8965938e80"
     data["customer_event_alias"] = "Fresh_subscription"
     if data["coupon_type"].present? && data["coupon_type"] == "user_referral"
        data["customer_event_alias"] = "referral"
     end
     if data["free_trail"].present? && data["free_trail"] == "true"
        data["customer_event_alias"] = "ooredo_free_trial"
     end


     version = params["data"]["user_data"]["app_version"].split(".").last.to_i
      if (params["data"]["user_data"]["os"] == "Ios" && version < 34)
        logger.info "-----------------IOS Api Call------#{data}----------"
     resp = `curl -vvv -H "X-IP-Override: #{user_ip}" -d '#{data.to_json}' 'https://api2.branch.io/v2/event/standard'`
       if Rails.env.production? 
          branch_log = Logger.new('/mnt/branch_events.log')
          branch_log.info("=====================Start of Final data for Branch event Logs=======================")
          branch_log.info("Final Parameters from Branch data ----------------------> #{data.to_json.inspect} <--------------------------------")
          branch_log.info("=====================Final branch post response #{resp.inspect}=======================")
          branch_log.info("=====================End of Final data for Branch event Logs=======================")
      end
      elsif (params["data"]["user_data"]["os"] != "Ios")
      resp = `curl -vvv -H "X-IP-Override: #{user_ip}" -d '#{data.to_json}' 'https://api2.branch.io/v2/event/standard'`
       if Rails.env.production? 
          branch_log = Logger.new('/mnt/branch_events.log')
          branch_log.info("=====================Start of Final data for Branch event Logs=======================")
          branch_log.info("Final Parameters from Branch data ----------------------> #{data.to_json.inspect} <--------------------------------")
          branch_log.info("=====================Final branch post response #{resp.inspect}=======================")
          branch_log.info("=====================End of Final data for Branch event Logs=======================")
        end
      else
      resp = ""
     end
     
     logger.info "resp :: #{resp.inspect}"

     
    return resp
   end


  def branch_custom_event
    # data = params["data"]
    # data["branch_key"] = "key_test_ilJk8Kq7A52Ai8J09DkMolidCujWPMsR"
    # logger.info "headers :: #{request.headers["HTTP_X_FORWARDED_FOR"].inspect}}"
    # user_ips = request.headers["HTTP_X_FORWARDED_FOR"]
    # user_ip = ""
    # user_ip = user_ips.split(",").first unless user_ips.nil?
    # data['user_data']["http_origin"] = "c65f433f-a2fe-4321-a494-f19d80603b21"
    # data['user_data']["mac_address"] = "83a54d80-b87c-459f-88b0-3e8965938e80"
    # logger.info "Branch custom  post data here-----#{data.to_json.inspect}"
    # resp = `curl -vvv -H "X-IP-Override: #{user_ip}" -d '#{data.to_json}' 'https://api2.branch.io/v2/event/custom'`
    # logger.info "Branch response for custom  post data-----#{resp.inspect}"
   render :json => {status: "true"}
  end

  def bd_user
    p cookies[:user_id].inspect
    if cookies[:user_id].present? 
      redirect_to "#{SITE}"
    elsif params[:login_type].present?
      unless params[:user_ses_id].present?
        render :json => {status: "false",:ret_url => ""}
      else
      account_resp = User.get_user_account_details(params[:user_ses_id])
      if account_resp.has_key?("error")
        st = "false"
        render :json => {status: st,:ret_url => ""}
      else
        url = ""
        ses_id = params[:user_ses_id]
        user_profiles = User.get_all_user_profiles(ses_id)
        all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
        first_profile = all_profiles.flatten.first.split("$")
        default_profile = user_profiles['data']['profiles'].collect{|x|x if x['default_profile'] == "true"}.compact.first
        user_profile_data  = {
          :user_profile => {
            :profile_id => default_profile['profile_id']
          }
        }
        user_st = "Registered"
        if account_resp["data"]["is_subscribed"] == true
          user_st = "Subscribed"
        end
        device_limit = "false"
        profile_assign  = User.assign_profile(ses_id,user_profile_data)
        city = $city
        first_name = "Guest"
        user_login_id = ""
        mobile_num = ""
        email_id = ""
     if account_resp["data"]["login_type"] == "external"
      if account_resp["data"]["user_type"] == "msisdn"
        mobile_num = account_resp["data"]["mobile_number"]
        email_id = "" 
      else
        mobile_num = ""
        ext_userid = account_resp["data"]["ext_user_id"]
        email_id = ""
      end
    elsif account_resp["data"]["login_type"] == "msisdn"
      mobile_num = account_resp["data"]["mobile_number"]
      email_id = ""
    else
      email_id = account_resp["data"]["email_id"]
      mobile_num = ""
    end
    if params[:sub_st] == "false"
      pay_resp = user_plan_init(params[:plan_id],email_id,mobile_num,ses_id,params)
      logger.info "=======Ghoori D2C Backend Response ====#{pay_resp.inspect}"
      url = pay_resp['data']['payment_url'] if pay_resp.has_key?("data")
    end
     render json: {status: "true",user_id: ses_id,user_name: default_profile['firstname'],user_profiles: all_profiles,profile_id: default_profile["profile_id"],login_id: user_login_id,user_parental_control: account_resp["data"]["parental_control"],pin: account_resp["data"]["parental_pin"],:profile_type => "non_kids",:user_analytic_id => account_resp["data"]["user_id"],:user_status => user_st, city_of_user: city,device_limit: device_limit.to_s, first_name: first_name.to_s, mob_num: mobile_num, email_id: email_id,:ret_url => url}
     end
    end
  end
end


  def telco_external_users
    @user_data ={}
    p cookies[:user_id].inspect
    if cookies[:user_id].present? 
      redirect_to "#{SITE}"
    elsif (params[:session_id].present? && params[:is_subscribed] == "true")
      user_details = external_user_detail( params[:session_id] )
      @user_data = {user_id:params[:session_id],user_name:user_details[:first_name],login_id: user_details[:user_login_id], pin: user_details[:pin], profile_id:user_details[:profile_id],user_profiles:user_details[:all_profile],is_parent_control:user_details[:account_resp]["data"]["parental_control"],user_profile_type:"non_kids",user_analytical_id:user_details[:account_resp]["data"]["user_id"],is_subscribed:"true"}
    elsif params[:login_type].present?
      unless params[:user_ses_id].present?
        render :json => {status: "false",:ret_url => ""}
      else
        user_details = external_user_detail(params[:user_ses_id])
        if user_details[:account_resp].has_key?("error")
          st = "false"
          render :json => {status: st,:ret_url => ""}
        else
          url = ""
          ses_id = params[:user_ses_id]
          if params[:sub_st] == "false"
            # pay_resp = telco_nontelco_user_plan_init(params[:plan_id], user_details[:email_id],user_details[:mobile_num],ses_id,params)
            # logger.info "======= Telco D2C Backend Response ====#{pay_resp.inspect}"
            # url = pay_resp['data']['payment_url'] if pay_resp.has_key?("data")
            url = "#{SITE}"
          end

          render json: {status: "true",user_id: ses_id,user_name: user_details[:first_name], user_profiles: user_details[:all_profiles],profile_id: user_details[:profile_id],login_id: user_details[:user_login_id],user_parental_control: user_details[:user_parental_control],pin: user_details[:pin],:profile_type => "non_kids",:user_analytic_id => user_details[:account_resp]["data"]["user_id"],:user_status => user_details[:user_st], city_of_user: user_details[:city],device_limit: user_details[:device_limit], first_name: user_details[:first_name], mob_num: user_details[:mobile_num], email_id: user_details[:email_id],:ret_url => url}
        end
      end # unless
    end # if condition
end

def external_user_detail(session_id)
  user_profiles = User.get_all_user_profiles(session_id)
  account_resp = User.get_user_account_details(session_id)
  all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
  first_profile = all_profiles.flatten.first.split("$")
  default_profile = user_profiles['data']['profiles'].collect{|x|x if x['default_profile'] == "true"}.compact.first
  user_profile_data  = {
    :user_profile => {
      :profile_id => default_profile['profile_id']
    }
  }
  user_st = "Registered"
  if account_resp["data"]["is_subscribed"] == true
    user_st = "Subscribed"
  end
  device_limit = "false"
  profile_assign  = User.assign_profile(session_id,user_profile_data)
  city = $city
  first_name = "Guest"
  user_login_id = ""
  mobile_num = ""
  email_id = ""
  if account_resp["data"]["login_type"] == "external"
    if account_resp["data"]["user_type"] == "msisdn"
      mobile_num = account_resp["data"]["mobile_number"]
      email_id = "" 
    else
      mobile_num = ""
      ext_userid = account_resp["data"]["ext_user_id"]
      email_id = ""
    end
  elsif account_resp["data"]["login_type"] == "msisdn"
    mobile_num = account_resp["data"]["mobile_number"]
    email_id = ""
  else
    email_id = account_resp["data"]["email_id"]
    mobile_num = ""
  end

  return {user_st: user_st, user_login_id:user_login_id, device_limit: device_limit, city: city, user_parental_control:account_resp["data"]["parental_control"], pin:account_resp["data"]["parental_pin"], first_name: first_name, mobile_num: mobile_num, ext_userid: ext_userid, account_resp:account_resp, email_id: email_id, default_url:user_profile_data, all_profile: all_profiles, profile_id: default_profile["profile_id"]}
end

def telco_nontelco_user_plan_init(p_id,email_id,mobile_no,ses_id,params)
    resp = Ott.all_access_plans
    all_plans = resp["data"]["catalog_list_items"]
    payment_type = params[:payment_gateway]
    sel_plan = all_plans[0]["plans"].collect{|x|x if x["id"] == p_id}.compact.first
    unless sel_plan.nil?
      plan_ids = sel_plan["id"]
      cel_plan = params[:all_dt]
      # browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
      browser_name = "Generic_Browser"
      platform_type = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
      new_plan_init_params = {
        :us => get_payment_signature_key(plan_ids,ses_id),
        :payment_gateway => payment_type,
        :platform => platform_type,
        :payment_info => {
          :net_amount => sel_plan["discounted_price"], 
          :price_charged => sel_plan["discounted_price"],
          :currency => params[:currency], 
          :packs => get_selected_packs(all_plans[0],p_id)
        },
        :transaction_info => {
          :app_txn_id => "", 
          :txn_message => "#{payment_type} init call", 
          :txn_status => "init", 
          :order_id => "", 
          :pg_transaction_id => ""  
         },
        :user_info => {
         :email => email_id,
         :mobile_number => mobile_no,
         :ext_user_id => ""
        },
        :miscellaneous => {
         :browser => browser_name,
         :device_brand =>  "NA",
         :device_IMEI => "NA",
         :device_model =>  "NA", 
         :device_OS =>  "NA" ,
         :device_type =>  "NA" ,
         :inet => "NA",
         :isp =>  "NA",
         :operator =>  "NA",
         :id => "NA",
         :opid => "NA",
         :p5 => "NA",
         :sid => "NA",
         :tpid => "NA",
         :m =>"NA",
         :seltxid => "NA"
        },
        :trans_info => {
        :id =>"948107552635019083", 
        :os => "Linux",
        :city => "Bengaluru",
        :device_id => "",
        :device_make => "",
        :user_ip => "127.0.0.1",
        :user_agent => "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36",
        :http_origin => "c65f433f-a2fe-4321-a494-f19d80603b21",
        :mac_address =>"83a54d80-b87c-459f-88b0-3e8965938e80"
      }
      }
      logger.info "======D2c final Init data here====#{new_plan_init_params.inspect}======"
      transaction_resp ={}
     # p transaction_resp.inspect
      if payment_type=="boost"
        transaction_resp = User.send_init_transaction(ses_id,new_plan_init_params)
        token_resp = generate_aoc_token(sel_plan["discounted_price"], email_id, mobile_no, platform_type, transaction_resp["data"]["order_id"])
        token_resp["order_id"] = transaction_resp["data"]["order_id"]
        add_aoc_token = User.add_aoc_token_data(token_resp)
        p add_aoc_token.inspect
        # Preprod URL
        # transaction_resp['data']["payment_url"] = "http://stg.mife-aoc.com/api/aoc?aocToken="+token_resp["aocToken"]
        # Production URL
        transaction_resp['data']["payment_url"] = "http://msisdn.mife-aoc.com/api/aoc?aocToken="+token_resp["aocToken"]
      else
        transaction_resp = User.send_init_transaction(ses_id,new_plan_init_params)
        p transaction_resp.inspect
      end
    else
      transaction_resp = {"error"=> "invalid plan id"}
    end
    return transaction_resp   
  end




def coupon_register
  begin
    signup_params = {
    :user => {
    :firstname=> params[:name],
    :password => params[:password],
    :region => $region,
    :email_id  => params[:email_id],
    :type  => params[:type]
    }
    }

    signup_params[:user][:device_data] = {
    :device_name => params[:browser_det],
    :device_type => "web"
    } 
    pk_st = "false"
      response = User.sign_up(signup_params)
      logger.info "=================Coupon Register"
      logger.info "^^^^^^^^^^^^^^^^^^^^#{signup_params.inspect}"
      logger.info "=========Backend user coupon data#{response.inspect}======="
      if  response.has_key?("data")
        user_profiles = User.get_all_user_profiles(response["data"]["session_id"])
        account_resp = User.get_user_account_details(response["data"]["session_id"])
        all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
        first_profile = all_profiles.flatten.first.split("$")
        if $region == "FR"
          if params[:coupon] == "CGICANADA" || params[:coupon] == "HCICANADA"
          else
          params[:coupon] = "EOIPARIS"
          end
        end
        if params[:coupon_type] != "accentive"
          unless params[:coupon].blank?
            coupon_params = {
              :coupon_code => params[:coupon].upcase,
              :region => $region,
              :plans_info => "false"
            }
            p coupon_params.inspect
            p "@@@@@@@@@@2"
          coupon_resp = User.validate_coupon(coupon_params)
          p coupon_resp.inspect
          p "coupon_resp"
            if coupon_resp.has_key?("data")
              plan_info = coupon_resp["data"]
              cpn_type = params[:coupon_type].present? ? params[:coupon_type] : "worldmeday"
              plan_init_params = get_promocode_post_data(params[:coupon],params[:email_id],response["data"]["session_id"],"","init",cpn_type,coupon_resp)
              p plan_init_params.inspect
              p "@@@@@@@@@@@@@@@@@@@@@@@@@@@@2"
              transaction_resp = User.send_init_transaction(response["data"]["session_id"],plan_init_params)
              p transaction_resp.inspect
              if transaction_resp.has_key?("data")
                 plan_suc_params = get_promocode_post_data(params[:coupon],params[:email_id],response["data"]["session_id"],transaction_resp["data"]["transaction_id"],"success",cpn_type,coupon_resp)
                 p plan_suc_params.inspect
                 final_pack_resp = User.send_init_transaction(response["data"]["session_id"],plan_suc_params)
                p final_pack_resp.inspect
                pk_st = "true"
              end
            end
          end
          else
           acntv_pack_to_user(response["data"]["session_id"],params[:pln_type],params[:email_id],params,account_resp["data"]["user_id"])
        end
        render json: {status: true,user_id: "#{response["data"]["session_id"]}",user_name: first_profile[1],user_profiles: all_profiles,profile_id: first_profile[0],:login_id => params[:email_id],user_parental_control: account_resp["data"]["parental_control"],pin: account_resp["data"]["parental_pin"],:profile_type => "non_kids",:user_analytic_id => account_resp["data"]["user_id"],:pack_st => pk_st}
      else
        set_response(response)
      end
    
  rescue Exception => e
    logger.info "Exception block herer===#{e.message.inspect}===================="
    render json: {status: false,error_message: "sorry something went wrong" }
    logger.info e.message
  end
end


def check_coupon_validity
  status = false
  check_validity = User.validate_coupon_before_register(params[:coupon])
  if  check_validity.has_key?("data")
    status = true
  end 
  render json: { status: status, valid_res: check_validity }
end  


def get_promocode_post_data(promo_code,email_id,session_id,order_id,tr_st,cpn_type,coup_resp)
  #browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
  browser_name = ""
  plan_id = "5d7b89c3a58c4049690012f2"
  sub_cat_id =  "5b3c917fc1df417b9a00002c"
  cat_pck_id = "5d7b89c3a58c4049690012f3"
  start_date = "February 27 2020,16:04:57" 
  cupn_id  = "5e58e9f4ed8f7d4efb000001"
  p cpn_type.inspect
  p "@@@@@@@@@@@@@@@@@@@@@@@"
  if cpn_type == "consult"
    if Rails.env.production?
      if promo_code == "INDIANCONSULATENY" || promo_code == "CGICANADA" || promo_code == "HCICANADA" || promo_code == "RAJCHANDRA" || promo_code == "PHILLYMM" || promo_code == "GMOCO" || promo_code == "GSHOUSTON" || promo_code === "GUJSAMAJ" || promo_code == "NAVRATRI"
        plan_id = "5e748c68b541703b64000000"
      elsif promo_code == "EOISPAIN"
        plan_id = coup_resp["data"]["payment_info"][0]["plan_id"]
      else
        plan_id = "5e8c4766b541704588000000"
      end
      sub_cat_id =  "5b3c917fc1df417b9a00002c"
      cat_pck_id = "5d7b89c3a58c4049690012f3"
    else
      if promo_code == "INDIANCONSULATENY"
        plan_id = "5e745c5c741cbb2739000001"
      elsif promo_code == "EOISPAIN"
        plan_id = coup_resp["data"]["payment_info"]["plan_id"]
        
      else
        plan_id = "5e8c4794741cbb5c06000000"
      end
    #plan_id = "5e745c5c741cbb2739000001"
      sub_cat_id =  "5b3c917fc1df417b9a00002c"
      cat_pck_id = "5d038a66741cbb20ea0000b5"
    end
    #start_date = "March 20 2020,16:04:57" 
    if promo_code == "EOIPARIS"
      #cupn_id = "5e858425741cbb6fe3000021"
      cupn_id = "5e870f86a609d217140025bc"
    elsif promo_code == "CGICANADA"
     cupn_id = "5e904093a609d20c8200000b"
    elsif promo_code == "HCICANADA"
      cupn_id = "5e9040d7a609d20c8200000c"
    elsif promo_code == "EOISPAIN"
        cupn_id = coup_resp["data"]["coupon_info"]["coupon_id"]
    elsif promo_code == "RAJCHANDRA"
      cupn_id = "5ef431c8a609d2345d000ffd"
    elsif promo_code == "PHILLYMM"
      cupn_id = "5efd9352a609d22f2e002dd0"
     elsif promo_code == "GSHOUSTON"
      cupn_id = "5f059a19a609d206c8001325"
     elsif promo_code == "GMOCO"
      cupn_id = "5f059a61a609d206c8001326"
      elsif promo_code == "GUJSAMAJ"
       cupn_id = "5f1a8734a609d206c8008b1f"
    else
    cupn_id = "5e749a87ed8f7d1dc40009df"
    end
  elsif promo_code == "GCSALABAMA"
    plan_id = "5b72a084c1df4137ef000003"
    sub_cat_id =  "5b3c917fc1df417b9a00002c"
    cat_pck_id = "5b72a084c1df4137ef000005"
    cupn_id = "609e310ea609d2066800ff99" 
  elsif promo_code == "GSMONT" 
    plan_id = "5b72a084c1df4137ef000003"
    sub_cat_id =  "5b3c917fc1df417b9a00002c"
    cat_pck_id = "5b72a084c1df4137ef000005"
    cupn_id = "609e31b5a609d2066800ff9c"
   elsif promo_code == "GSALAB" 
    plan_id = "5b72a084c1df4137ef000003"
    sub_cat_id =  "5b3c917fc1df417b9a00002c" # catalog_id
    cat_pck_id = "5b72a084c1df4137ef000005" #content_id
    cupn_id = "609e3262a609d2066800ff9d" # coupon code id
   elsif promo_code == "DETROIT" 
    plan_id = "5b72a084c1df4137ef000003"
    sub_cat_id =  "5b3c917fc1df417b9a00002c"
    cat_pck_id = "5b72a084c1df4137ef000005"
    cupn_id = "60a20d31a609d20668010893"
  elsif  promo_code == "GCSFLO"
    plan_id = "60a365d9b5417011110006d5"
    sub_cat_id =  "5b3c917fc1df417b9a00002c"
    cat_pck_id = "60a365d9b5417011110006d6"
    cupn_id = "60a20e08a609d20668010894"
  elsif promo_code == "VTOFNY"
    plan_id = "60a365d9b5417011110006d5"
    sub_cat_id =  "5b3c917fc1df417b9a00002c"
    cat_pck_id = "60a365d9b5417011110006d6"
    cupn_id = "60a20e56a609d20668010895"
    elsif promo_code == "FOGCANADA"
    plan_id = "60a365d9b5417011110006d5"
    sub_cat_id =  "5b3c917fc1df417b9a00002c"
    cat_pck_id = "60a365d9b5417011110006d6"
    cupn_id = "60a4bebca609d20668011285"
  elsif promo_code == "USVYO"
    plan_id = "60b8e49eb541703da2000000"
    sub_cat_id =  "5b3c917fc1df417b9a00002c"
    cat_pck_id = "5b72a084c1df4137ef000005"
    cupn_id = "60b9c904b54170452c000000"
  elsif promo_code == "GONAGUJ"
    plan_id = "5b72a084c1df4137ef000003"
    sub_cat_id =  "5b3c917fc1df417b9a000032" # catalog_id
    cat_pck_id = "5b72a084c1df4137ef000005" #content_id (subscruiption_id)
    cupn_id = "6246f8854210bb072f0236b1" # coupon code id
 end
  new_plan_init_params = {
  :us => get_user_payment_signature_key(plan_id, session_id),
  :payment_gateway => "coupon",
  :platform => "cms",
  :payment_info => {
  :net_amount => "0", 
  :price_charged => "0",
  :currency => get_currency_sign,
  :coupon_code=> promo_code,
  :coupon_code_id => cupn_id,
  :packs => [
        {
         :plan_id=> plan_id,
         :plan_categories=>["all_access_pack"],
         :category_type=>"all_access",
         :subscription_catalog_id=> sub_cat_id,
         :category_pack_id=> cat_pck_id
         #:start_date=> start_date,
     }
   ]
  },
  :transaction_info =>{
      :app_txn_id=>"12344",
     :txn_message=>"CMS",
     :txn_status=> tr_st,
     :pg_transaction_id=>"cms_1",
     :order_id => order_id
   },
  :user_info => {
  :email => email_id,
  :mobile_number => "",
  },
  :miscellaneous => {
    :browser => browser_name,
    :device_brand => "NA",
    :device_IMEI => "NA",
    :device_model => "NA", 
    :device_OS => "NA" ,
    :device_type => "NA" ,
    :inet => "NA",
    :isp => "NA",
    :operator => "NA"
   }
  }
  end

  def my_movies

   end


  def redeem_ticket
    @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
    if request.post?
     #      config_resp = Rails.cache.fetch("design_configuration_list_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
     #    Ott.get_configuration      
     #   }
     # tvod_pck_dt = config_resp["data"]["params_hash2"]["config_params"]["tvod_tags"]
     #  p tvod_pck_dt.inspect
      #begin
      # promocode_params = {
      #   :us => get_payment_signature_key(tvod_pck_dt["plan_id"],cookies[:user_id]), 
      #   :packs => [{
      #   :category_pack_id => tvod_pck_dt["plan_category_id"],  
      #   :plan_id => tvod_pck_dt["plan_id"]}], 
      #   :coupon_code=> params[:coupon], 
      #   :region=> $region
      # }
      tvod_pck_dt = ""
      promocode_params = {
        "coupon_code": params[:coupon].upcase,
        "region": $region,
        "plans_info":"false"
      }
       promo_resp = User.validate_coupon(promocode_params)
       p promo_resp.inspect
       p "@@@@@@@@@@@@@@@@@@@@2"
       if promo_resp.has_key?("error")
         render json: {status: false,error_message: get_lang_string("invalid_code") }
      else
        init_trans_resp = tvod_pack_activate(tvod_pck_dt,cookies[:user_id],promo_resp,"init","",params)
        p init_trans_resp.inspect
        p  "!!!!!!!!!!!!!!!!!!!!"
        if init_trans_resp.has_key?("data")
          sus_tans_resp = tvod_pack_activate(tvod_pck_dt,cookies[:user_id],promo_resp,"success",init_trans_resp["data"]["transaction_id"],params)
          if sus_tans_resp.has_key?("data")
            render json: {status: true,order_id: sus_tans_resp["data"]["transaction_id"],message: sus_tans_resp["data"]["message"],:title => sus_tans_resp["data"]["title"],:exp_dt => sus_tans_resp["data"]["expiry_date"]}
          else
            render json: {status: false,error_message: sus_tans_resp["error"]["message"] }
          end
        else
        render json: {status: false}
      end
      end

      # rescue
      #  render json: {status: false,error_message: "sorry something went wrong" }
      # end
    end
  end

  def participate
    begin
      mnt_pk_st = true  
      user_plans = Ott.user_plans(params[:user_id])
      feedback_check = false
      al_yr_st = true
      unless user_plans.empty?
        mnt_pk_st = false
        current_plans = user_plans['current_active_plans']
        cur_yearly_plans = current_plans.select{|plan| plan["plan_title"].downcase == "yearly"}
        cur_mnthy_plans = current_plans.select{|plan| plan["plan_title"].downcase == "monthly"}
        all_acs_yr = cur_yearly_plans.collect{|x| x if x["category"] == "all_access"}
        unless all_acs_yr.empty?
          st_dt = cur_yearly_plans.first["start_date"].split("T")[0].to_s 
          unless ("2020-06-10".."2020-07-10").member?(st_dt)
           al_yr_st = false
          end
        end
        mnt_pk_st = true   unless cur_mnthy_plans.empty?
        valid_plans = cur_yearly_plans.select{ |plan| 
        start_date = plan["start_date"].split("T")[0].to_s 
        ("2020-06-10".."2020-07-10").member?(start_date)
        }
        feedback_check = true  if valid_plans.count > 0
      end
      render json: {status: true , feedback_check:feedback_check,mnt_pln_st: mnt_pk_st,yr_pln_st: al_yr_st}
    rescue Exception => e 
     render json: {status: false}    
    end 
  end

  def contest
    redirect_to "#{SITE}"
    # if $region == "IN"
    #   render :layout => false
    # else
    #   redirect_to "#{SITE}"
    # end
  end

  
  def subs_contest
    if $region == "IN"
      render :layout => false
    else
      redirect_to "#{SITE}"
    end
  end

   def contest_questions
    if request.post?
      begin
        if Rails.env.production? || Rails.env.pre_production?
          qus_log = Logger.new('/mnt/users.log')
          qus_log.info("=====================Start of User question Logs=======================")
          qus_log.info("Parameters from  questions  ----------------------> #{params.inspect} <--- #{Time.now.inspect}-----------------------------")
          qus_log.info("=====================End of User question  Logs=======================")
        end
        uq = UserQuestion.where(:user_analytic_id => params[:user_id]).first
        ms_info =  {"Amar Akbar Anthony movie mein Anthony ka kirdaar kisne nibhaya tha?" => "Amithabh Bachan",
          "Jab We Met movie mein Shahid Kapoor ka kya naam hai" => "Aditya"
         }
        UserQuestion.create(:user_analytic_id => params[:user_id],:miscellaneous => ms_info,:ques_sub_time => Time.now) if uq.nil?
          render json: {status: true}
      rescue
       render json: {status: false,error_message: "sorry something went wrong" }

      end
    else
      if $region == "IN"
        render :layout => false
      else
        redirect_to "#{SITE}"
      end
    end
  end

  def user_prop
    begin
      resp = User.get_user_prop(params[:user_id])
      final_dt = resp["data"]
    rescue
      final_dt = {}
    end
      render json: {:status => true,:dt => final_dt}
  end

  def acntv_coupon
    #cupn_code=TX6Y2DS4
    #SHAN1081282559
    cpn_type = ""
     acntv_req = Typhoeus.get("#{ACTV_BASE_URL}/vouchers/ETX_001-#{params[:acntv_cupn]}", headers: {
        "x-correlation-id" => SecureRandom.hex(16),
        "x-client-id" => ACTV_CLIENT_ID,
        "x-client-secret" => ACTV_CLIENT_SECRET ,
        "content-type"  => "application/json"
      })
     st = "false"
    p JSON.parse(acntv_req.body)
    acntv_resp = JSON.parse(acntv_req.body)
    p acntv_resp.inspect
    p "@@@@@@@@@@@@@@@@@@@@"
    if acntv_req.code == 200
      st = "true"
      cpn_type = acntv_resp["data"]["product_code"]
    end
    p "status"+st
    render :json => {"status" => st,:error_message => "invalid coupon code",:cupn_type => cpn_type}
  end

  def acntv_cupn_authrozie cupn_code,order_id,user_id,params,user_analy_id
    st = "false"
    bd_dt = {
        "capture_mode" => "auto",
        "acceptance_point_ref" =>  user_analy_id+Time.now.strftime("%Y%m%d%H%M%S").to_s,
        "currency" => "INR",
        "vouchers" => [
          {
          "ref" =>  cupn_code,
          "pin_code" =>  "",
          "value" =>  1,
          "product_class" => "ETX_001"
          }
        ],
        "invoice_details" => {
          "ref" => order_id,
          "amount" => get_price_det(params[:pln_type])
        }
      }
      p bd_dt.inspect
      p "@@@@@@@@@@@@@@@"
      auth_rep = Typhoeus.post("#{ACTV_BASE_URL}/transactions?return_vouchers_info=true", headers: {
        "x-correlation-id" => SecureRandom.hex(16),
        "x-client-id" => ACTV_CLIENT_ID,
        "x-client-secret" => ACTV_CLIENT_SECRET ,
        "content-type"  => "application/json"
      }, body: bd_dt.to_json)
    p JSON.parse(auth_rep.body).inspect
    final_auth_resp = JSON.parse(auth_rep.body)
    if final_auth_resp.has_key?("data")
      #cp_link = final_auth_resp["links"].collect{|x|x if x["rel"] == "capture"}.compact.first["href"].split("/api/")[1]
      #acntv_cupn_capture(cupn_code,order_id,user_id,params,final_auth_resp["data"]["authorization_id"],cp_link)
     st = "true"
     transaction_det = {
      :order_id => order_id,
      :miscellaneous => final_auth_resp,
      :user_id => user_id,
      :payment_gateway => "accentive",
      :authorization_id => final_auth_resp["data"]["authorization_id"]
    }
    p transaction_det.inspect
    p "@@@@@@@@"
     trans_up  = User.update_transaction_det(user_id,order_id,transaction_det)
     p trans_up.inspect
    end
    #render :json => {"status" => st}
  end

   def acntv_cupn_capture cupn_code,order_id,user_id,params,auth_id,cap_url
    st = "false"
    cap_dt = { 
      "acceptance_point_ref" => Time.now.strftime("%Y%m%d%H%M%S"),  
      "vouchers" =>[
        { 
        "ref" => cupn_code, 
        "value" =>  1,
        "product_class" => "ETX_001"
        }
      ] ,
     "invoice_details" => {
        "ref" => order_id,
        "amount" => 3000
      },
      "customer_details" => {
        "email_address" => params[:email_id],
        "phone_number" => "+919538947694"
      }
      }
      p cap_dt.inspect
      p "9999999999999"
      capt_resp = Typhoeus.post("#{ACTV_BASE_URL}/#{cap_url}?return_vouchers_info=true", headers: {
        "x-correlation-id" => SecureRandom.hex(16),
        "x-client-id" => ACTV_CLIENT_ID,
        "x-client-secret" => ACTV_CLIENT_SECRET ,
        "content-type"  => "application/json"
      }, body: cap_dt.to_json)
    p JSON.parse(capt_resp.body).inspect
    final_auth_resp = JSON.parse(capt_resp.body)
  end

  def acntv_pack_to_user(user_id,pck_name,email_id,params,user_analytic_id)
     sec_key = "7c32c524a67f405812ca"+email_id+$region+"accentive"+pck_name
     p sec_key.inspect
     key_sign = Gibberish::MD5(sec_key) 
     pck_dt = {
      "user_info" => 
      {
      "uid" => email_id,
      "provider" => "accentive",
      "package" => pck_name,
     "plan_activation" => true
     },
     "signature" =>  key_sign,
     "region" => "IN"
   }
    pck_resp = User.vendor_pck_act(pck_dt)
    if pck_resp.has_key?("data")
     acntv_cupn_authrozie(params[:coupon],pck_resp["data"]["order_id"],user_id,params,user_analytic_id)
    end
  end

  def get_price_det pk_name
    price = 0
   case pk_name
   when "shemaroo_annual"
    price = 74900
   when "shemaroo_monthly"
    price = 19900
   when "shemaroo_half_yearly"
    price = 59900
   when "shemaroo_quarterly"
    price  = 29900
  end
  return price
  end

  def referral
    begin
       @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
      st = "false"
      login_user_id = params[:user_id].present? ? params[:user_id] : cookies[:user_id]
      user_resp = User.get_user_account_details(login_user_id)
      @user_det = user_resp["data"]
      ref_code = ""
      if @user_det["referral_code"].blank?
        if request.post?
          refer_dt = {
            :user_info => {
              :uid => @user_det['user_id'] ,
              #@user_det["primary_id"],
              :email_id => params[:email],
              :mobile =>  params[:phone],
              :first_name =>  params[:first_name],
              :last_name  =>  params[:last_name]
            }
          }
        refer_resp = User.ref_creation(refer_dt)
        logger.info "Referral user data ======#{refer_resp.inspect}"
        if refer_resp.has_key?("data")
          st = "true"
          ref_code  = refer_resp["data"]["referral_code"]
        end
         render :json => {:status => st,:ref_code => ref_code,:u_name => @user_det['firstname']}
       end
      else
        if params["lang"].present?
          url = "#{SITE}/#{params['lang']}/users/referral_code?name=#{@user_det['firstname']}&ref_code=#{@user_det['referral_code']}&theme=#{@theme}"
        else
        url = "#{SITE}/users/referral_code?name=#{@user_det['firstname']}&ref_code=#{@user_det['referral_code']}&theme=#{@theme}"
      end
        if params[:user_id].present?
          url = url+"&user_id=#{params[:user_id]}&user_analytical_id=#{params[:user_analytic_id]}&user_login_id=#{params[:user_login_id]}&theme=#{@theme}"
        end
        redirect_to url
      end
    rescue
      redirect_to "#{SITE}"
    end
  end


  def add_bank_account
    if cookies[:user_analytical_id].present?
      if request.post?
        if $region == "IN"
        bnk_dt = {
          :user_info => {
          :account_number =>  params[:account_no],
          :confirm_account_number => params[:account_no],
          :ifsc_code =>  params[:ifsc_code],
          :name =>  params[:recp_name],
          :pan_url  =>  params[:pan_url]
          }
        }
        else
        bnk_dt = {
        :user_info => {
         :account_number => params[:paypal_id]
        }}
        end
        p bnk_dt.inspect
        refer_resp = User.add_bnk_account(cookies[:user_analytical_id],bnk_dt)
        p refer_resp.inspect
        render :json => {:status => "true"}
      end
      s3_region =  "ap-southeast-1"

      # Replace with the name of your bucket
      bucket = "shem-info"

      current_dt = DateTime.now
      policy_date = current_dt.utc.strftime("%Y%m%d")
      x_amz_date = current_dt.utc.strftime("%Y%jT%H%M%SZ")
      x_amz_algorithm = "AWS4-HMAC-SHA256"
      x_amz_credential = "AKIA3D5VQMBSRXGBGGF2/#{policy_date}/#{s3_region}/s3/aws4_request"

      encoded_policy = get_encoded_policy_document( bucket, x_amz_algorithm, x_amz_credential, x_amz_date, "images/" )
      x_amz_signature = get_signature( policy_date, s3_region, encoded_policy )


     
        bucket_name = bucket,
        s3_region_endpoint = get_s3_region_endpoint(s3_region),
        x_amz_algorithm = x_amz_algorithm,
        x_amz_credential = x_amz_credential,
        x_amz_date = x_amz_date,
        x_amz_expires = 5184000,
        x_amz_signature = x_amz_signature,
        policy =  encoded_policy

      @s3_bucket_name = bucket
      @s3_endpoint = s3_region_endpoint
      @s3_algorithm = x_amz_algorithm
      @s3_cred = x_amz_credential
      @s3_dt = x_amz_date
      @s3_sig = x_amz_signature
      @s3_policy = encoded_policy
   else
    redirect_to "#{SITE}"
   end

  end


  def referral_code
    # unless params[:user_id].present?
    #   unless cookies[:user_id].present?
    #     redirect_to "#{SITE}"
    #   end
    # end
  end

  def my_earnings
    begin
      @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
      # user_resp = User.get_user_account_details(cookies[:user_id])
      # @user_det = user_resp["data"]
      if request.post?
        nxt_pg_st = "false"
        if params[:start_date].present?
           ear_resp = User.get_user_earnings_with_pagination(cookies[:user_analytical_id],Time.parse(params[:start_date]).strftime("%Y-%m-%d"),Time.parse(params[:end_date]).strftime("%Y-%m-%d"),params[:page_no])
        else
          ear_resp = User.get_user_earnings(cookies[:user_analytical_id],params[:page_no])
        end
         total_items = ear_resp["data"]["total_referrals"]
          pg_cnt = params[:page_no].to_i*5
          nxt_pg_st = "true" if pg_cnt > total_items
          nxt_pg_st = "true" if params[:page_no] == "0"
          amt = 0
          p nxt_pg_st.inspect
          p "next page status"
         currency = $region == "IN" ? "INR" : "USD"
         cur_symbol = $region == "IN" ? "₹" : "$"
         amt = ear_resp["data"]["user_referrals"].collect{|x|x['referral_amount']}.inject(:+)   if ear_resp["data"]["user_referrals"].count > 0
         render :json => {:earnings_dt => ear_resp["data"],:status => "true",:ref_amt => ear_resp["data"]["total_amount"] ,:cur => currency,:cur_sym => cur_symbol,:next_pg_st => nxt_pg_st,:sno_strt => (params[:page_no].to_i*5)+1}
      else
         ear_resp = User.get_user_earnings(cookies[:user_analytical_id],"0")
        #earning = Typhoeus.get("http://18.209.25.40:5050/users/1697/my_earnings?auth_token=J7NUGuDYXwcipBqnLkUh&start_date=2020-11-04&end_date=2020-11-11&page=0")
        @my_earnings = ear_resp["data"]
      end
        
      p @my_earnings.inspect
      if params[:theme_option].present?
        url = "#{SITE}/users/my_earnings&theme_option=#{params[:theme_option]}"
      end
     rescue 
      redirect_to "#{SITE}"
     end
  end

  def get_signature_key( key, date_stamp, region_name, service_name )
  k_date = OpenSSL::HMAC.digest('sha256', "AWS4" + key, date_stamp)
  k_region = OpenSSL::HMAC.digest('sha256', k_date, region_name)
  k_service = OpenSSL::HMAC.digest('sha256', k_region, service_name)
  k_signing = OpenSSL::HMAC.digest('sha256', k_service, "aws4_request")
  k_signing
end

def get_encoded_policy_document( bucket, x_amz_algorithm, x_amz_credential, x_amz_date, path )
  Base64.encode64(
    {
      "expiration" => 1.hour.from_now.utc.xmlschema,
      "conditions" => [
        { "bucket" =>  bucket },
        [ "starts-with", "$key", path ],
        { "acl" => "public-read" },
        [ "starts-with", "$Content-Type", "image/jpeg" ],
        {"x-amz-algorithm" => x_amz_algorithm },
        {"x-amz-credential" => x_amz_credential },
        {"x-amz-date" => x_amz_date},
        [ "content-length-range", 0, 524288000 ]
      ]
    }.to_json
  ).gsub("\n","")
end

def get_signature( policy_date, s3_region, encoded_policy )
  # Gets signature key that will be used in signing
  signature_key = get_signature_key("gNIrs+uMUotz6p6yYS3GZl78inySuLaFqgy0ZXRF", policy_date , s3_region, "s3")

  # Sign and return the signature
  OpenSSL::HMAC.hexdigest('sha256', signature_key, encoded_policy )
end

def get_s3_region_endpoint(region_name)
    # Returns S3 endpoint for the region

    case region_name
    when "us-east-1"
      "s3.amazonaws.com"
    else
      "s3.#{region_name}.amazonaws.com"
    end
end

 def upload_file
   file = params[:file].read
   filename = params[:file].original_filename
   File.open(File.join(Rails.root, 'public','pan_uploads',filename), 'wb') { |f| f.write file }
  begin
   s3_resource =  Aws::S3::Resource.new(
    access_key_id: 'AKIA3D5VQMBSRXGBGGF2',
    secret_access_key: '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',
    region: 'ap-southeast-1'
  )
   p s3_resource.inspect
  object = s3_resource.bucket("shem-info").object("images/#{filename}")
  path = Rails.root.join("public/pan_uploads/#{filename}")
  object.upload_file(path)
  pan_url = "https://shem-info.s3-ap-southeast-1.amazonaws.com/images/#{filename}"
  rescue
    pan_url = ""
  end
   render :json => {:status => "true",:user_bank_url => "#{SITE}/users/add_bank_account?accnt_no=#{params[:acnt_no]}&re_accunt_no=#{params[:re_acnt_no]}&ifsc=#{params[:ifsc]}&rec_name=#{params[:rec_name]}&pan_url=#{pan_url}"}  
end

def encrypt_bank_dt
    render :json => {:status => true,:accunt_no => encrypt(params[:acc_no]) ,re_accunt_no: encrypt(params[:re_acc_no]),:ifsc => encrypt(params[:ifsc_code]),:rec_name => encrypt(params[:recp_name])}
end

def encrypt(plain_text)
  if plain_text.length > 0
      key = "shemaroome_website"
      cipher = OpenSSL::Cipher.new('aes-256-cbc').encrypt
      cipher.key = Digest::SHA1.hexdigest(key).unpack('B32').first
      s = cipher.update(plain_text) + cipher.final
      s.unpack('H*')[0].upcase
  else
    s = ""
  end
  end

  def decrypt(cipher_text)
    if cipher_text.length > 0
      key = "shemaroome_website"
      cipher = OpenSSL::Cipher.new('aes-256-cbc').decrypt
      cipher.key = Digest::SHA1.hexdigest(key).unpack('B32').first
      s = [cipher_text].pack("H*").unpack("C*").pack("c*")

      cipher.update(s) + cipher.final
    else
       s = ""
    end
  end

  def claim_earnings
    resp = User.claim_earnings(cookies[:user_analytical_id])
    p resp.inspect
    render :json => {:status => "true"}
  end

   def referral_earn_money
     if cookies[:user_id].present?
       acnt_resp = User.get_user_account_details(cookies[:user_id])
        unless acnt_resp["data"]["referral_code"].blank?
           url = "#{SITE}/users/referral_code?name=#{acnt_resp["data"]['firstname']}&ref_code=#{acnt_resp["data"]['referral_code']}"
           redirect_to url
          #"#{SITE}/users/referral"
        end
     end
   end


  def profile
    user_details_resp = User.get_user_account_details(cookies[:user_id])
    @user_details = user_details_resp["data"]
    @parental_pin = @user_details["parental_pin"]
    @parental_control = @user_details["parental_control"]
    all_profiles = User.get_all_user_profiles(cookies[:user_id])
    @user_profiles = all_profiles['data']['profiles']
    lang = get_user_lang(params)
    user_plans = Ott.user_plans(cookies[:user_id],lang)
   @current_plans = user_plans['current_active_plans']
  end

  def select_language

  end

  def gamification
    @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
   user_id = params[:user_analytic_id].present? ? params[:user_analytic_id] : cookies[:user_analytical_id]
    user_resp = User.cntst_reg_chk(user_id)
    logger.info "--gamification-----#{user_resp}"
      if params[:user_id].present?
          url = "#{SITE}/users/gmfc_contest?user_id=#{params[:user_id]}&user_login_id=#{params[:user_login_id]}&user_analytic_id=#{params[:user_analytic_id]}&platform_type=#{params[:platform_type]}&browser=#{params[:browser]}"
        if params[:lang].present? && params[:lang] != "en"
          url = "#{SITE}/#{params[:lang]}/users/gmfc_contest?user_id=#{params[:user_id]}&user_login_id=#{params[:user_login_id]}&user_analytic_id=#{params[:user_analytic_id]}&platform_type=#{params[:platform_type]}&browser=#{params[:browser]}"
         end
        if params[:version].present?
          url = url+"&version=#{params[:version]}"
        end
       else
         url = "#{SITE}/users/gmfc_contest"
         url = "#{SITE}/#{params[:lang]}/users/gmfc_contest"  if params[:lang].present? && params[:lang] != "en"
      end
      if user_resp["status"] == "success"
         if params[:user_id].present?
          url  = url+"&mobile_no=#{user_resp["user"]["account_id"]}"
         else
          url  = url+"?mobile_no=#{user_resp["user"]["account_id"]}"
        end
        redirect_to url
      elsif params[:mobile_no].present?
          signup_dt = { :user => { 
          :account_id => params[:mobile_no], 
          :analytical_id => params[:user_analytic_id].present? ? params[:user_analytic_id] : cookies[:user_analytic_id],
          :login_type => "msisdn",
          :region => "IN",
          :is_send_otp => false
          }
        }
        gmfc_resp =  User.gmfc_user_register(signup_dt)
        site_url = "#{SITE}"
        if params[:lang].present?
          site_url = site_url+"/#{params[:lang]}"
        end
        url = "#{site_url}/users/gmfc_contest?user_id=#{params[:user_id]}&user_login_id=#{params[:user_login_id]}&user_analytic_id=#{params[:user_analytic_id]}&platform_type=#{params[:platform_type]}&browser=#{params[:browser]}&version=#{params[:version]}&mobile_no=#{params[:mobile_no]}&theme_option=#{@theme}"
        redirect_to url
      else
        acnt_id = params[:user_id].present? ? params[:user_id] : cookies[:user_id]
        user_acn  = User.get_user_account_details(acnt_id)
        if user_acn["data"]["user_type"] == "msisdn"
          signup_dt = { :user => { 
          :account_id => user_acn["data"]["mobile_number"], 
          :analytical_id => params[:user_analytic_id].present? ? params[:user_analytic_id] : cookies[:user_analytic_id],
          :login_type => "msisdn",
          :region => "IN",
          :is_send_otp => false
          }
        }
        p signup_dt.inspect
        st = "true"
        gmfc_resp =  User.gmfc_user_register(signup_dt)
        redirect_to url
        end
      end
  end
  
  def gmfc_contest
    gmfc_resp = User.get_contests
    if gmfc_resp.has_key?("error")
      @all_contests = []
     else
     @all_contests = gmfc_resp["data"]["items"]
    end
  end

  def gmfc_contest_questions
   gmfc_qus_resp = User.get_cntst_qsts(cookies[:user_analytical_id],params[:cnts_id],params[:slot_id],params[:lang])
   if gmfc_qus_resp["status"] == "success"
     @gmfc_lang = gmfc_qus_resp["data"]["languages"]
     @gmfc_cntst_qusts = gmfc_qus_resp["data"]["details"]
     @gmfc_slot_id = gmfc_qus_resp["data"]["slot_id"]
     @gmfc_contest_name = gmfc_qus_resp["data"]["contest_name"]
     @gmfc_win_dt = gmfc_qus_resp["data"]["winners_declared_at"]
     if gmfc_qus_resp["data"]["is_submitted"] == true
        if params[:lang].present? && params[:lang] != "en"
         redirect_to "#{SITE}/#{params[:lang]}/users/gmfc_thanks?dt=#{Date.parse(gmfc_qus_resp["data"]["winners_declared_at"]).strftime("%d/%m/%Y")}"
        else
        redirect_to "#{SITE}/users/gmfc_thanks?dt=#{Date.parse(gmfc_qus_resp["data"]["winners_declared_at"]).strftime("%d/%m/%Y")}"
        end
     end
    else
      @gmfc_win_dt = ""
      @gmfc_slot_id = ""
      @gmfc_cntst_qusts = []
    end
  end

  def gmfc_answers
    answ = params[:answers]
    cnts_qts = []
    answ.each do |a|
     cnts_qts << { :question_id =>  a.split("$")[0],
           :contest_id => params[:contest_id],
           :answer_id =>  a.split("$")[1].split("a_")[1],
           :language_code =>  params[:language],
            :slot_id => params[:slot_id]
      }
    end
      qu_dt = {
      :analytical_id => cookies[:user_analytical_id], 
      :user_contest_details => cnts_qts
      }
      p qu_dt.inspect
      ans_resp = User.send_cntst_answers(qu_dt)
      p ans_resp.inspect
      p "@@@@@@@@@@@@@@@@@@"
  end

  def gmfc_user_signup
    if ($region == "IN")
      signup_dt = { :user => { 
        :account_id => "91"+params[:phone_no], 
        :analytical_id => cookies[:user_analytical_id],
        :login_type => "msisdn",
        :region => "IN",
        :is_send_otp => true
        }
      }
    elsif ($region == "US")
      signup_dt = { :user => { 
        :account_id => params[:email], 
        :analytical_id => cookies[:user_analytical_id],
        :login_type => params[:email],
        :region => "US",
        :is_send_otp => false
        }
      }
   end 

    p signup_dt.inspect
    st = "true"
    gmfc_resp =  User.gmfc_user_register(signup_dt)
    p gmfc_resp.inspect
    st = "false" if gmfc_resp["status"] == "failed"
    render :json => {:status =>  st}
  end

  def gmfc_otp_verify
   otp_veri_dt = {
    :analytical_id => cookies["user_analytical_id"],
    :verification_key => params[:otp]
    }
  gmfc_otp_resp = User.gmfc_otp_verify(otp_veri_dt)
  resp_st = "true"
  resp_st = "false" if gmfc_otp_resp["status"] == "failed"
  render :json => {:status =>  resp_st}
  end

  def gmfc_winners
    if params[:start_date].present? 
      dt = params[:start_date]
    else
      dt = DateTime.now.strftime('%Q')
    end
    gmfc_win_resp = User.get_gmfc_winners(params[:cnts_id],dt)
    if gmfc_win_resp.has_key?("data")
     @winners = gmfc_win_resp["data"]["winners"]
    else
       @winners = []
    end
  end

  def ext_mobile
    mob_dt = {
      :data => {
        :user_id => cookies[:user_analytical_id],
        :mobile_number => "91"+params[:mob_no]
      }
    }
    ext_mob_resp = User.external_otp(mob_dt)
    p ext_mob_resp.inspect
    st = "true"
    message = ""
    if ext_mob_resp.has_key?("error")
      st = "false"
      message = ext_mob_resp["error"]["message"]
    end
    render :json => {:status => st,:msg => message}
  end

  def ext_valid_otp
    otp_dt = {
      :data => {
        :otp => params[:otp],
        :user_id => cookies[:user_analytical_id],
        :mobile_number => params[:mob_no]
      }
    }
    ext_mob_resp = User.external_verify_otp(otp_dt)
    p ext_mob_resp.inspect
    st = "true"
    if ext_mob_resp.has_key?("error")
      st = "false"
      message = ""
    end
    render :json => {:status => st,:msg => message}
  end

  def update_lang
   lang_data = {
    :data => {
      :user_id => params[:usr_anly_id],
      :language_code => params[:lang]
    }
   }
   p lang_data.inspect
   lang_resp = User.update_language(lang_data)
   render :json => {:status => "true"}
  end

  def sub_events
    logger.info "======custom params====#{params[:evnt_dt].to_json.inspect}======"
    p "subscriptions event data =====#{params[:evnt_dt].to_json.inspect}================"
    sub_response = User.send_sso_events(params[:evnt_dt])
    logger.info "======custom backend response====#{sub_response.inspect}======"
    p "subscriptions event response data =====#{sub_response.inspect}================"
    render :json => {:status => true}
  end

  def shuthayu_tnc
  end

 private

   def get_user_all_profiles(user_id)
     user_profiles = User.get_all_user_profiles(user_id)
     all_profiles = user_profiles['data']['profiles'].collect{|x|[x['profile_id']+"$"+x['firstname']+"$"+x["child"].to_s+"$"+x["default_profile"]]}.compact.flatten
     default_profile_name = user_profiles['data']['profiles'].collect{|x|x['firstname'] if x["default_profile"] == "true"}.compact.flatten
     return all_profiles, default_profile_name
   end

   def set_response(response)
    if response.has_key?('error')
      if response['error']['message']['email_id'] == "has already been taken"
        render json: {error_message: "Email Id has already registered",status: false}
      else
        render json: {error_message: "#{response['error']['message']}",status: false }
      end
    else
      if response.has_key?("data") && response["data"]["session"]
        render json: {error_message: "#{response['data']['message'] if response.has_key?("data")}",status: true,user_id: "#{response["data"]["session"]}",user_name: "#{response["data"]["firstname"]}"}
      else
       render json: {error_message: "#{response['data']['message'] if response.has_key?("data")}",status: true }
      end
    end
  end

  def user_plan_init(p_id,email_id,mobile_no,ses_id,params)
   resp = Ott.all_access_plans
   all_plans = resp["data"]["catalog_list_items"]
    payment_type = "ghoori"
    sel_plan = all_plans[0]["plans"].collect{|x|x if x["id"] == p_id}.compact.first
    unless sel_plan.nil?
    plan_ids = sel_plan["id"]
    cel_plan = params[:all_dt]
    plan_currency = "INR"
    cel_plan = params[:all_dt]
    browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
    platform_type = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
    new_plan_init_params = {
      :us => get_payment_signature_key(plan_ids,ses_id),
      :payment_gateway => payment_type,
      :platform => platform_type,
      :payment_info => {
        :net_amount => sel_plan["discounted_price"], 
        :price_charged => sel_plan["discounted_price"],
        :currency => "BDR", 
        :packs => get_selected_packs(all_plans[0],p_id)
      },
      :transaction_info => {
        :app_txn_id => "", 
        :txn_message => "Ghoori init call", 
        :txn_status => "init", 
        :order_id => "", 
        :pg_transaction_id => ""  
       },
      :user_info => {
       :email => email_id,
       :mobile_number => mobile_no,
       :ext_user_id => ""
      },
      :miscellaneous => {
       :browser => browser_name,
       :device_brand =>  "NA",
       :device_IMEI => "NA",
       :device_model =>  "NA", 
       :device_OS =>  "NA" ,
       :device_type =>  "NA" ,
       :inet => "NA",
       :isp =>  "NA",
       :operator =>  "NA",
       :id => cel_plan["id"],
       :opid => cel_plan["opid"],
       :p5 => cel_plan["p5"],
       :sid => cel_plan["sid"],
       :tpid => cel_plan["tpid"],
       :m => cel_plan["m"],
       :seltxid => cel_plan["seltxid"]
      }
    }
  logger.info "======D2c final Init data here====#{new_plan_init_params.inspect}======"
   transaction_resp = User.send_init_transaction(ses_id,new_plan_init_params)
   p transaction_resp.inspect
  else
    transaction_resp = {"error"=> "invalid plan id"}
  end
  return transaction_resp   
  end
  def get_payment_signature_key(plan_id,ses_id)
     secret_key = "7c32c524a67f405812ca"
     #sec_key = secret_key+cookies[:user_id]+$region+plan_id 
     p plan_id.inspect
     p "plan info heree"
     sec_key = secret_key+ses_id+$region+plan_id 
     p sec_key.inspect
     md5_sign = Gibberish::MD5(sec_key) 
    return md5_sign
   end

   def get_selected_packs(ac_pk,p_id)
    all_packs = []
    pack_data = {
    :plan_categories => ["all_access"], 
    :category_type =>"individual",
    :category_pack_id => ac_pk['content_id'],
    :subscription_catalog_id => ac_pk['catalog_id'], 
    :plan_id => p_id
    }
    all_packs << pack_data
   return all_packs
  end


  def tvod_pack_activate(pack_dt,user_id,promo_dt,trans_st,order_id,params)
    plan_det = promo_dt["data"]["payment_info"][0]
    vers = cookies[:app_version].nil? ? "" : cookies[:app_version]
    pln_id = plan_det["plan_id"]
    #browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
    browser_name = "Generic_browser"
    platform_type = cookies["mobile_browser_type"] == "android" ? "Android" : "WEB"
    account_resp = User.get_user_account_details(cookies[:user_id])
    acc_det = account_resp["data"]
   if acc_det["login_type"] == "external"
      if acc_det["user_type"] == "msisdn"
        mobile_no = acc_det["mobile_number"]
        email_id = "" 
      else
        mobile_no = ""
        ext_userid = acc_det["ext_user_id"]
        email_id = ""
      end
    else
      if acc_det["login_type"] == "msisdn"
      mobile_no = acc_det["mobile_number"]
      email_id = ""
    else
      email_id = acc_det["email_id"]
      mobile_no = ""
    end
  end
  if plan_det["plan_categories"][0] == "gujrathi" 
    pln_category = "gujarati"
  else
    pln_category = plan_det["plan_categories"][0]
  end
    new_plan_init_params = {
    :us => get_user_payment_signature_key(pln_id,cookies[:user_id]),
    :payment_gateway => "coupon",
    :platform => platform_type,
    :payment_info => {
    :net_amount => "0", 
    :price_charged => "0",
    :currency => get_currency_sign,
    :coupon_code=> promo_dt["data"]["coupon_info"]["coupon_code"],
    :coupon_code_id => promo_dt["data"]["coupon_info"]["coupon_id"],
    :packs => [{
    :plan_id=> pln_id,
    :plan_categories=> [pln_category],
    :category_type =>  plan_det["category_type"],
    :subscription_catalog_id => plan_det["subscription_catalog_id"],
    :category_pack_id => plan_det["category_pack_id"]}]},
    :transaction_info =>{
    :app_txn_id=> vers,
    :txn_message=>"CMS",
    :txn_status=> trans_st,
    :pg_transaction_id=>"cms_1",
    :order_id => order_id
    },
    :user_info => {
    :email => email_id,
    :mobile_number => mobile_no,
    },
    :miscellaneous => {
    :browser => browser_name
    },
    :trans_info => {
         :id => platform_type == "WEB" ? params['branch_data']['browser_fingerprint_id'] : params['branch_data']['aaid'],  
        :os => params['branch_data']['os'],
        :city => params['branch_data']['city'],
        :device_id => platform_type == "WEB" ? "" : params['branch_data']['device_id'],
        :device_make => platform_type == "WEB" ? "" : params['branch_data']['device_make'],
        :user_ip => platform_type == "WEB" ? $user_ip : params['branch_data']['user_ip'],
        :user_agent => params['branch_data']['user_agent'],
        :http_origin => "c65f433f-a2fe-4321-a494-f19d80603b21",
        :mac_address =>"83a54d80-b87c-459f-88b0-3e8965938e80"
      }
    } 
    logger.info "redeem data here----#{new_plan_init_params.inspect}-------" 
    transaction_resp = User.send_init_transaction(cookies[:user_id],new_plan_init_params)
    p transaction_resp.inspect
    return transaction_resp
  end
  
end
