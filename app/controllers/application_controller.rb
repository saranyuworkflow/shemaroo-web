class ApplicationController < ActionController::Base
 protect_from_forgery with: :null_session
 skip_before_action :verify_authenticity_token  

# protect_from_forgery unless: -> { request.format.json? }
before_action :get_region,:check_browser,:get_lang_app_strings, :user_status_check
before_action :get_home_tabs, :get_lang_popup, unless: -> { request.xhr? }
#[:sign_up,:sign_in,:validate_otp,:resend_otp]

include ApplicationHelper

def get_home_tabs
  lang = get_header_lang(params)
  resp = Rails.cache.fetch("home_new_tabs_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
    Ott.get_new_home_tabs(lang)   
  }
  @header_tabs = resp["data"]["catalog_list_items"] 
  @header_tabs_links = resp["data"]["catalog_list_items"] .collect{|x|x['home_link']}
  p @header_tabs_links.inspect
  lang_resp  = Rails.cache.fetch("config_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
    Ott.get_configuration   
  }
  p "@@@@@@@@@@@@@@@@@@@@@@@@@@@2"
  @mb_login_type = lang_resp["data"]["params_hash2"]["config_params"]["mobile_login_IN"]
  @all_langs = lang_resp["data"]["params_hash2"]["config_params"]["ml_languages"]

  browse_resp =  Rails.cache.fetch("header_browse_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_browse_categories(lang)      
       }
     @browse_list = browse_resp["data"]["catalog_list_items"]
end

def get_lang_app_strings
  lang = get_header_lang(params)
  lang_resp = Rails.cache.fetch("language_app_strings_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_app_strings(lang)    
  }
  @all_app_str = lang_resp["data"]
end



def get_lang_popup
  lang = get_header_lang(params)
  popup_resp = Rails.cache.fetch("language_app_popup_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_app_popup(lang)    
  }
  @all_app_popup = popup_resp["data"]
end



#get the user region based on ip address
  def get_region
      browser = Browser.new(request.env["HTTP_USER_AGENT"], accept_language: "en-us")
      logger.info "Browser platform==========#{browser.platform.name.inspect}===="
      logger.info "Browser details here===#{browser.inspect}=============="
    begin
       $user_ip = "127.0.0.1"
      if Rails.env == "development"
        #@region = "MY"
        #@region = "CA"
        @region = "IN"
        $region = @region
        p $region.inspect
        $city = "Bengaluru"
        @phone_prefix = "91"
        @max_ph_digits = "10"
        @country_name = "India"
      else
        # if params[:_branch_match_id].present?
        #   redirect_to "#{SITE}"
        # else
        allowed_regions = ["IN","US","BD","MY"]
        allowed_action_names = ["plans_summary","new_plans_summary","sign_in","sign_up","new_plans_init_transaction","add_new_plan","new_plan_summary","new_plans_init_transaction","check_valid_user_session","googlepay_response","login","activate_code","registered_devices","consulate_general","view_plans","plan_details","eoip","hcic","cgic","eois"]
        headers = request.headers
        ip = get_ip
        response = Ott.get_user_region(ip) #3.127.25r5.255 #91.74.212.214
        #canada "204.101.161.58"
        $city = response['region']['city']
        @region = response["region"]["country_code2"]
        @country_name = response["region"]["country_name"]
        $user_ip = ip
        $region = @region
        @phone_prefix = response["region"]["calling_code"].gsub("00","")
        @max_ph_digits = response["region"]["max_digits"]
        if headers["HTTP_FROM"].present? && headers["HTTP_FROM"] == "googlebot(at)googlebot.com"
          unless allowed_regions.include?($region)
            @region = "US"
            $region = @region
          end
        elsif params[:action] == "etisalat_terms" || params[:action] == "etisalat_privacy_policy" || params[:action] == "etisalat_faq"
          @region = "IN"
          $region = @region
        elsif ip.include?("face")
          unless allowed_regions.include?($region)
            @region = "US"
            $region = @region
          end
        elsif params[:browser] == "android" || params[:type] === "mobile"
          allowed_regions << $region
        elsif allowed_action_names.include?(params[:action])
          allowed_regions << $region 
        end

        p allowed_regions.inspect
          unless allowed_regions.include?($region)
            status = check_page_status(params[:action])
            # if status == "false"
            #   redirect_to "#{SITE}/landing_page", status: 302
            #   return false
            # end
          end
        end
       
        #end
    rescue
      @region = "IN"
      $region = @region
    end
  end


def check_page_status(action_name)
  status = "false"
 if action_name == "privacypolicy" || action_name == "rehlatoffer" || action_name == "term_conditions" || action_name == "index" || action_name == "landing_page" || action_name == "thanks" || action_name == "pre_register" || action_name == "coupon_register" ||  action_name == "validate_promocode" || action_name == "branch_s2s"
  status = "true"
 end
 return status
end

  def get_ip 
    begin
    if request.headers["HTTP_VIA"]
    status,ip = get_x_forwarded_for_ip request
    unless status
    ip = get_ip_from_http_true_client request
    end
    return ip
    else
    ip = get_ip_from_http_true_client request
    end
    rescue
    ip = get_ip_from_http_true_client request
    end
    return ip
  end

    def get_x_forwarded_for_ip request
      ip_addresses = request.headers["HTTP_X_FORWARDED_FOR"]
      if ip_addresses
      ip_array = ip_addresses.split(",")
      ip = ip_array.first
      return [true, ip]
      else
      return [false, ""]
      end
    end

    def get_ip_from_http_true_client request
      if request.headers["HTTP_TRUE_CLIENT_IP"]
      ip = request.headers["HTTP_TRUE_CLIENT_IP"]
      else
      ip = request.remote_ip
      end
      return ip
    end

  # def get_user_ip
  #   if request.headers["HTTP_VIA"]
  #     if request.headers["HTTP_VIA"].include?"1.1 Chrome-Compression-Proxy"
  #       status,ip = get_x_forwarded_for_ip
  #       unless status
  #         ip = get_ip_from_http_true_client
  #       end
  #     else
  #       ip = get_ip_from_http_true_client
  #     end
  #   elsif request.headers["HTTP_X_CONTENT_OPT"]
  #     if request.headers["HTTP_X_CONTENT_OPT"].downcase.include?"turbo"
  #       status,ip = get_x_forwarded_for_ip
  #       unless status
  #         ip = get_ip_from_http_true_client
  #       end
  #     else
  #       ip = get_ip_from_http_true_client
  #     end
  #   else
  #     ip = get_ip_from_http_true_client
  #   end    
  # end

  # def get_x_forwarded_for_ip
  #   ip_addresses = request.headers["X-FORWARDED-FOR"]
  #   if ip_addresses
  #     ip_array = ip_addresses.split(",")
  #     ip = ip_array.first
  #     return [true, ip]
  #   else
  #     return [false, ""]
  #   end
  # end

  # def get_ip_from_http_true_client
  #   if request.headers["HTTP_TRUE_CLIENT_IP"]
  #     ip = request.headers["HTTP_TRUE_CLIENT_IP"]
  #   else
  #     ip = request.remote_ip
  #   end
  #   return ip
  # end


    def check_browser
    ipad_device = request.user_agent.scan('iPad')
    if ipad_device.blank?
      @browser_type = detect_navigator
      if @browser_type.nil?
        @browser_type = 'desktop'
        cookies.delete "browser_type" if cookies["browser_type"]
      else
        cookies["browser_type"] = {
            value: @browser_type
        }
      end
    end
  end

  def detect_navigator
    return "mobile" if /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.match(request.user_agent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.match(request.user_agent[0..3])
  end

  def sign_smarturl smarturl,val=nil
    smart_url_key = PLAY_URL_TOKEN
    serv_id = "1"
    # if Rails.env.production?
    #   serv_id = "107"
    # else
    #   serv_id = "1"
    # end
    url = "#{smarturl}" + "?service_id=#{serv_id}&protocol=hls&play_url=yes"
    base_url = "#{url}&play_url=yes&us="
    signature = Digest::MD5.hexdigest("#{smart_url_key}#{base_url}")
    signed_url =  "#{base_url}#{signature}"
    header = {"Accept" => "application/json", "Cache-Control" => "no-cache"}
    resp = Typhoeus.get(signed_url,headers:header)
    obj = JSON.parse(resp.body)
    return obj
  end

  def get_category_seo(slug)
    case slug
    when "bollywood-plus"
     title = "Bollywood Movies | Watch Bollywood Movies Online On ShemarooMe"
     desc  = "Watch bollywood movies on ShemarooMe. Browse your favourite Hindi movies at ShemarooMe. Download ShemarooMe app to get access to unlimited bollywood movies, you can explore list of 1990,2000 bollywood films and more movies at ShemarooMe."
     keywords = "Watch bollywood movies online, bollywood movie, bollywood movies online"
    when "bollywood-classic"
      title = "Old Hindi Movie | Watch Old Bollywood Movies Online| Superhit Old Hindi Movies On ShemarooMe"
      desc  = "Watch old Classic movies On shemarooMe. Browse your favourite  old hindi Movies at ShemarooMe.   Download ShemarooMe app to get access to unlimited Old Hindi movies, you can explore old Bollywood Movies at ShemarooMe."
      keywords = "Old Hindi Movie, Superhit Old Hindi Movies, old hindi movie, old hindi film"
    when "gujarati"
      title = "Watch Gujarati Movies Online | Gujarati Songs | Gujarati Natak - ShemarooMe "
      desc  = "Watch High Quality Gujarati Movies, Gujarati Natak And Gujarati Superhit Songs on ShemarooMe. Browse your favourite Gujarati Movies, Natak & songs at ShemarooMe. Download ShemarooMe app to get access to unlimited Gujarati Movies, Natak & songs."
      keywords = "Watch Gujarati Movies Online, Watch Gujarati Songs, Watch Gujarati Natak,"
    when "marathi"
      title = "Watch Marathi Movies Online | Watch old Marathi Movies Online | List of Marathi Movies - ShemarooMe"
      desc  = "Watch High Quality Marathi Movies and Marathi Natak on ShemarooMe. Browse your favourite Marathi Movies and Natak  at ShemarooMe. Download ShemarooMe app to get access to unlimited Marathi Movies and Marathi Natak."
      keywords = "Marathi Movies, Marathi Songs, Marathi play, marathi Natak"
    when "comedy" 
       title = "comedy"
    #   desc  = ""
    #   keywords = ""
    when  "kids"
       title = "kids"
    #   desc  = ""
    #   keywords = ""
    when  "punjabi"
      title = "Watch Punjabi Movies | Punjabi songs | Punjabi Comedy |Gurubani - ShemarooMe"
      desc  = "Watch High Quality Punjabi Movies, Punjabi Songs, Gurubani and Punjabi Comedy on ShemarooMe. Browse your favourite Punjabi Movies, Gurubani and Punjabi Songs at ShemarooMe. Download ShemarooMe app to get access to unlimited Punjabi Movie, Punjabi Songs, Gurubani and Punjabi Comedy."
      keywords = "punjabi movies, gurubani, punjabi comedy, punjabi songs"
  end
  return title,desc,keywords
  end

  def get_header_lang(params)
     lang = "en"
     new_lang = request.path.split("/")[1]
    if ["en","hi","ma","gu"].include? new_lang
      params[:lang] = new_lang
    end
    if params[:lang].present?
     lang = params[:lang] 
    elsif cookies[:shm_sel_lang].present?
     lang = cookies[:shm_sel_lang] 
    end
     return lang
  end

   def get_user_lang(params)
     lang = "en"
     if(params[:lang].present? && ["en","hi","ma","gu"].include?(params[:lang]))
      lang = params[:lang] 
     elsif cookies[:shm_sel_lang].present?
      lang = cookies[:shm_sel_lang] 
    end
     return lang
  end


  def user_status_check
    @user_status = "false"
    if cookies[:user_id]
    user_plans = Ott.user_plans(cookies[:user_id],cookies[:shm_sel_lang])
      if (user_plans["current_active_plans"].present? && user_plans["current_active_plans"].count == 0 && user_plans['previous_plans'].present?)
          @user_status = "expired"
      elsif (user_plans["current_active_plans"].present? && user_plans["current_active_plans"].count > 0)
            @user_status = "active"
      end 
    end
  end
  
end



