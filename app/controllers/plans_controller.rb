#require "#{Rails.root}/lib/amazon_pay"
class PlansController < ApplicationController
  include PlansHelper
  include AmazonPay
  include ChargeStAmazonPay
  include LazyPay



  def all_plans
    if @region == "US"
      @currency = "$"
    else
      @currency = "₹"
    end
    @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
    login_user_id = params[:user_id].present? ? params[:user_id] : cookies[:user_id]
    lang = get_user_lang(params)
    response = Ott.new_plans(lang)
    config_resp = Rails.cache.fetch("design_configuration_list_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_configuration      
    }
    @plan_info_st = config_resp["data"]["params_hash2"]["config_params"]["plans_info_status"]
    @pln_msg = config_resp["data"]["params_hash2"]["config_params"]["plans_info"]
    @all_plans = response["data"]["catalog_list_items"]
    @cat_titles = []
    @is_postpaid = ""
    if params[:user_id].present?
      login_user_id  = params[:user_id]
    else
      login_user_id = cookies[:user_id]
    end
    if $region == "KW" && !login_user_id.nil?
      account_resp = User.get_user_account_details(login_user_id)
      login_type = account_resp["data"]["user_type"]
      if login_type == "msisdn"
        phone_num = account_resp["data"]["mobile_number"]
        user_id =  account_resp["data"]["user_id"]
        txn_id = user_id + "_" + phone_num
        #phone_num = "96560499190"
        @is_postpaid = pre_post_paid_check(phone_num,txn_id) 
      end
    end
    @all_plans.each do |plan|
      if plan["plans"].present?
        plan["plans"].each do |pl|
          title = "#{plan['category']}-#{pl['title']}"
          @cat_titles <<  title.downcase
        end
      end
    end

    @free_trail_status = "false"
    @free_trail_msg = ""
    if !login_user_id.nil?
      user_plans = Ott.user_plans(login_user_id,lang)
      @free_trail_status = user_plans["first_time_subscription"]
      @free_trail_msg = user_plans["message"]
      @free_trail_date = (Date.today+7.days).strftime("%d/%B/%Y")
      @free_trail_prev_date =  (Date.today+6.days).strftime("%d/%m/%Y")
      unless user_plans.has_key?("error")
        if user_plans["previous_plans"].count > 0 || user_plans["current_active_plans"].count > 0
          if params[:user_id].present?
            lang = get_user_lang(params)
              if lang != "en"
                 url = "#{SITE}/#{lang}/plans/view_plans?theme_option=#{@theme}&user_id=#{params[:user_id]}&user_login_id=#{params[:user_login_id]}&user_analytic_id=#{params[:user_analytic_id]}&platform_type=#{params[:platform_type]}&aaid=#{params[:aaid]}&city=#{params[:city]}&device_id=#{params[:device_id]}&device_make=#{params[:device_make]}&user_ip=#{params[:user_ip]}&os=#{params[:os]}&user_agent=#{params[:user_agent]}"
              else
                url = "#{SITE}/plans/view_plans?theme_option=#{@theme}&user_id=#{params[:user_id]}&user_login_id=#{params[:user_login_id]}&user_analytic_id=#{params[:user_analytic_id]}&platform_type=#{params[:platform_type]}&aaid=#{params[:aaid]}&city=#{params[:city]}&device_id=#{params[:device_id]}&device_make=#{params[:device_make]}&user_ip=#{params[:user_ip]}&os=#{params[:os]}&user_agent=#{params[:user_agent]}"
              end
            if params[:version].present?
              url = url+"&version=#{params[:version]}"
            end
             redirect_to url
          else
            if lang != "en"
              redirect_to "#{SITE}/#{lang}/plans/view_plans" 
           else
             redirect_to "#{SITE}/plans/view_plans"  
            end 
                                    
          end
        end
      end
    end
  end

  def upgrade_plans
    login_user_id = params[:user_id].present? ? params[:user_id] : cookies[:user_id]
    @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
    if login_user_id .present?
      lang = get_user_lang(params)
      user_plans = Ott.user_upgrade_plans(login_user_id,lang)
      if user_plans["data"].present?
        @user_plans = user_plans["data"]
        @old_sub_id = @user_plans['upgrade_old_sub_id']
        @old_plan_id = @user_plans['upgrade_old_plan_id']
        if (@user_plans['renewal'] == false && @user_plans['upgrade'] == false)
          @type =  params[:browser] ? 'android' : 'web'
          render "upgrade_alert"
        end
        @renewal_plns = []
        if @user_plans["renewal_plans"].present?
          @user_plans["renewal_plans"].each do |pln| 
            pln["plans"].each do |plan|
              @renewal_plns << [plan["id"], plan["discounted_price"]]
            end
          end
        end
        @upgrade_plns = []
        @user_plans["upgrade_plans"].each do |pln| 
          pln["plans"].each do |plan|
          @upgrade_plns << [plan["id"], plan["discounted_price"]]
        end
      end
      else
        if lang != "en"
            redirect_to "#{SITE}/#{lang}/plans/view_plans" 
       else
           redirect_to "#{SITE}/plans/view_plans"  
        end 
      end  
    else
      redirect_to "#{SITE}"
    end
  end

  def upgrade_alert
  end  

  def set_upgrade_plan
    amount = ''
     eval(params["amount"]).each do |val|
      amount = val.last if val.first == params['plan_id'] 
    end
      plans_params = { 
        :amount => amount, 
        :plan_id => params['plan_id'],
        :transaction_type => params['transaction_type'],
        :subscription_id => params['subscription_id']
        }

    login_user_id = params[:user_id].present? ? params[:user_id] : cookies[:user_id]
    lang = get_user_lang(params)
     logger.info "set_upgrade_plan============#{plans_params}=============="
    user_plans = Ott.set_upgrade_plan(login_user_id,lang,plans_params)
  end

  def validate_lzy_mobile
    if cookies[:user_login_id].present?
      mob_no = cookies[:user_login_id].last(10)
    else
      account_det = User.get_user_account_details(params[:user_id])
      mob_no = account_det["data"]["mobile_number"].last(10)
    end
    otp_chk = params[:otp_check].present? ? "true" : "false"
    amount  = params[:amt].present? ? params[:amt] : "129"
    pln_catgry = params[:plan_catgry].present? ? params[:plan_catgry].upcase : "MONTHLY"
    lazy_resp = lazpy_eligibil_check(mob_no,"",amount,pln_catgry,LZYPY_BASE_URL,LZYPY_ACCESS_KEY,LZYPY_SECRET_KEY,otp_chk)
    if lazy_resp["code"] == "LP_ELIGIBLE" && !lazy_resp["sub_err_code"].present?
      @vaild_lazy_num = true
    else
      @vaild_lazy_num = false
    end
  end


  def plans_summary
    @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
    plan_id = cookies[:plan_id]
    subscription_id = cookies[:sub_id]
    app_type = cookies[:app_type]
    payment_checkout = Ott.payment_checkout(plan_id, subscription_id, app_type)
    @payment_details = (payment_checkout["data"].present? && payment_checkout["data"]["items"].present?)  ? payment_checkout["data"]["items"] : " "
    all_plans =  Rails.cache.fetch("subscription_plans_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
      Ott.subscription_plans
    }
    @all_access_packs = all_plans["data"]["catalog_list_items"].last["catalog_list_items"]
    #byebug %d %b %Y
    @region = $region
    @free_trail_date = (Date.today+7.days).strftime("%d %B, %Y") ? (Date.today+7.days).strftime("%d %B, %Y") : ""
    @free_trail_prev_date =  (Date.today+6.days).strftime("%d %B, %Y")
    p @free_trail_prev_date.inspect
    @shipping_zip_codes = SHIPPING_ZIP_CODES
    p "@@@@@@@@@@@@"
    if params[:user_id].present?
      login_user_id  = params[:user_id]
    else
      login_user_id = cookies[:user_id]
    end
    account_resp = User.get_user_account_details(login_user_id)
    login_type = account_resp["data"]["user_type"]
    @vaild_lazy_num = false
     if  $region == "IN" && login_type == "msisdn"
       @user_mobile_no = account_resp["data"]["mobile_number"].last(10)
       @vaild_lazy_num = validate_lzy_mobile
     end

      if $region == "KW" && login_type == "msisdn"
        phone_num = account_resp["data"]["mobile_number"]
        user_id =  account_resp["data"]["user_id"]
        txn_id = user_id + "_" + phone_num
        #phone_num = "96560499190"
        @is_postpaid = pre_post_paid_check(phone_num,txn_id) 
    end
    # user_session = cookies[:user_id].to_s
    # user_plans = Ott.user_plans(user_session)
    # redirect_to plans_view_plans_path if user_plans["current_active_plans"].count >0 and return

    # response = Ott.subscription_plans
    # @all_plans = response["data"]["catalog_list_items"]
    # @all_access_packs = @all_plans.last["catalog_list_items"].last
    # if params["plans"].present? && params["plans"].split(",").count == 2 && params["plans"].split(",").map{|p| p.split("|")[2].downcase}.uniq.count == 1
    #   plan_title = params["plans"].split(",").map{|a| a.split("|")[2] }.last
    #   items  =   Ott.get_catalog_details("5b3c917fc1df417b9a00002c")
    #   @combo_plan = items["data"]["items"].map{|cp| cp if cp["category_type"]=="combo"}.compact[0]
    #   @combo_pack = @combo_plan["plans"].map{|e| e if e["title"].downcase == plan_title.downcase}.compact.last
    #   render "combo_plans_summary"
    # elsif params["plans"].present? && params["plans"].split(",").count == 4
    #   items  =   Ott.get_catalog_details("5b3c917fc1df417b9a00002c")
    #   @monthly_combo_plan = items["data"]["items"].map{|cp| cp if cp["category_type"]=="combo"}.compact[0]
    #   @monthly_combo_pack = @monthly_combo_plan["plans"].map{|e| e if e["title"].downcase =="monthly"}.compact.last

    #   @yearly_combo_plan = items["data"]["items"].map{|cp| cp if cp["category_type"]=="combo"}.compact[0]
    #   @yearly_combo_pack = @yearly_combo_plan["plans"].map{|e| e if e["title"].downcase == "yearly"}.compact.last
    #   render "two_combo_plans_summary"
    # end
    @amz_mobile_url = "#{SITE}/payment/mobile_pay_amazonpay"
  end

 

  def payment_success
    # params.require(:resp_data).permit
    # @resp = params[:resp_data]
    if @region == "US"
      @currency = "$"
    elsif @region == "CA"
      @currency = "CAD"
    end
  end

  def adyen_payment_success
    params[:region] = "US"
    begin
      puts "#{params.inspect}"
      response =  HTTP.post_https "payment_complete/adyen/secure_payment", params
      logger.info "@@@@@@@@@@@@@@@@@adyon payment api response @@@@@@@@@@@@@@@@@@@@@@@@@@"
       logger.info "============#{response.inspect}=============="
    rescue Exception => e
      logger.info "=====payment error #{e.inspect}===="
    end
      logger.info "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      logger.info "============#{response.inspect}=============="
     if response.has_key?("error")
      @message = response["error"]["message"]
      @status = "failure"
      @heading_msg = "Transaction Failed"
      @payment_resp = response["error"]
    else
      @payment_resp = response["data"]
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successful"
      @price = response["data"]["price_charged"]
    end
  end

  def ghoori_payment_success
    # if params[:bKashMsisdn].present?
    #    ghoori_data = {
    #     :user_info => {
    #       :provider => "ghoori",
    #       :uid => params[:mobile],
    #       :spTransID => params[:spTransID],
    #       :package => params[:package],
    #       :mobile => params[:mobile],
    #       :amount => params[:amount],
    #       :bKashMsisdn => params[:bKashMsisdn],
    #       :status => params[:status],
    #       :paymentDate => params[:paymentDate],
    #       :processingStatus => params[:processingStatus],
    #       :bKashTransID => params[:bKashTransID],
    #       :actionTaken => params[:actionTaken],
    #       :payment_gateway => "ghoori",
    #       :transaction => {
    #         :amount => params[:amount]
    #       }
    #     }
    #    }
    #    logger.info "===========Ghoori Parameters===#{ghoori_data.inspect}============="
    #    ghoori_resp = User.send_ghoori_registration_payment(ghoori_data)
    #    logger.info "================Backend response#{ghoori_resp.inspect}=============="
    #    logger.info "========================================"
    #    render :json => {:message => ghoori_resp["data"]["ghoori_resp"]}
      #render :json => {:message => "pack activated successfully"}
    #else
      begin
        puts "#{params.inspect}"
        # response =  HTTP.post_https "payment_complete/ghoori/secure_payment", params
        response = User.ghoori_secure_payment(params)
        logger.info "@@@@@@@@@@@@@@@@@ghoori payment api response @@@@@@@@@@@@@@@@@@@@@@@@@@"
        logger.info "============#{response.inspect}=============="
        if response.has_key?("data") && response["data"].has_key?("miscellaneous")
          post_bck_dt = response["data"]["miscellaneous"]
          if response.has_key?("error")
              transaction_id = response["error"]["message"]['transaction_id']
              order_id = response["error"]["message"]['order_id']
              st = "failure"
          else
            transaction_id = response["data"]['transaction_id']
            order_id = response["data"]['order_id']
            st = "success"
          end
          post_bck_dt["SMEOrderid"] = order_id
          post_bck_dt["ChargingTransID"] = transaction_id
          post_bck_dt["status"] = st
          logger.info "===================D2c posting data here----#{post_bck_dt.to_json.inspect}================"
          http_headers =  {"Content-Type" => "application/json", "Accept"=>"application/json" }
          call_bck_resp = Typhoeus.post("http://m.shemaroo.com/intl/Pay/ShemarooMeNonTelco/Callback.aspx",:body => post_bck_dt.to_json,:headers => http_headers)
          logger.info "=============D2c post call back response=====#{JSON.parse(call_bck_resp.body).inspect}================"
          logger.info "===================Partner call back for Ghoori posting data here----#{post_bck_dt.to_json.inspect}================"
          call_bck_resp1 = Typhoeus.post("http://120.50.12.54/club/shemaroo/api.php?msisdn=#{post_bck_dt["bKashMsisdn"]}&pkg=#{post_bck_dt['package']}&amount=#{post_bck_dt["amount"]}&channel=web",:headers => http_headers)
          logger.info "=============Partner call back for Ghoori response data here=====#{JSON.parse(call_bck_resp1.body).inspect}================"

        end
      rescue Exception => e
        logger.info "=====payment error #{e.inspect}===="
      end
      if request.get?
          logger.info "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
          logger.info "============#{response.inspect}=============="
         if response.has_key?("error")
          @message = ""
          #response["error"]["message"]["message"]
          @status = "failure"
          @heading_msg = response["error"]["message"]["message"]
          @payment_resp = response["error"]
          @transaction_id = response["error"]["message"]['transaction_id']
          @order_id = response["error"]["message"]['order_id']
        else
          @payment_resp = response["data"]
          @message =  response["data"]["message"]
          @status = "success"
          @heading_msg = "Transaction Successful"
          @transaction_id = @payment_resp['transaction_id']
          @order_id = @payment_resp['order_id']
          @price = response["data"]["price_charged"]
        end
      else
        render plain: 'SUBSCRIBED'
      end
    #end
    #
  end

  def ghoori_external_payment_success
    begin
      puts "#{params.inspect}"
      response =  HTTP.post_https "payment_complete/ghoori/secure_payment", params
      logger.info "@@@@@@@@@@@@@@@@@ghoori payment api response @@@@@@@@@@@@@@@@@@@@@@@@@@"
      logger.info "============#{response.inspect}=============="
    rescue Exception => e
      logger.info "=====payment error #{e.inspect}===="
    end
      logger.info "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      logger.info "============#{response.inspect}=============="
     if response.has_key?("error")
      @message = response["error"]["message"]
      @status = "failure"
      @heading_msg = "Transaction Failed"
      @payment_resp = response["error"]
    else
      @payment_resp = response["data"]
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successful"
      @price = response["data"]["price_charged"]
    end
  end

  def payment_failed
    # params.require(:resp_data).permit
    # @resp = params[:resp_data]
    if @region == "US"
      @currency = "$ "
    else
      @currency = "₹ "
    end
    p params.inspect
  end

  def payment_canceled
    enc_resp = params["encResp"]
    order_id = params["orderNo"]
    payment_params = {"encResp": enc_resp, "orderNo": order_id, "region":"IN", "auth_token":"Ts4XpMvGsB2SW7NZsWc3", "payment_gateway":"ccavenue"}
    @resp =  HTTP.post_https "payment_complete/ccavenue/secure_payment", payment_params
    @pymt_rep = @resp["error"]["message"]
  end

  def payment_response
    enc_resp = params["encResp"]
    order_id = params["orderNo"]
    payment_params = {"encResp": enc_resp, "orderNo": order_id, "region":"IN", "auth_token":"Ts4XpMvGsB2SW7NZsWc3", "payment_gateway":"ccavenue"}
    response =  HTTP.post_https "payment_complete/ccavenue/secure_payment", payment_params
    @pln_tit = ""
    @pln_exp_st = ""
    #response = {"data" => {"message" => "aesfsd","price_charged" => "49","title" => "Movie2","expiry_date" => Time.now().to_s}}
    logger.info "=====CC AVENUE PAYMENT RESPONSE========================"
    logger.info "===============#{response.inspect}==============================="
    if Rails.env.production? 
        ccavenue_log = Logger.new('/mnt/ccavenue_payments.log')
        ccavenue_log.info("=====================Start of CCAvenue Callabck Logs=======================")
        ccavenue_log.info("Parameters from CCAvenue Callabck ----------------------> #{params.inspect} <--------------------------------")
        ccavenue_log.info("Backend response from CCAvenue ----------------------> #{response.inspect} <--------------------------------")
        ccavenue_log.info("=====================End of CCAvenue Callabck Logs=======================")
    end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Your payment could not be processed"
      @coupon_code = ""
      @coupon_title = ""
      @coupon_type = ""

    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Your payment has been processed"
      @price = response["data"]["price_charged"]
      @pln_tit = response["data"]["title"]
      @pln_exp_st = response["data"]["expiry_date"]
      @coupon_code = response["data"]["coupon_code"]
      @coupon_title = response["data"]["coupon_code_title"]
      @coupon_type =  response["data"]["coupon_code_type"]
    end
    if @region == "US"
      @currency = "$ "
    else
      @currency = "₹ "
    end
  end

  def googlepay_response
    # google_pay_params = {
    #    :orderNo =>  params[:order_id], 
    #    :payment_gateway => "google",
    #    :transaction_id => params[:tr_id],
    #    :transaction_status => params[:transaction_status]
    #  }
    @pln_tit = ""
    @pln_exp_st = ""
    response =  User.secure_payment("google",params)
    logger.info "=====GOOGLE PAY PAYMENT RESPONSE========================"
    logger.info "===============#{response.inspect}==============================="
    if Rails.env.production? 
        ccavenue_log = Logger.new('/mnt/google_payments.log')
        ccavenue_log.info("=====================Start of Google pay Callabck Logs=======================")
        ccavenue_log.info("Parameters from Google pay Callabck ----------------------> #{params.inspect} <--------------------------------")
        ccavenue_log.info("=====================End of Google pay Callabck Logs=======================")
    end
    if response.has_key?("error")
      @message = response["error"]["message"]
      @status = "failure"
      @heading_msg = "Your payment could not be processed"
      @coupon_code = response["error"]["message"]["coupon_code"]
      @coupon_title = response["error"]["message"]["coupon_code_title"]
      @order_id = response["error"]["message"]["order_id"]
      @price = response["error"]["message"]["price_charged"]
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Your payment has been processed"
      @price = response["data"]["price_charged"]
      @pln_tit = response["data"]["title"]
      @pln_exp_st = response["data"]["expiry_date"]
      @coupon_code = response["data"]["coupon_code"]
      @coupon_title = response["data"]["coupon_code_title"]
    end
    if @region == "US"
      @currency = "$ "
    else
      @currency = "₹ "
    end
  end

  
  def paytm_vpa_validate_data

    logger.info("----------#{params}")

   paytm_url = "https://securegw.paytm.in" # production
   #paytm_url = "https://securegw-stage.paytm.in" # pre production
    if (params[:mid].present? && params[:context_token].present?)

      if params[:vpa].present?
        resp =  `curl --location --request POST '#{paytm_url}/paymentsFacade/api/v1/vpa/validate?mid=#{params[:mid]}&traceId=#{params[:traceId]}' \
        --header 'Content-Type: application/json' \
        -d '{"head": {"token": "#{params[:context_token]}", "tokenType":"PAYMENT_CONTEXT_TOKEN", "channelId":"#{params[:channelId]}"}, "body": {"vpa": "#{params[:vpa]}"}}'`


        logger.info "=====VPA VALIDATE RESPONSE===#{params[:vpa]}====================="
        logger.info "===============#{resp.inspect}==============================="

        if (Rails.env.production?) 
          trans_log = Logger.new("log/paytm_upi.log")
          trans_log.info("================= VPA VALIDATE RESPONSE ====#{params[:vpa]}======================\n")
          trans_log.info("------------------------------------> #{resp.inspect} <--------------------------------")
        end

        reaponse =  JSON.parse(resp)
          p resp.inspect

          if (reaponse["body"]["resultInfo"]["resultMsg"].present? && reaponse["body"]["resultInfo"]["resultMsg"] == "Success")

            psp_supported = reaponse["body"]["requestTypeContexts"][0]["featureDetails"]["pspSupported"]
            bank_supported = reaponse["body"]["requestTypeContexts"][0]["featureDetails"]["bankSupported"]

            validate_resp = User.validate_vpa(cookies[:user_id],params[:order_id],psp_supported,bank_supported)

            logger.info "=====backend RESPONSE========================"
            logger.info "===============#{validate_resp.inspect}==============================="

              if validate_resp["data"]["message"] == "Success"
                process_resp =  `curl --location -g --request POST '#{paytm_url}/theia/api/v1/processTransaction?mid=#{params[:mid]}&orderId=#{params[:order_id]}' \
                --header 'Content-Type: application/json' \
                -d '{"body": {"requestType": "NATIVE_SUBSCRIPTION", "mid": "#{params[:mid]}",  "orderId": "#{params[:order_id]}","paymentMode": "UPI","payerAccount": "#{params[:vpa]}"}, "head": {"txnToken":"#{validate_resp["data"]["txn_token"]}"}}'`
                  process_response =  JSON.parse(process_resp)

                logger.info "=====process Transaction RESPONSE========================"
                logger.info "===============#{process_resp.inspect}==============================="

                if (Rails.env.production?) 
                  trans_log = Logger.new("log/paytm_upi.log")
                  trans_log.info("================= Process Transaction RESPONSE ======#{params[:vpa]}====================\n")
                  trans_log.info("------------------------------------> #{process_resp.inspect} <--------------------------------")
                end


                  if process_response["body"]["resultInfo"]["resultMsg"] == "Success"
                    data = process_response["body"]["bankForm"]["redirectForm"]
                    render :json => {:payment_url => data["actionUrl"],:merchant_vpa => data["content"]["MERCHANT_VPA"],:channel => data["content"]["CHANNEL"],:externalSrNo => data["content"]["externalSrNo"],:mcc => data["content"]["MCC"],:payerVpa => data["content"]["payerVpa"],:txnAmount => data["content"]["txnAmount"],:txnToken => data["content"]["txnToken"]}
                  else
                    render :json => {:status => "process_trans_failure", :message => "Please try again with another Card or another payment options"}
                  end  
              else
                render :json => {:status => "failure" , :message => "Subscription failure"}
              end 
          else
            render :json => {:status => "failure" , :message => "VPA failure"}
          end
      else
        bin = params[:card_details].split('|')[1][0..5]
          resp =   `curl --location --request POST '#{paytm_url}/paymentsFacade/api/v1/bin/details?mid=#{params[:mid]}&traceId=#{params[:traceId]}' \
      --header 'content-type: application/json' \
      -d '{"head":{"tokenType":"PAYMENT_CONTEXT_TOKEN","channelId":"WEB","token":"#{params[:context_token]}"},"body":{"bin":"#{bin}", "requestTypeContexts": [{"requestType": "NATIVE_SUBSCRIPTION", "txnType": "NONE"}]}}'`

      reaponse = JSON.parse(resp)
       logger.info "=====CARD VALIDATE RESPONSE============#{params[:card_details]}============"
        logger.info "===============#{resp.inspect}==============================="

      if (Rails.env.production?) 
          trans_log = Logger.new("log/paytm_upi.log")
          trans_log.info("================= VPA VALIDATE RESPONSE ====#{params[:card_details]}======================\n")
          trans_log.info("------------------------------------> #{resp.inspect} <--------------------------------")
        end

      if (reaponse["body"]["resultInfo"]["resultMsg"].present? && reaponse["body"]["resultInfo"]["resultMsg"] == "Success")

        isSubscriptionAvailable = reaponse["body"]["isSubscriptionAvailable"]
        logger.info("-----is_subscription----- #{isSubscriptionAvailable}")
        validate_resp = User.validate_card_details(cookies[:user_id],params[:order_id],isSubscriptionAvailable)
            logger.info "=====backend RESPONSE========================"
            logger.info "===============#{validate_resp.inspect}==============================="

              if validate_resp["data"]["message"] == "Success"
                payment_mode = params[:card_details].split('|')[4]
                card_info = params[:card_details].split("|").take(4).join("|")

                logger.info("-----chk----- #{payment_mode}----#{card_info}")

              process_resp =  `curl --location -g --request POST '#{paytm_url}/theia/api/v1/processTransaction?mid=#{params[:mid]}&orderId=#{params[:order_id]}' \
                --header 'Content-Type: application/json' \
                -d '{"body": {"requestType": "NATIVE_SUBSCRIPTION", "mid": "#{params[:mid]}", "orderId": "#{params[:order_id]}","paymentMode": "#{payment_mode}","cardInfo": "#{card_info}", "authMode":"otp"}, "head": {"txnToken": "#{validate_resp["data"]["txn_token"]}"}}'`

                  process_response =  JSON.parse(process_resp)

                logger.info "=====processTransaction RESPONSE========================"
                logger.info "===============#{process_resp.inspect}==============================="

                if (Rails.env.production?) 
                  trans_log = Logger.new("log/paytm_upi.log")
                  trans_log.info("================= Process Transaction RESPONSE ======#{params[:card_details]}====================\n")
                  trans_log.info("------------------------------------> #{process_resp.inspect} <--------------------------------")
                end

                  if process_response["body"]["resultInfo"]["resultMsg"] == "Success"
                    data = process_response["body"]["bankForm"]["redirectForm"]
                      render :json => {:payment_url => data["actionUrl"], :md => (data["content"].present? && data["content"]["MD"].present? ) ? data["content"]["MD"] : "", :PaReq => (data["content"].present? && data["content"]["PaReq"].present? ) ? data["content"]["PaReq"] : "", :TermUrl => (data["content"].present? && data["content"]["TermUrl"].present? ) ? data["content"]["TermUrl"] : "", :PaymentID => (data["content"].present? && data["content"]["PaymentID"].present? ) ? data["content"]["PaymentID"] : ""}

                  else  
                   render :json => {:status => "process_trans_failure", :message => "Please try again with another Card or another payment options"}
                  end 
             else 
               render :json => {:status => "failure" , :message => "Subscription failure"}
            end   


       else
        render :json => {:status => "failure", :message => "Card validation failure"}
       end 

    end   

    else
      render :json => {:status => "failure" , :message => "something went wrong"}
    end 
  end



  def paytm_upi_response
    @pln_tit = ""
    @pln_exp_st = ""
      paytm_params = params
      paytm_params[:payment_gateway] = "paytm_upi"
    if Rails.env.production? 
      #logs start date is: 16-Dec-2020
      paytm_log = Logger.new('/mnt/paytm.log')
      paytm_log.info("=====================Start of Paytm initial Logs=======================")
      paytm_log.info("Parameters from Paytm initial data Callback ----------------------> #{params.inspect} <--------------------------------")
      paytm_log.info("=====================End of Paytm initial Logs=======================")
    end

      if params["CHECKSUMHASH"].present?
        response =  User.secure_payment("paytm_upi",paytm_params)
        logger.info "=====CC AVENUE PAYMENT RESPONSE========================"
        logger.info "===============#{response.inspect}==============================="
        if Rails.env.production? 
          #logs start date is: 16-Dec-2020
            paytm_log = Logger.new('/mnt/paytm.log')
            paytm_log.info("=====================Start of Paytm Callback Logs=======================")
            paytm_log.info("Parameters from Paytm Callback ----------------------> #{params.inspect} <--------------------------------")
            paytm_log.info("Backend Response-------> #{response.inspect}=======================")
            paytm_log.info("=====================End of Paytm Callback Logs=======================")
        end
        if response.has_key?("error")
          @message = response["error"]["message"]["message"]
          @status = "failure"
          @heading_msg = "Your payment could not be processed"
          @coupon_code = response["error"]["message"]["coupon_code"]
          @coupon_title = response["error"]["message"]["coupon_code_title"]
          @order_id = response["error"]["message"]["orderNo"]
          @price = response["error"]["message"]["price_charged"]
          @coupon_type = ""
        else
          @message =  response["data"]["message"]
          @status = response["data"]["txn_status"].present?  ? response["data"]["txn_status"] : "success"
          @heading_msg = response["data"]["popup_message"].present? ?  response["data"]["popup_message"] : "Your payment has been processed"
          @price = response["data"]["price_charged"]
          @pln_tit = response["data"]["title"]
          @pln_exp_st = response["data"]["expiry_date"]
          @coupon_code = response["data"]["coupon_code"]
          @coupon_title = response["data"]["coupon_code_title"]
          @coupon_type = response["data"]["coupon_code_type"]
        end
        if @region == "US"
          @currency = "$ "
        else
          @currency = "₹ "
        end
      end
  end


   def paytm_response
    @pln_tit = ""
    @pln_exp_st = ""
      paytm_params = params
      paytm_params[:payment_gateway] = "paytm"
    if Rails.env.production? 
      #logs start date is: 16-Dec-2020
      paytm_log = Logger.new('/mnt/paytm.log')
      paytm_log.info("=====================Start of Paytm initial Logs=======================")
      paytm_log.info("Parameters from Paytm initial data Callback ----------------------> #{params.inspect} <--------------------------------")
      paytm_log.info("=====================End of Paytm initial Logs=======================")
    end

      if params["CHECKSUMHASH"].present?
        response =  User.secure_payment("paytm",paytm_params)
        logger.info "=====CC AVENUE PAYMENT RESPONSE========================"
        logger.info "===============#{response.inspect}==============================="
        if Rails.env.production? 
          #logs start date is: 16-Dec-2020
            paytm_log = Logger.new('/mnt/paytm.log')
            paytm_log.info("=====================Start of Paytm Callback Logs=======================")
            paytm_log.info("Parameters from Paytm Callback ----------------------> #{params.inspect} <--------------------------------")
            paytm_log.info("Backend Response-------> #{response.inspect}=======================")
            paytm_log.info("=====================End of Paytm Callback Logs=======================")
        end
        if response.has_key?("error")
          @message = response["error"]["message"]["message"]
          @status = "failure"
          @heading_msg = "Your payment could not be processed"
          @coupon_code = response["error"]["message"]["coupon_code"]
          @coupon_title = response["error"]["message"]["coupon_code_title"]
          @order_id = response["error"]["message"]["orderNo"]
          @price = response["error"]["message"]["price_charged"]
          @coupon_type = ""
        else
          @message =  response["data"]["message"]
          @status = "success"
          @heading_msg = "Your payment has been processed"
          @price = response["data"]["price_charged"]
          @pln_tit = response["data"]["title"]
          @pln_exp_st = response["data"]["expiry_date"]
          @coupon_code = response["data"]["coupon_code"]
          @coupon_title = response["data"]["coupon_code_title"]
          @coupon_type = response["data"]["coupon_code_type"]
        end
        if @region == "US"
          @currency = "$ "
        else
          @currency = "₹ "
        end
      end
    render "paytm_response"
  end


  def razor_pay_response
    @pln_tit = ""
    @pln_exp_st = ""
    transaction_data, @status = get_razor_pay_res_data(params)    
    logger.debug "#{transaction_data }-----------------------------------------Razor Pay Response"
    if Rails.env.production? 
    #logs start date is: 16-Dec-2020
      razorpay_log = Logger.new('/mnt/razorpay.log')
      razorpay_log.info("=====================Start of Razorpay initial Logs=======================")
      razorpay_log.info("Parameters from Razorpay initial data Callback ----------------------> #{params.inspect} <--------------------------------")
     razorpay_log.info("Parameters from Razorpay transaction data Callback ----------------------> #{transaction_data.inspect} <--------------------------------")
     razorpay_log.info("=====================End of Razorpay initial Logs=======================")
    end
    response = User.razor_pay_sucess_call(transaction_data)
    logger.debug "#{response }   ----------------------------Backend Razor Pay Sucess call Response"
    if Rails.env.production? 
       razorpay_log = Logger.new('/mnt/razorpay.log')
       razorpay_log.info("=====================Start of Razor pay Callback Logs=======================")
       razorpay_log.info("Parameters from razorpay Callabck ----------------------> #{params.inspect} <--------------------------------")
       razorpay_log.info("Transaction data parameters  ----------------------> #{transaction_data.inspect} <--------------------------------")
       razorpay_log.info("************Backend Response #{response.inspect}*********************")
       razorpay_log.info("=====================End of Razorpay Callback Logs=======================")
    end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Your payment could not be processed"
      @coupon_code = response["error"]["message"]["coupon_code"]
      @coupon_title = response["error"]["message"]["coupon_code_title"]
      @order_id = response["error"]["message"]["order_id"]
      @price = response["error"]["message"]["price_charged"]
      @coupon_type = ""
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Your payment has been processed"
      @price = response["data"]["price_charged"]
      @order_id = response["data"]["order_id"]
      @pln_tit = response["data"]["title"]
      @pln_exp_st = response["data"]["expiry_date"]
      @coupon_code = response["data"]["coupon_code"]
      @coupon_title = response["data"]["coupon_code_title"]
      @coupon_type = response["data"]["coupon_code_type"]
    end
    if Rails.env.production? 
      ccavenue_log = Logger.new('/mnt/ccavenue_payments.log')
      ccavenue_log.info("=====================Start of CCAvenue Callabck Logs=======================")
      ccavenue_log.info("Parameters from CCAvenue Callabck ----------------------> #{params.inspect} <--------------------------------")
      ccavenue_log.info("=====================End of CCAvenue Callabck Logs=======================")
    end
    if @region == "US"
      @currency = "$ "
    else
      @currency = "₹ "
    end
  end

  def get_razor_pay_res_data(razor_pay_obj)
    transaction_data = {}
    transaction_data["payment_gateway"] = "razor_pay"
    status = ""
    if razor_pay_obj.has_key?("error")
      status  = "failure"
      error_obj =  razor_pay_obj["error"] 
      meta_data = JSON.parse(error_obj["metadata"])
      transaction_data["error"] = error_obj 
      transaction_data["razorpay_order_id"] = meta_data["order_id"].blank? ?  cookies[:razorpay_order_id] :   meta_data["order_id"] 
      transaction_data["payment_mode"] = params["payment_mode"]
      transaction_data["payment_id"] = meta_data["payment_id"] 
    else
      status  = "success"
      transaction_data["razorpay_payment_id"] =  razor_pay_obj["razorpay_payment_id"]
      transaction_data["razorpay_signature"] =  razor_pay_obj["razorpay_signature"]
      transaction_data["razorpay_order_id"] = razor_pay_obj["razorpay_order_id"] 

    end 
    return transaction_data, status
  end


   def amazonpay
    @pln_tit = ""
    @pln_exp_st = ""
    order_id  = cookies[:first_purcahse_amz_order_id]
    plan_amt = cookies[:first_purcahse_amz_plan_amt]
    logger.info "amount details are=== #{cookies[:first_purcahse_amz_plan_amt].inspect}"
    plan_amt = params["state"].split("amz_disc_amt=")[1]if params[:state].present? && params["state"].include?("amz_disc_amt=")
    if cookies[:first_purcahse_amz_order_id].nil?
      if params[:state].present?
          state_det = params["state"].split("&")
          order_id = state_det[0].split("order_id=")[1]
          plan_amt = state_det[1].split("amount=")[1]
          #plan_amt = state_det[1].split("amz_disc_amt=")[1]
      end
    end
    if params[:error].present?
      params[:status] = "failure"
      dt = {}
      dt[:response] = {:merchantTransactionId => order_id,:status => "FAILURE" }
      bkd_res =  User.secure_payment("amazon_pay_web",dt)
      p bkd_res.inspect
      @message = bkd_res["error"]["message"]["message"]
      #{}"Transaction Failed"
      #bkd_res["data"]["message"]
      @status = "failure"
      @heading_msg = "Your payment could not be processed"
      @order_no = order_id
    else
      unless params[:tr_st].present?
        p AMZ_CLIENT_ID.inspect
        consent_token_resp  = final_aws_pay(params[:code],plan_amt,order_id,AMZ_CLIENT_ID,AMZ_CLIENT_SECRET,AMZ_MERCHANT_ID,AMZ_SANDBOX,AMZ_ACCESS_KEY,AMZ_SECRET_KEY)
        logger.info "consent Token response"
        logger.info "============#{consent_token_resp.inspect}=========================================="
        logger.info "***************************************************************************"
         if Rails.env.production? 
           awspay_log = Logger.new('/mnt/amazon_payment.log')
           awspay_log.info("=====================Start of AmazonPay Callabck Logs=======================")
           awspay_log.info("=====================Consent token details are #{consent_token_resp.inspect}=======================")
           awspay_log.info("Parameters from AmazonPay  Callback ----------------------> #{params.inspect} <--------------------------------")
           awspay_log.info("=====================End of AmazonPay Callback Logs=======================")
        end
        unless consent_token_resp.has_key?("error")
          unless consent_token_resp["response"].has_key?("payURL")
            @amz_top_url = ""
            p consent_token_resp.inspect
            logger.info "Consent Token details are ==========#{consent_token_resp.inspect}===="
            response =  User.secure_payment("amazon_pay_web",consent_token_resp)
            if Rails.env.production? 
               awspay_log = Logger.new('/mnt/amazon_payment.log')
               awspay_log.info("======================= Start of backend logs for amazonpay =======")
               awspay_log.info("==================#{response.inspect}")
               awspay_log.info("=======================End of backend logs for amazonpay=======")
               awspay_log.info("########################################")
            end
            if response.has_key?("error")
              @message = response["error"]["message"]["message"]
              @status = "failure"
              @heading_msg = "Your payment could not be processed"
              @order_no = response["error"]["message"]["order_id"]
              @coupon_code = response["error"]["message"]["coupon_code"]
              @coupon_title = response["error"]["message"]["coupon_code_title"]
              @price = response["error"]["message"]["price_charged"]
              @coupon_type = ""
            else
              @message =  response["data"]["message"]
              @status = "success"
              @heading_msg = "Your payment has been processed"
              @price = response["data"]["price_charged"]
              @order_no = response["data"]["order_id"]
              @pln_tit = response["data"]["title"]
              @pln_exp_st = response["data"]["expiry_date"]
              @coupon_code = response["data"]["coupon_code"]
              @coupon_title = response["data"]["coupon_code_title"]
              @coupon_type = response["data"]["coupon_code_type"]
            end
          else
            logger.info "=======================transaction update"
            logger.info "=======================#{consent_token_resp['response'].inspect}"
            logger.info "=======================Refresh token === #{consent_token_resp['response']['refresh_token'].inspect}"
            logger.info "=======================#{consent_token_resp['response']['lookAheadToken'].inspect}"
            transaction_det = {
            :order_id => order_id,
            :payment_gateway => "amazonpay",
            :refresh_token => consent_token_resp['response']['refresh_token'],
            :lookhead_token => consent_token_resp['response']['lookAheadToken'],
            :user_id => cookies[:user_id]
            }
            resp = User.update_transaction_det(cookies[:user_id],order_id,transaction_det)
            if Rails.env.production? 
             awspay_log.info("======================= Start of backend logs for Update =======")
             awspay_log.info("==================#{resp.inspect}")
             awspay_log.info("=======================End of backend logs for Update=======")
              awspay_log.info("########################################")
          end
            @amz_top_url = consent_token_resp["response"]["payURL"]
            redirect_to @amz_top_url
            end
            logger.info "backend response for amazonpay"
            logger.info "=============================#{response.inspect}======================="
          end
        else
          @message =  params[:msg]
          @status = params[:tr_st]
          @heading_msg = params[:hd_msg]
          @price = params[:price]
          @order_no = params[:order]
      end
    end
  end
  

  def amazonpay_response
    @pln_tit = ""
    @pln_exp_st = ""
   amz_chrg_st = charge_status_check(AMZ_ACCESS_KEY,AMZ_MERCHANT_ID,params[:sellerOrderId],AMZ_SECRET_KEY,AMZ_SANDBOX)
   #logger.info "========================params====#{params.inspect}====="
   #logger.info "=======Charge status api======#{amz_chrg_st.inspect}==============="
   if Rails.env.production? 
    #logs start date is: 16-Dec-2020
      awspay_log = Logger.new('/mnt/amazon_payment.log')
      awspay_log.info("=====================Start of AmazonPay initial Logs=======================")
      awspay_log.info("Parameters from AmazonPay initial data Callback ----------------------> #{params.inspect} <--------------------------------")
      awspay_log.info("Parameters from AmazonPay charge status data Callback ----------------------> #{amz_chrg_st.inspect} <--------------------------------")
     awspay_log.info("=====================End of AmazonPay initial Logs=======================")
  end
   response =  User.secure_payment("amazon_pay_web",amz_chrg_st)
    if Rails.env.production? 
        awspay_log = Logger.new('/mnt/amazon_payment.log')
        awspay_log.info("=====================Start of AmazonPay Callabck Logs=======================")
        awspay_log.info("Parameters from AmazonPay  Callback ----------------------> #{params.inspect} <--------------------------------")
        awspay_log.info("=====================End of AmazonPay Callback Logs=======================")
        awspay_log.info("======================= Start of backend logs for amazonpay =======")
        awspay_log.info("==================#{response.inspect}")
        awspay_log.info("=======================End of backend logs for amazonpay=======")
        awspay_log.info("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
    end
    if response.has_key?("error")
     @message = response["error"]["message"]["message"]
     @status = "failure"
     @heading_msg = "Your payment could not be processed"
     @order_no = response["error"]["message"]["order_id"]
     @coupon_code = ""
     @coupon_title = ""
     @coupon_type = ""
    else
     @message =  response["data"]["message"]
     @status = "success"
     @heading_msg = "Your payment has been processed"
     @price = response["data"]["price_charged"]
     @order_no = response["data"]["order_id"]
     @pln_tit = response["data"]["title"]
     @pln_exp_st = response["data"]["expiry_date"]
     @coupon_code = response["data"]["coupon_code"]
     @coupon_title = response["data"]["coupon_code_title"]
     @coupon_type = response["data"]["coupon_code_type"]
    end
    logger.info "========================Charge status api backend success"
    logger.info "======================#{response.inspect}===================="
    render "amazonpay"
  end

def amazonpay_success
    @message =  params[:msg]
    @status = params[:tr_st]
    @heading_msg = params[:hd_msg]
    @price = params[:price]
    @order_no = params[:order]
    render "amazonpay"
end


  def aws_charge
   resp = aws_user_charge(params[:price],params[:pymt_info][:refresh_token],params[:pymt_info][:lat_token],params[:pymt_info][:order_id],AMZ_CLIENT_ID,AMZ_CLIENT_SECRET,AMZ_MERCHANT_ID,AMZ_SANDBOX,AMZ_ACCESS_KEY,AMZ_SECRET_KEY)
  logger.info "process charge api -----#{resp.inspect}==============="
  unless resp["response"].has_key?("reasonCode")
  unless resp.has_key?("error")
   resp["response"]["refresh_token"] = params[:pymt_info][:refresh_token]
   p resp.inspect
     unless resp["response"].has_key?("payURL")
      aws_bkd_resp =  User.secure_payment("amazon_pay_web",resp)
         if Rails.env.production? 
          awspay_log = Logger.new('/mnt/amazon_payment.log')
          awspay_log.info("=====================Start of AmazonPay Callabck Logs=======================")
          awspay_log.info("Parameters from AmazonPay  Callback ----------------------> #{params.inspect} <--------------------------------")
          awspay_log.info("=====================End of AmazonPay Callback Logs=======================")
          awspay_log.info("Parameters from charge status response from amazon pay ----------------------> #{resp.inspect} <--------------------------------")
          awspay_log.info("======================= Start of backend logs for amazonpay =======")
          awspay_log.info("==================#{aws_bkd_resp.inspect}")
          awspay_log.info("=======================End of backend logs for amazonpay=======")
       end
        if aws_bkd_resp.has_key?("error")
          @message = aws_bkd_resp["error"]["message"]["message"]
          @status = "failure"
          @heading_msg = "Your payment could not be processed"
          @order_no = aws_bkd_resp["error"]["message"]["order_id"]
          #@price = ""
        else
          @message =  aws_bkd_resp["data"]["message"]
          @status = "success"
          @heading_msg = "Your payment has been processed"
          @price = aws_bkd_resp["data"]["price_charged"]
          @order_no = aws_bkd_resp["data"]["order_id"]
        end
        render :json => {:status => "true",:mesg => @message,:tr_st => @status,:head_msg => @heading_msg,:price => @price,:order_no => @order_no }
      else
         transaction_det = {
            :order_id => params[:pymt_info][:order_id],
            :payment_gateway => "amazonpay",
            :refresh_token => params[:pymt_info][:refresh_token],
            :lookhead_token => params[:pymt_info][:lat_token],
            :user_id => cookies[:user_id]
           }
        trans_resp = User.update_transaction_det(cookies[:user_id],params[:pymt_info][:order_id],transaction_det)
        render :json => {:status => "false",:url =>  resp["response"]["payURL"]}
      end
    else
      render :json => {:status => "false",:url => "login_amazon",:type => "wallet"}
    end
  else
    render :json => {:status => "false",:url => "login_amazon",:type => ""}
  end
end

  def lazypay_response
      lazypay_params = params
      @pln_tit = ""
      @pln_exp_st = ""
      #lazypay_params[:payment_gateway] = "lazypay"
      # lazypay_params[:suscription_id] = params[:trans_id]
      # response =  User.secure_payment("lazypay",lazypay_params)
      # logger.info "=====LAZYPAY  PAYMENT RESPONSE========================"
      # logger.info "===============#{response.inspect}==============================="
      # if Rails.env.production? 
      #     ccavenue_log = Logger.new('/mnt/ccavenue_payments.log')
      #     ccavenue_log.info("=====================Start of CCAvenue Callabck Logs=======================")
      #     ccavenue_log.info("Parameters from CCAvenue Callabck ----------------------> #{params.inspect} <--------------------------------")
      #     ccavenue_log.info("=====================End of CCAvenue Callabck Logs=======================")
      # end
      #if response.has_key?("error")
       if params[:status] == "success"
         @message = "Subscribed Successfully"
         @heading_msg = "Transaction Successfull"
          @pln_tit = params[:title]
          @pln_exp_st = params[:pln_exp_dt]
      else
        @message = "Transaction Failed"
        @heading_msg = "Your payment could not be processed"
       end
        @status = params[:status]
        #@heading_msg = "Your payment could not be processed"
        @price = params[:price]
      # else
      #   @message =  response["data"]["message"]
      #   @status = "success"
      #   @heading_msg = "Your payment has been processed"
      #   @price = response["data"]["price_charged"]
      # end
        @currency = "₹ "
  end


  def operator_billing_response
    logger.info "Operator Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response =  User.secure_payment("celcom",params)
     if Rails.env.production? 
      #logs start date is: 7-Jan-2020
        celcom_log = Logger.new('/mnt/celcom.log')
        celcom_log.info("=====================Start of Celcom Callback Logs=======================")
        celcom_log.info("Parameters from celcom Callback ----------------------> #{params.inspect} <--------------------------------")
        celcom_log.info("Backend Response for celcom-------> #{response.inspect}=======================")
        celcom_log.info("=====================End of Celcom Callback Logs=======================")
    end
    p response.inspect
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Operator Billing"
    logger.info "=============================#{response.inspect}======================="
  end


  def update_transaction_details
    if SHIPPING_ZIP_CODES.include?(params[:zip_code])
      price = (params[:price_charged].to_f+params[:price_charged].to_f*6.625/100).round.to_s
      tax_amount = "6.625"
    else
     price = params[:price_charged]
     tax_amount = "0"
    end
    transaction_det = {
      :order_id => params[:order_id],
      :amount => price,
      :zip_code => params[:zip_code],
      :tax => tax_amount,
      :user_id => cookies[:user_id]
    }
    p transaction_det.inspect
    p "@@@@@@@@"
    resp = User.update_transaction_det(cookies[:user_id],params[:order_id],transaction_det)
    render json: {status: true}
  end


  def view_plans
    config_resp = Rails.cache.fetch("design_configuration_list_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_configuration      
    }
    @plan_info_st = config_resp["data"]["params_hash2"]["config_params"]["plans_info_status"]
    @pln_msg = config_resp["data"]["params_hash2"]["config_params"]["plans_info"]
    login_user_id = params[:user_id].present? ? params[:user_id] : cookies[:user_id]
    if login_user_id.present?
    lang = get_user_lang(params)
    user_plans = Ott.user_plans(login_user_id,lang)
    @current_plans = user_plans['current_active_plans']
    @plan_categories = @current_plans.collect{|x|x['plan_categories'][0]+"-"+x["plan_title"].downcase}
    @gift_trail_check = @current_plans.collect{|x|x['plan_categories'][0] if x['plan_categories'][0] == "free_trial"}.compact.count
    @expired_plans = user_plans['previous_plans']
    else
      redirect_to "#{SITE}/plans"
    end
  end

  def plan_details
    user_session = cookies[:user_id].to_s
    lang = get_user_lang(params)
    user_plans = Ott.user_plans(user_session,lang)
    @plan = user_plans["current_active_plans"].map{|a| a if a["id"]==params["id"].to_i}.compact.first
  end

  def unsubscribe_plan
    unsubscribe_params =  {
        "auth_token": "Ts4XpMvGsB2SW7NZsWc3",
        "data": {"remove_plans":{"plan_id": params["plan_id"],"subscripton_category_id": params["subscription_id"]},"region": @region}
    }
    response =  HTTP.post_https "users/#{cookies[:user_id]}/unsubscribe_pack", unsubscribe_params
    render json: {:message => response["data"]} , status: :ok
  end

  def modify_plans
    response = Ott.subscription_plans
    @all_plans = response["data"]["catalog_list_items"]
    user_session = cookies[:user_id].to_s
    lang = get_user_lang(params)
    user_plans = Ott.user_plans(user_session,lang)
    if user_plans["data"].count == 0
      redirect_to plans_path
    else
      ps_ids = []
      c_col_cts = []

      combos = user_plans["data"].map{|usr_plan| [usr_plan["plan_categories"],usr_plan["currency"],usr_plan["price_charged"],usr_plan["auto_renew"],usr_plan["plan_status"],usr_plan["plan_title"]] if usr_plan["category"] == "combo"}.compact
      combos.each do |combo|
        ps = []
        combo.first.each do |ct|
          if $region == "US"
            c_col_cts <<  ct+ "-" +combo.last.downcase+combo[1]+combo[2]
          else
            c_col_cts <<  ct+ "-" +combo.last.downcase+"$"+combo[1]+combo[2]
          end
          ps << @all_plans.map{|ap| ap if ap["category"] == ct}
        end
        ps.flatten.compact.each do |s|
          ps_ids << s["plans"].map{|t| t["id"]+"$"+combo[3]+"$"+ combo[4] if t["title"].downcase == combo.last.downcase}
        end
      end

      single_plan_ids = user_plans["data"].map{|usr_plan| usr_plan["plan_id"]+"$"+usr_plan["auto_renew"]+"$"+ usr_plan["plan_status"] unless usr_plan["category"]== "combo"}.compact.uniq
      s_col_cts= user_plans["data"].map{|usr_plan| usr_plan["plan_categories"][0]+"-"+usr_plan["plan_title"].downcase+"$"+usr_plan["price_charged"] unless usr_plan["category"]== "combo"}.compact.uniq

      @plan_ids =  ps_ids.flatten.compact + single_plan_ids
      @col_cts = c_col_cts + s_col_cts

      @cat_titles = []
      @all_plans.each do |plan|
        if plan["plans"].present?
          plan["plans"].each do |pl|
            title = "#{plan['category']}-#{pl['title']}"
            @cat_titles <<  title.downcase
          end
        end

      end
    end
  end
  
  ##New Implementation for Plans

  def modify_user_plan
    response = Ott.subscription_plans
    @all_plans = response["data"]["catalog_list_items"]
    lang  = get_user_lang(params)
    user_plans_resp = Ott.user_plans(cookies[:user_id],lang)
    plan_sub_id = user_plans_resp['data'].collect{|x|x["subscription_id"] if (x["plan_id"] == params[:plan_id])}.compact.first
    plans_data = {
      :user_plans  => {
        :current_plan_id => params[:plan_id],
        :subscription_category_id => plan_sub_id
      }
    }
    p plans_data.inspect

   modified_plans_resp = User.get_modified_plans(cookies[:user_id],plans_data)
   p modified_plans_resp.inspect
   @modify_plans = modified_plans_resp["data"]["modify_plans"]
  end

  def upgrade_plan
    upgrade_plan_params = {
      :data => {
        :add_plans => {
          :plan_id => params[:new_plan_id],
          :subscription_category_id => params[:new_plan_sub_category_id]
        },
        :modify_plans => {
          :current_plan_id => params[:currentplan_id],
          :subscription_category_id => params[:currentplan_sub_category_id]
        },
        :region => $region
      }
    }
   modified_plan_resp = User.get_modified_plan_amount(cookies[:user_id],upgrade_plan_params)
   if $region == "IN"
     price_charge = modified_plan_resp["data"]["pg_price"]["cc_avenue"]
   else
     price_charge = modified_plan_resp["data"]["pg_price"]["adyen"]
   end
   p modified_plan_resp.inspect
   p "@@@@@@@@@@@@@@@@@@@@@@@"
   amount = modified_plan_resp["data"]["currency"]+"|"+modified_plan_resp["data"]["total_amount"].to_s
   render :json => {:status => true,:price => amount,:plan_details =>  params[:currentplan_id]+"|"+params[:currentplan_sub_category_id]+"|"+params[:new_plan_id]+"|"+params[:new_plan_sub_category_id],:price_charged => price_charge}
  end

  


  def downgrade_user_plan

  end


  def init_transaction
     email_id = ""
     mobile_no = ""
    if $region == "IN"
      mobile_no = cookies[:user_login_id]
      payment_type = params[:payment_gateway]
    elsif $region == "BD"
      mobile_no = cookies[:user_login_id]
      payment_type = "ghoori"
    else
      email_id = cookies[:user_login_id]
      payment_type = "adyen"
    end
    modify_plan_type = params[:modify_plan_type]
    #browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
    browser_name = "Generic_Browser"
    platform_type = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
    init_params = {
      :us => get_payment_signature_key(params[:new_plan_id]),
      :payment_gateway => payment_type,
      :platform => platform_type,
      :payment_info => {
        :net_amount => params[:actual_price], 
        :price_charged => params[:charged_price],
        :currency => params[:currency], 
        :packs => [{
          :plan_categories => [params[:pack_category]], 
          :category_type =>"individual",
          :category_pack_id => params[:new_plan_sub_id],
          :subscription_catalog_id => params[:new_plan_catalog_id], 
          :plan_id => params[:new_plan_id]
          }
        ] 
      },
      :transaction_info => {
        :app_txn_id => "", 
        :txn_message => "One Day Pack_10.00", 
        :txn_status => "init", 
        :order_id => "", 
        :pg_transaction_id => "",
        :payment_mode => params[:payment_mode]
       },
      :modify_user_plans => {
       :current_plan_id => params[:current_plan_id],
       :current_subscription_category_id => params[:currentplan_sub_id]
      },
      :user_info => {
       :email => email_id,
       :mobile_number => mobile_no
      },
      :miscellaneous => {
       :browser => browser_name,
       :device_brand =>  "NA",
       :device_IMEI => "NA",
       :device_model =>  "NA", 
       :device_OS =>  "NA" ,
       :device_type =>  "NA" ,
       :inet => "NA",
       :isp =>  "NA",
       :operator =>  "NA"
      }
    }
    if params[:modify_plan_type] == "downgrade"
     init_params[:modify_user_plans][:modify_status] = "downgrade"
    end
    p "===================payment init call starts data here=========================================="
    p init_params.inspect
    transaction_resp = User.send_init_transaction(cookies[:user_id],init_params)
    p transaction_resp.inspect
    p "===================payment init  call response data here=========================================="
     if Rails.env.production? 
        ccavenue_log = Logger.new('/mnt/ccavenue_payments.log')
        ccavenue_log.info("CCAvenue first time payment frontend call logs ----------------------> #{params.inspect} <--------------------------------")
        ccavenue_log.info("=====================Start of first time CCAvenue Init Logs=======================")
        ccavenue_log.info("CCAvenue payment frontend call logs ----------------------> #{params.inspect} <--------------------------------")
        ccavenue_log.info("CCAvenue payment init call logs ----------------------> #{init_params.inspect} <--------------------------------")
        ccavenue_log.info("CCAvenue payment iit call backend response logs ----------------------> #{transaction_resp.inspect} <--------------------------------")
        ccavenue_log.info("=====================End of first time CCAvenue Init Logs=======================")
    end

    if transaction_resp.has_key?("data")
      if payment_type == "adyen"
        render json: {:paymenttype => "adyen",:init_data => transaction_resp["data"] ,:status => "ok"}
      else
        ccavenue_payment_url = "#{transaction_resp['data']['payment_url']}&encRequest=#{transaction_resp['data']['msg']}&access_code=#{transaction_resp['data']['access_code']}"
        render json: {:paymenttype => "ccavenue",:payment_url => ccavenue_payment_url} , status: :ok
      end
    else
      render json: {:status => "false",:error_mesg => transaction_resp["error"]["message"]}
    end
  end

  def modify_user_purchased_plan
     email_id = ""
     mobile_no = ""
     if $region == "IN"
       mobile_no = cookies[:user_login_id]
      payment_type = "ccavenue"
    else
      email_id = cookies[:user_login_id]
      payment_type = "adyen"
    end
    new_plan = params[:selected_plan].split("|")
    #5bac730dc1df4160ba000001|5b3c917fc1df417b9a00002c|5bac730dc1df4160ba000002|bollywood_plus|Bollywood plus packs|Yearly|45|$|4500|USD
    currency = new_plan[9]
    new_plan_category = new_plan[3]
    new_plan_category_pack_id = new_plan[2]
    new_plan_catalog_id = new_plan[1]
    new_plan_id = new_plan[0]
    
    #5bac730dc1df4160ba000000|5bac730dc1df4160ba000002|5b3c917fc1df417b9a00002c|Bollywood plus packs|bollywood_plus|Monthly
    activated_pack_info = params[:current_plan_ids].split("|")
    activated_plan_id = activated_pack_info[0]
    activated_plan_sub_category_id = activated_pack_info[1]

    #browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
    browser_name = 'Generic Browse'
    platform_type = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
    purchase_plan_params = {
      :payment_info => {
        :price_charged => params[:plan_price_charged],
        :currency => currency, 
        :packs => [{
          :plan_categories => [new_plan_category], 
          :category_type =>"individual",
          :category_pack_id => new_plan_category_pack_id,
          :subscription_catalog_id => new_plan_catalog_id, 
          :plan_id => new_plan_id
          }
        ] 
      },
      :transaction_info => {
        :order_id => params[:plan_order_id],
        :adyen_encrypted_data => params["adyen-encrypted-data"] 
       },
      :modify_user_plans => {
       :current_plan_id => activated_plan_id,
       :current_subscription_category_id => activated_plan_sub_category_id
      },
      :user_info => {
       :email => email_id,
       :mobile_number => mobile_no
      },
      :miscellaneous => {
       :browser => browser_name,
       :device_brand =>  "NA",
       :device_IMEI => "NA",
       :device_model =>  "NA", 
       :device_OS =>  "NA" ,
       :device_type =>  "NA" ,
       :inet => "NA",
       :isp =>  "NA",
       :operator =>  "NA"
      }
    }
    p "final payment parameters"
    p purchase_plan_params.inspect
    payment_response = User.send_final_payment_info(cookies[:user_id],purchase_plan_params)
    p "==========================payment for api response"
    p payment_response.inspect
    p "@@@@@@@@@@@@@@@@@@@@@@@@@@@2"
    if !payment_response["data"].blank? && (payment_response["data"]["message"] == "pack activated successfully" || payment_response["data"]["message"] == "Plans modified Successfully")
      redirect_to "#{SITE}/payment/payment_success?msg=#{payment_response['data']['message']}&order_id=#{payment_response["data"]["order_id"]}&price=#{payment_response["data"]["price_charged"]}"
    else
      redirect_to "#{SITE}/payment/payment_failed?msg=#{payment_response['error']['message']}&error_code=#{payment_response["error"]["code"]}"
    end
  end

  def upgrade_user_plans
    @user_modified_plans = get_modified_user_plans(params[:plan_id],"upgrade")
  end


  def downgrade_user_plans
    @user_modified_plans = get_modified_user_plans(params[:plan_id],"downgrade")
    @downgrade_plans = @user_modified_plans['downgrade_plans'].collect{|x|x['category']+"_"+x["plan_title"].downcase}
    p@user_modified_plans.inspect
  end

  def remove_user_plan
    remove_plan_params = {
      :data => {
        :remove_plans => {
          :plan_id => params[:plan_id].split("|")[0],
          :subscripton_category_id => params[:plan_id].split("|")[1]
        },
        :region => $region
      }
    }
   plans_remove_resp = User.remove_pack(cookies[:user_id],remove_plan_params)
   returnd_response = plans_remove_resp.has_key?("error") ? plans_remove_resp["error"] : plans_remove_resp["data"]
    if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY" && params[:paymnt_gatway_type] == "unifi"
      remove_logger = Logger.new('log/unifi_transaction.log')
      remove_logger.info("=================== Unsubcription start here ======================================\n")
      remove_logger.info "Subscribed plans id ---------------------------> #{params[:plan_id]} <--------------"
      remove_logger.info "Subscribeed plan category ------------------> #{params[:plan_id]} <-----------------"
      remove_logger.info "Payment gateway type ------------------> #{params[:paymnt_gatway_type]} <-----------------"
      remove_logger.info "Termination request body ----------------------> #{returnd_response["message"]["request_body"].inspect if returnd_response["message"].present?} <----------"
      remove_logger.info "Termination response body ----------------------> #{returnd_response["message"]["response_body"].inspect if returnd_response["message"].present? } <-------"
      remove_logger.info "Plan's Unsubscription returned status ------------------> #{returnd_response["message"]["message"].inspect if returnd_response["message"].present?} <------"
    end
   st = "true"
   st = "false" if plans_remove_resp.has_key?("error")
   p plans_remove_resp.inspect
   render :json => {:status => st}
  end

   def reactivate_user_plan
    reactive_plan_params = {
      :data => {
        :user_plans => {
          :plan_id => params[:plan_id].split("|")[0],
          :subscription_category_id => params[:plan_id].split("|")[1]
        },
        :region => $region
      }
    }
    p reactive_plan_params.inspect
    p "1111111111"
   plans_reactivate_resp = User.reactivate_pack(cookies[:user_id],reactive_plan_params)
   p plans_reactivate_resp.inspect
   render :json => {:status => true}
  end

  def add_new_plan
    @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
    config_resp = Rails.cache.fetch("design_configuration_list_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_configuration      
    }
    @plan_info_st = config_resp["data"]["params_hash2"]["config_params"]["plans_info_status"]
    @pln_msg = config_resp["data"]["params_hash2"]["config_params"]["plans_info"]
    if @region == "US"
      @currency = "$"
    else
      @currency = "₹"
    end
    if cookies[:user_id].present?
      lang = get_user_lang(params)
      @user_monthly_count = 0
      @user_yearly_count = 0
      @all_access_month_status = true
      response = Ott.new_plans(lang)
      @all_access_quar_status = true
      @new_plans = response["data"]["catalog_list_items"]
       @is_postpaid = ""
      if params[:user_id].present?
        login_user_id  = params[:user_id]
      else
        login_user_id = cookies[:user_id]
      end 
      if $region == "KW"
        account_resp = User.get_user_account_details(login_user_id)
        login_type = account_resp["data"]["user_type"]
        if login_type == "msisdn"
          phone_num = account_resp["data"]["mobile_number"]
          user_id =  account_resp["data"]["user_id"]
          txn_id = user_id + "_" + phone_num
          #phone_num = "96560499190"
          @is_postpaid = pre_post_paid_check(phone_num,txn_id) 
        end
    end
    lang = get_user_lang(params)
      user_plans = Ott.user_plans(cookies[:user_id],lang)
      unless user_plans['current_active_plans'].empty?
       @active_pack_categories = user_plans['current_active_plans'].collect{|x|x["plan_categories"][0]}.compact
       @active_pack_titles = user_plans['current_active_plans'].collect{|x|x["plan_categories"][0]+"-"+x["plan_title"].downcase}.compact
        if @active_pack_categories.include?("all_access_guj")
          @new_plans = response["data"]["catalog_list_items"].select{|x| x["title"] == "Gujarati Premium Plan" }
        end
       if @active_pack_titles.include?("all_access_pack-monthly")
         @all_access_monthly_check = true
         @all_access_month_status = false
         if $region == "IN" && user_plans["first_time_subscription"] == "true"
          @all_access_month_status = true
         end
       end
       if @active_pack_titles.include?("all_access_pack-quarterly")
         @all_access_quar_status = false
      end
       active_plans_cnt = user_plans['current_active_plans'].collect{|x|x["plan_title"].downcase if (x["plan_categories"][0] != "all_access_pack")}.compact
       @user_monthly_count = active_plans_cnt.count("monthly")
       @user_yearly_count = active_plans_cnt.count("yearly")
      else
        @active_pack_categories = []
        @active_pack_titles = []
      end
      @user_all_plans_titles = response["data"]["catalog_list_items"].collect{|x|x['plans'].collect{|y|x["category"]+"-"+y['title'].downcase} unless x['plans'].nil?}.compact.flatten
     if $region == "US" && @active_pack_titles.include?("all_access_pack-yearly") && !@active_pack_titles.include?("worldbbtv")
       @active_pack_categories = ["all_access_pack","bollywood_plus","bollywood_classic","kids","punjabi","gujarati","marathi","comedy",""]
    end
   else
    redirect_to "#{SITE}"
   end
  end

  def new_plan_summary

    @theme = params[:theme_option].present? ? params[:theme_option] : cookies[:theme_option]
    plan_id = cookies[:new_plan_id]
    subscription_id = cookies[:new_sub_id]
    app_type = cookies[:new_app_type]
    logger.info "chk params and cookies----- #{cookies[:new_plan_id].inspect}--#{cookies[:new_sub_id].inspect}--#{cookies[:new_app_type].inspect}---"
    payment_checkout = Ott.payment_checkout(plan_id, subscription_id, app_type)
    @payment_details = (payment_checkout["data"].present? && payment_checkout["data"]["items"].present? ) ? payment_checkout["data"]["items"] : ""
    @lazypay_check = "false"
  if $region == "IN"
    lang = get_user_lang(params)
     user_plans = Ott.user_plans(cookies[:user_id],lang)
     unless user_plans['current_active_plans'].empty?
      active_plans = user_plans['current_active_plans'].collect{|x|x['payment_mode'].downcase}.uniq
      if active_plans.include?("lazypay")
        active_sub = user_plans['current_active_plans'].collect{|x|x['auto_renew'].downcase if (x['payment_mode']).downcase == "lazypay"}.compact.uniq
        if !active_sub.empty? && active_sub.include?("true")
          @lazypay_check = "true"
        end
      end
     end
  end
    p @lazypay_check .inspect
    @lazy_check = "false"
    @shipping_zip_codes = SHIPPING_ZIP_CODES
    
    # account_resp = User.get_user_account_details(cookies[:user_id])
    # login_type = account_resp["data"]["user_type"]
    # @vaild_lazy_num = false
    #  if $region == "IN" && login_type == "msisdn"
    #    @vaild_lazy_num = validate_lzy_mobile
    #  end
    @amz_mobile_url = "#{SITE}/payment/mobile_pay_amazonpay"
  end



  def new_plan_purchase
    @shipping_zip_codes = SHIPPING_ZIP_CODES
  end

  def generate_aoc_token(params, email_id, phone_no, platform_type='web', order_id)
    contact_info = email_id.empty? ? phone_no : email_id
    url = "http://prod.mife-aoc.com/api/getAOCToken"
    body = {'apiKey'=>'koFh0dzxibO4zMt0','username'=>'shemaroo','spTransID' => order_id,'description' =>'Wallet Payment Shemaroo','currency' =>'MYR',"amount" => params[:charged_price], 'onBehalfOf' => 'APIGATE_AOC-Shemaroo','channel' => platform_type,'operator' => 'Boost','taxAmount' =>'0','callbackURL' =>'https://www.shemaroome.com/payment/boost_response','isSubscription' =>'false','purchaseCategoryCode' =>'Video','contactInfo' => contact_info,'isWallet' =>'true'}
    headers = {'content-type'=> "application/x-www-form-urlencoded"}
    response = Typhoeus::Request.post(url, body: body, headers: headers, ssl_verifypeer: false)
    response = JSON.parse(response.body)
    if (Rails.env.pre_production? || Rails.env.production?)
      trans_log = Logger.new("log/boost_transaction.log")
      trans_log.info("*********************** getAOCToken Start here ***************")
      trans_log.info("AOCToken Url -----------------------> #{url} <----------------")
      trans_log.info("Request Payload ------------> #{body.inspect} <---------------")
      trans_log.info("AOCToken response ----------> #{response.inspect} <-----------")
      trans_log.info("*********************** getAOCToken End here *****************")
    end

    return response["data"]
  end

  def new_plans_init_transaction
     new_plan_details = params[:new_plan_det]
     email_id = ""
     mobile_no = ""
     ext_userid = ""
     vers = cookies[:app_version].nil? ? "" : cookies[:app_version]
     device_type = cookies[:mobile_browser_type].nil? ? "WEB" : "Androidapp"
    if $region == "IN" || $region == "BD" || $region == "MY" || $region == "KW" || $region == "QA" || $region == "OM" || $region == "AE" || $region == "BH"
      mobile_no = cookies[:user_login_id]
      # payment_type = "ccavenue"
      payment_type = params[:payment_gateway]
      #payment_type = "ghoori" if $region == "BD"
      actual_price = params[:actual_price]
      price_charged = params[:charged_price].to_f.floor(2)   
      lang = "en"
    else
      email_id = cookies[:user_login_id]
      #payment_type = "adyen"
      payment_type = params[:payment_gateway]
      if payment_type == "google"
        actual_price = new_plan_details.collect{|x|x.split("|")[6].to_i}.inject(:+)
        price_charged = actual_price
      else
        if payment_type == "adyen"
          #actual_price = new_plan_details.collect{|x|x.split("|")[7].to_i}.inject(:+)
          #price_charged = actual_price
          actual_price = (params[:actual_price].to_f * 100).round.to_s
          price_charged = (params[:charged_price].to_f * 100).round.to_s
        else
          actual_price = params[:actual_price]
          price_charged = params[:charged_price]
        end
       end
    end
    account_resp = User.get_user_account_details(cookies[:user_id])
    acc_det = account_resp["data"]
   if acc_det["login_type"] == "external"
      if acc_det["user_type"] == "msisdn"
        mobile_no = acc_det["mobile_number"]
        email_id = "" 
      else
        mobile_no = ""
        ext_userid = acc_det["ext_user_id"]
        email_id = ""
      end
    else
      if acc_det["login_type"] == "msisdn"
      mobile_no = acc_det["mobile_number"]
      email_id = ""
    else
      email_id = acc_det["email_id"]
      mobile_no = ""
    end

   end
    plan_ids = new_plan_details.collect{|x|x.split("|")[0]}.join("")
    plan_currency = new_plan_details[0].split("|")[4]
    #browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
    browser_name = 'Generic_Browser'
    platform_type = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
    new_plan_init_params = {
      :us => get_payment_signature_key(plan_ids),
      :payment_gateway => payment_type,
      :platform => device_type,
      :payment_info => {
        :net_amount => actual_price.to_s, 
        :price_charged => price_charged.to_s,
        :currency => plan_currency, 
        :packs => get_selected_packs(new_plan_details)
      },
      :transaction_info => {
        :app_txn_id => vers, 
        :txn_message => "One Day Pack_10.00", 
        :txn_status => "init", 
        :order_id => "", 
        :pg_transaction_id => "" ,
        :payment_mode => params[:payment_mode],
        :transaction_type => params[:plan_type] ? params[:plan_type] : 'success',
        :old_sub_id => params[:old_sub_id] ? params[:old_sub_id] : "",
        :old_plan_id => params[:old_plan_id] ? params[:old_plan_id] : "",
        :discount_offer => params[:discount_offer] ? params[:discount_offer] : ""
       },
      :user_info => {
       :email => email_id,
       :mobile_number => mobile_no,
       :ext_user_id => ext_userid
      },
      :miscellaneous => {
       :browser => browser_name,
       :device_brand =>  "NA",
       :device_IMEI => "NA",
       :device_model =>  "NA", 
       :device_OS =>  "NA" ,
       :device_type =>  "NA" ,
       :inet => "NA",
       :isp =>  "NA",
       :operator =>  "NA",
       :transaction_type => params[:plan_type] ? params[:plan_type] : 'success',
       :old_sub_id => params[:old_sub_id] ? params[:old_sub_id] : "",
       :old_plan_id => params[:old_plan_id] ? params[:old_plan_id] : "",
       :discount_offer => params[:discount_offer] ? params[:discount_offer] : ""
      },
      :trans_info => {
        :id => device_type == "WEB" ? params['branch_data']['browser_fingerprint_id'] : params['branch_data']['aaid'],  
        :os => params['branch_data']['os'],
        :city => params['branch_data']['city'],
        :device_id => device_type == "WEB" ? "" : params['branch_data']['device_id'],
        :device_make => device_type == "WEB" ? "" : params['branch_data']['device_make'],
        :user_ip => device_type == "WEB" ? $user_ip : params['branch_data']['user_ip'],
        :user_agent => params['branch_data']['user_agent'],
        :http_origin => "c65f433f-a2fe-4321-a494-f19d80603b21",
        :mac_address =>"83a54d80-b87c-459f-88b0-3e8965938e80"
      }
   
    }
  
    # if params[:payment_purchase_type].present?
    #   new_plan_init_params[:transaction_info][:mode] = params[:payment_purchase_type]
    # end

    if params[:promo_check].present? && params[:promo_check] == "true"
      new_plan_init_params[:payment_info][:coupon_code] = params[:coupon_code].upcase
      new_plan_init_params[:payment_info][:coupon_code_id] = params[:coupon_id]
      if $region == "US"
       # new_plan_init_params[:payment_info][:price_charged] = params[:adyen_promo_amount]
        #new_plan_init_params[:payment_info][:price_charged] = (price_charged.to_i - params[:adyen_promo_amount].to_i).to_s
      end
    end

    # if payment_type == "adyen"
    #   new_plan_init_params[:transaction_info][:mode] = "credit_card"
    # end
    p new_plan_init_params.inspect
    transaction_resp = User.send_init_transaction(cookies[:user_id],new_plan_init_params)
    p transaction_resp.inspect
    if (Rails.env.pre_production? || Rails.env.production?) && $region == "MY" && payment_type == "unifi"
      trans_log = Logger.new("log/unifi_transaction.log")
      trans_log.info("================= Init params info ==========================\n")
      trans_log.info("Parameters ------------------------------------> #{params.inspect} <--------------------------------")
      trans_log.info("*****************************  Transaction start here  *******************************************\n")
      trans_log.info("Subscriber email-id----------------> #{acc_det["email_id"] if acc_det.present?} <-------------------")
      trans_log.info("Subscriber external email-id -------> #{acc_det["ext_account_email_id"] if acc_det.present?} <------")
      trans_log.info("Plans signature_key------------------------> #{new_plan_init_params[:us]} <-------------------------")
      trans_log.info("Selected pack----------------> #{new_plan_init_params[:payment_info][:packs]} <---------------------")
      trans_log.info("User's browser---------------> #{new_plan_init_params[:miscellaneous][:browser]} <------------------")
      trans_log.info("Payment type---------------------------------> #{payment_type} <------------------------------------")
      trans_log.info("transcation reponse ------------------------->#{ transaction_resp.inspect } <-----------------------")
    end

    if Rails.env.production? 
        ccavenue_log = Logger.new('/mnt/ccavenue_payments.log')
        ccavenue_log.info("CCAvenue second time payment frontend call logs ----------------------> #{params.inspect} <--------------------------------")
        ccavenue_log.info("=====================Start of second time CCAvenue Init Logs=======================")
        ccavenue_log.info("CCAvenue payment init call logs ----------------------> #{new_plan_init_params.inspect} <--------------------------------")
        ccavenue_log.info("CCAvenue payment init call backend response logs ----------------------> #{transaction_resp.inspect} <--------------------------------")
        ccavenue_log.info("=====================End of CCAvenue second time Init Logs=======================")
    end
    if params[:payment_page].present? && params[:payment_page] == "false"
      sucess_plan_params = {
        :us => get_payment_signature_key(plan_ids),
        :payment_gateway => "coupon",
        :payment_info => {
          :net_amount => actual_price.to_s,
          :price_charged => price_charged.to_s,
          :currency => plan_currency, 
          :packs => get_selected_packs(new_plan_details),
          :coupon_code => params[:coupon_code].upcase,
          :coupon_code_id => params[:coupon_id]
        },
      :transaction_info => {
        :order_id => transaction_resp["data"]["transaction_id"],
        :txn_status => "success",
        :txn_message => "One Day Pack_10.00",
        :pg_transaction_id => SecureRandom.urlsafe_base64(5),
        :transaction_type => params[:plan_type] ? params[:plan_type] : 'success',
        :old_sub_id => params[:old_sub_id] ? params[:old_sub_id] : "",
        :old_plan_id => params[:old_plan_id] ? params[:old_plan_id] : "",
        :discount_offer => params[:discount_offer] ? params[:discount_offer] : ""
       },
      :user_info => {
       :email => email_id,
       :mobile_number => mobile_no
      },
      :miscellaneous => {
       :browser => browser_name,
       :device_brand =>  "NA",
       :device_IMEI => "NA",
       :device_model =>  "NA", 
       :device_OS =>  "NA" ,
       :device_type =>  "NA" ,
       :inet => "NA",
       :isp =>  "NA",
       :operator =>  "NA",
       :transaction_type => params[:plan_type] ? params[:plan_type] : 'success',
       :old_sub_id => params[:old_sub_id] ? params[:old_sub_id] : "",
       :old_plan_id => params[:old_plan_id] ? params[:old_plan_id] : "",
       :discount_offer => params[:discount_offer] ? params[:discount_offer] : ""
      }
    }
    p "@@@@@@@@@@@@@@@@@@"
    p sucess_plan_params.inspect
    p "1111111111111111"
      sucess_payment_response = User.send_init_transaction(cookies[:user_id],sucess_plan_params)
    p sucess_payment_response.inspect
    end
    if payment_type == "boost"
      if (Rails.env.pre_production? || Rails.env.production?)
        trans_log = Logger.new("log/boost_transaction.log")
        trans_log.info("***************************  Transaction start here  *******************************************\n")
        trans_log.info("Subscriber email-id--------------> #{acc_det["email_id"] if acc_det.present?} <-------------------")
        trans_log.info("Params ----------------> #{params.inspect} <-------------------")
        trans_log.info("Order-id ----------------> #{transaction_resp["data"]["order_id"].inspect} <-------------------")
      end
      token_resp = generate_aoc_token(params, email_id, mobile_no, platform_type, transaction_resp["data"]["order_id"])
      token_resp["order_id"] = transaction_resp["data"]["order_id"]
      add_aoc_token = User.add_aoc_token_data(token_resp)
      p add_aoc_token.inspect
      transaction_resp['data']["payment_url"] = "http://msisdn.mife-aoc.com/api/aoc?aocToken="+token_resp["aocToken"]
    end
  p transaction_resp.inspect
    if (transaction_resp.has_key?("error"))
      render json: {:message => transaction_resp["error"]["message"] ,:status => "error",:code =>transaction_resp["error"]["code"]}
    elsif  payment_type == "adyen"
      render json: {:paymenttype => "adyen",:init_data => transaction_resp["data"] ,:status => "ok"}
    elsif payment_type == "paytm"
      #ccavenue_payment_url = "#{transaction_resp['data']['payment_url']}&encRequest=#{transaction_resp['data']['msg']}&access_code=#{transaction_resp['data']['access_code']}"
      render json: {:paymenttype => "paytm",:payment_data => transaction_resp['data'] } , status: :ok
    elsif payment_type == "razor_pay"
      #ccavenue_payment_url = "#{transaction_resp['data']['payment_url']}&encRequest=#{transaction_resp['data']['msg']}&access_code=#{transaction_resp['data']['access_code']}"
      render json: {:paymenttype => "razor_pay",:payment_data => transaction_resp['data'] } , status: :ok
    else
      if $region == "IN"
         paymenturl = "#{transaction_resp['data']['payment_url']}&encRequest=#{transaction_resp['data']['msg']}&access_code=#{transaction_resp['data']['access_code']}"
      elsif $region == "BD" || $region == "MY" || $region == "KW" || $region == "QA" || $region == "OM" || $region == "SA" || $region == "AE" || $region == "BH"
         paymenturl = transaction_resp['data']['payment_url']
      end
    logger.info "Payment data here----#{transaction_resp['data'].inspect}-------"
      #render json: {:paymenttype => "ccavenue",:payment_url => ccavenue_payment_url} , status: :ok
     render json: {:paymenttype => payment_type,:payment_url => paymenturl , :payment_dat => transaction_resp['data'] ,status: :ok}
    end
  end

  def  plans_purchase
   @region = $region
 end

def canada_zipcode_check
  resp = User.zip_code_canada(params[:zip_code])
  tax = resp["data"]["total_tax"]
   zip_code = resp["data"]["zip_code"]
   province = resp["data"]["province"]
  render json: {total_tax: tax, zip_code:zip_code, province:province}
end  


  def new_plan_user_purchase
    p params.inspect
    p "@@@@@@@@@@@@@@@"
    tax_amount = 0
    tax_percentage = "0"
    new_plans = params[:new_selected_plan].split(",")
    plan_currency = new_plans[0].split("|")[4]
    email_id = ""
    mobile_no = ""
    if $region == "IN"
      mobile_no = cookies[:user_login_id]
      payment_type = "ccavenue"
      actual_price = params[:actual_price]
      price_charged = params[:price_charged]
    else
      payment_type = "adyen"
      actual_price = (new_plans.collect{|x|x.split("|")[7].to_i}.inject(:+)).round.to_s 
      price_charged = (params[:plan_price_charged].to_f * 100).round.to_s
      email_id = cookies[:user_login_id]
    end
    
    #browser_name = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us").name.split(" ").join("_")
    browser_name = "Generic_Browser"
    platform_type = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
    purchase_plan_params = {
      :payment_info => {
        :price_charged => price_charged.to_s,
        :currency => plan_currency, 
        :packs => get_selected_packs(new_plans)
      },
      :transaction_info => {
        :order_id => params[:new_plan_order_id],
        :adyen_encrypted_data => params["adyen-encrypted-data"],
        :zip_code => params[:zip_code].present? ? params[:zip_code] : ""
       },
      :user_info => {
       :email => email_id,
       :mobile_number => mobile_no
      },
      :miscellaneous => {
       :browser => browser_name,
       :device_brand =>  "NA",
       :device_IMEI => "NA",
       :device_model =>  "NA", 
       :device_OS =>  "NA" ,
       :device_type =>  "NA" ,
       :inet => "NA",
       :isp =>  "NA",
       :operator =>  "NA"
      }
    }
    if $region == "US"
      purchase_plan_params[:transaction_info][:mode] = "credit_card"
    end
    
    if params[:promo_check].present? && params[:promo_check] == "true"
      purchase_plan_params[:payment_info][:coupon_code] = params[:coupon_code].upcase
      purchase_plan_params[:payment_info][:coupon_code_id] = params[:coupon_id]
      if $region == "US"
        purchase_plan_params[:payment_info][:price_charged] = (price_charged.to_i - params[:adyen_promo_amount].to_i).to_s
      end
    end
    if params[:is_tax_amount].present? && params[:is_tax_amount] == "true"
     p "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2"
     p params[:actual_price].inspect
     tax_amount = (price_charged.to_f * 6.625)/100
     p tax_amount.inspect
     tax_percentage = "6.625"
     purchase_plan_params[:payment_info][:price_charged] = (price_charged.to_f + tax_amount.to_f).round.to_s
    end

    if $region == "US"
     transaction_det = {
        :order_id => params[:new_plan_order_id],
        :amount => (params[:plan_price_charged].to_f + tax_amount).round.to_s,
        :zip_code => params[:zip_code],
        :tax => tax_percentage,
        :user_id => cookies[:user_id]
      }
    end
    if $region == "CA"
      #resp = User.zip_code_canada(params[:zip_code])
       purchase_plan_params[:payment_info][:price_charged] = price_charged.to_f
      if params[:ca_tax_amount].to_i > 0  
        tax = params[:ca_tax_amount]
        tax_amount = (price_charged.to_f * tax.to_f)/100
        purchase_plan_params[:payment_info][:price_charged] = (price_charged.to_f + tax_amount.to_f).round.to_s
      end
      transaction_det = {
        :order_id => params[:new_plan_order_id],
        :amount => purchase_plan_params[:payment_info][:price_charged],
        :zip_code => params[:zip_code],
        :tax => params[:ca_tax_amount],
        :user_id => cookies[:user_id]
      }
    end
    p transaction_det.inspect
    p "@@@@@@@@"
    resp = User.update_transaction_det(cookies[:user_id],params[:new_plan_order_id],transaction_det)
      
    if $region == "US" && params[:user_free_trail].present? && params[:user_free_trail] == "true"
      purchase_plan_params[:payment_info][:price_charged] = "0"
    end
    p "price charged_price"
    p "#{purchase_plan_params[:payment_info][:price_charged].inspect}"
    p "final payment parameters"

    logger.info "--------------------------------final backend parameters--------------------"
    logger.info "!!!!!!!!!!!!!!!!!!!!!!!!#{purchase_plan_params.inspect}!!!!!!!!!!!!!!!!!!!"
    p "===================================================================="
    p purchase_plan_params.inspect
    payment_response = User.send_final_payment_info(cookies[:user_id],purchase_plan_params)
    p "==========================payment for api response"
    p payment_response.inspect
    p "@@@@@@@@@@@@@@@@@@@@@@@@@@@2"
    # if !payment_response["data"].blank? && (payment_response["data"]["message"] == "pack activated successfully" || payment_response["data"]["message"] == "Plans modified Successfully")
    #   redirect_to "#{SITE}/payment/payment_success?msg=#{payment_response['data']['message']}&order_id=#{payment_response["data"]["order_id"]}&price=#{payment_response["data"]["price_charged"]}"
    # else
    #   redirect_to "#{SITE}/payment/payment_failed?msg=#{payment_response['error']['message']}&error_code=#{payment_response["error"]["code"]}"
    # end

    if !payment_response["data"].blank? 
      redirect_to "#{SITE}/payment/payment_success?message=#{payment_response['data']['message']}&order_id=#{payment_response["data"]["order_id"]}&price=#{payment_response["data"]["price_charged"]}&transaction_id=#{payment_response["data"]["pspReference"]}&title=#{payment_response["data"]["title"]}&dt=#{payment_response["data"]["expiry_date"]}&cupn_code=#{payment_response["data"]["coupon_code"]}&copun_tit=#{payment_response["data"]["coupon_code_title"]}&coupon_code_type=#{payment_response["data"]["coupon_code_type"]}"
    else
      redirect_to "#{SITE}/payment/payment_failed?message=#{payment_response['error']['message']['message']}&error_code=#{payment_response["error"]["code"]}&order_id=#{payment_response["error"]["message"]["order_id"]}&price=#{payment_response["error"]["message"]["price_charged"]}&cupn_code=&copun_tit=&coupon_code_type="
    end

  end

  def plans_purchase
   @region = $region
   @shipping_zip_codes = SHIPPING_ZIP_CODES
 end

  def promocode_apply
    begin
      plans_details = params[:new_plan_det]
      act_price = params[:actual_price].to_f
      plan_ids = plans_details.collect{|x|x.split("|")[0]}.join("")
      promocode_params = {
        :us => get_payment_signature_key(plan_ids),
        :packs => get_promocode_selected_packs(plans_details),
        :coupon_code => params[:promocode].upcase,
        :region => $region,
        :transaction_type =>  params[:transaction_type], 
        :amount =>  act_price  }
      p promocode_params.inspect
      p "@@@@@@@@@@@@@@@@"
      promocode_response = User.promocode_apply(cookies[:user_id], promocode_params)
      p promocode_response.inspect
      p "promocode response backend"
      p promocode_response.inspect 
      if promocode_response.has_key?("data")
        promo_coupon = promocode_response["data"]["payment"]
        no_of_days = promocode_response["data"]["payment"]["days"]
        start_date = (Date.today+(no_of_days.to_i.days)).strftime("%d %B, %Y")
        end_date = (Date.today+(no_of_days.to_i-1).days).strftime("%d %B, %Y")
        all_py_modes = promocode_response["data"]["payment"]["payment_modes"]
        if all_py_modes.empty?
          all_py_modes = ["all"]
        end
        render json: {:coupon_amount => promo_coupon["coupon_amount"],:grand_total => promo_coupon["net_amount"] , :status => true,:promo_coupon_id => promocode_response["data"]["payment"]["coupon_id"],:adyen_disc_amount => promocode_response["data"]["payment"]["pg_price"]["adyen"],:payment_page => promocode_response["data"]["payment"]["payment_page"],:coupon_type => promocode_response["data"]["payment"]["type"],:free_trail_coupon_msg => promocode_response["data"]["payment"]["coupon_message"],:free_coupon_start_date => start_date,:free_coupon_end_date => end_date,:subs_type => promocode_response["data"]["payment"]["subs_type"],:payment_modes => all_py_modes}
 
        #grand_total = (act_price - net_amount).to_f.truncate(2)
        #render json: {:coupon_amount => promo_coupon["coupon_amount"], :grand_total => promo_coupon["net_amount"] , :status => true,:promo_coupon_id => promocode_response["data"]["payment"]["coupon_id"],:adyen_disc_amount => promocode_response["data"]["payment"]["pg_price"]["adyen"],:payment_page => promocode_response["data"]["payment"]["payment_page"]}
      else
        render json: {:error_message => promocode_response["error"]["message"], :status => false}
      end
      rescue Exception => e
        p e.message.inspect
      end
   end

  def payu_payment_response
    # begin
     
      response = User.payu_response_call(params)
      if params["unmappedstatus"] == "userCancelled"
        redirect_to "#{SITE}/plans/plans_summary" and return
      else
        if response.has_key?("error")  
          @message = response["error"]["message"]["message"]
          @status = "failure"
          @heading_msg = "Your payment could not be processed"
          @order_id =  response["error"]["message"]["order_id"]
        else
          @message =  response["data"]["message"]
          @order_id =  response["data"]["order_id"]
          @status = "success"
          @heading_msg = "Your payment has been processed"
          @price = response["data"]["price_charged"]
          @pln_tit = response["data"]["title"]
          pln_exp_st = response["data"]["expiry_date"]
          @pln_exp_st =   pln_exp_st.blank? ? nil : pln_exp_st
        end
        if @region == "US"
          @currency = "$ "
        else
          @currency = "₹ "
        end
        if Rails.env.production? 
          payu_log = Logger.new('/mnt/payu.log')
          payu_log.info("=====================Start payu parameters #{params.inspect}=======================")
          payu_log.info("=====================Start of payu success response from backend #{response.inspect}=======================")
        end
      end
    # rescue Exception => e
      # render json: {:status => "false",:error_mesg => "Sorry something went wrong"}
    # end
  end

  def pre_post_paid_check phone_num,txn_id
    url = "http://m.shemaroo.com/intl/ComvivaService/CheckSubscriberProfileType/"
    header = {"Accept" => "application/json", "Cache-Control" => "no-cache"}
    #phone_num = "96550167685"
    #txn_id = "236789_96550167685"
    data = {"msisdn": phone_num ,"PromoID": "29329", "PartnerId": "9","TransactionID": txn_id}
    resp = Typhoeus.post(url,body: data , headers: header)
    obj = JSON.parse(resp.body)
    p obj.inspect
    #obj = 1
    obj["ocsResponse"]["subsType"]
  end


  def ooredoo_billing_response
    logger.info "Ooredoo Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response =  User.secure_payment("ooredoo",params)
     if Rails.env.production? 
      #logs start date is: 7-Jan-2020
        ooredoo_log = Logger.new('/mnt/ooredoo.log')
        ooredoo_log.info("=====================Start of Ooredoo Qatar Callback Logs=======================")
        ooredoo_log.info("Parameters from Ooredoo Qatar Callback ----------------------> #{params.inspect} <--------------------------------")
        ooredoo_log.info("Backend Response for Ooredoo Qatar -------> #{response.inspect}=======================")
        ooredoo_log.info("=====================End of Ooredoo Qatar Callback Logs=======================")
    end
    p response.inspect
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Ooredoo Billing"
    logger.info "=============================#{response.inspect}======================="
  end

  def ooredoo_kwbilling_response
    logger.info "Ooredoo Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response =  User.secure_payment("ooredo_kuwait",params)
    if Rails.env.production? 
      #logs start date is: 7-Jan-2020
        ooredoo_log = Logger.new('/mnt/ooredoo.log')
        ooredoo_log.info("=====================Start of Ooredoo Kuwait Callback Logs=======================")
        ooredoo_log.info("Parameters from Ooredoo Kuwait Callback ----------------------> #{params.inspect} <--------------------------------")
        ooredoo_log.info("Backend Response for Ooredoo Kuwait-------> #{response.inspect}=======================")
        ooredoo_log.info("=====================End of Ooredoo Kuwait Callback Logs=======================")
    end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Ooredoo Billing"
    logger.info "=============================#{response.inspect}======================="

  end


  def ksa_zain_billing_response
    logger.info "KSA ZAIN Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response =  User.secure_payment("ksa-zain",params)
    if Rails.env.production? 
      #logs start date is: 7-Jan-2020
        ooredoo_log = Logger.new('log/ksa_zain.log')
        ooredoo_log.info("=====================Start of Ksa Zain Callback Logs=======================")
        ooredoo_log.info("Parameters from Ksa Zain Callback ----------------------> #{params.inspect} <--------------------------------")
        ooredoo_log.info("Backend Response for Ksa Zain-------> #{response.inspect}=======================")
        ooredoo_log.info("=====================End of Ksa Zain Callback Logs=======================")
    end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Ksa Zain Billing"
    logger.info "=============================#{response.inspect}======================="

  end

  def kuwait_viva_billing_response
    logger.info "Kuwait VIVA Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response =  User.secure_payment("viva_kuwait",params)
    p response.inspect
    logger.info "backend response for Ooredoo Billing"
    logger.info "=============================#{response.inspect}======================="
  end


  def otm_response
    logger.info "OTM Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response =  User.secure_payment("unifi",params)
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end

    logger.info "backend response for OTM"
   logger.info "=============================#{response.inspect}======================="

  end

  def unifi_valid_ip_check
    st = "false"
    iv_code =  ""
    user_name = ""
    ip_adrs_resp = Typhoeus::Request.get("#{UNIFY_IP_URL}/?ip=#{$user_ip}",:connecttimeout =>10,:timeout => 40)
    if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
      unifi_validate_ip = Logger.new('log/unifi_transaction.log')
      unifi_validate_ip.info "************ Unifi request log data ****************"
      unifi_validate_ip.info "User IP Address ------------>#{$user_ip.inspect}<-----------------------------"
      unifi_validate_ip.info "Unify IP Url ---------------->#{UNIFY_IP_URL}<---------------------------------"
      unifi_validate_ip.info "Transaction Id ==============#{params[:trans_id].inspect} ===================="
      unifi_validate_ip.info "******************* Ip Address passing to Unifi *******************"
      unifi_validate_ip.info "IP Address #{$user_ip.inspect} passed to Unifi ----->#{ip_adrs_resp.code==200 ? "True" : "False"} <-----"
      unifi_validate_ip.info "Unifi_ip_url Response =======#{JSON.parse(ip_adrs_resp.body)}============="
      logger.info("==================== OMG Validation starts from here  =======================")
    end
    begin 
      ip_dt = JSON.parse(ip_adrs_resp.body)
      logger.info("==================== Unifi_ip_url Response: #{ip_dt} =======================")
      if ip_dt["status"] == "ELIGIBLE"
      logger.info("====================Unifi_ip_url status check: #{ip_dt["status"]} =======================")
        user_name = ip_dt["username"]
        #user_name = "shemaroo2@unifi"
        prod_st_code,mesg =  otm_prod_validation(params[:trans_id],user_name)
        logger.info("====================Unifi omg validation::ResponseCode :#{prod_st_code}:====== ResponseMsg: #{mesg} =======================")
        if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
          logger.info("==========Rails enviroment: #{Rails.env}=======================")
          unifi_validate_ip = Logger.new('log/unifi_transaction.log')
          unifi_validate_ip.info("Product validation status code-----------> #{prod_st_code} <-------------")
          unifi_validate_ip.info("Product validation msg--------------------> #{mesg} <--------------------")
        end
        p mesg.inspect
        if prod_st_code == "0"
          st = "true"
          gc_st_code,mesg,iv_code = otm_omg_gen_vode(params[:trans_id],user_name)
          mesg = "Sorry, your unifi account is currently suspended, please contact unifi"  if mesg == "Account is NOT active"
          if gc_st_code == "0"
            st = "true"
          end
        end
      else
        mesg = "Please check if you are connected to unifi WIFI connection & try again"
      end
    rescue
      mesg = "Sorry something wentwrong"
    end
    
    render :json => {:status => st,:msg => mesg,:iv_code => iv_code,:user_name => user_name,:prod_id => "PROALA024",:trans_id => params[:trans_id]}
  end

  def otm_prod_validation(trans_id,user_name)
    sign = otm_gen_sign(user_name)
    url = URI("#{OTM_URL}")
    http = Net::HTTP.new(url.host, url.port)
    prod_vald_req = Net::HTTP::Post.new(url)
    prod_vald_req["content-type"] = 'text/xml'
    prod_vald_req.body = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body><OMG_ProductValidation xmlns=\"http://omg.hypp.tv/\"><OMG_ValidationReq><OTT_ID>shemaroo</OTT_ID><TM_AccountID>#{user_name}</TM_AccountID><ProductID>PROALA024</ProductID><OTT_UserID></OTT_UserID><Signature>#{sign}</Signature></OMG_ValidationReq></OMG_ProductValidation></soap12:Body></soap12:Envelope>"
    prod_valid_resp = http.request(prod_vald_req)
    final_prod_resp = prod_valid_resp.read_body
    prod_val_dt = Hpricot.XML(final_prod_resp)
    if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
      otm_prod_valid = Logger.new('log/unifi_transaction.log')
      otm_prod_valid.info("==================== OTM Product Validation Start ==================================")
      otm_prod_valid.info("User name --------------------------> #{user_name} <------------------------------------------------")
      otm_prod_valid.info("Otm product validation Request ------------->#{prod_vald_req.body.inspect}<-------------------------")
      otm_prod_valid.info("Otm product validation Response --------------->#{final_prod_resp.inspect}<-------------------------")
      otm_prod_valid.info("Otm Product XML response--------------------------->#{prod_val_dt.inspect}<---------------------------------")
    end

    st_code = ""
    msg = ""
    prod_val_dt.search("soap:Envelope").each do |membership|
      body = membership.search("soap:Body")
      prod_resp = body.search("OMG_ProductValidationResponse")
      st_code = prod_resp.search("ResponseCode").inner_html
      msg = prod_resp.search("ResponseMsg").inner_html 
    end
    return st_code,msg
  end

  def otm_omg_gen_vode(tr_id,user_name)
    p tr_id.inspect
    sign = otm_gen_sign(user_name)
    url = URI("#{OTM_URL}")
    http = Net::HTTP.new(url.host, url.port)
    omg_gc_req = Net::HTTP::Post.new(url)
    omg_gc_req["content-type"] = 'text/xml'
    omg_gc_req.body = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body><OMG_GenerateVCode xmlns=\"http://omg.hypp.tv/\"><OMG_VCodeReq><OTT_ID>shemaroo</OTT_ID><OTT_TXNID>#{tr_id}</OTT_TXNID><TM_AccountID>#{user_name}</TM_AccountID><ProductID>PROALA024</ProductID><Signature>#{sign}</Signature></OMG_VCodeReq></OMG_GenerateVCode></soap12:Body></soap12:Envelope>"
    omg_gc_resp = http.request(omg_gc_req)
    final_gc_resp = omg_gc_resp.read_body
    gc_dt = Hpricot.XML(final_gc_resp)
    if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
      otm_gen_sign = Logger.new('log/unifi_transaction.log')
      otm_gen_sign.info("====================  Otm Omg Verification Code generation   ===================")
      otm_gen_sign.info("Current User --------------------------> #{user_name} <--------------------------")
      otm_gen_sign.info("Otm genation Signature----------------------> #{sign} <--------------------------")
      otm_gen_sign.info("Otm vode Request-----------------> #{omg_gc_req.body.inspect} <------------------")
      otm_gen_sign.info("Otm vode Response-------------------> #{final_gc_resp.inspect} <----------------")
    end

    st_code = ""
    msg = ""
    iv_code = ""
    gc_dt.search("soap:Envelope").each do |membership|
      body = membership.search("soap:Body")
      gc_resp = body.search("OMG_GenerateVCodeResponse")
      st_code = gc_resp.search("ResponseCode").inner_html
      msg = gc_resp.search("ResponseMsg").inner_html 
      if st_code == "0"
        iv_code =  gc_resp.search("InitialVCode").inner_html 
      end
    end
    return st_code,msg,iv_code
  end

  def validate_otp_ott_order
    p params.inspect
    pln_tit = ""
    pln_exp = ""
    sign = otm_gen_sign(params[:u_name])
    url = URI("#{OTM_URL}")
    http = Net::HTTP.new(url.host, url.port)
    otm_order_req = Net::HTTP::Post.new(url)
    otm_order_req["content-type"] = 'text/xml'
    otm_order_req.body = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body><OMG_NewOTTOrder xmlns=\"http://omg.hypp.tv/\"><OMG_NewOrderReq><OTT_ID>shemaroo</OTT_ID><OrderMode>1</OrderMode><OMG_TXNID></OMG_TXNID><OTT_TXNID>#{params[:tr_id]}</OTT_TXNID><TM_AccountID>#{params[:u_name]}</TM_AccountID><ProductID>#{params[:pr_id]}</ProductID><VerificationCode>#{params[:otm_otp]}</VerificationCode><Signature>#{sign}</Signature></OMG_NewOrderReq></OMG_NewOTTOrder></soap12:Body></soap12:Envelope>"
    otm_order_resp = http.request(otm_order_req)
    final_order_resp = otm_order_resp.read_body
    ott_dt = Hpricot.XML(final_order_resp)
    if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
      validate_otp_log = Logger.new('log/unifi_transaction.log')
      validate_otp_log.info("===================== UNIFI validate OTP logs Start ========================")
      validate_otp_log.info("Parameters----------------------> #{params.inspect} <--------------------------------------")
      validate_otp_log.info("OMG OTTOrder Request ---------------->#{otm_order_req.body.inspect} <----------------------")
      validate_otp_log.info("OMG OTTOrder Response -------------------->#{final_order_resp.inspect} <-------------------")
      validate_otp_log.info("XML response ----------------------> #{ott_dt.inspect} <-----------------------------------")
    end
    st = "false"
    msg = ""
    actv_dt = ""
    ott_dt.search("soap:Envelope").each do |membership|
      body = membership.search("soap:Body")
      order_resp = body.search("OMG_NewOrderResp")
      st_code = order_resp.search("ResponseCode").inner_html
      msg = order_resp.search("ResponseMsg").inner_html
      p st_code.inspect
      p "status code here" 
      if st_code == "0"
        st = "true"
        actv_dt =  order_resp.search("ActivationDate").inner_html 
        p "11111111111111111"
      end
    end
    if st == "true"
      prod_st_code,mesg  = otm_prod_validation(params[:tr_id],params[:u_name])
      logger.info "@@@@@@@@@@@@@@@@@ UNIFI production validation response #{prod_st_code.inspect} @@@@@@@@@@@@@@@@@@@@@@@@@@"
      pack_suc_resp = activate_unifi_pack_user(params,actv_dt,final_order_resp,prod_st_code)
      pln_tit = pack_suc_resp["data"]["title"]
      pln_exp = pack_suc_resp["data"]["expiry_date"]
    end
        #pack_suc_resp = activate_unifi_pack_user(params,actv_dt,final_order_resp)
    render :json => {:status => st,:activ_dt => actv_dt,:mesg => msg,:plan_title => pln_tit,:pln_exp_dt => pln_exp,:trans_status => "success" }
  end

  def token_gen(tr_id,user_name)
    sign_val = "&&shemaroo##AppSHM2020##"+"#{user_name}"+"&&"
    encry_sha1_sig = (Digest::SHA1.hexdigest sign_val).upcase
    url = URI("#{OTM_URL}")
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Post.new(url)
    request["content-type"] = 'text/xml'
    request.body = "<?xml version=\"1.0\" encoding=\"utf-8\"?><soap12:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap12=\"http://www.w3.org/2003/05/soap-envelope\"><soap12:Body><OMG_GenerateToken xmlns=\"http://omg.hypp.tv/\"><OMG_TokenReq><OTT_ID>shemaroo</OTT_ID><OTT_TXNID>#{tr_id}</OTT_TXNID><TM_AccountID>#{user_name}</TM_AccountID><ProductID>PROALA024</ProductID><CallBackURL>#{SITE}/payment/otm_response</CallBackURL><Signature>#{encry_sha1_sig}</Signature></OMG_TokenReq></OMG_GenerateToken></soap12:Body></soap12:Envelope>"
    response = http.request(request)
    resp = response.read_body
    data = Hpricot.XML(resp)
    token = ""
    if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
      unifi_res_log = Logger.new('log/unifi_transaction.log')
      unifi_res_log.info("======================== Token Generation Starts ============================\n")
      unifi_res_log.info("User Name--------------------#{user_name} <------------------------------------")
      unifi_res_log.info("Request to generate token -------------->#{request.body.inspect} <-------------")
      unifi_res_log.info("Response  ----------------->#{response.read_body.inspect} <--------------------")
    end 

    data.search("soap:Envelope").each do |membership|
      body = membership.search("soap:Body")
      omg_token = body.search("OMG_TokenResp")
      response_code = omg_token.search("ResponseCode").inner_html
      p response_code.inspect
      if response_code == "0"
        ottid = omg_token.search("OTT_ID").inner_html
        ott_txn_id = omg_token.search("OTT_TXNID").inner_html
        tm_account_id = omg_token.search("TM_AccountID").inner_html
        token = omg_token.search("Token").inner_html
        response_msg = omg_token.search("ResponseMsg").inner_html
      end
    end
    return token
  end

  def unifi_response
    @pln_tit = ""
    @pln_exp_st = ""
    if params[:status] == "success"
      @message = "Subscribed Successfully"
      @heading_msg = "Transaction Successful"
      @pln_tit = params[:title]
      @pln_exp_st = params[:pln_exp_dt]
    else
      @message = "Transaction Failed"
      @heading_msg = "Your payment could not be processed"
    end
    if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
      unifi_res_log = Logger.new('log/unifi_transaction.log')
      unifi_res_log.info("===================== Unifi Response ======================================\n")
      unifi_res_log.info("Unifi Response Status-----------------> #{params[:status]} <-----------------")
      unifi_res_log.info("Unifi Response Price------------------> #{params[:price]} <------------------")
      unifi_res_log.info("Unifi Activated message: ----------------> #{@message} <------------------------")
      unifi_res_log.info("===================== Transaction Ends ======================================\n")
    end

    @status = params[:status]
    @price = params[:price]
    @currency = "MYR "
  end

  def otm_resnd_otp
    st = "false"
    gc_st_code,mesg,iv_code = otm_omg_gen_vode(params[:trans_id],params[:u_name])
    if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
      otm_resend_log = Logger.new('log/my_region_transaction.log')
      otm_resend_log.info("======================== Otm resend Otp ================================")
      otm_resend_log.info("Otm transaction id----------------#{params[:trans_id]}-----------------------")
      otm_resend_log.info("Otm user name----------------#{params[:u_name]}------------------------------")
      otm_resend_log.info("IV code----------------#{iv_code}--------------------------------------------")
    end
    if gc_st_code == "0"
      st = "true"
    end
    render :json => {:status => st,:iv_code => iv_code}
  end

  def activate_unifi_pack_user(p,dt,resp,free_trial_check)
   unifi_req = {}
   unifi_req["subscription_id"] = p[:u_name]
   unifi_req["product_id"] = p[:pr_id]
   unifi_req["order_id"] = p[:tr_id]
   unifi_req["miscellaneous"] = resp
   unifi_req['status'] = "SUCCESS"
   if free_trial_check == "4"
    unifi_req["free_trial"] = "true"
   else
    unifi_req["free_trial"] = "false"
   end
   p unifi_req.inspect
   p "unifi request data heree"
    response =  User.secure_payment("unifi",unifi_req)
   if (Rails.env.pre_production? || Rails.env.production?) && $region=="MY"
    pck_activate_log = Logger.new('log/unifi_transaction.log')
    pck_activate_log.info("=========================  Unifi pack activation start ==================================\n")
    pck_activate_log.info("Unifi request data --------------------> #{unifi_req.inspect} <----------------------------")
    pck_activate_log.info("Unifi response data --------------------> #{response.inspect} <----------------------------")
   end

    p response.inspect
    return response
  end

  def digi_billing_response
    logger.info "Digi Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response =  User.secure_payment("digi",params)
     if Rails.env.production? 
        ccavenue_log = Logger.new('/mnt/digi_payments.log')
        ccavenue_log.info("=====================Start of Digi Callabck Logs=======================")
        ccavenue_log.info("Parameters from Digi Callabck ----------------------> #{params.inspect} <--------------------------------")
        ccavenue_log.info("Backend response from Digi ----------------------> #{response.inspect} <--------------------------------")
        ccavenue_log.info("=====================End of Digi Callabck Logs=======================")
    end
    p response.inspect
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Digi Billing"
    logger.info "=============================#{response.inspect}======================="
  end


 def zain_kwbilling_response
  logger.info "Zain Billing Request Details"
  logger.info "============#{params.inspect}=========================================="
  logger.info "***************************************************************************"
  response = User.secure_payment("zain_kuwait",params)
   if Rails.env.production? 
      zain_kw_log = Logger.new('/mnt/zain_kuwait_payment.log')
      zain_kw_log.info("=====================Start of Zain Kuwait Callabck Logs=======================")
      zain_kw_log.info("Parameters from Zain Kuwait Callabck ----------------------> #{params.inspect} <--------------------------------")
      zain_kw_log.info("Backend response from Zain Kuwait ----------------------> #{response.inspect} <--------------------------------")
      zain_kw_log.info("=====================End of Zain Kuwait Callabck Logs=======================")
   end
  if response.has_key?("error")
    @message = response["error"]["message"]["message"]
    @status = "failure"
    @heading_msg = "Transaction failed"
    @order_no = response["error"]["message"]["order_id"]
  else
    @message = response["data"]["message"]
    @status = "success"
    @heading_msg = "Transaction Successfull"
    @price = response["data"]["price_charged"]
    @order_no = response["data"]["order_id"]
  end
  logger.info "backend response for Zain Billing"
  logger.info "=============================#{response.inspect}======================="
end

  def oman_response
    logger.info "Oman Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response = User.secure_payment("omantel",params)
    if Rails.env.production? 
      omantel_log = Logger.new('/mnt/omantel_payment.log')
      omantel_log.info("=====================Start of Omantel Callabck Logs=======================")
      omantel_log.info("Parameters from Omantel Callback ----------------------> #{params.inspect} <--------------------------------")
      omantel_log.info("Backend response from Omantel ----------------------> #{response.inspect} <--------------------------------")
      omantel_log.info("=====================End ofOmantel Callback Logs=======================")
   end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message = response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Oman Billing"
    logger.info "=============================#{response.inspect}======================="
  end

 def ksa_stc_billing_response
  logger.info "Zain Billing Request Details"
  logger.info "============#{params.inspect}=========================================="
  logger.info "***************************************************************************"
  response = User.secure_payment("ksa-stc",params)
   if Rails.env.production? 
      zain_kw_log = Logger.new('log/ksa_stc_payment.log')
      zain_kw_log.info("=====================Start of Ksa Stc Callabck Logs=======================")
      zain_kw_log.info("Parameters from Ksa Stc Callabck ----------------------> #{params.inspect} <--------------------------------")
      zain_kw_log.info("Backend response from Ksa Stc ----------------------> #{response.inspect} <--------------------------------")
      zain_kw_log.info("=====================End of Ksa Stc Callabck Logs=======================")
   end
  if response.has_key?("error")
    @message = response["error"]["message"]["message"]
    @status = "failure"
    @heading_msg = "Transaction failed"
    @order_no = response["error"]["message"]["order_id"]
  else
    @message = response["data"]["message"]
    @status = "success"
    @heading_msg = "Transaction Successfull"
    @price = response["data"]["price_charged"]
    @order_no = response["data"]["order_id"]
  end
  logger.info "backend response for Ksa Stc Billing"
  logger.info "=============================#{response.inspect}======================="
end

def etisalat_uae_billing_response 
  logger.info "Etisalat Billing Request Details"
  logger.info "============#{params.inspect}=========================================="
  logger.info "***************************************************************************"
  response =  User.secure_payment("etisalat_uae",params)
  if Rails.env.production? 
    #logs start date is: 7-Jan-2020
      ooredoo_log = Logger.new('log/etisalat.log')
      ooredoo_log.info("=====================Start of Etisalat UAE Callback Logs=======================")
      ooredoo_log.info("Parameters from Etisalat UAE Callback ----------------------> #{params.inspect} <--------------------------------")
      ooredoo_log.info("Backend Response for Etisalat UAE-------> #{response.inspect}=======================")
      ooredoo_log.info("=====================End of Etisalat UAE Callback Logs=======================")
  end
  if response.has_key?("error")
    @message = response["error"]["message"]["message"]
    @status = "failure"
    @heading_msg = "Transaction failed"
    @order_no = response["error"]["message"]["order_id"]
  else
    @message =  response["data"]["message"]
    @status = "success"
    @heading_msg = "Transaction Successfull"
    @price = response["data"]["price_charged"]
    @order_no = response["data"]["order_id"]
  end
  logger.info "backend response for Etisalat Billing"
  logger.info "=============================#{response.inspect}======================="
end

  def mobily_response
    logger.info "Mobily Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response = User.secure_payment("mobily",params)
    if Rails.env.production? 
      omantel_log = Logger.new('/mnt/mobily_payment.log')
      omantel_log.info("=====================Start of Mobily Callabck Logs=======================")
      omantel_log.info("Parameters from Mobily Callback ----------------------> #{params.inspect} <--------------------------------")
      omantel_log.info("Backend response from Mobily ----------------------> #{response.inspect} <--------------------------------")
      omantel_log.info("=====================End of Mobily Callback Logs=======================")
   end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message = response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Mobily Billing"
    logger.info "=============================#{response.inspect}======================="
  end

  def boost_response
    logger.info "Boost Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    response = User.secure_payment("boost",params)
    if Rails.env.pre_production? || Rails.env.production? 
      boost_log = Logger.new('log/boost_transaction.log')
      boost_log.info("***************** ChargeStatus api start here ****************")
      boost_log.info("Parameters from Boost Malaysia Callback ----------------------> #{params.inspect} <--------------------------------")
      if response["data"] && response["data"]["charge_status"]
        boost_log.info("Backend request to Boost Malaysia ----------------------> #{response["data"]["charge_status"]["req"].inspect } <--------------------------------")
        boost_log.info("Backend response from Boost Malaysia ----------------------> #{response["data"]["charge_status"]["res"].inspect } <--------------------------------")
      end
      boost_log.info("***************** ChargeStatus api end here ****************")
   end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message = response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Boost Billing"
    logger.info "=============================#{response.inspect}======================="
  end

  def du_uae_billing_response
    logger.info "DU UAE Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response =  User.secure_payment("du",params)
    if Rails.env.production? 
      #logs start date is: 7-Jan-2020
        ooredoo_log = Logger.new('log/du_uae.log')
        ooredoo_log.info("=====================Start of DU UAE Callback Logs=======================")
        ooredoo_log.info("Parameters from DU UAE Callback ----------------------> #{params.inspect} <--------------------------------")
        ooredoo_log.info("Backend Response for DU UAE-------> #{response.inspect}=======================")
        ooredoo_log.info("=====================End of DU UAE Callback Logs=======================")
    end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message =  response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for DU Billing"
    logger.info "=============================#{response.inspect}======================="
  end

  def stc_bahrain_billing_response
    logger.info "Stc Bahrain Billing Request Details"
    logger.info "============#{params.inspect}=========================================="
    logger.info "***************************************************************************"
    response = User.secure_payment("stc-bahrain",params)
    if Rails.env.production? 
      stc_bahrain_log = Logger.new('/mnt/stc_bahrain_payment.log')
      stc_bahrain_log.info("=====================Start of Stc Bahrain Callabck Logs=======================")
      stc_bahrain_log.info("Parameters from Stc Bahrain Callback ----------------------> #{params.inspect} <--------------------------------")
      stc_bahrain_log.info("Backend response from Stc Bahrain ----------------------> #{response.inspect} <--------------------------------")
      stc_bahrain_log.info("=====================End of Stc Bahrain Callback Logs=======================")
   end
    if response.has_key?("error")
      @message = response["error"]["message"]["message"]
      @status = "failure"
      @heading_msg = "Transaction failed"
      @order_no = response["error"]["message"]["order_id"]
    else
      @message = response["data"]["message"]
      @status = "success"
      @heading_msg = "Transaction Successfull"
      @price = response["data"]["price_charged"]
      @order_no = response["data"]["order_id"]
    end
    logger.info "backend response for Stc Bahrain Billing"
    logger.info "=============================#{response.inspect}======================="
  end

  private

  def get_modified_user_plans(plan_id,plan_status)
      response = Ott.subscription_plans
      @all_plans = response["data"]["catalog_list_items"]
      lang =  get_user_lang(params)
      user_plans_resp = Ott.user_plans(cookies[:user_id],lang)
      plan_sub_id = user_plans_resp['current_active_plans'].collect{|x|x["subscription_id"] if (x["plan_id"] == params[:plan_id])}.compact.first 
      current_plan_det = user_plans_resp['current_active_plans'].collect{|x|x if (x["plan_id"] == params[:plan_id])}.compact.first 
      p current_plan_det, "0"*100
      @plan_payment_type = current_plan_det["payment_gateway"]
      @free_trail_status = "false"
      @free_trail_end_date  = ""
      if plan_sub_id.nil?        
        redirect_to "#{SITE}/plans"
      else
        plans_data = {
          :user_plans  => {
            :current_plan_id => params[:plan_id],
            :subscription_category_id => plan_sub_id
          }
        }

        @free_trail_status = current_plan_det["free_trial_pack"]
        @free_trail_end_date = current_plan_det["free_trial_end_date"].to_date.strftime("%d %b %Y") if current_plan_det.has_key?("free_trial_end_date") and @free_trail_status == "true"
        modified_plans_resp = User.get_user_modified_plans(cookies[:user_id],plans_data,plan_status)
        @user_modified_plans = modified_plans_resp["data"]["modify_plans"]
       
      end
  end

  def get_selected_packs(all_plans)
    all_packs = []
    all_plans.each do |p|
    plan = p.split("|")
    pack_data = {
    :plan_categories => [plan[2]], 
    :category_type =>"individual",
    :category_pack_id => plan[9],
    :subscription_catalog_id => plan[1], 
    :plan_id => plan[0]
    }
    p pack_data.inspect
    all_packs << pack_data
   end
   return all_packs      

  end

  def get_promocode_selected_packs(packs)
    selected_plans = []
    packs.each do |p|
    plan = p.split("|")
    pack_data = {
    :category_pack_id => plan[9],
    :plan_id => plan[0]
    }
    p pack_data.inspect
    selected_plans << pack_data
   end
   return selected_plans
  end

  def otm_gen_sign(user_name)
    encry_sha1_sig = ""
    sign_val = "&&shemaroo##AppSHM2020##"+"#{user_name}"+"&&"
    encry_sha1_sig = (Digest::SHA1.hexdigest sign_val).upcase
    return encry_sha1_sig
  end




end