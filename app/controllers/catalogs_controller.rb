class CatalogsController < ApplicationController
  def index
    #if $region == "US" || $region == "IN" || $region == "BD" || $region == "UAE" || $region == "MY" 
      begin
        page_number = 0
        page_size = 10
        new_lang = request.path.split("/")[1]
        params[:lang] = new_lang
        lang = get_user_lang(params)
       response = Rails.cache.fetch("home_page_list_#{$region}_#{page_number}_#{page_size}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_home_list(page_number,page_size,lang)    
       }
       @home_list = response["data"]["catalog_list_items"]
       browse_resp =  Rails.cache.fetch("home_page_browse_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_browse_categories(lang)      
       }
       @browse_items = browse_resp["data"]["catalog_list_items"]
       @items = @home_list
       @fst_itms =  @home_list[1..4]
       @scnd_itms = @home_list[5..@items.length]
      rescue Exception => e
        logger.info e.message
       Rails.cache.delete("home_page_list_#{$region}")
       @home_list = []
        redirect_to "#{SITE}/500"
      end 
     # else
     #  redirect_to "#{SITE}/landing_page",status: 302
     # end
  end

  def channel_catalog_item
    params[:catalog_name] = "channels"
    channel_response = Rails.cache.fetch("channels_list", expires_in: CACHE_EXPIRY_TIME){
        Ott.get_channel_list
      }
       #raise  channel_response.inspect
    @items_list = channel_response["data"]["catalog_list_items"]
    @item_length = @items_list.length - 1

    @channel_home_link = ""

    @items_list.each_with_index do |list,index|
      if ["layout_type"] == "t_button"
        @channel_home_link = list["home_link"]
        puts "aaaaaaaaaaaaaaaaaaaaa"
      end
    
      if @channel_home_link == ""
       view_all_channel_link_response = Ott.get_channel_list_pagination(1,20)
        @items_link_list = view_all_channel_link_response["data"]["catalog_list_items"]
        @items_link_list.each_with_index do |list,index|
          if list["layout_type"] == "t_button"
            @channel_home_link = list["home_link"]
            puts "bbbbbbbbbbbbbbbbbbbbbbb"
          end
        end
      end
    end

  end  

  def all_channels_playlist
    params[:catalog_name] = "channels"
    lang = get_user_lang(params)
    if request.xhr?
      next_page = "true"
      if params[:theme_type] == "t_2_3_movie" || params[:theme_type] == "movies"
        page_size = 24
      else
        page_size = 20
      end
      if params[:page_no].present?
        page_number = params[:page_no].to_i
      end
      response = Rails.cache.fetch("all_channel_list#{params[:cat_name]}_#{page_number}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
      #Ott.get_items_list_with_pagination(params[:cat_name],page_number,page_size,lang)
      Ott.get_all_channel_playlist(params[:cat_name],page_number,page_size,lang)
      }
      no_of_pages = (response["data"]["total_items_count"]/page_size).round
      # if params[:theme_type] == "t_2_3_movie"
      #   no_of_pages = no_of_pages+1
      # end
      if page_number >= no_of_pages
        next_page = "false"
      end
      items =  response["data"]["catalog_list_items"].collect{|x|x["ml_title"]+"$"+get_load_more_item_image_url(params[:theme_type],x)+"$"+get_item_url(x)+"$"+get_premium_tag(x)+"$"+x['ml_genres'][0].capitalize+"$"+x['ml_language'][0].capitalize+"$"+"$"+x['theme']+"$"+x['access_control']['is_free'].to_s}

      render :json => {:status => true,:items => items,:next_page => next_page,:pageno => page_number+1}
    else
      begin 
        response = Rails.cache.fetch("all_channel_list#{params[:cat_name]}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
        #Ott.get_items_list_with_pagination(params[:cat_name],0,24,lang)
        Ott.get_all_channel_playlist(params[:cat_name],0,24,lang)
        }
        @all_items = response["data"]["catalog_list_items"]
        @layout_type =  response["data"]["layout_type"]
        @next_page = "false"
        if @layout_type == "t_2_3_movie" || @layout_type == "movies"
          @all_items =  @all_items.first(24)
          if response["data"]["total_items_count"] > 24
            @next_page = "true"
          end
        else
          @all_items =  @all_items.first(20)
          if response["data"]["total_items_count"] > 20
            @next_page = "true"
          end
        end
        @layout_scheme = response["data"]["layout_scheme"]
        #@title = response["data"]["ml_title"]
        @title = response["data"]["ml_display_title"]
        @theme = response["data"]["theme"]
        #@meta_title = @title+" of #{@layout_scheme.gsub("_"," ")}"
        @meta_title = @title
        @meta_title = response["data"]["meta_title"] ? response["data"]["meta_title"] : ""
        @meta_description = response["data"]["meta_description"] ? response["data"]["meta_description"] : ""
        @meta_keywords = response["data"]["meta_keywords"] ? response["data"]["meta_keywords"] : ""
        #@next_page = true
      rescue Exception => e
        logger.info e.message
        Rails.cache.delete("all_items_list_#{params[:catalog_name]}_#{lang}")
        @all_items = []
      end
    end
  end

  def more_channels_list
    response = Rails.cache.fetch("more_channel_list_#{$region}_#{params[:channel_id]}", expires_in: CACHE_EXPIRY_TIME) {
      Ott.get_more_channel_list(params[:channel_id])    
     }
      more_channel_list = []
      unless response.empty?
        more_channel_list = response['data']['catalog_list_items'].collect{|x|(x["ml_title"].present? ? x["ml_title"]:x["title"])+"$"+(x["ml_item_caption"].present? ? x["ml_item_caption"] : x["item_caption"])+"$"+x["thumbnails"]["large_16_9"]["url"]+"$"+x['seo_web_url']+"$"+get_premium_tag(x)+"$"+(get_premium_tag(x) == "false" ? "display:none" : "")+"$"+x["content_definition"]+"$"+x["access_control"]["premium_tag"].to_s+"$"+x['access_control']['is_free'].to_s+"$"+x['description']}
      end
    render :json => {:status => true,:more_channel_list => more_channel_list}
  end

  def show_catalog_item
   #begin
     lang = get_user_lang(params)
     if params[:catalog_name] == "firebase-messaging-sw"
      render :json => {:status => "true"}
    elsif params[:catalog_name] == "all-channels"
      redirect_to "#{SITE}"
    elsif  params[:lang].present? && params[:lang] == "all-channels"
      catalog_name = "all-channels"
      show_name = params[:catalog_name]
       item_response = Rails.cache.fetch("item_details_#{catalog_name}_#{show_name}_#{$region}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_details(catalog_name,show_name,lang)
        }
        @item_details = item_response["data"]
        @theme = item_response["data"]["theme"]
        if @theme == "linear"
          programs_response = Ott.get_channel_programs(catalog_name,show_name)
          @channel_all_programs = programs_response["data"]["items"]
          channels_response = Ott.get_all_channels
          @all_channels = channels_response["data"]["catalog_list_items"]
        elsif @theme == "live"
          more_items_live = Rails.cache.fetch("more_items_#{params[:catalog_name]}_#{@item_details['catalog_object']['plan_category_type']}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
            Ott.get_more_livetv(catalog_name,@item_details['catalog_object']['plan_category_type'])
          }
          @more_livtev_items = more_items_live["data"]["items"]
          
        end
        #@md5_key,@time = get_md5_key(item_response['data'])
         #@new_play_url,@key,@sub_tit_url =  get_play_url_key(item_response['data'])
        more_item_response = Rails.cache.fetch("more_items_#{catalog_name}_#{@item_details['genres'][0]}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_genre(catalog_name,@item_details['genres'][0],lang)
        }
        @layout_scheme = item_response["data"]["catalog_object"]["layout_scheme"]
        @genere_items = more_item_response["data"]["items"]
        @layout_type = item_response["data"]["catalog_object"]["layout_type"]
        @is_free = item_response["data"]['access_control']['is_free']
        @video_premium_status = item_response["data"]["access_control"]["login_required"]
        @episode_status = item_response["data"]['next_episode_icon']
        @episode_start_time =item_response["data"]['next_episode_start_time']
        if $region == "US"  && @theme == "live"
          @video_premium_status = item_response["data"]["access_control"]["login_required"]
          #@video_premium_status = true
        end
        if @item_details.has_key?("associated_videos") && @item_details["associated_videos"] == true
          associated_video_resp = Rails.cache.fetch("associated_videos_#{params[:catalog_name]}_#{@item_details['friendly_id']}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
            Ott.get_associated_videos(catalog_name,@item_details['friendly_id'])
          }
          @associated_videos = associated_video_resp["data"]["items"]
        else
          @associated_videos = []
        end
        render "item_details"
    else
    params[:catalog_name] =   params[:catalog_name] ? params[:catalog_name] :request.fullpath.split("/")[1]
    cat_name = params[:catalog_name] 

    cat_name = get_head_link(params[:catalog_name],"category") #if Rails.env.production?
    p cat_name.inspect
    p "@@@@@@@@@@@@@@@@@@@@@@"
    response = Rails.cache.fetch("catalog_item_list_#{cat_name}_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
     Ott.get_items_list(cat_name,lang)
    }
    @items_list = response["data"]["catalog_list_items"]
    @fst_itms =  @items_list[1..5]
    @schem_tag = []
    @fst_itms.each_with_index do |l,i|
      hash_tag = {
       "@type": "ListItem",
        "position": i+1,
        "name": l['display_title'],
        "item":   l["layout_type"] == "t_16_9_epg" ? SITE + "#{get_item_url(l['catalog_list_items'].first)}" : SITE + "#{get_list_url(l)}"
       }
       @schem_tag << hash_tag
    end
    @catalog_items = @items_list.drop(1)
    @show_catalog_item = response["data"]["friendly_id"]
    title,desc,keywords = get_category_seo(response["data"]["friendly_id"])
    @meta_title = title
    @meta_desc = desc
    @meta_keywords = keywords
    @layout_scheme = response["data"]["layout_scheme"]
    @item_count =  response["data"]["total_items_count"]
    @catalog_name = cat_name
    @cat_link_name = response["data"]["ml_display_title"]


    # if params[:catalog_name] == "channel"
    #   render  "channel_catalog_item"
    # end  
    #params[:catalog_name]
    
    # subscribe_item = response["data"]["catalog_list_items"].collect{|x| x if(x["layout_type"] == "t_subscription")}.compact.first
    # @subscribe_img = subscribe_item["list_item_object"]["banner_image"]
    # @plan_det = subscribe_item["catalog_list_items"][0]["plans"].first
  end
   # rescue Exception => e
   #  redirect_to "#{SITE}"
   #  #redirect_to "#{SITE}/500"
   #    logger.info e.message
   #   Rails.cache.delete("catalog_item_list_#{cat_name}")
   #   @items_list = []
   #  end
  end

  def indiv_category_landing
    begin
      lang = get_user_lang(params)
      if params[:lang] != lang
        redirect_to "#{SITE}"
      else
        cat_name = request.fullpath.split("/")[2]
        params[:catalog_name] = cat_name
        cat_name = get_head_link(params[:catalog_name],"category") #if Rails.env.production?
        p cat_name.inspect
        p "@@@@@@@@@@@@@@@@@@@@@@"
        response = Rails.cache.fetch("catalog_item_list_#{cat_name}_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_list(cat_name,lang)
        }
        @items_list = response["data"]["catalog_list_items"]
        @catalog_items = @items_list.drop(1)
        @show_catalog_item = response["data"]["friendly_id"]
        title,desc,keywords = get_category_seo(response["data"]["friendly_id"])
        @meta_title = title
        @meta_desc = desc
        @meta_keywords = keywords
        @layout_scheme = response["data"]["layout_scheme"]
        @item_count =  response["data"]["total_items_count"]
        @catalog_name = cat_name
        render "show_catalog_item"
      end
    rescue Exception => e
      redirect_to "#{SITE}"
      logger.info e.message
       Rails.cache.delete("catalog_item_list_#{cat_name}")
       @items_list = []
    end
  end

  def all_items_list
     lang = get_user_lang(params)
    if request.xhr?
      next_page = "true"
      if params[:theme_type] == "t_2_3_movie" || params[:theme_type] == "movies"
        page_size = 24
      else
        page_size = 20
      end
      if params[:page_no].present?
        page_number = params[:page_no].to_i
      end
      response = Rails.cache.fetch("all_items_list_#{params[:catalog_name]}_#{page_number}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
      Ott.get_items_list_with_pagination(params[:catalog_name],page_number,page_size,lang)
      }
      no_of_pages = (response["data"]["total_items_count"]/page_size).round
      # if params[:theme_type] == "t_2_3_movie"
      #   no_of_pages = no_of_pages+1
      # end
      if page_number >= no_of_pages
        next_page = "false"
      end
      #items =  response["data"]["catalog_list_items"].collect{|x|x["ml_title"]+"$"+get_load_more_item_image_url(params[:theme_type],x)+"$"+get_item_url(x)+"$"+get_premium_tag(x)+"$"+x['ml_genres'][0].capitalize+"$"+x['ml_language'][0].capitalize+"$"+"$"+x['theme']+"$"+x['access_control']['is_free'].to_s}
      items =  response["data"]["catalog_list_items"].collect{|x|x["ml_title"]+"$"+get_load_more_item_image_url(params[:theme_type],x)+"$"+x["seo_web_url"]+"$"+get_premium_tag(x)+"$"+x['ml_genres'][0].capitalize+"$"+x['ml_language'][0].capitalize+"$"+"$"+x['theme']+"$"+x['access_control']['is_free'].to_s}

      render :json => {:status => true,:items => items,:next_page => next_page,:pageno => page_number+1}
    else
      begin 
        response = Rails.cache.fetch("all_items_list_#{params[:catalog_name]}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
        Ott.get_items_list_with_pagination(params[:catalog_name],0,24,lang)
        }
        @all_items = response["data"]["catalog_list_items"]
        @layout_type =  response["data"]["layout_type"]
        @next_page = "false"
        if @layout_type == "t_2_3_movie" || @layout_type == "movies"
          @all_items =  @all_items.first(24)
          if response["data"]["total_items_count"] > 24
            @next_page = "true"
          end
        else
          @all_items =  @all_items.first(20)
          if response["data"]["total_items_count"] > 20
            @next_page = "true"
          end
        end
        @layout_scheme = response["data"]["layout_scheme"]
        @theme = response["data"]["catalog_list_items"][0]["theme"]
        #@title = response["data"]["ml_title"]
        @title = response["data"]["ml_display_title"]
        #@meta_title = @title+" of #{@layout_scheme.gsub("_"," ")}"
        @meta_title = @title
        @meta_title = response["data"]["meta_title"] ? response["data"]["meta_title"] : ""
        @meta_description = response["data"]["meta_description"] ? response["data"]["meta_description"] : ""
        @meta_keywords = response["data"]["meta_keywords"] ? response["data"]["meta_keywords"] : ""
        #@next_page = true
      rescue Exception => e
        logger.info e.message
        Rails.cache.delete("all_items_list_#{params[:catalog_name]}_#{lang}")
        @all_items = []
      end
    end
  end


  def seasons_items_list
     lang = get_user_lang(params)
     
      response = Rails.cache.fetch("seasons_#{params[:season_id]}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
      Ott.get_seasons_epsiodes(params[:season_id],lang)
      }
      @all_items = response["data"]["items"]
      @layout_type =  response["data"]["layout_type"]
  end

  def item_details
    lang = get_user_lang(params)
    begin
      if params[:item_id].present?
          current_url = request.fullpath.split("/")[1]
          current_url = request.fullpath.split("/")[2] if params[:lang].present?
          theme_type = get_theme_type(current_url)
          item_params = {
            :data => {
              :item_id => params[:item_id],
              :theme => theme_type
            }
          }
          p item_params.inspect
          p "@@@@@@@@@@@@@@@@@@@@@"
          url_resp = User.get_item_catalog_id(item_params)

          p url_resp.inspect
          catalog_name = url_resp["data"]["friendly_id"]
          show_name = params[:item_id]
      else
        catalog_name = params[:catalog_name]
        show_name = params[:show_name]
      end
       item_response = Rails.cache.fetch("item_details_#{catalog_name}_#{show_name}_#{$region}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_details(catalog_name,show_name,lang)
        }

      if item_response.has_key?("error")
        redirect_to "#{SITE}/404"
      else

        content_info = Rails.cache.fetch("item_details_#{catalog_name}_#{show_name}_#{$region}_#{lang}_content_info", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_content_info(catalog_name,show_name,lang)
        }
        @content_info = content_info["data"]["catalog_list_items"]
        
      if item_response["data"].has_key?("episode_flag")
        @layout_scheme = item_response["data"]["catalog_object"]["layout_scheme"]
        @episode_details = item_response["data"]
        p @episode_details['item_caption'].inspect
        #@content_id = @episode_details["last_episode"]["content_id"]
        @image_url = @episode_details["thumbnails"]['large_16_9']['url']
        tvshow_response =  Rails.cache.fetch("all_epsiodes_#{catalog_name}_#{show_name}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_all_epsiodes(catalog_name,show_name,lang)
        }

        @all_episodes = tvshow_response["data"]["items"]

        catalog_response = Rails.cache.fetch("more_details_#{catalog_name}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_catalog_details(catalog_name)
        }
        
        if @all_episodes.count > 0
          @video_duration = @all_episodes.first['duration_string']
          @content_id = @all_episodes.first['content_id']
          #@md5_key,@time = get_md5_key(@all_episodes.first)
          #@new_play_url,@key,@sub_tit_url =  get_play_url_key(@all_episodes.first)
        else
          @new_play_url,@key,@sub_tit_url = ""
          @content_id = tvshow_response["data"]["content_id"]
          @video_duration = ""
        end
        @other_items = catalog_response["data"]["items"]
        @catalog_name = catalog_response["data"]["name"]
        @tvshow_name = item_response["data"]["title"]
        @video_preview = nil
        #@video_premium_status = "false"
       
        if @all_episodes.present?
          @video_premium_status = @all_episodes.first['access_control']['login_required']
          @skip_status = @all_episodes.first['skip_intro']
          @skip_start_time = @all_episodes.first['skip_start_time']
          @skip_end_time = @all_episodes.first['skip_end_time']
          @content_definition = @all_episodes.first['content_definition']
          @is_free = @all_episodes.first['access_control']['is_free']
          @episode_status = @all_episodes.first['next_episode_icon']
          @episode_start_time = @all_episodes.first['next_episode_start_time']
        else
          @video_premium_status = item_response["data"]["access_control"]["login_required"]
          @skip_status = item_response["data"]['skip_intro']
          @skip_start_time = item_response["data"]['skip_start_time']
          @skip_end_time = item_response["data"]['skip_end_time']
          @content_definition = item_response['content_definition']
          @is_free = item_response["data"]['access_control']['is_free']
          @episode_status = item_response["data"]['next_episode_icon']
          @episode_start_time = item_response["data"]['next_episode_start_time']
        end
        @theme_type = "show"
        render "show_details"
      else
        @item_details = item_response["data"]
        @theme = item_response["data"]["theme"]
        
        #@md5_key,@time = get_md5_key(item_response['data'])
        #@new_play_url,@key,@sub_tit_url =  get_play_url_key(item_response['data'])
        more_item_response = Rails.cache.fetch("more_items_#{catalog_name}_#{@item_details['genres'][0]}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_genre(catalog_name,@item_details['genres'][0],lang)
        }
        @layout_scheme = item_response["data"]["catalog_object"]["layout_scheme"]
        @genere_items = more_item_response["data"]["items"]
        @layout_type = item_response["data"]["catalog_object"]["layout_type"]
        @is_free = item_response["data"]['access_control']['is_free']
        @video_premium_status = item_response["data"]["access_control"]["login_required"]
        @skip_status = item_response["data"]['skip_intro']
        @skip_start_time = item_response["data"]['skip_start_time']
        @skip_end_time = item_response["data"]['skip_end_time']
        @content_definition = item_response['content_definition']
        @episode_status = item_response["data"]['next_episode_icon']
        @episode_start_time =item_response["data"]['next_episode_start_time']
        @meta_title = item_response["data"]["title"] ? item_response["data"]["title"] : ""
        @meta_description = item_response["data"]["description"] ? item_response["data"]["description"] : ""
        @meta_keywords = item_response["data"]["keywords"] ? item_response["data"]["keywords"] : ""
        if $region == "US"  && @theme == "live"
          @video_premium_status = item_response["data"]["access_control"]["login_required"]
          #@video_premium_status = true
        end
        if @item_details.has_key?("associated_videos") && @item_details["associated_videos"] == true
          associated_video_resp = Rails.cache.fetch("associated_videos_#{params[:catalog_name]}_#{@item_details['friendly_id']}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
            Ott.get_associated_videos(catalog_name,@item_details['friendly_id'])
          }
          @associated_videos = associated_video_resp["data"]["items"]
        else
          @associated_videos = []
        end
        if @theme == "linear"
          programs_response = Ott.get_channel_programs(catalog_name,show_name)
          @channel_all_programs = programs_response["data"]["items"]
          channels_response = Ott.get_all_channels
          @all_channels = channels_response["data"]["catalog_list_items"]
        elsif @theme == "live"
          more_items_live = Rails.cache.fetch("more_items_#{params[:catalog_name]}_#{@item_details['catalog_object']['plan_category_type']}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
            Ott.get_more_livetv(catalog_name,@item_details['catalog_object']['plan_category_type'])
          }
          @more_livtev_items = more_items_live["data"]["items"]
        elsif @theme == "vod_playlist"
          media_list_response = Rails.cache.fetch("media_list_#{$region}_#{catalog_name}_#{show_name}", expires_in: CACHE_EXPIRY_TIME) {
            Ott.get_media_list(catalog_name,show_name,lang)    
          }
          @media_list = media_list_response["data"]["items"]
          #@media_new_play_url,@media_key,@media_sub_tit_url =  get_play_url_key(item_response['data'])
          render "channels_details"
        end
      end
    end
    rescue Exception => e
      redirect_to "#{SITE}/500"
      logger.info e.message
      Rails.cache.delete("item_details_#{catalog_name}_#{show_name}")
      @item_details = []
    end
  end

  def episode_details
     begin
      if params[:item_id].present?
        current_url = request.fullpath.split("/")[1]
          item_params = {
            :data => {
              :item_id => params[:item_id],
              :theme => "episode"
            }
          }
          url_resp = User.get_item_catalog_id(item_params)
          catalog_name = url_resp["data"]["friendly_id"]
          item_name = params[:item_id]
     
      else
        catalog_name = params[:catalog_name]
        item_name = params[:item_name]

      end
      lang =  get_user_lang(params)
      if params[:catalog_name].present? && params[:show_name] == "all-channels"
        catalog_name = params[:show_name]
       show_name = params[:item_name]
       item_response = Rails.cache.fetch("item_details_#{catalog_name}_#{show_name}_#{$region}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_details(catalog_name,show_name,lang)
        }
        @item_details = item_response["data"]
        @theme = item_response["data"]["theme"]



        if @theme == "linear"
          programs_response = Ott.get_channel_programs(catalog_name,show_name)
          @channel_all_programs = programs_response["data"]["items"]
          channels_response = Ott.get_all_channels
          @all_channels = channels_response["data"]["catalog_list_items"]
        elsif @theme == "live"
          more_items_live = Rails.cache.fetch("more_items_#{params[:catalog_name]}_#{@item_details['catalog_object']['plan_category_type']}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
            Ott.get_more_livetv(catalog_name,@item_details['catalog_object']['plan_category_type'])
          }
          @more_livtev_items = more_items_live["data"]["items"]
          
        end
        #@md5_key,@time = get_md5_key(item_response['data'])
         #@new_play_url,@key,@sub_tit_url =  get_play_url_key(item_response['data'])
        more_item_response = Rails.cache.fetch("more_items_#{catalog_name}_#{@item_details['genres'][0]}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_genre(catalog_name,@item_details['genres'][0],lang)
        }
        @layout_scheme = item_response["data"]["catalog_object"]["layout_scheme"]
        @genere_items = more_item_response["data"]["items"]
        @layout_type = item_response["data"]["catalog_object"]["layout_type"]
        @is_free = item_response["data"]['access_control']['is_free']
        @video_premium_status = item_response["data"]["access_control"]["login_required"]
         @skip_status = item_response["data"]['skip_intro']
          @skip_start_time = item_response["data"]['skip_start_time']
          @skip_end_time = item_response["data"]['skip_end_time']
          @content_definition = item_response['content_definition']
         @episode_status = item_response["data"]['next_episode_icon']
        @episode_start_time =item_response["data"]['next_episode_start_time']
        if $region == "US"  && @theme == "live"
          @video_premium_status = item_response["data"]["access_control"]["login_required"]
          #@video_premium_status = true
        end
        if @item_details.has_key?("associated_videos") && @item_details["associated_videos"] == true
          associated_video_resp = Rails.cache.fetch("associated_videos_#{params[:catalog_name]}_#{@item_details['friendly_id']}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
            Ott.get_associated_videos(catalog_name,@item_details['friendly_id'])
          }
          @associated_videos = associated_video_resp["data"]["items"]
        else
          @associated_videos = []
        end
        render "item_details"
      else
      response = Rails.cache.fetch("item_details_#{params[:catalog_name]}_#{params[:show_name]}_#{item_name}", expires_in: CACHE_EXPIRY_TIME){
       Ott.get_episode_details(catalog_name,params[:show_name],item_name,lang)
      }
      if response.has_key?("error")
        redirect_to "#{SITE}/404"
      else

        # content_info = Rails.cache.fetch("item_details_#{catalog_name}_#{show_name}_#{$region}_#{lang}_content_info", expires_in: CACHE_EXPIRY_TIME){
        #  Ott.get_items_content_info(catalog_name,params[:show_name],lang)
        # }
        # @content_info = content_info["data"]["catalog_list_items"]
      @theme_type = "show"
      @episode_details = response["data"]
      @video_duration = @episode_details['duration_string']
      @content_id = @episode_details["content_id"]
      @layout_scheme = response["data"]["catalog_object"]["layout_scheme"]
       #@md5_key,@time = get_md5_key(response['data'])
      #@new_play_url,@key,@sub_tit_url =  get_play_url_key(response["data"])
      @video_preview = response["data"]["preview"]
      tvshow_response =  Rails.cache.fetch("all_epsiodes_#{catalog_name}_#{params[:show_name]}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_all_epsiodes(catalog_name,params[:show_name],lang)
        }
      unless tvshow_response.has_key?("error")
        @all_episodes = tvshow_response["data"]["items"]
        catalog_response = Rails.cache.fetch("more_details_#{catalog_name}", expires_in: CACHE_EXPIRY_TIME){
           Ott.get_catalog_details(catalog_name)
          }
        @other_items = catalog_response["data"]["items"]
        @catalog_name = catalog_response["data"]["name"]
        @tvshow_name = response["data"]["show_name"]
        @layout_type = catalog_response["data"]["catalog_object"]["layout_type"]
        @theme = "episode"
        @is_free = response["data"]["access_control"]["is_free"]
        @video_premium_status = response["data"]["access_control"]["login_required"]
        @skip_status = response["data"]['skip_intro']
        @skip_start_time = response["data"]['skip_start_time']
        @skip_end_time = response["data"]['skip_end_time']
        @content_definition = response['content_definition']
        @episode_status = response["data"]['next_episode_icon']
        @episode_start_time = response["data"]['next_episode_start_time']
        @theme_type = "episode"
      else
        redirect_to "#{SITE}/404"
      end
    end
    end
    rescue Exception => e
      redirect_to "#{SITE}/500"
      logger.info e.message
     Rails.cache.delete("item_details_#{catalog_name}_#{params[:show_name]}_#{item_name}")
     @epsiode_details = []
    end

  end

  def genre_all_items
   lang = get_user_lang(params)
   begin
      items_response = Rails.cache.fetch("all_items_#{params[:catalog_name]}_#{params[:genre]}_#{lang}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_items_genre(params[:catalog_name],params[:genre],lang)
       }
      unless items_response.has_key?("error")
        @all_genere_items = items_response["data"]["items"]
        @theme = items_response["data"]["theme"]
        @meta_title = "More in #{params[:genre]} of #{params[:catalog_name]}"
        @layout_scheme =  items_response["data"]["catalog_object"]["layout_scheme"]
      else
        response = Rails.cache.fetch("all_items_list_#{params[:genre]}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
        Ott.get_items_list_with_pagination(params[:genre],0,24,lang)
        }
        @all_items = response["data"]["catalog_list_items"]
        @layout_type =  response["data"]["layout_type"]
        @next_page = "false"
        if @layout_type == "t_2_3_movie" || @layout_type == "movies"
          @all_items =  @all_items.first(24)
          if response["data"]["total_items_count"] > 24
            @next_page = "true"
          end
        else
          @all_items =  @all_items.first(20)
          if response["data"]["total_items_count"] > 20
            @next_page = "true"
          end
        end
        @layout_scheme = response["data"]["layout_scheme"]
        @title = response["data"]["ml_title"]
        @meta_title = @title+" of #{@layout_scheme.gsub("_"," ")}"
        #@next_page = true
        render "all_items_list"
      end 
   rescue Exception => e
    logger.info e.message
    @all_genere_items = []
     Rails.cache.delete("all_items_#{params[:catalog_name]}_#{params[:genre]}_#{lang}_#{$region}")
   end
  end

  def other_tvshows
   begin 
    items_response = Rails.cache.fetch("other_tvshows_#{params[:catalog_name]}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_catalog_details_with_pagination(params[:catalog_name])
       }
    @title = "Other  "+items_response["data"]["name"]
    @all_tvshows = items_response["data"]["items"]
    @layout_scheme =  items_response["data"]["catalog_object"]["layout_scheme"]
    rescue
      Rails.cache.delete("other_tvshows_#{params[:catalog_name]}")
      @all_tvshows = []
    end
  end
  
  def all_episodes
    next_page = "true"
    if params[:catalog_name].present?
      catalog_name = params[:catalog_name]
    else
       current_url = request.fullpath.split("/")[1]
          item_params = {
            :data => {
              :item_id => params[:show_name],
              :theme => "show"
            }
          }
          url_resp = User.get_item_catalog_id(item_params)
          catalog_name = url_resp["data"]["friendly_id"] 
    end
   
    params[:catalog_name]
    if params[:page_no].present?
      page_number = params[:page_no].to_i
    else
     page_number = 0
    end
    lang = get_user_lang(params)
    response = Ott.get_all_epsiodes_with_pagination(catalog_name,params[:show_name],page_number,lang)
    no_of_pages = response["data"]["total_items_count"]/10.round
    items =  response["data"]["items"].collect{|x|(x["ml_title"].present? ? x["ml_title"] : x["title"])+"$"+x["thumbnails"]["small_16_9"]["url"]+"$"+get_duration_time(x['duration_string']) +"$"+get_lang_url(x['seo_web_url'])+"$"+x['episode_number']+"$"+get_premium_tag(x)+"$"+(x['ml_genres'][0].present? ? x['ml_genres'][0].capitalize : x['genres'][0].capitalize)+"$"+(x['ml_language'].present? ? x['ml_language'][0].capitalize : x['language'].capitalize)+"$"+(x["ml_release_date"].present? ? x["ml_release_date"].split("-")[0] : x['release_date_string'].split("-")[0])+"$"+x['theme']+"$"+x['access_control']['is_free'].to_s}
    if page_number+1 > no_of_pages
     next_page = "false"
    end
    render :json => {:status => true,:episodes => items,:next_page => next_page,:pageno => page_number+1}
  end

 def add_items_list
    next_page = "true"
      lang = get_user_lang(params)
      page_size = 5
    if params[:page_no].present?
      page_number = params[:page_no].to_i
    end
    cat_name = get_head_link(params[:catalog_name],"category")
    response = Rails.cache.fetch("all_items_list_#{cat_name}_#{page_number}_#{lang}", expires_in: CACHE_EXPIRY_TIME){
    Ott.get_items_list_with_pagination(cat_name,page_number,page_size,lang)
    }
    #no_of_pages = ((response["data"]["total_items_count"].to_i - 10)/page_size).round
    no_of_pages = ((30 - 10)/page_size).round
    @item_list = response["data"]["catalog_list_items"]

    items = []
    response["data"]["catalog_list_items"].each_with_index do |list,index|
      if (list.has_key?("catalog_list_items") && list['catalog_list_items'].count > 0)
        item = []
        item <<  list['catalog_list_items'].collect{|x|get_lang_url(x['seo_web_url'])+"$$"+x['ml_title']+"$$"+(x['ml_genres'].present? ? x['ml_genres'][0].capitalize : x['genres'][0].capitalize )+"$$"+(x['ml_language'].present? ? x['ml_language'][0].capitalize : x['language'].capitalize)+"$$"+$region+"$$"+get_image_url(x,@item_list[index]["layout_type"]).to_s+"$$"+get_premium_tag(x)+"$$"+(x["ml_release_date"].present? ? x["ml_release_date"].split("-")[0] : '0')+"$$"+x['theme']+"$$"+x['access_control']['is_free'].to_s} 
        item << (list['ml_display_title'].present? ? list['ml_display_title'] : list['display_title'])+"$$"+get_list_url(list)+"$$"+index.to_s+"$$"+@item_list[index]["layout_type"]+"$$"+list['catalog_list_items'].count.to_s+"$$"+get_item_color(list['layout_scheme']+"$$"+list['catalog_list_items'].count.to_s)+"$$"+get_img_type(@item_list[index]["layout_type"]).to_s 
        items << item 
      end
    end
     if (response["data"]["catalog_list_items"] == 0 || page_number > no_of_pages)
     next_page = "false"
    end
    render :json => {:status => true,:items_list => items,:next_page => next_page,:pageno => page_number+1}
  end

  def season_episodes
    lang = get_user_lang(params)
    response = Rails.cache.fetch("seaason_list_#{$region}_#{params[:season_id]}", expires_in: CACHE_EXPIRY_TIME) {
      Ott.get_seasons_epsiodes(params[:season_id],lang)    
     }
      season_list = []
      unless response.empty?
        season_list = response['data']['items'].collect{|x|(x["ml_title"].present? ? x["ml_title"]:x["title"])+"$"+(x["ml_item_caption"].present? ? x["ml_item_caption"] : x["item_caption"])+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+get_lang_url(x['seo_web_url'])+"$"+get_premium_tag(x)+"$"+(get_premium_tag(x) == "false" ? "display:none" : "")+"$"+x["content_definition"]+"$"+x["access_control"]["premium_tag"].to_s+"$"+x['access_control']['is_free'].to_s+"$"+x['description']}
      end
    render :json => {:status => true,:season_list => season_list}
  end 

  def get_trailers

    lang = get_user_lang(params)
    response = Rails.cache.fetch("trailer_list_#{$region}_#{params[:item_id]}", expires_in: CACHE_EXPIRY_TIME) {
      Ott.get_all_trailers(params[:catalog_id],params[:item_id],lang)    
     }
      trailer_list = []
      unless response.empty?
      trailer_list = response['data']['items'].collect{|x|(x["ml_title"].present? ? x["ml_title"]:x["title"])+"$"+(x["ml_item_caption"].present? ? x["ml_item_caption"] : "")+"$"+(x["thumbnails"]["medium_16_9"]["url"] rescue "")+"$"+(x['play_url']['saranyu']['url'].present? ? get_preview_url(x['play_url']['saranyu']['url']) : "")}
      end
    render :json => {:status => true,:trailer_list => trailer_list}
  end 

  def add_home_list
    next_page = "true"
    page_size =  5
    if params[:page_no].present?
      page_number = params[:page_no].to_i
    else
     page_number = 1
    end
    lang = "en"
    lang = params[:lang] if params[:lang].present? && params[:lang] != "undefined"
    p page_number.inspect
    response = Rails.cache.fetch("home_page_list_#{$region}_#{page_number}_#{page_size}", expires_in: CACHE_EXPIRY_TIME) {
      Ott.get_home_list(page_number,page_size,lang)    
     }
     @home_list = response["data"]["catalog_list_items"]
     #no_of_pages = ((response["data"]["total_items_count"].to_i - 10)/page_size).round
     no_of_pages = ((40 - 10)/page_size).round
    items = []
    response["data"]["catalog_list_items"].each_with_index do |list,index|
    if (list.has_key?("catalog_list_items") && list['catalog_list_items'].count > 0)
      item = []
      #Rails.logger.info("--title--#{list['display_title']}----#{@home_list[index]["layout_type"]}-------")
      item <<  list['catalog_list_items'].collect{|x|get_lang_url(x['seo_web_url'])+"$$"+get_tit(x)+"$$"+(x['ml_genres'].present? ? x['ml_genres'][0].capitalize : '')+"$$"+(x['ml_language'].present? ? x['ml_language'][0].capitalize : '')+"$$"+$region+"$$"+get_image_url(x,@home_list[index]["layout_type"]).to_s+"$$"+get_premium_tag(x)+"$$"+(x["ml_release_date"].present? ? x["ml_release_date"].split("-")[0] : '0')+"$$"+x['theme']+"$$"+x['access_control']['is_free'].to_s+"$$"+x['content_definition'].to_s}
      puts item
      item << (list['ml_display_title'].present? ? list['ml_display_title'] : list['display_title'])+"$$"+list["layout_scheme"]+"$$"+list["home_link"]+"$$"+index.to_s+"$$"+@home_list[index]["layout_type"]+"$$"+list['catalog_list_items'].count.to_s+"$$"+get_item_color(list['layout_scheme']+"$$"+list['catalog_list_items'].count.to_s)+"$$"+get_img_type(@home_list[index]["layout_type"]).to_s
      items << item
    end   
    end
    if (response["data"]["catalog_list_items"] == 0 || page_number > no_of_pages)
     next_page = "false"
    end
    render :json => {:status => true,:home_list => items,:next_page => next_page,:pageno => page_number+1}
  end


  def episodes_list
    if params[:catalog_name].present?
      catalog_name = params[:catalog_name]
    else
       current_url = request.fullpath.split("/")[1]
          item_params = {
            :data => {
              :item_id => params[:show_name],
              :theme => "show"
            }
          }
          url_resp = User.get_item_catalog_id(item_params)
          catalog_name = url_resp["data"]["friendly_id"] 
    end
    lang = get_user_lang(params)
    tvshow_response =  Rails.cache.fetch("all_epsiodes_#{catalog_name}_#{params[:show_name]}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_all_epsiodes(catalog_name,params[:show_name],lang)
        }
      @all_episodes = tvshow_response["data"]["items"]
      @layout_type =  tvshow_response["data"]["catalog_object"]["layout_type"]

  end


  def all_channels
   begin 
    channels_response = Rails.cache.fetch("all_channels_#{$region}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_all_channels
       }
     @all_channels = channels_response["data"]["catalog_list_items"]
    rescue
      Rails.cache.delete("all_channels_#{$region}")
      @all_channels = []
    end
  end

  def all_movies
    begin 
      page_number = 0
      page_size = 24
      @next_page = "true"
     if params[:page_no].present?
        page_number = params[:page_no].to_i
    end
       movies_resp = Rails.cache.fetch("all_movies", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_all_items_with_pagination("movies",page_number,page_size)
       }
     @all_movies = movies_resp["data"]["items"]
     if movies_resp["data"].has_key?("total_items_count")
       cnt = movies_resp["data"]["total_items_count"]
    else
      cnt =  24
    end
     no_of_pages = (cnt/page_size).round
     if page_number >= no_of_pages
        @next_page = "false"
      end
     if @all_movies.empty?
      redirect_to "#{SITE}"
     end
     if params[:page_no].present?
      items =  movies_resp["data"]["items"].collect{|x|x["title"]+"$"+x["thumbnails"]["medium_2_3"]["url"]+"$"+x["seo_web_url"]+"$"+get_premium_tag(x)}
      render :json => {:status => true,:items => items,:next_page => @next_page,:pageno => page_number+1}
     end
     rescue
       Rails.cache.delete("all_movies")
       @all_movies = []
     end
  end

  def all_tvshows
    begin 
     page_number = 0
      page_size = 20
      @next_page = "true"
     if params[:page_no].present?
        page_number = params[:page_no].to_i
    end
    shows_resp = Rails.cache.fetch("all_shows_page_#{page_number}_#{page_size}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_all_items_with_pagination("shows",page_number,page_size)
       }
     @all_shows = shows_resp["data"]["items"]
     if @all_shows.empty?
      redirect_to "#{SITE}"
     end
     if shows_resp["data"].has_key?("total_items_count")
       cnt = shows_resp["data"]["total_items_count"]
    else
      cnt =  20
    end
    p cnt.inspect
     no_of_pages = (cnt/page_size).round
     p no_of_pages.inspect
     if page_number >= no_of_pages
        @next_page = "false"
     end
     if params[:page_no].present?
       items =  shows_resp["data"]["items"].collect{|x|x["title"]+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+x["seo_web_url"]+"$"+get_premium_tag(x)}
       render :json => {:status => true,:items => items,:next_page => @next_page,:pageno => page_number+1}
     end
    rescue
      Rails.cache.delete("all_shows")
      @all_shows = []
    end
  end

  def all_music
   begin 
    page_number = 0
      page_size = 20
      @next_page = "true"
     if params[:page_no].present?
        page_number = params[:page_no].to_i
    end
     music_resp = Rails.cache.fetch("all_music_page_#{page_number}_#{page_size}", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_all_items_with_pagination("music",page_number,page_size)
       }
     @all_music = music_resp["data"]["items"]
     if @all_music.empty?
      redirect_to "#{SITE}"
     end
     if music_resp["data"].has_key?("total_items_count")
       cnt = music_resp["data"]["total_items_count"]
    else
      cnt =  20
    end
     no_of_pages = (cnt/page_size).round
     p no_of_pages.inspect
     if page_number >= no_of_pages
        @next_page = "false"
     end
     if params[:page_no].present?
       items =  music_resp["data"]["items"].collect{|x|x["title"]+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+x["seo_web_url"]+"$"+get_premium_tag(x)}
       render :json => {:status => true,:items => items,:next_page => @next_page,:pageno => page_number+1}
     end
    rescue
      Rails.cache.delete("all_music_page_#{page_number}_#{page_size}")
      @all_music = []
    end
  end

  def all_short_films
   begin 
    short_film_resp = Rails.cache.fetch("all_short_films", expires_in: CACHE_EXPIRY_TIME){
         Ott.get_all_items("short-films")
       }
     @all_short_films = short_film_resp["data"]["items"]
     if @all_short_films.empty?
      redirect_to "#{SITE}"
     end
    rescue
      Rails.cache.delete("all_short_films")
      @all_short_films = []
    end
  end

  def category_landing
    if $region != "IN"
      if !cookies[:user_id].present?
       begin
         category_resp = Rails.cache.fetch("category_landing_#{params[:category_name]}_#{$region}", expires_in: CACHE_EXPIRY_TIME){
             Ott.get_category_landing(params[:category_name])
           }
          if category_resp.has_key?("data")
            @category_data = category_resp["data"]
          else
            redirect_to "#{SITE}"
          end
       rescue
          Rails.cache.delete("category_landing_#{params[:category_name]}_#{$region}")
       end
      else
        redirect_to "#{SITE}"
      end
    else
      redirect_to "#{SITE}"
    end
  end

  def partner
   begin
    lang = get_user_lang(params)
    req_param = request.fullpath == "/partners-db"?  "dainik-list" : "ixigo-list"
    response = Rails.cache.fetch("catalog_item_list_partners_#{req_param}", expires_in: CACHE_EXPIRY_TIME){
     Ott.get_items_list(req_param, lang)
    }
    @items_list = response["data"]["catalog_list_items"]
    @catalog_items = @items_list.drop(1)
    @meta_title = response["data"]["display_title"]
    @meta_keywords = ""
    @layout_scheme = response["data"]["layout_scheme"]
   rescue Exception => e
    redirect_to "#{SITE}"
    logger.info e.message
     Rails.cache.delete("catalog_item_list_ixigo-list")
     @items_list = []
    end
  end


  def promotion_landing
     catalog_id = "5e3d259bed8f7d3296002998"
    if $region == "IN"
     redirect_to "#{SITE}"
   else
    if !cookies[:user_id].present?
      if Rails.env == "development"
       #catalog_id = "5dfb27b02995d338900000c9"
       catalog_id = "5e3d259bed8f7d3296002998"
      end
    response =  Ott.get_promotion_landing(params[:category_name],catalog_id)
    @promotion_dt = response["data"]
    render :layout => false
  else
    redirect_to "#{SITE}"
  end
  end
  end

  def promotion_landing_template2
     catalog_id = "5e09da87741cbb4461000003"
    if $region == "IN"
      redirect_to "#{SITE}"
    else
      if !cookies[:user_id].present?
    if Rails.env == "development"
       catalog_id = "5dfb27b02995d338900000c9"
    end
        response =  Ott.get_promotion_landing(params[:category_name], catalog_id)
        @promotion_dt = response["data"]
        render :layout => false
      else
       redirect_to "#{SITE}"
      end
    end
  end

private

  def encrypt_play_url(url,sub_url)
    new_url = ""
    aes = OpenSSL::Cipher::Cipher.new('AES-128-CBC')
    aes.encrypt
    key = aes.random_key
    aes.key = key
    e_key = Base64.encode64(key).gsub(/\n/, '')
    encrypted = aes.update(url) + aes.final
    new_url = Base64.encode64(encrypted).gsub(/\n/, '')
    return new_url,e_key,sub_url
  end

  def get_play_url_key(response)
    url = sign_smarturl response['play_url']['saranyu']['url']
    if response["theme"] == "live" || response["theme"] == "linear"
       play_url = url["adaptive_urls"][0]['playback_url']
    else
      play_url = url["adaptive_urls"].collect{|x|x["playback_url"] if x["label"] == "laptop_free_#{$region.downcase}_logo"}.compact.first
      play_url = url["adaptive_urls"].collect{|x|x["playback_url"] if x["label"] == "laptop_free_in"}.compact.first if play_url.nil?
    end
     sub_tit_url = ""
    if url.has_key?("subtitles")
     sub_tit_url = url["subtitles"]["subtitles"].collect{|x|x["url"] if x["tag"] == "without logo" && x["language"] == "english"}.compact.first
    end
    p play_url
    if play_url.nil?
      new_play_url = encrypt_play_url(url["adaptive_urls"][0]['playback_url'],sub_tit_url)
    else
      new_play_url = encrypt_play_url(play_url,sub_tit_url)
    end
    p sub_tit_url.inspect
    p "@@@@@@@@@@@@@@@@@2"
    return new_play_url
  end

  def get_theme_type(theme)
    type = ""
   case theme
      when "videos"
       type = "video"
      when "movies"
       type = "movie"
      when "shows"
       type = "show"
     else
      type = theme
   end
   return type
  end


 

end
