class StaticsController < ApplicationController
 
 def terms_conditions

 end


 def privacy_policy
 
 end

 def cookie_policy
 
 end

 def faq
  #render :layout => false
 end

 def faq_in

 end

 def faq_us

 end

 def terms_conditions_in

 end

 def terms_conditions_us

 end

#Main Page for rendering FAQ based on the region
def referal_faq   
   if $region == "IN"
    redirect_to '/faq_in'
  elsif $region == "US"
     redirect_to '/faq_us'
  end

end

#Main Page for rendering TAC based on the region
def referal_terms_conditions
  
end

 def privacypolicy
  render :layout => false
 end

 def coming_soon
  render :layout => false
 end

 def apple_site_notification
   render :layout => false
 end

 def pki_txt
  #render :layout => false
 end

 def contest_gamification
 end


 def contact_us
  @user_mobilno = ""
  @user_email = ""
  p cookies[:user_id].inspect
  if cookies[:user_id].present?
    resp = User.get_user_account_details(cookies[:user_id])
    if resp.has_key?("data")
      # if $region == "IN" 
      if resp["data"]["login_type"] == "msisdn"
      	@user_mobilno = resp["data"]["mobile_number"][2..11]
        if resp["data"]["login_type"] == "external"
          @user_mobilno = ""
        end
      else
      	@user_email = resp["data"]["email_id"]
      end
    end
  end
 if request.post?
   config_resp = Rails.cache.fetch("design_configuration_list", expires_in: CACHE_EXPIRY_TIME) {
        Ott.get_configuration      
       }
    catalog_id =  config_resp["data"]["params_hash2"]["config_params"]["feedback"]["catalog_id"]
   contact_us_params = {
    :content => {
      :title => "feedback",
      :status => "published",
      :phone => params[:mobile_no].present? ?  params[:mobile_no] : "",
      :type_of_query => params[:query],
      :email_id => params[:email_id],
      :subject => params[:desc]
    }
   }
   resp = User.send_feedback(contact_us_params,catalog_id)

   render :json => {:status => "true"}
 end

 end

 def content_not_available 

 	 render :layout => false

 end	

 def thanks
   render :layout => false
 end

 def landing_page
  if $region == "IN" || $region == "US"
    redirect_to "#{SITE}",:status => 301 
  else
  render :layout => false
  end
 end


 def engage_page
  render :layout => false
 end

 def health_check
  render :json => {:status => "true"}
 end
 
 def term_conditions
  render :layout => false
end

def terms_conditions

end

 def termsconditions
  render :layout => false
 end

 def etisalat_terms
  render :layout => false
 end

 def etisalat_faq
  render :layout => false
 end

 def etisalat_privacy_policy
    render :layout => false

 end

  def health_check
    render :json => {:status => "true"}
  end

  def offers
   #if $region == "IN"
     redirect_to "#{SITE}"
   #end
  end


   def gifts
   if $region == "IN"
     redirect_to "#{SITE}"
   end
  end

  def digital_marketing
   if $region == "IN"
     redirect_to "#{SITE}"
   end
  end

  def hotstar
    if $region == "IN"
      redirect_to "#{SITE}"
    else
      if !cookies[:user_id].present?
        all_plans = Ott.subscription_plans
        @all_access_packs = all_plans["data"]["catalog_list_items"].last["catalog_list_items"]
        @monthly_plan_det = @all_access_packs.collect{|x|x["plans"][0]}.first
      else
        redirect_to "#{SITE}"
      end
    end
  end

  def hotstar1
    if $region == "IN"
      redirect_to "#{SITE}"
    else
      if !cookies[:user_id].present?
        all_plans = Ott.subscription_plans
        @all_access_packs = all_plans["data"]["catalog_list_items"].last["catalog_list_items"]
        @monthly_plan_det = @all_access_packs.collect{|x|x["plans"][0]}.first
      else
        redirect_to "#{SITE}"
      end
    end
  end

  def gujarathi_landing
    if $region == "IN"
      redirect_to "#{SITE}"
    else
      if !cookies[:user_id].present?
        all_plans = Ott.subscription_plans
        all_packs = all_plans["data"]["catalog_list_items"]
        @gujarathi_packs = all_packs.collect{|x|x if x["category"] == "gujarati"}.compact.first
        render :layout => false
      else
        redirect_to "#{SITE}"
      end
    end
  end

  def bollywood_landing
     if $region == "US"
      redirect_to "#{SITE}"
    else
       if !cookies[:user_id].present?
       all_plans = Ott.subscription_plans
       all_packs = all_plans["data"]["catalog_list_items"]
       @gujarathi_packs = all_packs.collect{|x|x if x["category"] == "bollywood_plus"}.compact.first
       render :layout => false
       else
        redirect_to "#{SITE}"
      end
  end
  end

  def guj_landing
     if $region == "IN"
      redirect_to "#{SITE}"
    else
      if !cookies[:user_id].present?
        all_plans = Ott.subscription_plans
        all_packs = all_plans["data"]["catalog_list_items"]
        @gujarathi_packs = all_packs.collect{|x|x if x["category"] == "gujarati"}.compact.first
        render :layout => false
      else
        redirect_to "#{SITE}"
      end
    end
  end

  def bollywood_category
     if $region == "IN"
      redirect_to "#{SITE}"
    else
      if !cookies[:user_id].present?
        all_plans = Ott.subscription_plans
        all_packs = all_plans["data"]["catalog_list_items"][6]["catalog_list_items"]
        @all_access_pack = all_packs.collect{|x|x if x["category"] == "all_access_pack"}.compact.first
        render :layout => false
      else
        redirect_to "#{SITE}"
      end
    end
  end




  def us_swagatam
    if $region == "US"
     render :layout => false
    else
      redirect_to "#{SITE}"
    end
  end


  def bhavans
    if $region == "IN"
      redirect_to "#{SITE}"
    else
      if !cookies[:user_id].present?
        all_plans = Ott.subscription_plans
        all_packs = all_plans["data"]["catalog_list_items"]
        @gujarathi_packs = all_plans["data"]["catalog_list_items"].last["catalog_list_items"].first
        render :layout => false
      else
        redirect_to "#{SITE}"
      end
    end
  end


  def gujm
     if $region == "IN"
      redirect_to "#{SITE}"
    else
      if !cookies[:user_id].present?
        all_plans = Ott.subscription_plans
        all_packs = all_plans["data"]["catalog_list_items"]
        @gujarathi_packs = all_packs.collect{|x|x if x["category"] == "gujarati"}.compact.first
        render :layout => false
      else
        redirect_to "#{SITE}"
      end
    end
  end

 def worldmeday
  if $region == "IN"
    #redirect_to "#{SITE}"
  end
 end

 def rehlatoffer
  #rehlatoffer will be allowed for all countries

  # allowed_regions = ["SA","AE","BH","QA","EG","US","IN"]
  # unless allowed_regions.include?($region)
  #   redirect_to "#{SITE}"
  # end
 end


 def consulate_general
  if $region == "IN"
    redirect_to "#{SITE}"
  elsif $region == "US"
    redirect_to "#{SITE}/homeis"
  else
   render :layout => false
 end
 end

  def rajchandraji
    if $region == "US"
      render :layout => false
    else
      redirect_to "#{SITE}"   
   end
 end

 def all_access_offer
  if $region == "US"
     all_plans = Ott.subscription_plans
       @all_access_packs = all_plans["data"]["catalog_list_items"].last["catalog_list_items"]
        @monthly_plan_det = @all_access_packs.collect{|x|x["plans"][1]}.last
       p @monthly_plan_det.inspect
        #render :layout => false 
    else
      redirect_to "#{SITE}"
    
    end
 end

 def blackfriday
   if $region == "US"
    if Rails.env.production?
      @coupon_code = "DIWALI2020"
      @coupon_id = "5facdcefa609d2104b006a68"
    else
      @coupon_code = "BLACKFRI50"
      @coupon_id = "5fab71ef741cbb312e000000"
    end
    @promo_amt = "50.0"
    @us_promo_amt = "4999"
    @prm_disc_amt = "49.99"
     all_plans = Ott.subscription_plans
       @all_access_packs = all_plans["data"]["catalog_list_items"].last["catalog_list_items"]
        @monthly_plan_det = @all_access_packs.collect{|x|x["plans"][0]}.last
       p @monthly_plan_det.inspect
        #render :layout => false 
    else
      redirect_to "#{SITE}"
    
    end

 end

 def bollyclassicoffer
  if $region == "US"
    all_plans = Ott.subscription_plans
    all_packs = all_plans["data"]["catalog_list_items"]
    @monthly_plan_det = all_packs.collect{|x|x if x["category"] == "bollywood_classic"}.compact.first
    #render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end


 def bollypremiereoffer
  if $region == "US"
    all_plans = Ott.subscription_plans
    all_packs = all_plans["data"]["catalog_list_items"]
    @monthly_plan_det = all_packs.collect{|x|x if x["category"] == "bollywood_plus"}.compact.first
    p @monthly_plan_det.inspect
    #render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end


 def gujaratioffer
  if $region == "US"
    all_plans = Ott.subscription_plans
    all_packs = all_plans["data"]["catalog_list_items"]
    @monthly_plan_det = all_packs.collect{|x|x if x["category"] == "gujarati"}.compact.first
    p @monthly_plan_det.inspect
    #render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end

  def lohana_samaj
  if $region == "US"
    @coupon_code = "LOHANSAMAJ"
    if Rails.env.production?
      @coupon_id = "5ec7612da609d206ce000e9e"
    else
      @coupon_id = "5ec614e6741cbb1d06000008"
    end
    @promo_amt = "25.0"
    @us_promo_amt = "2499"
    @prm_disc_amt = "24.99"
    all_plans = Ott.subscription_plans
    all_packs = all_plans["data"]["catalog_list_items"]
    @monthly_plan_det = all_packs.collect{|x|x if x["category"] == "gujarati"}.compact.first
    #render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end


  def phillymm
  if $region == "US"
    render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end

  def gmoco
  if $region == "US"
    render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end


  def gujaratisamaj
  if $region == "US"
    render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end

 def navratri
  if $region == "US"
    render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end

 
  def gshouston
  if $region == "US"
    render :layout => false 
  else
    redirect_to "#{SITE}"
  end
 end

 def eoip
  if $region == "FR"
    render :layout => false
  else
    redirect_to "#{SITE}"
 end
 end

 def eois
  if $region == "ES"
    render :layout => false
  else
    redirect_to "#{SITE}"
 end
 end

  def homeis
  if $region == "US"
    render :layout => false
  else
    redirect_to "#{SITE}"
 end
 end

 def cgic
  if $region == "CA"
    render :layout => false
  else
    redirect_to "#{SITE}"
 end
 end

 def hcic
    if $region == "CA"
    render :layout => false
  else
    redirect_to "#{SITE}"
 end
 end

 def accentiv
  if $region == "IN"
    render :layout => false
  else
     redirect_to "#{SITE}"
  end
 end

 def  ref_earn_money
 end

 def icic
  #if $region == "US"
  render :layout => false
# else
#   redirect_to "#{SITE}"
# end
 end

 def about_us
  redirect_to "https://www.shemarooent.com/about-us/"
 end

 def sulekha
  render :layout => false
 end

 def hungamacity
  if $region == "US"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

 def store
  render :layout => false
 end

 def store1

 end

 def gcs_alabama
   if $region == "US"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

 def montgomery
   if $region == "US"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

 def vyo_landing
  if $region == "US"
    render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

 def gs_alabama
   if $region == "US"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

 def gona
   if $region == "US"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

 def gsdetroit
   if $region == "US"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end


 def gcs_florida
   if $region == "US"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

 def vtofny
   if $region == "US"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

def  fog_canada
   if $region == "CA"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end

 def microsite_classic
  if $region == "IN"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
 end 
 
 def mobikwik_terms
 end 

 def grievance
 end 

def shemaroo_classic
  if $region != "IN"
   
    redirect_to "#{SITE}"
  end
end

def evergreen_classic
  if $region == "IN"
   render :layout => false
  else
    redirect_to "#{SITE}"
  end
end 

def nyl
  if $region == "US"
    render :layout => false
  else
    redirect_to "#{SITE}"
  end
end



end
