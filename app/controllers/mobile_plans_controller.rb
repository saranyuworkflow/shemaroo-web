class MobilePlansController < ApplicationController
  include PlansHelper

  def mobile_all_plans
    #cookies[:user_id] = params["app_session_id"] if params["app_session_id"].present?
    user_session = cookies[:user_id].to_s
    user_plans = Ott.user_plans(user_session)
     if false #user_plans["data"].count >0
       redirect_to plans_view_plans_path and return
     else
    response = Ott.subscription_plans
    @all_plans = response["data"]["catalog_list_items"]
    @cat_titles = []
    @all_plans.each do |plan|
      if plan["plans"].present?
     plan["plans"].each do |pl|
        title = "#{plan['category']}-#{pl['title']}"
        @cat_titles <<  title.downcase
     end
     end
    end
    end
    render :layout => false
  end

  def mobile_plans_summary
 
    user_session = cookies[:user_id].to_s
    user_plans = Ott.user_plans(user_session)
    redirect_to plans_view_plans_path if user_plans["data"].count >0

    response = Ott.subscription_plans
    @all_plans = response["data"]["catalog_list_items"]
    @all_access_packs = @all_plans.last["catalog_list_items"].last
    if params["plans"].present? && params["plans"].split(",").count == 2 && params["plans"].split(",").map{|p| p.split("|")[2].downcase}.uniq.count == 1
      plan_title = params["plans"].split(",").map{|a| a.split("|")[2] }.last
      items  =   Ott.get_catalog_details("5b3c917fc1df417b9a00002c")
      @combo_plan = items["data"]["items"].map{|cp| cp if cp["category_type"]=="combo"}.compact[0]
      @combo_pack = @combo_plan["plans"].map{|e| e if e["title"].downcase == plan_title.downcase}.compact.last
      render "combo_plans_summary"
    end
  end

  def mobile_payment_url
    if @region == "US"
      payment_gateway = "adyen"
    else
      payment_gateway = "ccavenue"
    end
    platform = "android"  #TODO
   
    if params["combo_pack_id"].blank? #plans.count != 2 &&  !plans.empty?
      plans = params["plans"].split(",")
      packs = []
      plan =  plans[0]
      content_id  = plan.split("|")[5]
      pack_id  = plan.split("|").first
      pd  =   HTTP.get "catalogs/5b3c917fc1df417b9a00002c/items/#{content_id}?auth_token=Ts4XpMvGsB2SW7NZsWc3&region=#{@region}" ,"catalog"
      sp = pd["data"]["plans"].map{|e| e if e["id"] == pack_id}.compact.last

      all_price = sp["price"]
      currency = sp["currency"]
      price_charged = sp["discounted_price"]
      all_price_charged = params["disc_amount"] == "null" ? price_charged : params["disc_amount"]
      sub_pack = {}
      sub_pack["plan_categories"] = [pd["data"]["category"]]
      sub_pack["category_type"] = pd["data"]["category_type"]
      sub_pack["category_pack_id"] = content_id
      sub_pack["subscription_catalog_id"] = pd["data"]["catalog_id"]
      sub_pack["plan_id"] = sp["id"]
      packs << sub_pack
    else
      pack_id =params["combo_pack_id"]
      items  =   Ott.get_catalog_details("5b3c917fc1df417b9a00002c")
      @combo_plan = items["data"]["items"].map{|cp| cp if cp["category_type"]=="combo"}.compact[0]
      @combo_pack = @combo_plan["plans"].map{|e| e if e["id"] == pack_id}.compact.last
      all_price = @combo_pack["price"]
      all_price_charged = params["disc_amount"] == "null" ? @combo_pack["discounted_price"] : params["disc_amount"]
      currency = @combo_pack["currency"]
      packs = []
      sub_pack = {}
      sub_pack["plan_categories"] = params["plans"].split(",").map{|a| a.split("|")[6] }
      sub_pack["category_type"] = @combo_plan["category_type"]
      sub_pack["category_pack_id"] = @combo_plan["category_id"]
      sub_pack["subscription_catalog_id"] = @combo_plan["catalog_id"]
      sub_pack["plan_id"] = @combo_pack["id"]
      packs << sub_pack
    end
 

    if  params["coupon_code"] == "null"
      payment_info = {"net_amount": all_price, "price_charged": all_price_charged,"currency": currency, "packs": packs }
    else
      coupon_code = params["coupon_code"]
      coupon_code_id = params["coupon_code_id"]
      payment_info = {"net_amount": all_price, "price_charged": all_price_charged,"currency": currency, "packs": packs, "coupon_code": coupon_code,"coupon_code_id": coupon_code_id  }
    end
 
    transaction_info = 	{"app_txn_id":"", "txn_message":"One Day Pack_10.00", "txn_status":"init", "order_id":"", "pg_transaction_id":""	}
    user_info = {"email": cookies[:user_login_id], "mobile_number": cookies[:user_login_id]}

    browser = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us")
    device_os = "NA" #,browser.ios? ? 'iOS' : (browser.android? ? 'android' : browser.platform.to_s)
    browser_name = browser.name.split(" ").join("_")
    browser_version=  "NA" #browser.full_version
    device_type = "NA" #browser.mobile? ? 'mobile' : browser.tablet? ? 'tablet' : 'desktop'
    subscription_status =  cookies["is_subscribed"] == "yes" ? "subscribed" : "unsubscribed"
    platform  = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
    device_brand = 'NA'
    device_model = 'NA'
    device_imei = 'NA'
    operator = cookies['operator'] != "undefined"  ? cookies['operator'] : 'NA'
    internet_network = 'NA'
    isp_name = 'NA'

    miscellaneous ={"browser": browser_name, "device_brand": device_brand,"device_IMEI": device_imei, "device_model": device_model, "device_OS": device_os ,"device_type": device_type ,"inet": internet_network,"isp": isp_name,"operator": operator}

    us = get_signature_key(payment_info[:packs])
    purchase_params = {
    "auth_token":"Ts4XpMvGsB2SW7NZsWc3", "us": us, "region": @region, "payment_gateway": payment_gateway,"platform": platform,
    "payment_info": payment_info,
    "transaction_info": transaction_info,
    "user_info": user_info,
    "miscellaneous": miscellaneous
    }
    response =  HTTP.post_https "users/#{cookies[:user_id]}/transactions", purchase_params
    if payment_gateway == "adyen"
      render json: {:message => "adyen payment iniated",:init_data => response["data"] } , status: :ok
    else
      payment_url = "#{response['data']['payment_url']}&encRequest=#{response['data']['msg']}&access_code=#{response['data']['access_code']}"
      render json: {:message => "ccavenue payment iniated",:payment_url => payment_url} , status: :ok
    end
  end

  def mobile_purchase_plans

    user_session = cookies[:user_id].to_s
    user_plans = Ott.user_plans(user_session)
    redirect_to plans_view_plans_path if user_plans["data"].count >0

    payment_gateway = "admin" #TODO
    platform = "android"  #TODO

    if params["combo_plan_id"].blank?
      plan_id = params["plans"].split(",").map{|a| a.split("|")[-2]}[0]
      pack_id = params["plans"].split(",").map{|a| a.split("|")[0]}[0]
    else
      plan_id = params["combo_plan_id"]
      pack_id = params["combo_pack_id"]
    end
   
    pd  =   HTTP.get "catalogs/5b3c917fc1df417b9a00002c/items/#{plan_id}?auth_token=Ts4XpMvGsB2SW7NZsWc3&region=#{@region}" ,"catalog"
    sp = pd["data"]["plans"].map{|e| e if e["id"] == pack_id}.compact.last
   

    if @region == "IN"
      price_charged = sp["pg_price"]["cc_avenue"]
    else
      price_charged =  sp["pg_price"]["adyen"]
    end

    if params["combo_plan_id"].blank?
      plan_categories = [ pd["data"]["category"]]
    else
      plan_categories = params["plans"].split(",").map{|a| a.split("|")[6] }
    end


    payment_info = { "price_charged": price_charged,"currency": sp["currency"],
    "packs":[
        {"plan_categories": plan_categories,
    "category_type": pd["data"]["category_type"],
    "subscription_catalog_id": pd["data"]["catalog_id"],
    "category_pack_id": plan_id,
    "plan_id": sp["id"]
    }]}
    adyen_encrypted_data = params["adyen-encrypted-data"]
    user_info = {"email": cookies[:user_login_id], "mobile_number": cookies[:user_login_id]}
   

    plans_purchase_params = {
        "auth_token": "Ts4XpMvGsB2SW7NZsWc3",
        "region" => @region,
        "payment_info": payment_info,
    "transaction_info": {"order_id": params["order_id"], "adyen_encrypted_data": adyen_encrypted_data},
        "user_info": user_info
    }

        response =  HTTP.post_https "users/#{cookies[:user_id]}/transactions/cse_payment", plans_purchase_params
        if !response["data"].blank? && (response["data"]["message"] == "pack activated successfully" || response["data"]["message"] == "Plans modified Successfully")
          redirect_to  action: 'payment_success',  resp_data: response
        else
          redirect_to  action: 'payment_failed',  resp_data: response
        end
  end

 


  def mobile_payment_success
    params.require(:resp_data).permit
    @resp = params[:resp_data]
  end

  def mobile_payment_failed
    params.require(:resp_data).permit
    @resp = params[:resp_data]
  end

  def mobile_payment_canceled
    enc_resp = params["encResp"]
    order_id = params["orderNo"]
    payment_params = {"encResp": enc_resp, "orderNo": order_id, "region":"IN", "auth_token":"Ts4XpMvGsB2SW7NZsWc3", "payment_gateway":"ccavenue"}
    @resp =  HTTP.post_https "payment_complete/ccavenue/secure_payment", payment_params
  end


  def mobile_payment_response
    enc_resp = params["encResp"]
    order_id = params["orderNo"]
    payment_params = {"encResp": enc_resp, "orderNo": order_id, "region":"IN", "auth_token":"Ts4XpMvGsB2SW7NZsWc3", "payment_gateway":"ccavenue"}
    response =  HTTP.post_https "payment_complete/ccavenue/secure_payment", payment_params
    @resp = response["data"]
  end

  def mobile_apply_promocode
    if params["combo_plan_id"].blank?
      plan_id = params["plans"].split(",").map{|a| a.split("|")[-2]}[0]
      pack_id = params["plans"].split(",").map{|a| a.split("|")[0]}[0]
    else
      plan_id = params["combo_plan_id"]
      pack_id = params["combo_pack_id"]
    end
    pd  =   HTTP.get "catalogs/5b3c917fc1df417b9a00002c/items/#{plan_id}?auth_token=Ts4XpMvGsB2SW7NZsWc3&region=#{@region}" ,"catalog"
    sp = pd["data"]["plans"].map{|e| e if e["id"] == pack_id}.compact.last
    if params["combo_plan_id"].blank?
      plan_categories = [ pd["data"]["category"]]
    else
      plan_categories = params["plans"].split(",").map{|a| a.split("|")[6] }
    end

    packs = [
        {"plan_categories": plan_categories,
    "category_type": pd["data"]["category_type"],
    "subscription_catalog_id": pd["data"]["catalog_id"],
    "category_pack_id": plan_id,
    "plan_id": sp["id"]
    }]

    us = get_signature_key(packs)
    coupon_params = {
        "auth_token": "Ts4XpMvGsB2SW7NZsWc3",
        "us": us ,
    "category_pack_id": plan_id ,
    "plan_id": sp["id"],
    "coupon_code": params["promocode"],
    "region": @region
    }
    response = HTTP.post_https "users/#{cookies[:user_id]}/apply_coupon_code", coupon_params
    if @region == "IN"
      price_charged = sp["pg_price"]["cc_avenue"]
    else
      price_charged =  sp["pg_price"]["adyen"].to_f/100
    end
    cpn_id = response["data"]["payment"]["coupon_id"]
    cpn_name = response["data"]["payment"]["coupon_code"]
    cpn_price = price_charged.to_f - response["data"]["payment"]["net_amount"].to_f
    render json: {:cpn_price => cpn_price, :net_amount => response["data"]["payment"]["net_amount"],:cpn_name => cpn_name, :cpn_id => cpn_id } , status: :ok

  end

  def mobile_view_plans
    user_session = cookies[:user_id].to_s
    user_plans = Ott.user_plans(user_session)
    @current_plans =[]
    @expired_plans =[]
    user_plans["data"].each do |plan|
      if Time.now > Time.parse(plan["valid_till"])
        @expired_plans << plan
      else
        @current_plans << plan
      end
    end
  end

  def mobile_plan_details
    user_session = cookies[:user_id].to_s
    user_plans = Ott.user_plans(user_session)
    @plan = user_plans["data"].map{|a| a if a["id"]==params["id"].to_i}.compact.first
  end

   def mobile_unsubscribe_plan
     unsubscribe_params =  {
      "auth_token": "Ts4XpMvGsB2SW7NZsWc3", 
      "data": {"remove_plans":{"plan_id": params["plan_id"],"subscripton_category_id": params["subscription_id"]},"region": @region}
      }
     response =  HTTP.post_https "users/#{cookies[:user_id]}/unsubscribe_pack", unsubscribe_params
  end










   

    def mobile_modify_plans
      response = Ott.subscription_plans
      @all_plans = response["data"]["catalog_list_items"]
      user_session = cookies[:user_id].to_s
      user_plans = Ott.user_plans(user_session)
      @all_plans.map{|ap| ap["category"]}
      @ps_ids = []    
      combos = user_plans["data"].map{|up| [up["plan_categories"],up["currency"],up["price_charged"],up["plan_title"]] if up["category"] == "combo"}.compact
      c_col_cts = []
      combos.each do |combo|
          ps = []
          combo.first.each do |ct|
           c_col_cts <<  ct+ "-" +combo.last.downcase+combo[1]+combo[2]
            ps << @all_plans.map{|ap| ap if ap["category"] == ct}  
          end
        @ps_ids = []                
        ps.flatten.compact.each do |s|
          @ps_ids << s["plans"].map{|t| t["id"] if t["title"].downcase == combo.last.downcase}
        end
      end
      single_plan_ids = user_plans["data"].map{|up| up["plan_id"] unless up["category"]== "combo"}.compact.uniq    
      s_col_cts= user_plans["data"].map{|up| up["plan_categories"][0]+"-"+up["plan_title"].downcase+"$"+up["price_charged"] unless up["category"]== "combo"}.compact.uniq    
      @plan_ids = single_plan_ids + @ps_ids.flatten.compact
      @col_cts = c_col_cts + s_col_cts
      p @col_cts.inspect
  end


    def mobile_modify_plans_summary
    response = Ott.subscription_plans
    @all_plans = response["data"]["catalog_list_items"]
    @all_access_packs = @all_plans.last["catalog_list_items"].last
    
    user_session = cookies[:user_id].to_s
    @user_plans = Ott.user_plans(user_session)
   

    if @user_plans["data"].count > 0
   
       plans = params["plans"].split(",")
       #plans.count

      if plans.count == 2 && @user_plans["data"].map{|s| s["plan_title"].downcase}.flatten.include?(params["plans"].split(",").last.split("|")[2].downcase)
      # IT IS MOTHLY / YEARLY COMBO

        combo_cur_plan = @user_plans["data"].map{|s| s if s["plan_title"].downcase == params["plans"].split(",").last.split("|")[2].downcase && s["category"] == "individual"}.compact
       
        cur_cat_id = combo_cur_plan[0]["subscription_id"]  
        cur_plan_id = combo_cur_plan[0]["plan_id"] 
      
        plan_title = params["plans"].split(",").map{|a| a.split("|")[2] }.last
        items  =   Ott.get_catalog_details("5b3c917fc1df417b9a00002c")
        @combo_plan = items["data"]["items"].map{|cp| cp if cp["category_type"]=="combo"}.compact[0]
        @combo_pack = @combo_plan["plans"].map{|e| e if e["title"].downcase == plan_title.downcase}.compact.last
       
        new_cat_id = @combo_plan["category_id"]
        new_plan_id =  @combo_pack["id"]
       
        modify_params =  {"auth_token": "Ts4XpMvGsB2SW7NZsWc3","data": {"add_plans":  { "plan_id": new_plan_id,"subscription_category_id": new_cat_id},    "modify_plans":{"current_plan_id": cur_plan_id,"subscription_category_id": cur_cat_id},"region": @region}}
        @response =  HTTP.post_https "users/#{cookies[:user_id]}/get_modified_amount", modify_params
        render "modify_combo_plans_summary"
      elsif plans.count >= 4 && @user_plans["data"].map{|s| s if s["category"] == "combo" }.count == 1  && 
      #MODIFY FROM ONE COMBO TO ANOTHER 
        current_combo = @user_plans["data"].map{|s| s if s["category"] == "combo" }

        cur_cat_id = current_combo[0]["subscription_id"]  
        cur_plan_id = current_combo[0]["plan_id"] 
      
        plan_title =current_combo[0]["plan_title"].downcase == "monthly" ? "yearly" : "monthly" #params["plans"].split(",").map{|a| a.split("|")[2] }.last
       
        items  =   Ott.get_catalog_details("5b3c917fc1df417b9a00002c")
        @combo_plan = items["data"]["items"].map{|cp| cp if cp["category_type"]=="combo"}.compact[0]
        @combo_pack = @combo_plan["plans"].map{|e| e if e["title"].downcase == plan_title.downcase}.compact.last
       
        new_cat_id = @combo_plan["category_id"]
        new_plan_id =  @combo_pack["id"]
       
        modify_params =  {"auth_token": "Ts4XpMvGsB2SW7NZsWc3","data": {"add_plans":  { "plan_id": new_plan_id,"subscription_category_id": new_cat_id},    "modify_plans":{"current_plan_id": cur_plan_id,"subscription_category_id": cur_cat_id},"region": @region}}
        @response =  HTTP.post_https "users/#{cookies[:user_id]}/get_modified_amount", modify_params
        render "modify_combo_plans_summary"





      elsif plans.count == 3 && !@user_plans["data"].map{|s| s["plan_title"].downcase}.flatten.include?(params["plans"].split(",").last.split("|")[2].downcase)
                 render "modify_single_plans_summary"
      else
        
        current_plan =    @user_plans["data"].map{|s| s if s["subscription_id"] == params["plans"].split(",").last.split("|")[5] }.compact
       if current_plan.present?
        #same category/plan from monthly to yearly
        cur_cat_id = current_plan[0]["subscription_id"]  
        cur_plan_id = current_plan[0]["plan_id"] 
        new_cat_id = params["plans"].split("|")[5]
        new_plan_id =  params["plans"].split("|")[0]
        modify_params =  {"auth_token": "Ts4XpMvGsB2SW7NZsWc3","data": {"add_plans":  { "plan_id": new_plan_id,"subscription_category_id": new_cat_id},    "modify_plans":{"current_plan_id": cur_plan_id,"subscription_category_id": cur_cat_id},"region": @region}}
        @response =  HTTP.post_https "users/#{cookies[:user_id]}/get_modified_amount", modify_params
        render "modify_plans_summary"
      end
      end
    end
  end


    def mobile_modify_payment_url
        if @region == "US"
          payment_gateway = "adyen"
        else
          payment_gateway = "ccavenue"
        end
        platform = "android"  #TODO
       
        if params["combo_pack_id"].blank?
          plans = params["plans"].split(",")
          packs = []
          plan =  plans[0]
          content_id  = plan.split("|")[5]
          pack_id  = plan.split("|").first
          pd  =   HTTP.get "catalogs/5b3c917fc1df417b9a00002c/items/#{content_id}?auth_token=Ts4XpMvGsB2SW7NZsWc3&region=#{@region}" ,"catalog"
          sp = pd["data"]["plans"].map{|e| e if e["id"] == pack_id}.compact.last

          all_price = sp["price"]
          currency = sp["currency"]
          price_charged = sp["discounted_price"]
         
          if params["modified_amount"].present? 
            all_price_charged = params["modified_amount"]
          else
            all_price_charged = params["disc_amount"] == "null" ? price_charged : params["disc_amount"]
          end
         
          sub_pack = {}
          sub_pack["plan_categories"] = [pd["data"]["category"]]
          sub_pack["category_type"] = pd["data"]["category_type"]
          sub_pack["category_pack_id"] = content_id
          sub_pack["subscription_catalog_id"] = pd["data"]["catalog_id"]
          sub_pack["plan_id"] = sp["id"]
          packs << sub_pack
       
        else
          pack_id =params["combo_pack_id"]
          items  =   Ott.get_catalog_details("5b3c917fc1df417b9a00002c")
          @combo_plan = items["data"]["items"].map{|cp| cp if cp["category_type"]=="combo"}.compact[0]
          @combo_pack = @combo_plan["plans"].map{|e| e if e["id"] == pack_id}.compact.last
         
          all_price = @combo_pack["price"]
          all_price_charged = params["disc_amount"] == "null" ? @combo_pack["discounted_price"] : params["disc_amount"]
          currency = @combo_pack["currency"]
         
          packs = []
          sub_pack = {}
          sub_pack["plan_categories"] = params["plans"].split(",").map{|a| a.split("|")[6] }
          sub_pack["category_type"] = @combo_plan["category_type"]
          sub_pack["category_pack_id"] = @combo_plan["category_id"]
          sub_pack["subscription_catalog_id"] = @combo_plan["catalog_id"]
          sub_pack["plan_id"] = @combo_pack["id"]
          packs << sub_pack
        end


        if  params["coupon_code"] == "null"
          payment_info = {"net_amount": all_price, "price_charged": all_price_charged,"currency": currency, "packs": packs }
        else
          coupon_code = params["coupon_code"]
          coupon_code_id = params["coupon_code_id"]
          payment_info = {"net_amount": all_price, "price_charged": all_price_charged,"currency": currency, "packs": packs, "coupon_code": coupon_code,"coupon_code_id": coupon_code_id  }
        end
       

        transaction_info =  {"app_txn_id":"", "txn_message":"One Day Pack_10.00", "txn_status":"init", "order_id":"", "pg_transaction_id":""  }
      
        user_info = {"email": cookies[:user_login_id], "mobile_number": cookies[:user_login_id]}

        browser = Browser.new(:ua => request.env["HTTP_USER_AGENT"], :accept_language => "en-us")
        device_os = "NA" #,browser.ios? ? 'iOS' : (browser.android? ? 'android' : browser.platform.to_s)
        browser_name = browser.name.split(" ").join("_")
        browser_version=  "NA" #browser.full_version
        device_type = "NA" #browser.mobile? ? 'mobile' : browser.tablet? ? 'tablet' : 'desktop'
        subscription_status =  cookies["is_subscribed"] == "yes" ? "subscribed" : "unsubscribed"
        platform  = cookies["browser_type"] == "mobile" ? "WAP" : "WEB"
        device_brand = 'NA'
        device_model = 'NA'
        device_imei = 'NA'
        operator = cookies['operator'] != "undefined"  ? cookies['operator'] : 'NA'
        internet_network = 'NA'
        isp_name = 'NA'

        miscellaneous ={"browser": browser_name, "device_brand": device_brand,"device_IMEI": device_imei, "device_model": device_model, "device_OS": device_os ,"device_type": device_type ,"inet": internet_network,"isp": isp_name,"operator": operator}

        us = get_signature_key(payment_info[:packs])

     
        user_session = cookies[:user_id].to_s
        @user_plans = Ott.user_plans(user_session)

      if params["modified_amount"].present? 
         current_plan =    @user_plans["data"].map{|s| s if s["subscription_id"] == params["plans"].split(",").last.split("|")[5] }.compact
        #same category/plan from monthly to yearly
        cur_cat_id = current_plan[0]["subscription_id"]  
        cur_plan_id = current_plan[0]["plan_id"] 
        # new_cat_id = params["plans"].split("|")[5]
        # new_plan_id =  params["plans"].split("|")[0]

        modify_user_plans = {"current_plan_id": cur_plan_id,"current_subscription_category_id": cur_cat_id}

        purchase_params = {
            "auth_token":"Ts4XpMvGsB2SW7NZsWc3", "us": us, "region": @region, "payment_gateway": payment_gateway,"platform": platform,
        "payment_info": payment_info,
        "transaction_info": transaction_info,
        "modify_user_plans": modify_user_plans,
        "user_info": user_info,
        "miscellaneous": miscellaneous
        }

      elsif @user_plans["data"].map{|s| s["plan_title"].downcase}.flatten.include?(params["plans"].split(",").last.split("|")[2].downcase)
        combo_cur_plan = @user_plans["data"].map{|s| s if s["plan_title"].downcase == params["plans"].split(",").last.split("|")[2].downcase && s["category"] == "individual"}.compact
        cur_cat_id = combo_cur_plan[0]["subscription_id"]  
        cur_plan_id = combo_cur_plan[0]["plan_id"] 
        modify_user_plans = {"current_plan_id": cur_plan_id,"current_subscription_category_id": cur_cat_id}

        purchase_params = {
            "auth_token":"Ts4XpMvGsB2SW7NZsWc3", "us": us, "region": @region, "payment_gateway": payment_gateway,"platform": platform,
        "payment_info": payment_info,
        "transaction_info": transaction_info,
        "modify_user_plans": modify_user_plans,
        "user_info": user_info,
        "miscellaneous": miscellaneous
        }
      else
      purchase_params = {
        "auth_token":"Ts4XpMvGsB2SW7NZsWc3", "us": us, "region": @region, "payment_gateway": payment_gateway,"platform": platform,
        "payment_info": payment_info,
        "transaction_info": transaction_info,
        "user_info": user_info,
        "miscellaneous": miscellaneous
        }
      end



        response =  HTTP.post_https "users/#{cookies[:user_id]}/transactions", purchase_params
        if payment_gateway == "adyen"
          render json: {:message => "adyen payment iniated",:init_data => response["data"] } , status: :ok
        else
          payment_url = "#{response['data']['payment_url']}&encRequest=#{response['data']['msg']}&access_code=#{response['data']['access_code']}"
          render json: {:message => "ccavenue payment iniated",:payment_url => payment_url} , status: :ok
        end
  end



   def mobile_modify_purchase_plans
          payment_gateway = "admin"
          platform = "android"

          if params["combo_plan_id"].blank?
            plan_id = params["plans"].split(",").map{|a| a.split("|")[-2]}[0]
            pack_id = params["plans"].split(",").map{|a| a.split("|")[0]}[0]
          else
            plan_id = params["combo_plan_id"]
            pack_id = params["combo_pack_id"]
          end
          pd  =   HTTP.get "catalogs/5b3c917fc1df417b9a00002c/items/#{plan_id}?auth_token=Ts4XpMvGsB2SW7NZsWc3&region=#{@region}" ,"catalog"
          sp = pd["data"]["plans"].map{|e| e if e["id"] == pack_id}.compact.last
          if @region == "IN"
            price_charged = sp["pg_price"]["cc_avenue"]
          else
            price_charged =  sp["pg_price"]["adyen"]
          end

          if params["combo_plan_id"].blank?
            plan_categories = [ pd["data"]["category"]]
          else
            plan_categories = params["plans"].split(",").map{|a| a.split("|")[6] }
          end


          payment_info = { "price_charged": price_charged,"currency": sp["currency"],
          "packs":[
              {"plan_categories": plan_categories,
          "category_type": pd["data"]["category_type"],
          "subscription_catalog_id": pd["data"]["catalog_id"],
          "category_pack_id": plan_id,
          "plan_id": sp["id"]
          }]}
        
          adyen_encrypted_data = params["adyen-encrypted-data"]
        
          user_info = {"email": cookies[:user_login_id], "mobile_number": cookies[:user_login_id]}
        

          user_session = cookies[:user_id].to_s
          @user_plans = Ott.user_plans(user_session)

          if params["modified_amount"].present? 
            current_plan =    @user_plans["data"].map{|s| s if s["subscription_id"] == params["plans"].split(",").last.split("|")[5] }.compact
            #same category/plan from monthly to yearly
            cur_cat_id = current_plan[0]["subscription_id"]  
            cur_plan_id = current_plan[0]["plan_id"] 
            modify_user_plans = {"current_plan_id": cur_plan_id,"current_subscription_category_id": cur_cat_id}

            plans_purchase_params = {
                      "auth_token": "Ts4XpMvGsB2SW7NZsWc3",
                      "region" => @region,
                      "payment_info": payment_info,
                      "modify_user_plans": modify_user_plans,
                      "transaction_info": {"order_id": params["order_id"], "adyen_encrypted_data": adyen_encrypted_data},
                      "user_info": user_info
                  }
           
          elsif @user_plans["data"].map{|s| s["plan_title"].downcase}.flatten.include?(params["plans"].split(",").last.split("|")[2].downcase)
                 combo_cur_plan = @user_plans["data"].map{|s| s if s["plan_title"].downcase == params["plans"].split(",").last.split("|")[2].downcase && s["category"] == "individual"}.compact
                  cur_cat_id = combo_cur_plan[0]["subscription_id"]  
                  cur_plan_id = combo_cur_plan[0]["plan_id"] 
                   modify_user_plans = {"current_plan_id": cur_plan_id,"current_subscription_category_id": cur_cat_id}

            plans_purchase_params = {
                  "auth_token": "Ts4XpMvGsB2SW7NZsWc3",
                  "region" => @region,
                  "payment_info": payment_info,
                  "modify_user_plans": modify_user_plans,
                  "transaction_info": {"order_id": params["order_id"], "adyen_encrypted_data": adyen_encrypted_data},
                  "user_info": user_info
              }
          else
            plans_purchase_params = {
                "auth_token": "Ts4XpMvGsB2SW7NZsWc3",
                "region" => @region,
                "payment_info": payment_info,
                 "transaction_info": {"order_id": params["order_id"], "adyen_encrypted_data": adyen_encrypted_data},
                "user_info": user_info
            }
          end
         
          response =  HTTP.post_https "users/#{cookies[:user_id]}/transactions/cse_payment", plans_purchase_params
         
          if !response["data"].blank? && (response["data"]["message"] == "pack activated successfully" || response["data"]["message"] == "Plans modified Successfully")
            redirect_to  action: 'payment_success',  resp_data: response
          else
            redirect_to  action: 'payment_failed',  resp_data: response
          end
 end
end
