class SearchController < ApplicationController
  def index
		lang = "en"
		lang = params[:lang] if params[:lang].present?
		begin
			@search_results = []
			@theme = cookies[:theme_option]
			if cookies[:user_profile_type].present? && cookies[:user_profile_type] == "kids"
				category_list_response = Ott.get_search_kids_category_list(lang)
				@category_list = category_list_response["data"]["catalog_list_items"]
				category_type = "kids"
			else
				category_list_response = Ott.get_search_category_list(lang)
				@category_list = category_list_response["data"]["catalog_list_items"]
				category_type = "all"	
			end
			if params[:search].present?
				#search_response = Ott.get_search_results(params[:search].gsub('%','%25').gsub('#','%23').gsub('&','%26').gsub(';','%3B').gsub(" ","%20"), category_type,lang)
				search_response = Ott.get_v2_search_results(params[:search].gsub('%','%25').gsub('#','%23').gsub('&','%26').gsub(';','%3B').gsub(" ","%20"),category_type,lang)
				res = []
				if params[:search].length > 1 
					search_response["data"]["catalog_list_items"].each_with_index do |list,index|
						if (list.has_key?("catalog_list_items") && list['catalog_list_items'].count > 0)
						item = []
						item << list["catalog_list_items"].collect{|x|get_lang_url(x["seo_web_url"])+"$"+x["thumbnails"]["medium_16_9"]["url"]+"$"+(x["ml_title"].present? ? x["ml_title"] : x["title"] )+"$"+ (x["ml_item_caption"].present? ? x["ml_item_caption"] : x["item_caption"] )+"$"+get_search_premium_tag(x)+"$"+(x['duration_string'].present? ? get_duration_time(x['duration_string']) : '')+"$"+x['theme']+"$"+x['catalog_object']['plan_category_type']+"$"+(x["access_control"].present? && x["access_control"]["is_free"] == true ? "free" : "premium")+"$"+(x['ml_release_date'].present? ? x['ml_release_date'].split("-")[0].to_s : x['release_date_string'].present? ? x['release_date_string'].split("-")[0].to_s : " ")+"$"+(x['ml_item_caption'].present? ? x['ml_item_caption'].split("|")[0].to_s+'|' : x['item_caption'].present? ? x['item_caption'].split("|")[0].to_s+'|' : "")+"$"+(x['ml_item_caption'].present? ? x['ml_item_caption'].split("|")[1].to_s+'|' : x['item_caption'].present? ? x['item_caption'].split("|")[1].to_s+'|' : "")}
						item << (list['ml_display_title'].present? ? list['ml_display_title'].capitalize : list['display_title'].capitalize)+"$"+list["home_link"]+"$"+index.to_s
						res << item
						type = "double"
						end
					end 
				else
				res = []
				type = "single"
				end
				render :json => {:status => true,:results => res,:type => type}
			elsif params[:q].present?
				search_result = params[:q].gsub('%','%25').gsub('#','%23').gsub('&','%26').gsub(';','%3B').gsub(" ","%20")
				#search_response = Ott.get_search_results(search_result,category_type,lang)
				category_type = params[:type] if params[:type].present?
				search_response = Ott.get_v2_search_results(search_result,category_type,lang)
				@search_results = search_response["data"]["catalog_list_items"]
			else
				catgry = "all"
				catgry = "kids" if cookies[:user_profile_type].present? && cookies[:user_profile_type] == "kids"
				trending_search = Ott.get_trending_results(lang,catgry)
				@search_results = trending_search["data"]["catalog_list_items"]
			end
		rescue
			@search_results = []
		end
	end
  
  def all_items
  	lang = "en"
    lang = params[:lang]  if params[:lang].present?
  	search_response = Ott.get_v2_search_results_theme(params[:q].gsub(" ","%20"),params[:type],lang,params[:theme])
    @search_items =  search_response["data"]["catalog_list_items"][0]["catalog_list_items"]
  end
end
