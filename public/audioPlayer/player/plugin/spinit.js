/**
 * Saranyu Insta Player
 * Player Version: 4.0.0
 * Base Version: 4.0.0
 */

var spjquery, spcore, splogger, spcast, Player;
isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
function SaranyuInstaPlayer(e, n, a) { sL(e, n, a) }
function cT(e) {
	return "" == e.file[0].content_url ? (mediaType = "hls", "hls") : -1 != e.file[0].content_url.indexOf("mpd") ? (mediaType = "mpd", "mpd") : -1 != e.file[0].content_url.indexOf("m3u8") ? (mediaType = "hls", "hls") : (mediaType = "mp4", "mp4")
}
function cL(e, n) { gP(e, n) }
function sL(e, n, a) {
	var container = n, object = a, mediaType = e;
	sphls    = "/audioPlayer/player/plugin/core/hls.latest.min.js";
	//spjquery = "/audioPlayer/player/external/jquery/jquery.min.js";
	splogger = "/audioPlayer/player/external/logger/logger.min.js";
    spcore = "/audioPlayer/player/plugin/spcore.js";
	var s = document.createElement("script"); s.type = "text/javascript", s.src = sphls, document.body.appendChild(s);
	s.addEventListener("load", function () { cL(n, a) });
}
function gP(e, n) {
	//var j = document.createElement("script"); j.type = "text/javascript", j.src = spjquery, document.body.appendChild(j);
	var l = document.createElement("script"); l.type = "text/javascript", l.src = splogger, document.body.appendChild(l);
	var a = document.createElement("script"); a.type = "text/javascript", a.src = spcore;

	l.addEventListener("load", function() { document.body.appendChild(a)});
	a.addEventListener("load", function () {
		Player = new SaranyuInstaPlayer.MediaPlayer(e, n, cT(n));		
		try {
			window.InstaPlayer = Player;
			n.registerCallBacks();
		} 
		catch (e) {
			SaranyuInstaPlayer.Utils.DLOG.INFO(e);
		}
	});
}