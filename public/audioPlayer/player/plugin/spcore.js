/**
 * Saranyu Insta Player
 * Player Version: 4.0.0
 * Base Version: 4.0.0
 */

	var isFirstTime = true,
	userAgent = window.navigator.userAgent.toLowerCase(),
	isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent),
	isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent),
	keySystem,
	certificate,
	multiaudioKey,
	subtitleKey,
	SaranyuInstaPlayer = window.SaranyuInstaPlayer || {};
	CastSender = window.CastSender || {};
	SaranyuInstaPlayer.PLUGIN_NAME = "Saranyu Insta Player",
    SaranyuInstaPlayer.BUILD_VERSION = "4.0.0",
	SaranyuInstaPlayer.inActivityTimeout = 3000,
	SaranyuInstaPlayer.bigPlayClickCounter = 0,
	SaranyuInstaPlayer.bigPlayClickDELAY = 350,
	SaranyuInstaPlayer.bigPlayClickCounterTimer,
	SaranyuInstaPlayer.defaultPoster = "data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTkyMCIgaGVpZ2h0PSIxMDgwIiB2aWV3Qm94PSIwIDAgMTkyMCAxMDgwIiBmaWxsPSJub25lIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgo8cmVjdCB3aWR0aD0iMTkyMCIgaGVpZ2h0PSIxMDgwIiBmaWxsPSIjRTVFNUU1Ii8+CjxyZWN0IHdpZHRoPSIxOTIwIiBoZWlnaHQ9IjEwODAiIGZpbGw9ImJsYWNrIi8+CjxnIGNsaXAtcGF0aD0idXJsKCNjbGlwMCkiPgo8cGF0aCBkPSJNODEyLjkxNCAyOTUuMjg5Qzc2Ni43NTQgMjY4LjgxMSA3MjkuMzMxIDI5MC41MDIgNzI5LjMzMSAzNDMuNjk5VjczNi4yNjRDNzI5LjMzMSA3ODkuNTEzIDc2Ni43NTQgODExLjE3NiA4MTIuOTE0IDc4NC43MjNMMTE1Ni4wNCA1ODcuOTQ1QzEyMDIuMjEgNTYxLjQ1OCAxMjAyLjIxIDUxOC41NDUgMTE1Ni4wNCA0OTIuMDY0TDgxMi45MTQgMjk1LjI4OVoiIGZpbGw9IndoaXRlIiBmaWxsLW9wYWNpdHk9IjAuMTIiLz4KPC9nPgo8ZGVmcz4KPGNsaXBQYXRoIGlkPSJjbGlwMCI+CjxyZWN0IHdpZHRoPSI1MTIiIGhlaWdodD0iNTEyIiBmaWxsPSJ3aGl0ZSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoNzA0IDI4NCkiLz4KPC9jbGlwUGF0aD4KPC9kZWZzPgo8L3N2Zz4K"
    SaranyuInstaPlayer.defaultFeatures = {features: ['playpause','volume','videotitle','time','progressbar','qualityswitch','fullscreen','bigicons','hotkeys','backarrow','eventcallbacks']},
	SaranyuInstaPlayer.defaultOptions = {
		debug: true,
		type: "video",
		autoplay: true,
		videotitle: "Saranyu Insta Player",
		content: "VOD",
		theme: "light",
		titleStrings: {
			multiaudio: "Default",
			subtitles: "OFF"
		},
		errorStrings: {
			generic: {
				text: "Something went wrong!",
				subtext: "There was an error processing your request. Please close the player and try again."
			},
			issue: {
				text: "The movie stream is not compatible with this browser.",
				subtext: "Please download <a href='#' target='_blank'>InstaPlayer App</a> for a Seamless Streaming Experience."
			}
		}
	},
	SaranyuInstaPlayer.Utils = {
		DLOG: {
			ERR: function (t) {
				true == SaranyuInstaPlayer.defaultOptions.debug && (Logger.useDefaults(), Logger.setLevel(Logger.WARN), Logger.error(t))
			},
			WARN: function (t) {
				true == SaranyuInstaPlayer.defaultOptions.debug && (Logger.useDefaults(), Logger.setLevel(Logger.WARN), Logger.warn(t))
			},
			INFO: function (t) {
				true == SaranyuInstaPlayer.defaultOptions.debug && (Logger.useDefaults(), Logger.info(t))
			}
		},
		convertSecondsTimecode: function (t) {
			if (t > 0) {
				t = Math.max(0, Math.round(t * 1));
				var s = t % 60;
				var m = Math.floor(t / 60) % 60;
				var h = Math.floor(t / (60 * 60));
				if (s < 10) s = '0' + s;
				if (m < 10) m = '0' + m;
				if (h < 10) h = '0' + h;
				var f = '';
				f = f + h + ':';
				f = f + m + ':' + s;
				return f;
			}
		},
		getWidthInPercentage: function (t) {
			var e = t.parent();
			return (t.width() / e.width() * 100).toFixed(0) + "%";
		},
		getHeightInPercentage: function (t) {
			var e = t.parent();
			return (t.height() / e.height() * 100).toFixed(0) + "%"
		},
		preventSelectionOfTextInMouseMove: function (t) {
			return t.stopPropagation && t.stopPropagation(),
			t.preventDefault && t.preventDefault(),
			t.cancelBubble = !0, t.returnValue = !1, t
		},
		getBrowserInfo: function () {
			var UA = window.navigator.userAgent, TEM, CriOS_Ver, M=UA.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || []; 
			if(/trident/i.test(M[1])) {
				TEM=/\brv[ :]+(\d+)/g.exec(UA) || []; 
				return {name:'IE',version:(TEM[1]||'')};
			}   
			if(M[1]==='Chrome') {
				TEM=UA.match(/\bOPR|Edge\/(\d+)/)
				if(TEM!=null){return {name:'Opera',version:TEM[1]}}
			}
			if(M[1]==='Safari') {
				if(UA.match('CriOS')) {
					var CriOS_Ver=parseInt((UA.match(/OS (\d+)_(\d+)_?(\d+)?/))[1], 10)
					return {name:'iOS Chrome', version:CriOS_Ver}
				}
				else if(UA.match('FxiOS')) {
					var FxiOS_Ver=parseInt((UA.match(/OS (\d+)_(\d+)_?(\d+)?/))[1], 10)
					return {name: 'iOS Firefox', version: FxiOS_Ver}
				}
			}
			M=M[2]?[M[1],M[2]]:[window.navigator.appName, window.navigator.appVersion,'-?'];
			if((TEM=UA.match(/version\/(\d+)/i))!=null) {M.splice(1,1,TEM[1])}
			return {name:M[0],version:M[1]};
		}
	},
	SaranyuInstaPlayer.MediaPlayer = function (t, e, o) {
		var l = this;
		l.masterElementid = t;
		l.options = e;
		l.playerType = o;
		l.$masterContainer = $(t);
		l.init();
	},
	SaranyuInstaPlayer.MediaPlayer.prototype = {
		init: function () {
			var t = this;
			var a = t.options.file[0];
			t.container = $('<div class="sp-main-container"><div class="sp-player-inner">' + '<div class="sp-media-element"></div></div></div>').appendTo(t.$masterContainer),
			t.mainContainerWrapper = t.container.find(".sp-main-container-wrapper"),
			t.mainContainer = t.container.find(".sp-main-container"),
			t.playerInner = t.container.find(".sp-player-inner"),
			t.mediaElement = t.container.find(".sp-media-element"),
			t._validateBrowser();
			t._createMediaElement();
			t._createIndividualControls();
			t._createAndAppendMedia(a);
		},
		_validateBrowser: function() {
			var t = this;
			var browserInfo = SaranyuInstaPlayer.Utils.getBrowserInfo();
			if(isMobile) {
				if((browserInfo.name == 'iOS Chrome') && (browserInfo.version < 13)) {
					t._attachErrorview(SaranyuInstaPlayer.defaultOptions.errorStrings.issue.text, SaranyuInstaPlayer.defaultOptions.errorStrings.issue.subtext);
					return
				}
				else if((browserInfo.name == 'iOS Firefox')) {
					t._attachErrorview(SaranyuInstaPlayer.defaultOptions.errorStrings.issue.text, SaranyuInstaPlayer.defaultOptions.errorStrings.issue.subtext);
					return
				}
				else if((browserInfo.name == 'Safari') && (browserInfo.version < 11)) {
					t._attachErrorview(SaranyuInstaPlayer.defaultOptions.errorStrings.issue.text, SaranyuInstaPlayer.defaultOptions.errorStrings.issue.subtext);
					return
				}
				else if((browserInfo.name == 'Firefox') && (a.encryption == true))  {
					t._attachErrorview(SaranyuInstaPlayer.defaultOptions.errorStrings.issue.text, SaranyuInstaPlayer.defaultOptions.errorStrings.issue.subtext);
					return
				}
			}
			else if(browserInfo.name == 'IE' && a.encryption == true) {
				t._attachErrorview(SaranyuInstaPlayer.defaultOptions.errorStrings.issue.text, SaranyuInstaPlayer.defaultOptions.errorStrings.issue.subtext);
				return
			}
		},
		_createMediaElement: function () {
			var t = this.mediaElement;
			t.append("<audio tabindex='-1' preload='metadata' webkit-playsinline playsinline></audio>");
			ME = this.mediaElement.videoElement = t.find("audio")[0];
		},
		_createIndividualControls: function () {
			var t = this;
			var e = t.options.hasOwnProperty('features') ? t.options.features : SaranyuInstaPlayer.defaultFeatures.features;
			for (featureIndex in e) {
				feature = e[featureIndex];
				try {
					t["_build" + feature] && t["_build" + feature]();
				}
				catch (t){}
			}
		},
		_createAndAppendMedia: function (t) {
			var o = this, l = o.mediaElement.videoElement, e = t.content_url, a = o.options;
			t.seekonload ? a.seekonload = t.seekonload : a.seekonload = 0;
			/* MSE - HLS */ 
			o._createAndAppendMedia._attachHLStoAudio = function () {
				Hls.isSupported() ? (
				hlsConfig = {debug: false, autoStartLoad: true },
				o.mediaElement.videoElement.hlsObj = new Hls(hlsConfig),
				o.mediaElement.videoElement.hlsObj.loadSource(e),
				o.mediaElement.videoElement.hlsObj.attachMedia(l),
				o.mediaElement.videoElement.mediaId = t.mediaid,
				o.mediaElement.videoElement.hlsObj.on(Hls.Events.ERROR, function (event, data) {
					if (data.fatal) {
						switch (data.type) {
							case Hls.ErrorTypes.NETWORK_ERROR:
								SaranyuInstaPlayer.Utils.DLOG.ERR("Fatal Network Error, Retrying..."),
								o._attachErrorview(SaranyuInstaPlayer.defaultOptions.errorStrings.generic.text, SaranyuInstaPlayer.defaultOptions.errorStrings.generic.subtext);
								o.mediaElement.videoElement.hlsObj.startLoad();
							break;
							case Hls.ErrorTypes.MEDIA_ERROR:
								SaranyuInstaPlayer.Utils.DLOG.ERR("Media Error, Retrying..."),
								o._attachErrorview(SaranyuInstaPlayer.defaultOptions.errorStrings.generic.text, SaranyuInstaPlayer.defaultOptions.errorStrings.generic.subtext);
								o.mediaElement.videoElement.hlsObj.recoverMediaError();
							break;
							default:
								o.mediaElement.videoElement.hlsObj.destroy();
							break;
						}
					}
				}),
				o.mediaElement.videoElement.hlsObj.on(Hls.Events.MANIFEST_PARSED, function () {
					if (a.autoplay && a.seekonload >= 0) {
						o.mediaElement.videoElement.hlsObj.startLoad(a.seekonload);
						l.play().then(() => {
							SaranyuInstaPlayer.Utils.DLOG.INFO("Autoplay Success With Volume!!!");
						}).catch(() => {
							SaranyuInstaPlayer.Utils.DLOG.ERR("Autoplay Prevented With Volume!!!");
							l.muted = true;
							l.play().then(() => {
							SaranyuInstaPlayer.Utils.DLOG.INFO("Autoplay Success With Mute!!!");
							});
						});
					}					
					else if (!a.autoplay && a.seekonload >= 0) {
						o.mediaElement.videoElement.hlsObj.startLoad(a.seekonload);
					}
				})) : SaranyuInstaPlayer.Utils.DLOG.ERR("MSE (Media Source Extension) Not Supported On Your Browser")
			};
			if ("hls" == o.playerType && o._isSupportedMSE()) {
				SaranyuInstaPlayer.Utils.DLOG.INFO("HLS On Browser With MSE Support");
				o._createAndAppendMedia._attachHLStoAudio();
			}else if ("hls" == o.playerType) {
				console.log("for iOS");
				o.mediaElement.videoElement.src = e;
				o.mediaElement.videoElement.mediaId = t.mediaid;
						l.play();

			} else if ("mp4" == o.playerType) {
				SaranyuInstaPlayer.Utils.DLOG.INFO("MP4 On Browser");
				o.mediaElement.videoElement.src = e;
				o.mediaElement.videoElement.mediaId = t.mediaid;
				if (a.autoplay && a.seekonload >= 0) {
						l.play().then(() => {
							SaranyuInstaPlayer.Utils.DLOG.INFO("Autoplay Success With Volume!!!");
						}).catch(() => {
							SaranyuInstaPlayer.Utils.DLOG.ERR("Autoplay Prevented With Volume!!!");
						l.muted = true;
						l.play().then(() => {
							SaranyuInstaPlayer.Utils.DLOG.INFO("Autoplay Success With Mute!!!");
						});
					});
				}
			}
			try {}
			catch (e) {}
		},
		_buildplaypause: function () {
			var e = this,
			o = e.mediaElement.videoElement;
			e.fullControls.bottomControlBar.bottomPlayerControls.append('<div class="sp-button sp-play-pause sp-play"><button class="sp-play-pause-btn"></button></div>'),
			e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause = e.fullControls.bottomControlBar.bottomPlayerControls.find(".sp-button.sp-play-pause.sp-play"),
			e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.click(function (t) {
				SaranyuInstaPlayer.Utils.DLOG.INFO("Clicked on Play Pause Icon"),
				t.preventDefault(),
				o.paused ? e._audioPlayerControls("play") : e._audioPlayerControls("pause"),
				o.ended && e._audioPlayerControls("seek", 0),
				o.focus();
			});
			false === e.options.autoplay || "false" === e.options.autoplay && (SaranyuInstaPlayer.Utils.DLOG.INFO("Autoplay False"),
			e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.addClass("sp-pause"),
			e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-play"),
			e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-replay"))
			o.addEventListener("play", function () {
				try {
					SaranyuInstaPlayer.Utils.DLOG.INFO("Play Triggered"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.addClass("sp-play"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-pause"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-replay")
				}
				catch (t) {}
			}, !1);
			o.addEventListener("playing", function () {
				try {
					SaranyuInstaPlayer.Utils.DLOG.INFO("Playing Triggered"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.addClass("sp-play"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-pause"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-replay")
				}
				catch (t) {}
			}, !1);
			o.addEventListener("pause", function () {
				try {
					SaranyuInstaPlayer.Utils.DLOG.INFO("Pause Triggered"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.addClass("sp-pause"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-play"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-replay")
				}
				catch (t) {}
			}, !1);
			o.addEventListener("ended", function () {
				try {
					SaranyuInstaPlayer.Utils.DLOG.INFO("Re-Play Triggered"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.addClass("sp-replay"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-play"),
					e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.removeClass("sp-pause")
					e._createFullControls.showFullControls();
				}
				catch (t) {}
			}, !1);
			e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.mouseenter(function () {
				e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.tooltip.addClass("active")
			});
			e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.mouseleave(function () {
				e.fullControls.bottomControlBar.bottomPlayerControls.saranyuPlaypause.tooltip.removeClass("active")
			});
		},
		_audioPlayerControls: function (t, e) {
			var o = this,
				l = o.mediaElement.videoElement;
			switch (o.options, t) {
				case "play":
					if ("hls" === o.playerType && 0 === o.mediaElement.videoElement.duration) {
						o._createAndAppendMedia._attachHLStoAudio();
					}
					else {
						l.play().then(() => {
						}).catch(() => {
						l.muted = true;
						l.play().then(()=>{});
						});
					}
					break;
				case "pause":
					l.pause();
					break;
				case "mute":
					l.muted = true;
					break;
				case "unmute":
					l.muted = false;
					break;
				case "volumechange":
					l.volume = e;
					break;
				case "seek":
					l.currentTime = e;
					break;
				case "backwardseek":
					l.currentTime = e;
			}
		},
		_getCurrentTime: function () {
			return ME.currentTime;
		},
		_getTotalDuration: function () {
			return ME.duration;
		},
		_isSupportedMSE: function () {
			return window.MediaSource = window.MediaSource || window.WebKitMediaSource, window.MediaSource && "function" == typeof window.MediaSource.isTypeSupported && window.MediaSource.isTypeSupported('video/mp4; codecs="avc1.42E01E,mp4a.40.2"')
		},
		_destroyMedia: function () {
			var t = window.InstaPlayer;
			e = ME;
			if (t.playerType == "hls") {
				try {
					e.hlsObj.destroy();
				}
				catch (i) {}
			} else if (t.playerType == "mp4") {
				try {
					e = "";
				}
				catch (i) {}
			}
		}
	}