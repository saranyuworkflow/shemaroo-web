class CreateUserQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :user_questions do |t|
      t.integer "user_analytic_id"
      t.datetime "ques_sub_time"
      t.datetime "created_at", null: false
      t.datetime "updated_at", null: false
      t.json "miscellaneous", default: {}
      t.timestamps
    end
  end
end
