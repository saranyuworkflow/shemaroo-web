
Rails.application.routes.draw do

 root "catalogs#index"

   get "/hi" => "catalogs#index"
   get "/gu" => "catalogs#index"
   get "/mr" => "catalogs#index"
   get "/te" => "catalogs#index"
   get "/ta" => "catalogs#index"
   get "/kn" => "catalogs#index"
  
  catgrs = ['bollywood-plus', 'bollywood-classic','gujarati','marathi','kids','comedy','bhakti','punjabi','ibaadat','video','movie','show','livetv','hot-movie']
  catgrs.each do |c|
    get "/:lang/#{c}" => "catalogs#indiv_category_landing"
  end

  get "/channels" => "catalogs#channel_catalog_item"
  get "/:lang/channels" => "catalogs#channel_catalog_item"
  get "/all_channels_playlist/:cat_name" => "catalogs#all_channels_playlist"
  get "/:lang/all_channels_playlist/:cat_name" => "catalogs#all_channels_playlist"
  get "/catalogs/more_channels_list" => "catalogs#more_channels_list"

  get "/live-tv-1/all" => "catalogs#show_catalog_item"


  get '/plans' => 'plans#all_plans'
  get '/:lang/plans' => 'plans#all_plans'

 
 require 'sidekiq/web'
 mount Sidekiq::Web => '/sidekiq_shemaroo'

 ##STATIC PAGES ROUTES STARTS HERE####
 get "/terms-and-conditions" => "statics#terms_conditions"

 
 #Main referal route for showing FAQ and TAQ based on Region
 get "/referal_terms_conditions" => "statics#referal_terms_conditions"


 get "/.well-known/apple-app-site-association" => "statics#apple_site_notification"

 get "/.well-known/pki-validation/BA4DB7294E74E2DFF9157CDBE07F934F.txt" => "statics#pki_txt"

 get "/offers" => "statics#offers"

 get "/worldmeday" => "statics#worldmeday"

 get "/rehlatoffer" => "statics#rehlatoffer"

 get "/8koffers" => "statics#offers"

 get "/hotstar" => "statics#hotstar"

 get "/hotstar1" => "statics#hotstar1"

 get "/gifts" => "statics#gifts"

 get "/shemaroo_docs_offer" => "statics#shemaroo_doc_app"

 get "/digital_marketing" => "statics#digital_marketing"

 get "/terms_and_conditions" => "statics#terms_conditions"

 get "/:lang/terms_and_conditions" => "statics#terms_conditions"


 get "/etisalat_termsconditions" => "statics#etisalat_terms"

 get "/etisalat_privacy_policy" => "statics#etisalat_privacy_policy"

 get "/etisalat_faq" => "statics#etisalat_faq"

 get "/gujarati-landing" => "statics#gujarathi_landing"

 get "/bollywood-landing" => "statics#bollywood_landing"

 get "/best-of-bollywood-entertainment" => "statics#bollywood_landing"

 get "/best-of-gujarati-entertainment" => "statics#gujarathi_landing"

 get "/gujarati-landing_page" => "statics#gujarathi_landing_page"

 get "/accentiv"  => "statics#accentiv"

 get "/:lang/contest_gamification" => "statics#contest_gamification"

 get "/contest_gamification" => "statics#contest_gamification"

 get "/partners" => "catalogs#partner"

 get "/partners-db" => "catalogs#partner"

 get "/comingsoon" => "statics#coming_soon"

 get "/consulategeneraln" => "statics#consulate_general"

 get "/CGNY" => "statics#consulate_general"

 get "/cgny" => "statics#consulate_general"

 get "/rajchandraji" => "statics#rajchandraji"

 get "/EOIP" => "statics#eoip"

 get "/eoip" => "statics#eoip"

 get "/allaccessoffer" => "statics#all_access_offer"
 get "/blackfriday" =>"statics#blackfriday"
 get "/ALLACCESSOFFER" => "statics#all_access_offer"

 get "/GujaratiOffer" => "statics#gujaratioffer"
 get "/BollyPremiereOffer" => "statics#bollypremiereoffer"
 get "/BollyClassicOffer" => "statics#bollyclassicoffer"

 get "/gujaratioffer" => "statics#gujaratioffer"
 get "/bollypremiereoffer" => "statics#bollypremiereoffer"
 get "/bollyclassicoffer" => "statics#bollyclassicoffer"
 get "/lohana-samaj" => "statics#lohana_samaj"
 get "/Lohana-Samaj" => "statics#lohana_samaj"
 get "/navratri"  => "statics#navratri"
 get "/NAVRATRI"  => "statics#navratri"
 get "/guj" => "statics#guj_landing"
 get "/bhavans" => "statics#bhavans"
 get "/gujm" => "statics#gujm"
 get "/usswagatam" => "statics#us_swagatam"
 get "/usvat-vat-ma" => "statics#hungamacity"
 get "/gcs-alabama" => "statics#gcs_alabama"
 get "/montgomery" => "statics#montgomery"
 get "/vyo" => "statics#vyo_landing"
 get "/gs-alabama" => "statics#gs_alabama"
 get "/gona" => "statics#gona"
 get "/gsdetroit" => "statics#gsdetroit"
 get "/gcs-florida" => "statics#gcs_florida"
 get "/vtofny"  => "statics#vtofny"
 get "/phillymm" => "statics#phillymm"
 get "/fog-canada"=> "statics#fog_canada"
 get "/mobikwik_terms" => "statics#mobikwik_terms"
 get "/grievance" => "statics#grievance"
 get "/bolly" => "statics#bollywood_category"

 get "/GSHOUSTON" => "statics#gshouston"
 get "/gshouston" => "statics#gshouston"
 get "/gmoco" => "statics#gmoco"
 get "/GMOCO" => "statics#gmoco"

 get "/gujaratisamaj" => "statics#gujaratisamaj"

 post "/payment/payu_payment_response" => "plans#payu_payment_response"
 

 get "/homeis" => "statics#homeis"

 get "/HOMEIS" => "statics#homeis"

  get "/CGIC" => "statics#cgic"

  get "/cgic" => "statics#cgic"

  get "/HCIC" => "statics#hcic"

  get "/hcic" => "statics#hcic"

  get "/cgny2" => "statics#cgny2"

  get "/eois" => "statics#eois"

  get "/EOIS" => "statics#eois"

 get "/privacy-policy" => "statics#privacy_policy"

 get "/:lang/privacy-policy" => "statics#privacy_policy"
 get "/privacy_policy" => "statics#privacy_policy"

  get "/cookie-policy" => "statics#cookie_policy"

  get "/offer_terms_conditions" =>  "statics#offer_terms_conditions"

  get "/offer_rules_regulations" =>  "statics#offer_rules_regulations"

 get "/content_not_available" => "statics#content_not_available"

 get "/faq" => "statics#faq"


 get "/about-us" => "statics#about_us"
 
 get "/:lang/faq" => "statics#faq"

 get "/thanks" => "statics#thanks"

 get "/health_check" => "statics#health_check"

 get "/landing_page" => "statics#landing_page"

 get "/engage_page" => "statics#engage_page"

 get "/:lang/contact-us" => "statics#contact_us"

 match "/contact-us" => "statics#contact_us",via: [:get,:post]

 get "/referral_earn_money" => "statics#ref_earn_money"

 get "/icicim2i" => "statics#icic"
 
 get "/:lang/tv" => "users#activate_code"

 get "/tv" => "users#activate_code"

 post "/users/coupon_register"  => "users#coupon_register"

 get "/sulekha" =>  "statics#sulekha"
 get "/hungamacity" =>  "statics#hungamacity"
 get "/HUNGAMACITY" =>  "statics#hungamacity"
 get "/store" => "statics#us_swagatam"
 get "/store1" => "statics#store1"
 # get "/bollywoodclassic" => "statics#shemaroo_classic"
 get "/bollywoodclassic" => "statics#microsite_classic"
 get "/evergreen" => "statics#evergreen_classic"
 get "/nyl" => "statics#nyl"

 ##STATIC PAGES ROUTES ENDS HERE####

 ##USERS PAGES ROUTES STARTS HERE####
  post "/participate" => "users#participate"
  get "/contest" => "users#contest"
  get "/subscribe-and-win-contest" => "users#contest"
  get "/subscribe-win-contest" => "users#subs_contest"
  match "/contest_questions" => "users#contest_questions",via: [:get,:post]

  get  "/:lang/users/sign_in" => "users#sign_in"
  get  "/:lang/users/profile" => "users#profile"
  get  "/:lang/users/watch_list"=> "users#watch_list" 
  get  "/:lang/users/settings" => "users#settings" 
  get  "/:lang/users/create_pin" => "users#create_pin" 
  get  "/:lang/users/referral" => "users#referral" 
  get  "/:lang/users/referral_earn_money" => "users#referral_earn_money"
  get  "/:lang/users/reset_pin"  => "users#reset_pin" 
  get  "/:lang/users/account_details" => "users#account_details"
  get  "/:lang/users/change_password" => "users#change_password" 
  get  "/:lang/users/redeem_ticket" => "users#redeem_ticket" 
  get  "/:lang/users/manage_profiles" => "users#manage_profiles" 
  get  "/:lang/users/referral_code" => "users#referral_code"
  get  "/:lang/users/registered_devices" => "users#registered_devices"
  get  "/:lang/users/who_is_watching" => "users#who_is_watching"
  get  "/:lang/users/verify_pin" => "users#verify_pin"
  get  "/:lang/users/my_earnings" => "users#my_earnings"
  get "/:lang/users/add_bank_account" => "users#add_bank_account"
  get "/:lang/users/continue_watching" => "users#continue_watching"
  post "/:lang/users/continue_watching" => "users#continue_watching"
  get "/:lang/users/user_recommendation" => "users#user_recommendation"
  post "/:lang/users/user_recommendation" => "users#user_recommendation"
  get "/:lang/users/gamification" => "users#gamification"
  get "/:lang/users/forgot_password" => "users#forgot_password"
  get "/:lang/users/forgot_pin" => "users#forgot_pin"
  get "/:lang/users/gmfc_contest" => "users#gmfc_contest"
  get "/:lang/users/gmfc_contest_questions" => "users#gmfc_contest_questions"
  get "/:lang/users/gmfc_winners" => "users#gmfc_winners"
  get "/:lang/users/gmfc_thanks" => "users#gmfc_thanks"
  get "/:lang/users/login" => "users#login"
  get "/search/all" => "search#all_items"
  get "/shu-thayu/tnc" => "users#shuthayu_tnc"
	namespace :users do
    match 'account_details', via: [:get,:post]
    match 'change_password',via: [:get,:post]
    match 'redeem_ticket',via: [:get,:post]
    match 'reset_pin',via: [:get,:post]
    get 'settings'
    match 'referral',via: [:get,:post]
    match  'watch_list',via: [:get,:post]
    match 'create_pin',via: [:get,:post]
    match 'sign_in', via: [:get,:post]
    post 'check_coupon_validity'
    post 'sub_events'
    get 'manage_profiles'
	  get 'register'
    get 'get_location_details'
    post 'pre_register'
	  get 'login'
    get 'sign_in'
    get 'profile'
	  post 'sign_up'
	  get 'welcome'
    get 'welcome_offer'
    match 'forgot_password_otp_verify', via: [:get,:post]
    match 'reset_password', via: [:get,:post]
    match 'reset_password_email', via: [:get,:post]
    match 'forgot_password',via: [:get,:post]
    match 'telco_external_users',via: [:get,:post]
    get 'change_profile'
	  post 'sign_out'
	  get  'verify_otp'
    get  'otp_verify'
    post 'update_profile'
    post 'delete_profile'
    match 'registered_devices',via: [:get,:post]
    get 'streaming'
    get 'parent_control'
    match 'activate_code',via: [:get,:post]
    match  'update_personal_details',via: [:get,:post]
    match  'add_profile',via: [:get,:post]
    match 'verify_password',via: [:get,:post]
    match 'verify_pin',via: [:get,:post]
    match 'forgot_pin',via: [:get,:post]
    match 'verify_pin_otp',via: [:get,:post]
    match 'remove_pin',via: [:get,:post]
    match 'forgot_pin_email',via: [:get,:post]
	  post 'validate_otp'
	  post 'resend_otp'
    post 'mobile_no_signup'
	  post 'subscriptions'
    post 'clear_watchhistory'
    post 'add_watch_later'
    post 'user_all_lists'
    get 'who_is_watching'
    get 'who_is_watching_register'
    post 'player_persistant'
    get 'continue_watching_list'
    get 'user_recommendation_list'
    match 'user_recommendation', via:[:get,:post]
    match 'continue_watching',via: [:get,:post]
    post 'check_valid_user_session'
    post 'validate_promocode'
    match 'firebase_success',via: [:get,:post]
    post 'account_merge'
    post 'validate_lzy_mobile'
    post 'lazpay_otp_valid'
    post 'lzypay_rsd_otp'
    post 'branch_s2s'
    post 'branch_custom_event'
    match 'bd_user',via: [:get,:post]
    get 'my_movies'
    match 'promocode',via: [:get,:post]
    post 'update_lang'
    post 'user_prop'
    post 'acntv_coupon'
    
    get 'referral_code'
    match 'my_earnings',via: [:get,:post]
    match 'add_bank_account',via: [:get,:post]
    get 'referral3'
    post 'upload_file'
    post 'encrypt_bank_dt'
    post 'claim_earnings'
    get 'referral_earn_money'
    get 'gamification'
    get 'gmfc_contest'
    get 'gmfc_contest_questions'
    post 'gmfc_answers'
    get 'gmfc_thanks'
    post 'gmfc_user_signup'
    post 'gmfc_otp_verify'
    get 'gmfc_winners'
    post 'ext_mobile'
    post 'ext_valid_otp'
    get 'select_language'
	end
  get "/users/edit_profile/:profile_id" => "users#edit_profile"
  get "/:lang/users/edit_profile/:profile_id" => "users#edit_profile"

  get "/users/change_profile/:profile_id" => "users#remote_profile"

  get "/:lang/users/change_profile/:profile_id" => "users#remote_profile"
 ##USERS PAGES ROUTES STARTS HERE####

 ##PLANS PAGES ROUTES STARTS HERE####
 get "/plans_new" => "plans#plans_new"
 get "/plans_new_summary" => "plans#plans_new_summary"



   get '/plans' => 'plans#all_plans'

  get '/:lang/plans' => 'plans#all_plans'

  get '/:lang/plans/plans_summary' => 'plans#plans_summary'

  get '/plans/plans_summary' => 'plans#plans_summary'
  
  get '/plans/ccaveneue_new' => 'plans#ccaveneue_new'

  get '/plans/ccaveneue_request' => 'plans#ccaveneue_request'

  get '/plans/canada_zipcode_check' => 'plans#canada_zipcode_check'
  
 
  get '/plans/plans_purchase' => 'plans#plans_purchase'
 
 
  post '/plans/payment_url' => 'plans#payment_url'
  

  get '/plans/purchase_plans' => 'plans#purchase_plans'
 
 
  get '/payment/payment_processing' => 'plans#payment_processing'
  get '/payment/payment_success' => 'plans#payment_success'
  match '/payment/ghoori_payment_success' => 'plans#ghoori_payment_success', via: [:get, :post]
  match '/payment/ghoori_external_payment_success' => 'plans#ghoori_external_payment_success', via: [:get, :post]


  match '/payment/ooredoo_kwbilling_response' => 'plans#ooredoo_kwbilling_response', via: [:get, :post]

  match '/payment/ooredoo_billing_response' => 'plans#ooredoo_billing_response', via: [:get, :post]

  match '/payment/kuwait_viva_billing_response' => 'plans#kuwait_viva_billing_response', via: [:get, :post]

  match '/payment/zain_kwbilling_response' => 'plans#zain_kwbilling_response', via: [:get, :post]

  match '/payment/ksa_stc_billing_response' => 'plans#ksa_stc_billing_response', via: [:get, :post]

  match '/payment/etisalat_uae_billing_response' => 'plans#etisalat_uae_billing_response', via: [:get, :post]

  match '/payment/stc_bahrain_billing_response' => 'plans#stc_bahrain_billing_response', via: [:get, :post]
  
 match '/payment/oman_response' => 'plans#oman_response', via: [:get, :post]
 

  match '/payment/otm_response' => 'plans#otm_response', via: [:get, :post]


  match '/payment/adyen_payment_success' => 'plans#adyen_payment_success', via: [:get, :post]

  match '/payment/ksa_zain_billing_response' => 'plans#ksa_zain_billing_response', via: [:get, :post]

 
  get '/payment/payment_failed' => 'plans#payment_failed'
 
  match '/payment/payment_canceled' => 'plans#payment_canceled', via: [:get, :post]
 
  # post '/payment/payment_response' => 'plans#payment_response'
  # post '/payment/m_payment_response' => 'mobile_plans#mobile_payment_response'
 
  match '/payment/payment_response' => 'plans#payment_response', via: [:get, :post]

  match '/payment/paytm_upi_response' => 'plans#paytm_upi_response', via: [:get, :post]

  match '/payment/paytm_response' => 'plans#paytm_response', via: [:get, :post]

  match '/payment/paytm_upi_response' => 'plans#paytm_upi_response', via: [:get, :post]

  match '/payment/razor_pay_response' => 'plans#razor_pay_response', via: [:get, :post]


  match '/payment/amazonpay' => 'plans#amazonpay', via: [:get, :post]

  match '/payment/mobile_pay_amazonpay' => 'plans#mobile_pay_amazonpay', via: [:get, :post]


  match '/payment/amazonpay_response' => 'plans#amazonpay_response', via: [:get, :post]

  match '/payment/amazonpay_success' => 'plans#amazonpay_success', via: [:get, :post]


  match '/payment/operator_billing_response' => 'plans#operator_billing_response', via: [:get, :post]

  match '/payment/digi_billing_response' => 'plans#digi_billing_response', via: [:get, :post]
  match '/payment/mobily_response' => 'plans#mobily_response', via: [:get, :post]

  match '/payment/boost_response' => 'plans#boost_response', via: [:get, :post]

  match '/payment/du_uae_billing_response' => 'plans#du_uae_billing_response', via: [:get, :post]

  match '/payment/googlepay_response' => 'plans#googlepay_response', via: [:get, :post]

  match '/payment/lazypay_response' => 'plans#lazypay_response', via: [:get, :post]

  match '/payment/unifi_response' => 'plans#unifi_response', via: [:get, :post]

 match '/payment/tvod_success' => 'plans#tvod_success', via: [:get, :post]

  get '/:lang/payment/tvod_success' => 'plans#tvod_success'


  match '/plans/unifi_otp' => 'plans#unifi_otp', via: [:get, :post]

  match '/plans/unifi_otp' => 'plans#unifi_otp', via: [:get, :post]


  post '/plans/otm_otp_valid' => 'plans#validate_otp_ott_order'

  post '/plans/otm_resnd_otp' => 'plans#otm_resnd_otp'


  get '/plans/apply_promocode' => 'plans#apply_promocode'
  # get '/plans/m_apply_promocode' => 'mobile_plans#mobile_apply_promocode'

  get '/plans/view_plans' => 'plans#view_plans'


  get '/:lang/plans/view_plans' => 'plans#view_plans'
  
  get '/plans/plan_details/:id', to: 'plans#plan_details', as: 'plan'

  get '/:lang/plans/plan_details/:id', to: 'plans#plan_details'

  get '/plans/unsubscribe_plan' => 'plans#unsubscribe_plan'
 


  get '/plans/delete_inactive_plan' => 'plans#delete_inactive_plan'
  get '/plans/m_delete_inactive_plan' => 'mobile_plans#mobile_delete_inactive_plan'

  #Start of New plans pages related code starts here

  get "/plans/modify_user_plan/:plan_id" => "plans#modify_user_plan"
  
  post "/plans/upgrade_plan" => "plans#upgrade_plan"

  post "/plans/downgrade_plan" => "plans#downgrade_user_plan"

  get "/plans/modify_user_plan_purchase" => "plans#modify_user_plan_purchase"

  get "/plans/modify_user_plan_summary" => "plans#modify_user_plan_summary"

  post "/plans/init_transaction" => "plans#init_transaction"

  get "/plans/modify_user_purchased_plan" => "plans#modify_user_purchased_plan"

  get "/plans/upgrade_user_plan/:plan_id" => "plans#upgrade_user_plans"

  get "/plans/downgrade_user_plan/:plan_id" => "plans#downgrade_user_plans"

  get "/plans/modify_upgrade_plan_purchase" => "plans#modify_upgrade_plan_purchase"

  get "/plans/add_new_plan" => "plans#add_new_plan"

  get "/:lang/plans/add_new_plan" => "plans#add_new_plan"

  get "/plans/new_plan_summary" => "plans#new_plan_summary"

  get "/:lang/plans/new_plan_summary" => "plans#new_plan_summary"

  get "/plans/upgrade_plans" => "plans#upgrade_plans"

  get "/:lang/plans/upgrade_plans" => "plans#upgrade_plans" 

  post "/plans/set_upgrade_plan" => "plans#set_upgrade_plan"

  post "/plans/new_plans_init_transaction" => "plans#new_plans_init_transaction"

  get "/plans/new_plan_purchase" => "plans#new_plan_purchase"
 
  get "/plans/new_plan_user_purchase" => "plans#new_plan_user_purchase"

  post "/plans/promocode_apply" => "plans#promocode_apply"

  post "/plans/remove_user_plan" => "plans#remove_user_plan"

  post "/plans/reactivate_user_plan" => "plans#reactivate_user_plan"

  post "/plans/update_transaction_details" => "plans#update_transaction_details"

  post "/plans/aws_charge" => "plans#aws_charge"

  post "/plans/ip_check" => "plans#unifi_valid_ip_check"

  get "/plans/upgrade_alert" => "plans#upgrade_alert"

  get "/:lang/plans/upgrade_alert" => "plans#upgrade_alert" 
  
  post "/plans/paytm_vpa_validate_data" => "plans#paytm_vpa_validate_data"
 

  #End of New plans pages related code starts here

 ##PLANS PAGES ROUTES ENDS HERE####


 ###SEARCH ROUTES STARTS HERE#################
 match "/search" => "search#index",via: [:get,:post]
 get "/:lang/search"  => "search#index"

 get "/search_list" => "search#search_list"
 get "/:lang/search_list"  => "search#search_list"
 # get "/search" => "search#index"

 ###SEARCH ROUTES ENDS HERE#################


 ##CATALOGS PAGES ROUTES STARTS HERE####
  get "/catalogs/add_home_list" => "catalogs#add_home_list"
  get "/catalogs/add_items_list" => "catalogs#add_items_list"
  get "/catalogs/season_episodes" => "catalogs#season_episodes"
  get "/catalogs/get_trailers" => "catalogs#get_trailers"


 get "/movies" => "catalogs#all_movies"
 get "/shows" => "catalogs#all_tvshows"
 get "/music" => "catalogs#all_music"
 get "/short-films" => "catalogs#all_short_films"
 get "/promotions/:category_name" => "catalogs#category_landing"
 get "/promotion1/:category_name" => "catalogs#promotion_landing_template2"
 get "/promotion/:category_name" => "catalogs#promotion_landing"
 get "/:lang/movies/:item_id" => "catalogs#item_details"
 get "/movies/:item_id" => "catalogs#item_details"


 get "/:lang/shows/:show_name/all_episodes" => "catalogs#all_episodes"
 get "/shows/:show_name/all_episodes" => "catalogs#all_episodes"

 get "/:lang/shows/:show_name/episodes" => "catalogs#episodes_list"
 get "/shows/:show_name/episodes" => "catalogs#episodes_list"
 
 get "/shows/:show_name/:item_id" => "catalogs#episode_details"


 #get "/gujarati-plays/:item_id" => "catalogs#item_details"
 #get "/:lang/gujarati-plays/:item_id" => "catalogs#item_details"


 #get "/marathi-plays/:item_id" => "catalogs#item_details"
 # get "/:lang/marathi-plays/:item_id" => "catalogs#item_details"

 # #get "/marathi-promos/:item_id" => "catalogs#item_details"
 # get "/:lang/marathi-promos/:item_id" => "catalogs#item_details"

 # #get "/mararhi-15min-movies/:item_id" => "catalogs#item_details"
 # get "/:lang/mararhi-15min-movies/:item_id" => "catalogs#item_details"

 # #get "/bollywood-plus-15min-movies/:item_id" => "catalogs#item_details"
 # get "/:lang/bollywood-plus-15min-movies/:item_id" => "catalogs#item_details"

 # #get "/bollywood-retro-15min-movies/:item_id" => "catalogs#item_details"
 # get "/:lang/bollywood-retro-15min-movies/:item_id" => "catalogs#item_details"

 # #get "/kids-rhymes/:item_id" => "catalogs#item_details"
 # get "/:lang/kids-rhymes/:item_id" => "catalogs#item_details"

 # #get "/gujarati-kids-rhymes/:item_id" => "catalogs#item_details"
 # get "/:lang/gujarati-kids-rhymes/:item_id" => "catalogs#item_details"

 # #get "/gujarati-video-songs/:item_id" => "catalogs#item_details"
 # get "/:lang/gujarati-video-songs/:item_id" => "catalogs#item_details"

 # #get "/editorji-catalog/:item_id" => "catalogs#item_details"
 # get "/:lang/editorji-catalog/:item_id" => "catalogs#item_details"

 # #get "/editorji-ipl/:item_id" => "catalogs#item_details"
 # get "/:lang/editorji-ipl/:item_id" => "catalogs#item_details"

 # #get "/kids-rhymes/:item_id" => "catalogs#item_details"
 # get "/:lang/kids-rhymes/:item_id" => "catalogs#item_details"

 # #get "/gujarati-kids-rhymes/:item_id" => "catalogs#item_details"
 # get "/:lang/gujarati-kids-rhymes/:item_id" => "catalogs#item_details"


 get "/:lang/shows/:item_id" => "catalogs#item_details"
 get "/shows/:item_id" => "catalogs#item_details"
 get "/music/:item_id" => "catalogs#item_details"
 get "/show/:catalog_name/collection" => "catalogs#all_items_list"
 get "/:lang/show/:catalog_name/collection" => "catalogs#all_items_list"

 get "/season/:season_id/collection" => "catalogs#seasons_items_list"
 get "/:lang/season/:season_id/collection" => "catalogs#seasons_items_list"


 get "/:lang/shows/:item_id" => "catalogs#item_details"
 get "/:lang/music/:item_id" => "catalogs#item_details"
 get "/:lang/short-films/:item_id" => "catalogs#item_details"
 get "/short-films/:item_id" => "catalogs#item_details"
 
 
 get "/:lang/:catalog_name/others/collection" => "catalogs#other_tvshows"

 
 
 get "/movie/:catalog_name/collection" => "catalogs#all_items_list"
 get "/livetv/:catalog_name/collection" => "catalogs#all_items_list"
 get "/video/:catalog_name/collection" => "catalogs#all_items_list"

 get "/:lang/movie/:catalog_name/collection" => "catalogs#all_items_list"
 get "/:lang/livetv/:catalog_name/collection" => "catalogs#all_items_list"
 get "/:lang/video/:catalog_name/collection" => "catalogs#all_items_list"


 get "/:catalog_name/collection" => "catalogs#all_items_list"
 get "/:catalog_name/:genre/collection" => "catalogs#genre_all_items"

 get "/:lang/:catalog_name/collection" => "catalogs#all_items_list"
 get "/:lang/:catalog_name/:genre/collection" => "catalogs#genre_all_items"

 get "/:lang/:catalog_name/:show_name" => "catalogs#item_details"

 get "/:lang/shows/:show_name/:item_id" => "catalogs#episode_details"
 get "/all_channels" => "catalogs#all_channels"
 get "/:lang/all_channels" => "catalogs#all_channels"
 get "/all_channels/:show_name" => "catalogs#item_details"
 
 #get "/:lang/:catalog_name" => "catalogs#show_catalog_item"
 get "/:catalog_name" => "catalogs#show_catalog_item"
 get "/:catalog_name/all" => "catalogs#all_items_list"
 get "/:lang/:catalog_name/all" => "catalogs#all_items_list"
 get "/:lang/:catalog_name/:show_name" => "catalogs#item_details"

 get "/:catalog_name/:show_name" => "catalogs#item_details"
 get "/:catalog_name/others/all" => "catalogs#other_tvshows"
 get "/:catalog_name/others/collection" => "catalogs#other_tvshows"
 get "/:lang/:catalog_name/:show_name/all_episodes" => "catalogs#all_episodes"
 get "/:catalog_name/:show_name/all_episodes" => "catalogs#all_episodes"
 get "/:catalog_name/:genre/all" => "catalogs#genre_all_items"
 get "/:catalog_name/:show_name/:item_name" => "catalogs#episode_details"

 ##CATALOGS PAGES ROUTES ENDS HERE####

end

