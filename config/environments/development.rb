Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Do not eager load code on boot.
  config.eager_load = false




  # Show full error reports.
  config.consider_all_requests_local = true

  # Enable/disable caching. By default caching is disabled.
  # Run rails dev:cache to toggle caching.
  if Rails.root.join('tmp', 'caching-dev.txt').exist?
    config.action_controller.perform_caching = true

    config.cache_store = :memory_store
    config.public_file_server.headers = {
      'Cache-Control' => "public, max-age=#{2.days.to_i}"
    }
  else
    config.action_controller.perform_caching = false

    #config.cache_store = :dalli_store
    #config.cache_store = :dalli_store, '127.0.0.1:11211'

  end

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local
 

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  config.action_mailer.perform_caching = false

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Highlight code that triggered database queries in logs.
  config.active_record.verbose_query_logs = true

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  # Use an evented file watcher to asynchronously detect changes in source code,
  # routes, locales, etc. This feature depends on the listen gem.
  config.file_watcher = ActiveSupport::EventedFileUpdateChecker

  #API_SERVER = "http://3.85.70.32:6060" #Development SERVER API END POINT
  API_SERVER = "http://3.85.70.32:3000"
  #API_SERVER = "http://staging.api.shemaroome.com" #STAGING SERVER API END POINT
  #API_SERVER = "https://prod.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  #API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER WEBSITE API END POINT
  #API_SERVER = "http://18.209.25.40" #PRE-PRODUCTION SERVER  API END POINT
  #API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  #API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  #API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  # API_SERVER = "http://3.85.70.32:3000" #Development/STAGING SERVER API END POINT
  # API_SERVER = "http://18.209.25.40:5050" #PRE-PRODUCTION SERVER  API END POINT
  #API_SERVER = "http://18.209.25.40" #PRE-PRODUCTION SERVER  API END POINT
  API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  # API_SERVER = "http://18.209.25.40"
  #API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  #API_SERVER = "http://18.209.25.40"
  #API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  #API_SERVER = "http://18.209.25.40"
  #API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  #API_SERVER = "http://18.209.25.40"
  #API_SERVER = "https://prod.web.api.shemaroome.com" #PRODUCTION SERVER APPS API END POINT
  #API_SERVER = "http://3.220.184.30"
  #API_SERVER = "https://18.209.25.40/" #PRODUCTION SERVER APPS API END POINT
  #API_SERVER = "http://18.209.25.40:6060"
   #API_SERVER = "http://3.88.100.197"
  #API_SERVER = "http://54.85.188.40/"

  AUTH_TOKEN = "3zZmzoHg8z6SM3wpDoyw"
  CONTEST_SERVER = "http://34.204.190.112:6060"
  CACHE_EXPIRY_TIME = 1.seconds
  PLAY_URL_TOKEN = "ywVXaTzycwZ8agEs3ujx"
  SITE = "http://localhost:3000"
  #SITE = "http://54.144.222.119:3001"
  PLATFORM_TYPE = "web"
  SITE = "http://localhost:3000"
  #SITE = "http://54.144.222.119:3004"
  PAYTM_UPI_URL = "https://securegw-stage.paytm.in" #PRE PRODUCTION
  #PAYTM_UPI_URL = "https://securegw.paytm.in"# PRODUCTION
  ENV['FACEBOOK_APP_KEY'] = "349483068579253"
  LZYPY_BASE_URL = "https://api.lazypay.in"
  LZYPY_ACCESS_KEY = "EQO3TO2XDRWMHGZNUEJR"
  LZYPY_SECRET_KEY = "4e48094e354e229e34f9ba45519d0d5453fa21fa"
  AMZ_SANDBOX = "true"
  AMZ_CLIENT_ID = "amzn1.application-oa2-client.44766ffeadb349bcbfc3b366f2e295b5"
  AMZ_CLIENT_SECRET = "612947353006f81ec9eeb3208c2874635118af9c35649f9187f77af2a15d2ec3" 
  AMZ_MERCHANT_ID = "AZ4WQCLDT2DF0"
  AMZ_ACCESS_KEY = "f57bbc68-fa6a-4c5e-a281-d39db51a26c6"
  AMZ_SECRET_KEY = "DXhOoPOgYdJLYgRzNtV4sPe10DRIzaGE526oYdguMdM0SsfXSJOsmlUr5-SHAWMr"
  SHIPPING_ZIP_CODES = ["07001","07002","007003","07004","07005","07006","07007","07008","07009","07010","07011","07012","07013","07014","07015","07016","07017","07018","07019","07020","07021","07022","07023","07024","07026","07027","07028","07029","07030","07031","07032","07033","07034","07035","07036","07039","07040","07041","07042","07043","07044","07045","07046","07047","07050","07051","07052","07054","07055","07057","07058","07059","07060","07061","07062","07063","07064","07065","07066","07067","07068","07069","07070","07071","07072","07073","07074","07075","07076","07077","07078","07079","07080","07081","07082","07083","07086","07087","07088","07090","07091","07092","07093","07094","07095","07096","07097","07099","07101","07102","07103","07104","07105","07106","07107","07108","07109","07110","07111","07112","07114","07175","07182","07184","07188","07189","07191","07192","07193","07194","07195","07198","07199","07201","07202","07203","07204","07205","07206","07207","07208","07302","07303","07304","07305","07306","07307","07308","07309","07310","07311","07395","07399","07401","07403","07405","07407","07410","07416","07417","07418","07419","07420","07421","07422","07423","07424","07428","07430","07432","07435","07436","07438","07439","07440","07442","07444","07446","07450","07451","07452","07456","07457","07458","07460","07461","07462","07463","07465","07470","07474","07477","07480","07481","07495","07501","07502","07503","07504","07505","07506","07507","07508","07509","07510","07511","07512","07513","07514","07522","07524","07533","07538","07543","07544","07601","07602","07603","07604","07605","07606","07607","07608","07620","07621","07624","07626","07627","07628","07630","07631","07632","07640","07641","07642","07643","07644","07645","07646","07647","07648","07649","07650","07652","07653","07656","07657","07660","07661","07662","07663","07666","07670","07675","07676","07677","07699","07701","07702","07703","07704","07709","07710","07711","07712","07715","07716","07717","07718","07719","07720","07721","07722","07723","07724","07726","07727","07728","07730","07731","07732","07733","07734","07735","07737","07738","07739","07740","07746","07747","07748","07750","07751","07752","07753","07754","07755","07756","07757","07758","07760","07762","07763","07764","07765","07777","07799","07801","07802","07803","07806","07820","07821","07822","07823","07825","07826","07827","07828","07829","07830","07831","07832","07833","07834","07836","07837","07838","07839","07840","07842","07843","07844","07845","07846","07847","07848","07849","07850","07851","07852","07853","07855","07856","07857","07860","07863","07865","07866","07869","07870","07871","07874","07875","07876","07877","07878","07879","07880","07881","07882","07885","07890","07901","07902","07920","07921","07922","07924","07926","07927","07928","07930","07931","07932","07933","07934","07935","07936","07938","07939","07940","07945","07946","07950","07950","07960","07961","07962","07963","07970","07974","07976","07977","07978","07979","07980","07981","07983","07999","08001","08002","08003","08004","08005","08006","08007","08008","08009","08010","08011","08012","08014","08015","08016","08018","08019","08020","08021","08022","08023","08025","08026","08027","08028","08029","08030","08031","08032","08033","08034","08035","08036","08037","08038","08039","08041","08042","08043","08045","08046","08048","08049","08050","08051","08052","08053","08054","08055","08056","08057","08059","08060","08061","08062","08063","08064","08065","08066","08067","08068","08069","08070","08071","08072","08073","08074","08075","08076","08077","08078","08079","08080","08081","08083","08084","08085","08086","08087","08088","08089","08090","08091","08092","08093","08094","08095","08096","08096","08097","08098","08099","08101","08102","08103","08104","08104","08105","08106","08107","08107","08108","08108","08109","08110","08201","08202","08203","08204","08205","08210","08212","08213","08214","08215","08217","08218","08219","08220","08221","08223","08224","08225","08226","08230","08231","08232","08234","08240","08241","08242","08243","08244","08245","08246","08247","08248","08250","08251","08252","08260","08270","08302","08310","08311","08312","08313","08314","08315","08316","08317","08318","08319","08320","08321","08322","08323","08324","08326","08327","08328","08329","08330","08332","08340","08341","08342","08343","08344","08345","08346","08347","08348","08349","08350","08352","08353","08360","08361","08362","08401","08402","08403","08404","08405","08406","08501","08502","08504","08505","08510","08511","08512","08514","08515","08518","08520","08525","08526","08527","08528","08530","08533","08534","08535","08536","08540","08541","08542","08543","08544","08550","08551","08553","08554","08555","08556","08557","08558","08559","08560","08561","08562","08601","08602","08603","08604","08605","08606","08607","08608","08609","08610","08611","08618","08619","08620","08625","08628","08629","08638","08640","08641","08645","08646","08647","08648","08650","08666","08677","08690","08691","08695","08701","08720","08721","08722","08723","08724","08730","08731","08732","08733","08734","08735","08736","08738","08739","08740","08741","08742","08750","08751","08752","08753","08754","08755","08756","08757","08758","08759","08801","08802","08803","08804","08805","08807","08808","08809","08810","08812","08816","08817","08818","08820","08821","08822","08823","08824","08825","08826","08827","08828","08829","08830","08831","08832","08833","08834","08835","08836","08837","08840","08844","08846","08848","08850","08852","08853","08854","08855","08857","08858","08859","08861","08862","08863","08865","08867","08868","08869","08870","08871","08872","08873","08875","08876","08879","08880","08882","08884","08885","08886","08887","08888","08889","08890","08896","08899","08901","08902","08903","08904","08905","08906","08922","08933","08988","08989"]
  #  ACTV_CLIENT_ID = "5b5f9bad11174890bc61ac93b86a015f"
  #  ACTV_CLIENT_SECRET = "763cd820f0314898806B1d062A1793dA"
  # ACTV_BASE_URL = "http://xp-voucher-stg-sg-v1.sg-s1.cloudhub.io/api"
  ACTV_CLIENT_ID = "ec9fafaa458747d6ac31ec4cd20491b9"
  ACTV_CLIENT_SECRET = "bA078327b93b491585cD30B3F7f49B41"
  ACTV_BASE_URL = "https://voucher.ap.edenred.io/v1"
  UNIFY_IP_URL = "https://nocgrid.tm.com.my/api/ott/shemaroo"
  #OTM_URL = "http://omgdev.hypp.tv/WebService/OMGSystem.asmx"
  OTM_URL = "http://omg.hypp.tv/WebService/OMGSystem.asmx"
end
