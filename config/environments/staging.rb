Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # Code is not reloaded between requests.
  config.cache_classes = true

  # Eager load code on boot. This eager loads most of Rails and
  # your application in memory, allowing both threaded web servers
  # and those relying on copy on write to perform better.
  # Rake tasks automatically ignore this option for performance.
  config.eager_load = true

  # Full error reports are disabled and caching is turned on.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Ensures that a master key has been made available in either ENV["RAILS_MASTER_KEY"]
  # or in config/master.key. This key is used to decrypt credentials (and other encrypted files).
  # config.require_master_key = true

  # Disable serving static files from the `/public` folder by default since
  # Apache or NGINX already handles this.
  config.public_file_server.enabled = ENV['RAILS_SERVE_STATIC_FILES'].present?

  # Compress JavaScripts and CSS.
  # config.assets.js_compressor = :uglifier
  config.assets.js_compressor = Uglifier.new(harmony: true)
  # config.assets.css_compressor = :sass

  # Do not fallback to assets pipeline if a precompiled asset is missed.
  config.assets.compile = true

  # `config.assets.precompile` and `config.assets.version` have moved to config/initializers/assets.rb

  # Enable serving of images, stylesheets, and JavaScripts from an asset server.
  # config.action_controller.asset_host = 'http://assets.example.com'

  # Specifies the header that your server uses for sending files.
  # config.action_dispatch.x_sendfile_header = 'X-Sendfile' # for Apache
  # config.action_dispatch.x_sendfile_header = 'X-Accel-Redirect' # for NGINX

  # Store uploaded files on the local file system (see config/storage.yml for options)
  config.active_storage.service = :local

  # Mount Action Cable outside main process or domain
  # config.action_cable.mount_path = nil
  # config.action_cable.url = 'wss://example.com/cable'
  # config.action_cable.allowed_request_origins = [ 'http://example.com', /http:\/\/example.*/ ]

  # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
  # config.force_ssl = true

  # Use the lowest log level to ensure availability of diagnostic information
  # when problems arise.
  config.log_level = :debug

  # Prepend all log lines with the following tags.
  config.log_tags = [ :request_id ]

  # Use a different cache store in production.
  # config.cache_store = :mem_cache_store

  # Use a real queuing backend for Active Job (and separate queues per environment)
  # config.active_job.queue_adapter     = :resque
  # config.active_job.queue_name_prefix = "shemaroo_#{Rails.env}"

  config.action_mailer.perform_caching = false

  # Ignore bad email addresses and do not raise email delivery errors.
  # Set this to true and configure the email server for immediate delivery to raise delivery errors.
  # config.action_mailer.raise_delivery_errors = false

  # Enable locale fallbacks for I18n (makes lookups for any locale fall back to
  # the I18n.default_locale when a translation cannot be found).
  config.i18n.fallbacks = true

  # Send deprecation notices to registered listeners.
  config.active_support.deprecation = :notify

  # Use default logging formatter so that PID and timestamp are not suppressed.
  config.log_formatter = ::Logger::Formatter.new

  # Use a different logger for distributed setups.
  # require 'syslog/logger'
  # config.logger = ActiveSupport::TaggedLogging.new(Syslog::Logger.new 'app-name')

  if ENV["RAILS_LOG_TO_STDOUT"].present?
    logger           = ActiveSupport::Logger.new(STDOUT)
    logger.formatter = config.log_formatter
    config.logger    = ActiveSupport::TaggedLogging.new(logger)
  end
  config.cache_store = :dalli_store


  # Do not dump schema after migrations.
  config.active_record.dump_schema_after_migration = false
  #API_SERVER = "http://18.210.75.7"
  NEW_API_SERVER = "http://18.210.75.7:8080"
  API_SERVER = "http://3.85.70.32:3000"
  AUTH_TOKEN = "3zZmzoHg8z6SM3wpDoyw"
  CACHE_EXPIRY_TIME = 120.seconds
  PLAY_URL_TOKEN = "ywVXaTzycwZ8agEs3ujx"
  SITE = "http://54.221.118.191"
  ENV['FACEBOOK_APP_KEY'] = "1963321883746360"
  LZYPY_BASE_URL = "https://sboxapi.lazypay.in"
  LZYPY_ACCESS_KEY = "R26Z1KW2GJUO19SLBDV9"
  LZYPY_SECRET_KEY = "d756fa79d766ac74d9b666439c6129678bf8e81d"
  AMZ_CLIENT_ID = "amzn1.application-oa2-client.44766ffeadb349bcbfc3b366f2e295b5"
  AMZ_CLIENT_SECRET = "612947353006f81ec9eeb3208c2874635118af9c35649f9187f77af2a15d2ec3" 
  AMZ_MERCHANT_ID = "AZ4WQCLDT2DF0"
  AMZ_ACCESS_KEY = "f57bbc68-fa6a-4c5e-a281-d39db51a26c6"
  AMZ_SECRET_KEY = "DXhOoPOgYdJLYgRzNtV4sPe10DRIzaGE526oYdguMdM0SsfXSJOsmlUr5-SHAWMr"
  SHIPPING_ZIP_CODES = ["7001","7002","7003","7004","7005","7006","7007","7008","7009","7010","7011","7012","7013","7014","7015","7016","7017","7018","7019","7020","7021","7022","7023","7024","7026","7027","7028","7029","7030","7031","7032","7033","7034","7035","7036","7039","7040","7041","7042","7043","7044","7045","7046","7047","7050","7051","7052","7054","7055","7057","7058","7059","7060","7061","7062","7063","7064","7065","7066","7067","7068","7069","7070","7071","7072","7073","7074","7075","7076","7077","7078","7079","7080","7081","7082","7083","7086","7087","7088","7090","7091","7092","7093","7094","7095","7096","7097","7099","7101","7102","7103","7104","7105","7106","7107","7108","7109","7110","7111","7112","7114","7175","7182","7184","7188","7189","7191","7192","7193","7194","7195","7198","7199","7201","7202","7203","7204","7205","7206","7207","7208","7302","7303","7304","7305","7306","7307","7308","7309","7310","7311","7395","7399","7401","7403","7405","7407","7410","7416","7417","7418","7419","7420","7421","7422","7423","7424","7428","7430","7432","7435","7436","7438","7439","7440","7442","7444","7446","7450","7451","7452","7456","7457","7458","7460","7461","7462","7463","7465","7470","7474","7477","7480","7481","7495","7501","7502","7503","7504","7505","7506","7507","7508","7509","7510","7511","7512","7513","7514","7522","7524","7533","7538","7543","7544","7601","7602","7603","7604","7605","7606","7607","7608","7620","7621","7624","7626","7627","7628","7630","7631","7632","7640","7641","7642","7643","7644","7645","7646","7647","7648","7649","7650","7652","7653","7656","7657","7660","7661","7662","7663","7666","7670","7675","7676","7677","7699","7701","7702","7703","7704","7709","7710","7711","7712","7715","7716","7717","7718","7719","7720","7721","7722","7723","7724","7726","7727","7728","7730","7731","7732","7733","7734","7735","7737","7738","7739","7740","7746","7747","7748","7750","7751","7752","7753","7754","7755","7756","7757","7758","7760","7762","7763","7764","7765","7777","7799","7801","7802","7803","7806","7820","7821","7822","7823","7825","7826","7827","7828","7829","7830","7831","7832","7833","7834","7836","7837","7838","7839","7840","7842","7843","7844","7845","7846","7847","7848","7849","7850","7851","7852","7853","7855","7856","7857","7860","7863","7865","7866","7869","7870","7871","7874","7875","7876","7877","7878","7879","7880","7881","7882","7885","7890","7901","7902","7920","7921","7922","7924","7926","7927","7928","7930","7931","7932","7933","7934","7935","7936","7938","7939","7940","7945","7946","7950","7950","7960","7961","7962","7963","7970","7974","7976","7977","7978","7979","7980","7981","7983","7999","8001","8002","8003","8004","8005","8006","8007","8008","8009","8010","8011","8012","8014","8015","8016","8018","8019","8020","8021","8022","8023","8025","8026","8027","8028","8029","8030","8031","8032","8033","8034","8035","8036","8037","8038","8039","8041","8042","8043","8045","8046","8048","8049","8050","8051","8052","8053","8054","8055","8056","8057","8059","8060","8061","8062","8063","8064","8065","8066","8067","8068","8069","8070","8071","8072","8073","8074","8075","8076","8077","8078","8079","8080","8081","8083","8084","8085","8086","8087","8088","8089","8090","8091","8092","8093","8094","8095","8096","8096","8097","8098","8099","8101","8102","8103","8104","8104","8105","8106","8107","8107","8108","8108","8109","8110","8201","8202","8203","8204","8205","8210","8212","8213","8214","8215","8217","8218","8219","8220","8221","8223","8224","8225","8226","8230","8231","8232","8234","8240","8241","8242","8243","8244","8245","8246","8247","8248","8250","8251","8252","8260","8270","8302","8310","8311","8312","8313","8314","8315","8316","8317","8318","8319","8320","8321","8322","8323","8324","8326","8327","8328","8329","8330","8332","8340","8341","8342","8343","8344","8345","8346","8347","8348","8349","8350","8352","8353","8360","8361","8362","8401","8402","8403","8404","8405","8406","8501","8502","8504","8505","8510","8511","8512","8514","8515","8518","8520","8525","8526","8527","8528","8530","8533","8534","8535","8536","8540","8541","8542","8543","8544","8550","8551","8553","8554","8555","8556","8557","8558","8559","8560","8561","8562","8601","8602","8603","8604","8605","8606","8607","8608","8609","8610","8611","8618","8619","8620","8625","8628","8629","8638","8640","8641","8645","8646","8647","8648","8650","8666","8677","8690","8691","8695","8701","8720","8721","8722","8723","8724","8730","8731","8732","8733","8734","8735","8736","8738","8739","8740","8741","8742","8750","8751","8752","8753","8754","8755","8756","8757","8758","8759","8801","8802","8803","8804","8805","8807","8808","8809","8810","8812","8816","8817","8818","8820","8821","8822","8823","8824","8825","8826","8827","8828","8829","8830","8831","8832","8833","8834","8835","8836","8837","8840","8844","8846","8848","8850","8852","8853","8854","8855","8857","8858","8859","8861","8862","8863","8865","8867","8868","8869","8870","8871","8872","8873","8875","8876","8879","8880","8882","8884","8885","8886","8887","8888","8889","8890","8896","8899","8901","8902","8903","8904","8905","8906","8922","8933","8988","8989"]

end
