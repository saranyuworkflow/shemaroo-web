module LazyPay
  def lazpy_eligibil_check(mob_no,email_id,amt,category,base_url,acc_key,sec_key,otp_st)
    begin
      logger.info "================lazypay eligibility check==============="
      # amount = "250.00"
      # mobile = "9136920201"
      # email = "revenue@shemaroome.com"
      amount = amt
      mobile = mob_no
      email = email_id
      currency = "INR"
      host_url = "#{base_url}/api/lazypay/v2/payment/eligibility"
      #data = mobile + email + amount + currency
      data = mobile + amount + currency
      request_data = {
        "userDetails":
        {
          "mobile": mobile
           #"email": email
        },
        "amount": {
          "value": amount,
          "currency": "INR"
        }
      }
      signature = OpenSSL::HMAC.hexdigest("SHA1", sec_key, data)
      p request_data.inspect
      headers = {"Content-Type" => "application/json","accessKey" => acc_key,"signature" => signature}
      eligb_req = Typhoeus::Request.post(host_url, :body => request_data.to_json,:headers => headers, :ssl_verifypeer => false)
      elig_resp = JSON.parse(eligb_req.body)
      puts "Eligibility Response :: #{elig_resp} \n\n"
      # {"txnEligibility"=>true, "reason"=>"Hola!! Avail LazyPay Credit with just an OTP", "code"=>"LP_ELIGIBLE", "userEligibility"=>true, "eligibilityResponseId"=>"baae3d58-741f-293f-15ce-d3d02f3ab638", "customParams"=>nil, "autoDebit"=>false, "merchantLogo"=>nil, "addressReq"=>false, "firstNameReq"=>false, "lastNameReq"=>false, "signUpModes"=>nil, "dueDate"=>"03 February 2020", "repayConfirmation"=>false, "emailRequired"=>false} 
      if elig_resp["code"] == "LP_ELIGIBLE" && otp_st == "true"
        elg_resp_id = elig_resp["eligibilityResponseId"]
        sub_req_id = ""
        sub_re_er_code = ""
        sub_res = sub_create_lazypy(elg_resp_id,mob_no,email_id,amt,category,base_url,acc_key,sec_key)
        if sub_res.has_key?("error")
          sub_req_err = sub_res["message"]
          sub_re_er_code = sub_res["errorCode"]
        end
        p sub_res.inspect
        p sub_res.has_key?("error").inspect
        p "@@@@@@@@@@@@@@2"
        sub_req_id = sub_res["subscriptionId"]
        # int_res = int_pay_lazypy(elg_resp_id,mob_no,email_id,amt,base_url,acc_key,sec_key)
        # tran_ref_id = int_res["txnRefNo"]
        final_resp = {"elgres_id" => elg_resp_id,"subreq_id" => sub_req_id,"tranref_id" => "","code" => "LP_ELIGIBLE","sub_err_code" => sub_re_er_code,"sub_req_er_msg" => sub_req_err}
        return final_resp
      else
        return elig_resp
      end
     rescue
      elig_resp = {"code" => "API_ERROR","reason" => "Sorry something went wrong"}
      return elig_resp
     end
  end

  def sub_create_lazypy(elig_res_id,mob_no,email_id,amt,category,base_url,acc_key,sec_key)
    sub_host_url = "#{base_url}/api/subscriptions/create"
    chrg_st_dt = (Date.today).strftime("%Y-%m-%d")
    chrg_ed_dt = ((Date.today)+3.year).strftime("%Y-%m-%d")
    sub_dt = mob_no + email_id + amt + "INR" + chrg_st_dt
    sub_sign = OpenSSL::HMAC.hexdigest("SHA1", sec_key, sub_dt)
    category = "ANNUALLY" if category == "YEARLY"
    sub_req_dt = {
      "mobile": mob_no,
      # "email": email_id,
      "amount": {
        "value": amt,
        "currency": "INR"
      },
      "frequency": category,
      "bufferTimeInDays": "1",
      "subscriptionType": "MERCHANT",
      "chargeStartDate": chrg_st_dt,
      "chargeEndDate": chrg_ed_dt
    }
   headers = {"Content-Type" => "application/json","accessKey" => acc_key,"signature" => sub_sign}
   p sub_req_dt.inspect
   p "@@@@@@@@@@@@@@@@@@@@@@@@2"
   sub_req = Typhoeus::Request.post(sub_host_url, :body => sub_req_dt.to_json,:headers => headers, :ssl_verifypeer => false)
   sub_resp = JSON.parse(sub_req.body)
   #{"subscriptionId"=>"89fc7c5c-92b3-49df-94f2-a741fe5b428f", "dueDate"=>"03 February 2020"} 
   puts "Create subscription Response:: #{sub_resp} \n\n"  
   return sub_resp 
  end

  def int_pay_lazypy(el_res_id,mob_no,email_id,amt,base_url,acc_key,sec_key)
    rand_no =  SecureRandom.hex
    payment_init_dt = {
      "eligibilityResponseId": el_res_id,
      "merchantTxnId": rand_no,
      "userDetails": {
      "mobile": mob_no
      # "email": email_id
      }, 
      "amount": {
        "value": amt,
        "currency": "INR"
      }
    }
    py_init_dt = "merchantAccessKey=#{acc_key}&transactionId=#{rand_no}&amount=#{amt}"
    py_init_sign = OpenSSL::HMAC.hexdigest("SHA1", sec_key, py_init_dt)
    py_init_url = "#{base_url}/api/lazypay/v2/payment/initiate"
    headers = {"Content-Type" => "application/json","accessKey" => acc_key,"signature" => py_init_sign}
    py_in_req = Typhoeus::Request.post(py_init_url, :body => payment_init_dt.to_json,:headers => headers, :ssl_verifypeer => false)
    py_in_res = JSON.parse(py_in_req.body)
    #{"customParams"=>nil, "txnRefNo"=>"TFBUWDEwMDk1NTU0NQ==", "paymentModes"=>["OTP", "AUTO_DEBIT"], "lpTxnId"=>"LPTX100955545", "checkoutPageUrl"=>nil, "responseData"=>nil, "dueDate"=>"03 February 2020", "repayConfirmation"=>true}
    puts "Initiate Pay Response:: #{py_in_res} \n\n"  
    return py_in_res
  end

  def sub_otp_valid_lazypay(lp_sub_id,mobile_no,amt,mob_otp,base_url,acc_key,sec_key)
    sub_otp =  mobile_no+lp_sub_id+mob_otp
    otp_signa_gen = OpenSSL::HMAC.hexdigest("SHA1", sec_key, sub_otp)
    otp_valid_req = {"mobile": mobile_no, "subscriptionId": lp_sub_id, "otp": mob_otp}
    headers = {"Content-Type" => "application/json","accessKey" => acc_key,"signature" => otp_signa_gen}
    pymt_otp_sub_url = "#{base_url}/api/subscriptions/validate"
    p headers.inspect
    p otp_valid_req.inspect
    py_otp_res = Typhoeus::Request.post(pymt_otp_sub_url, :body => otp_valid_req.to_json,:headers => headers, :ssl_verifypeer => false)
    py_pd_resp = JSON.parse(py_otp_res.body)
    # if py_pd_resp.has_key?("token")
    #  lazypay_sub_execute(lp_sub_id,py_pd_resp["token"],mobile_no,amt,base_url,acc_key,sec_key)
    # end
    #{\"subscriptionId\"=>\"398ce829-4f1e-4647-8686-91bedae9de2a\", \"mobile\"=>\"9136920201\", \"token\"=>\"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJTSV9TVUJTQ1JJUFRJT04iLCJJRCI6IjM5OGNlODI5LTRmMWUtNDY0Ny04Njg2LTkxYmVkYWU5ZGUyYSIsIkFDQ0VTU19LRVkiOiJSMjZaMUtXMkdKVU8xOVNMQkRWOSIsIk1PQklMRSI6IjkxMzY5MjAyMDEifQ.qE6NNzrWSBjuAYPV2M2EVBI4LzkLaQZ8jDi1p23OpEY\", \"invoice\"=>{\"invoiceId\"=>\"315\", \"invoiceRaiseDate\"=>\"2020-02-03 10:30:48\", \"invoiceChargeDate\"=>\"2020-02-03 10:30:48\", \"lazypayTxnId\"=>\"LPTX100956536\", \"amount\"=>\"49.0\", \"subscriptionId\"=>\"398ce829-4f1e-4647-8686-91bedae9de2a\", \"invoiceStatus\"=>\"SUCCESS\"}}
    puts "Payment Otp validation:: #{py_pd_resp} \n\n"
    return py_pd_resp
  end

 

 
  def lazypay_sub_execute(sub_id,authr_token,mobile_no,amt,base_url,acc_key,sec_key)
    sub_exe_req = {"subscriptionId": sub_id,"mobile": mobile_no,"amount": {"value": amt,"currency": "INR"}}
    headers = {"Content-Type" => "application/json","Authorization" => authr_token,"accessKey" => acc_key}
    lazy_sub_url = "#{base_url}/api/subscriptions/execute"
    p sub_exe_req.inspect
    p "headers are here-----"
    p headers.inspect
    sub_exe_reqst = Typhoeus::Request.post(lazy_sub_url, :body => sub_exe_req.to_json,:headers => headers, :ssl_verifypeer => false)
    sub_exe_resp = JSON.parse(sub_exe_reqst.body)
    puts "Payment Subscription execute:: #{sub_exe_resp} \n\n"
    return sub_exe_resp
  end


  def otp_valid_lazypy(lp_tran_id,mob_otp,base_url,acc_key,sec_key)
    otp_sign = "merchantAccessKey=#{acc_key}&txnRefNo=#{lp_tran_id}"
    otp_pay_sign = OpenSSL::HMAC.hexdigest("SHA1", sec_key, otp_sign)
    pymt_otp_req = {"paymentMode": "OTP", "txnRefNo": lp_tran_id, "otp": mob_otp}
    headers = {"Content-Type" => "application/json","signature" => otp_pay_sign}
    pymt_paid_url = "#{base_url}/api/lazypay/v0/payment/pay"
    py_pd_res = Typhoeus::Request.post(pymt_paid_url, :body => pymt_otp_req.to_json,:headers => headers, :ssl_verifypeer => false)
    py_pd_resp = JSON.parse(py_pd_res.body)
    #{\"transactionId\"=>\"LPTX100955596\", \"merchantOrderId\"=>\"43b0920d842cc0f1b661b1ff95cd2e2c\", \"amount\"=>251.0, \"currency\"=>\"INR\", \"signature\"=>\"d29f2b6cd9c5c249ff9c605a5ab50bf9a1631ca5\", \"responseData\"=>{\"OTP\"=>{\"reason\"=>\"Transaction is successful\", \"pgResponseCode\"=>\"0\", \"status\"=>\"SUCCESS\"}}, \"customParams\"=>nil, \"productSkuDetails\"=>nil, \"userDetails\"=>{\"firstName\"=>nil, \"lastName\"=>nil, \"mobile\"=>\"9136920201\", \"email\"=>\"revenue@shemaroome.com\"}}
    puts "Payment OTP success Response:: #{py_pd_resp} \n\n"  
    return py_pd_resp
  end

end