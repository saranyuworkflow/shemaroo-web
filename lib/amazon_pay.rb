module AmazonPay

def getSignatureKey(key, dateStamp, regionName, serviceName)
  kDate    = OpenSSL::HMAC.digest('sha384', "AWS4"+key, dateStamp)
  kRegion  = OpenSSL::HMAC.digest('sha384', kDate, regionName)
  kService = OpenSSL::HMAC.digest('sha384', kRegion, serviceName)
  kSigning = OpenSSL::HMAC.digest('sha384', kService, "aws4_request")
  kSigning
end

#Username: Jayant.kumar@shemaroo.com

#Password:  logitech@123
#https://stuff-things.net/2007/06/11/encrypting-sensitive-data-with-ruby-on-rails/


def get_access_token(code,amz_client_id,amz_secret_id)
	p amz_client_id.inspect
	url = "https://api.amazon.co.uk/auth/o2/token"
	body = {}
	body['grant_type'] = "authorization_code"
	body['code'] = code
	body['client_id'] = amz_client_id
	body['client_secret'] = amz_secret_id
	#body['redirect_uri'] = "https://preprod.shemaroome.com/payment/amazonpay"
	body['redirect_uri'] = "https://www.shemaroome.com/payment/amazonpay"
	p body.inspect
	response = Typhoeus::Request.post(url,:body => body,:timeout => 1000,headers: {'Content-Type'=> "application/x-www-form-urlencoded"})
	data = JSON.parse response.body
	p data.inspect
	return data
end

def get_response can_request,req_parameters,amzdate,datestamp,headers,host,api,amz_sec_key
	puts "=========================================API :: #{api}============================================"
	#secret_key = "DXhOoPOgYdJLYgRzNtV4sPe10DRIzaGE526oYdguMdM0SsfXSJOsmlUr5-SHAWMr"
	secret_key = amz_sec_key
	region = 'eu-west-1'
	service = 'AmazonPay'

	str1 = OpenSSL::Digest.new("sha384").hexdigest(can_request)
	string_to_sign = ["AWS4-HMAC-SHA384","#{amzdate}","#{datestamp}/eu-west-1/AmazonPay/aws4_request","#{str1}"].join("\n")
	sign_key = getSignatureKey(secret_key, "#{datestamp}", region, service)
	logger.info "sign_key -----#{sign_key}--------------------"
	signature = OpenSSL::HMAC.digest('sha384', sign_key,string_to_sign)
	logger.info "signature------#{signature.inspect}"
	signature = Base64.urlsafe_encode64(signature)
	req_parameters['signature'] = signature
	req_json = req_parameters.to_json
	key = SecureRandom.random_bytes(16)
	iv =  SecureRandom.random_bytes(16)
	cipher = OpenSSL::Cipher::AES.new(128, :GCM).encrypt
	cipher.padding=0
	cipher.key_len=16
	cipher.key = key
	cipher.iv_len=16
	cipher.iv = iv
	cipher_data = cipher.update(req_json) + cipher.final + cipher.auth_tag
	payload_data = Base64.urlsafe_encode64(cipher_data)

	#public_key = "-----BEGIN PUBLIC KEY-----\n"+"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq92yAzXaCQbGIid0mMBfulkGK8HqvAardDowtgbfGUZ+hIx6lhYKFMrluTr7bIlQ4qgJY85c9adkZSxHtr/DhTV/ch5CCHDET3YC/DaFTKDp5t2uHKQAIb2Rl/73HQOd/pgImTiaLHPBr/gyz4iztYmlJQIm0vVuPktIANDGpK8qhizdztA3as1bLtILQZ5VtOjNn/xl1HQ+JDtBhUVr13BuJPosecQz6ouhEtR+5i/grg6sUzayqPD1dY6AGRLR9ao/6DCeHT5arSYjlkx6BECuKoiARo7ItDfLameXJ1gLd8lkMzArIG275jbxAiPd4OchHEfcqBADYB51FYDTwQIDAQAB\n"+"-----END PUBLIC KEY-----"

	#rsa_public_key = OpenSSL::PKey::RSA.new(public_key)
	public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq92yAzXaCQbGIid0mMBfulkGK8HqvAardDowtgbfGUZ+hIx6lhYKFMrluTr7bIlQ4qgJY85c9adkZSxHtr/DhTV/ch5CCHDET3YC/DaFTKDp5t2uHKQAIb2Rl/73HQOd/pgImTiaLHPBr/gyz4iztYmlJQIm0vVuPktIANDGpK8qhizdztA3as1bLtILQZ5VtOjNn/xl1HQ+JDtBhUVr13BuJPosecQz6ouhEtR+5i/grg6sUzayqPD1dY6AGRLR9ao/6DCeHT5arSYjlkx6BECuKoiARo7ItDfLameXJ1gLd8lkMzArIG275jbxAiPd4OchHEfcqBADYB51FYDTwQIDAQAB"
	rsa_public_key = OpenSSL::PKey::RSA.new(Base64.decode64(public_key))
	encrypted_string = rsa_public_key.public_encrypt(key, OpenSSL::PKey::RSA::PKCS1_OAEP_PADDING)
	encrypted_key = Base64.urlsafe_encode64(encrypted_string)
	iv = Base64.urlsafe_encode64(iv)

	body_data = {payload: payload_data,iv: iv ,key: encrypted_key}
	
	puts "Canonical Request :: #{can_request} \n\n"
	puts "String to sign In :: #{string_to_sign} \n\n"
	puts "Request Parameters :: #{req_parameters} \n\n"
	puts "Request JSON :: #{req_json} \n\n"
	puts "Signature :: #{signature} \n\n"
	puts "Payload :: #{payload_data} \n\n"
	puts "Body Data :: #{body_data} \n\n"

	response = Typhoeus::Request.post("#{host}", :body => body_data.to_json,:headers => headers, :ssl_verifypeer => false)
	response = JSON.parse(response.body)
	puts "Response :: #{response} \n\n"
	return response
end

def get_lat_token current_epoch_time, amzdate, datestamp, access_token,merchant_id, token,amz_acc_key,amz_sec_key
	service = 'AmazonPay'
	host = 'https://amazonpay.amazon.in/link/login/v1'
	region = 'eu-west-1'
	#secret_key = "DXhOoPOgYdJLYgRzNtV4sPe10DRIzaGE526oYdguMdM0SsfXSJOsmlUr5-SHAWMr"
	secret_key = amz_sec_key
	mapping_id = SecureRandom.hex(10)
	#accesskey_id = "f57bbc68-fa6a-4c5e-a281-d39db51a26c6"
	accesskey_id = amz_acc_key
	#Canonical Requrest
	can_request = ["POST","amazonpay.amazon.in/link/login/v1","accessKeyId=#{accesskey_id}&customerIdType=CONSENT_TOKEN&customerIdValue=#{access_token}&mappingId=#{mapping_id}&merchantId=#{merchant_id}&signatureMethod=HmacSHA384&signatureVersion=4&timeStamp=#{current_epoch_time}"].join("\n")
	#Request Parameters
	req_parameters = {"timeStamp":"#{current_epoch_time}","mappingId":"#{mapping_id}","accessKeyId":"#{accesskey_id}","customerIdValue":"#{token}","merchantId":"#{merchant_id}","signatureMethod":"HmacSHA384","customerIdType":"CONSENT_TOKEN","signatureVersion":"4"}
	#headers
	headers = { "Content-Type" => "application/json", "Accept"=>"application/json","merchantId" => "#{merchant_id}","isSandbox" => "false"}
	#Get Response
	api = "Lat Token "
	response = get_response can_request,req_parameters,amzdate,datestamp,headers,host,api,amz_sec_key
	lat_token = response['response']['lookAheadToken']
	return lat_token
end

def get_encoded_values values
	output = []
	values.each do |v|
		output << URI.encode_www_form_component(v)
	end
	return output
end

def get_process_charge_response current_epoch_time,amzdate,datestamp,access_token,merchant_id,token,amt,merch_trans_id,sandbox_mode,amz_acc_key,amz_sec_key
	#accesskey_id = "f57bbc68-fa6a-4c5e-a281-d39db51a26c6"
	accesskey_id = amz_acc_key
	host = 'https://amazonpay.amazon.in/payment/charge/AMAZON_PAY_BALANCE/v1'
	#amount = '800.00'
	amount = amt
	#merchant_trans_id = SecureRandom.hex(10)
	merchant_trans_id = merch_trans_id
	merchant_note_customer = "TestTransaction"
	#return_url = "https://preprod.shemaroome.com/payment/amazonpay_response"
	return_url  = "https://www.shemaroome.com/payment/amazonpay_response"
	signature_method = "HmacSHA384"
	customer_id_type = "CONSENT_TOKEN"
	lat_token = get_lat_token current_epoch_time,amzdate,datestamp,access_token,merchant_id,token,amz_acc_key,amz_sec_key

	public_key = "-----BEGIN PUBLIC KEY-----\n"+"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq92yAzXaCQbGIid0mMBfulkGK8HqvAardDowtgbfGUZ+hIx6lhYKFMrluTr7bIlQ4qgJY85c9adkZSxHtr/DhTV/ch5CCHDET3YC/DaFTKDp5t2uHKQAIb2Rl/73HQOd/pgImTiaLHPBr/gyz4iztYmlJQIm0vVuPktIANDGpK8qhizdztA3as1bLtILQZ5VtOjNn/xl1HQ+JDtBhUVr13BuJPosecQz6ouhEtR+5i/grg6sUzayqPD1dY6AGRLR9ao/6DCeHT5arSYjlkx6BECuKoiARo7ItDfLameXJ1gLd8lkMzArIG275jbxAiPd4OchHEfcqBADYB51FYDTwQIDAQAB\n"+"-----END PUBLIC KEY-----"

	values = ["#{accesskey_id}","#{amount}","INR","CONSENT_TOKEN","#{token}","#{lat_token}","#{merchant_id}","TestTransaction","#{return_url}","#{merchant_trans_id}","#{sandbox_mode}","#{signature_method}","4","#{current_epoch_time}"]
	
	values = get_encoded_values values

   can_request = ["POST","amazonpay.amazon.in/payment/charge/AMAZON_PAY_BALANCE/v1","accessKeyId=#{values[0]}&amount=#{values[1]}&currencyCode=#{values[2]}&customerIdType=#{values[3]}&customerIdValue=#{values[4]}&lookAheadToken=#{values[5]}&merchantId=#{values[6]}&merchantNoteToCustomer=#{values[7]}&merchantReturnToUrl=#{values[8]}&merchantTransactionId=#{values[9]}&sandbox=#{values[10]}&signatureMethod=#{values[11]}&signatureVersion=#{values[12]}&timeStamp=#{values[13]}"].join("\n")

   req_parameters = {"accessKeyId":"#{accesskey_id}","amount":"#{amount}","currencyCode":"INR","customerIdType":"CONSENT_TOKEN","customerIdValue":"#{token}","lookAheadToken":"#{lat_token}","merchantNoteToCustomer":"TestTransaction","merchantId":"#{merchant_id}","merchantReturnToUrl":"#{return_url}","merchantTransactionId":"#{merchant_trans_id}","sandbox":"#{sandbox_mode}","signatureMethod":"#{signature_method}","signatureVersion":"4","timeStamp":"#{current_epoch_time}"} 

    logger.info "Process charge for amzonpay"
    logger.info "=============================#{req_parameters.inspect}======================="

	headers = { "Content-Type" => "application/json", "Accept"=>"application/json","merchantId" => "#{merchant_id}","timeStamp" => "#{current_epoch_time}", "attributableProgram" => "S2S_PAY"}
	api = "Process Charge"
	response = get_response can_request,req_parameters,amzdate,datestamp,headers,host, api,amz_sec_key
end

puts "========================================API :: Access token==============================================="
=begin
data = get_access_token(code)
access_token = URI.escape(data['access_token'])
token = data['access_token']
puts "Access Token Before Encryption :: #{token} \n\n"
puts "Access Token After Encryption :: #{access_token} \n\n"

current_epoch_time = (Time.now.to_f * 1000).ceil
amzdate = Time.now.utc.strftime("%Y%m%dT%H%M%SZ")
datestamp = Time.now.utc.strftime("%Y%m%d")
merchant_id = 'AZ4WQCLDT2DF0'

response = get_process_charge_response current_epoch_time,amzdate,datestamp,access_token,merchant_id,token
=end
	def final_aws_pay(code,amt,merch_trans_id,amz_client_id,amz_secret_id,amz_mech_id,sandbox_mode,amz_acc_key,amz_sec_key)
		data = get_access_token(code,amz_client_id,amz_secret_id)
		unless data.has_key?("error")
			access_token = URI.escape(data['access_token'])
			token = data['access_token']
			# token = "Atza|IwEBIJ62HM0pHmylE3JU4H0vnsU_WWJyOsRSDyBS8hc9V_xLlEOWRfqWYI9Va1AgOs2WfezQwYG1HL5smTF3OTlwHwOYcxzisrHr0_AMHIkvr5v6_Z8BweNhUug88l3Q0Py7PFStL-Wcbt09C73pMdBEu_inz0fzS9SAeOUfHC4ZGUZXyw_RlJxi8OP7csPO3hQ594vCVuye9eZYdHWo753NecBLfCO1IDyQ8x_EIRJl4tKtRemeG299ZcvCa0PvFGsR9lx5uXFYqxCblAOAI7S8dAl2DTIr_PSpp3R8g6l9AbGWkBW35XPPYNoXlN7kclfWDZuPoDVpzk3boom2LqW6Out7XI7I2C0cEOJhnW10seTI2LDALIPEgrQBEitRh5mHDze7YcsFW195PWqUrIVpsC9qoyRMYN0ePRvRwNtTMp8BERisA-Co-3pOpT9ua9wnFp95rcb6t-Z8SnxtjcSWX0dLk5SROm8yNCXtRMDwGNMI7y1OwZWuEqCVPkYA2bXtr3UeeIonKAzENXqdyEFbhUp3EpOU9mIHKakHP1UxSZvYow"
			# access_token = URI.escape(token)
			puts "Access Token Before Encryption :: #{token} \n\n"
			puts "Access Token After Encryption :: #{access_token} \n\n"
			current_epoch_time = (Time.now.to_f * 1000).ceil
			amzdate = Time.now.utc.strftime("%Y%m%dT%H%M%SZ")
			datestamp = Time.now.utc.strftime("%Y%m%d")
			merchant_id = amz_mech_id
			amount = amt
			response = get_process_charge_response current_epoch_time,amzdate,datestamp,access_token,merchant_id,token,amount,merch_trans_id,sandbox_mode,amz_acc_key,amz_sec_key
		  response["response"]["refresh_token"] = data['refresh_token']
		else
			response = data
		end
		return response
	end

 def get_refresh_token refresh_token
  url = "https://api.amazon.com/auth/o2/token"
  body = {}
  body['grant_type'] = "refresh_token"
  body['refresh_token'] = refresh_token
  body['client_id'] = "amzn1.application-oa2-client.44766ffeadb349bcbfc3b366f2e295b5"
  body['client_secret'] = "612947353006f81ec9eeb3208c2874635118af9c35649f9187f77af2a15d2ec3"
  response = Typhoeus::Request.post(url,:body => body,:timeout => 1000,headers: {'Content-Type'=> "application/x-www-form-urlencoded"})
  data = JSON.parse response.body
  return data
end

  def aws_user_charge(amt,refs_token,lattoken,merch_trans_id,amz_client_id,amz_secret_id,amz_mech_id,sandbox_mode,amz_acc_key,amz_sec_key)
		#accesskey_id = "f57bbc68-fa6a-4c5e-a281-d39db51a26c6"
		accesskey_id = amz_acc_key
		host = 'https://amazonpay.amazon.in/payment/charge/AMAZON_PAY_BALANCE/v1'
		amount = amt
		response = get_refresh_token refs_token
		unless response.has_key?("error")
			access_token = URI.escape(response['access_token'])
			token = response['access_token']
			current_epoch_time = (Time.now.to_f * 1000).ceil
			amzdate = Time.now.utc.strftime("%Y%m%dT%H%M%SZ")
			datestamp = Time.now.utc.strftime("%Y%m%d")
			merchant_trans_id = merch_trans_id
			merchant_note_customer = "TestTransaction"
			#return_url = "https://preprod.shemaroome.com/payment/amazonpay_response"
			#return_url = "https://preprod.shemaroome.com/payment/amazonpay"
			return_url  = "https://www.shemaroome.com/payment/amazonpay_response"
			signature_method = "HmacSHA384"
			customer_id_type = "CONSENT_TOKEN"
			lat_token = lattoken
			merchant_id = amz_mech_id
			public_key = "-----BEGIN PUBLIC KEY-----\n"+"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq92yAzXaCQbGIid0mMBfulkGK8HqvAardDowtgbfGUZ+hIx6lhYKFMrluTr7bIlQ4qgJY85c9adkZSxHtr/DhTV/ch5CCHDET3YC/DaFTKDp5t2uHKQAIb2Rl/73HQOd/pgImTiaLHPBr/gyz4iztYmlJQIm0vVuPktIANDGpK8qhizdztA3as1bLtILQZ5VtOjNn/xl1HQ+JDtBhUVr13BuJPosecQz6ouhEtR+5i/grg6sUzayqPD1dY6AGRLR9ao/6DCeHT5arSYjlkx6BECuKoiARo7ItDfLameXJ1gLd8lkMzArIG275jbxAiPd4OchHEfcqBADYB51FYDTwQIDAQAB\n"+"-----END PUBLIC KEY-----"

			values = ["#{accesskey_id}","#{amount}","INR","CONSENT_TOKEN","#{token}","#{lat_token}","#{merchant_id}","TestTransaction","#{return_url}","#{merchant_trans_id}","#{sandbox_mode}","#{signature_method}","4","#{current_epoch_time}"]

			values = get_encoded_values values

			can_request = ["POST","amazonpay.amazon.in/payment/charge/AMAZON_PAY_BALANCE/v1","accessKeyId=#{values[0]}&amount=#{values[1]}&currencyCode=#{values[2]}&customerIdType=#{values[3]}&customerIdValue=#{values[4]}&lookAheadToken=#{values[5]}&merchantId=#{values[6]}&merchantNoteToCustomer=#{values[7]}&merchantReturnToUrl=#{values[8]}&merchantTransactionId=#{values[9]}&sandbox=#{values[10]}&signatureMethod=#{values[11]}&signatureVersion=#{values[12]}&timeStamp=#{values[13]}"].join("\n")

			req_parameters = {"accessKeyId":"#{accesskey_id}","amount":"#{amount}","currencyCode":"INR","customerIdType":"CONSENT_TOKEN","customerIdValue":"#{token}","lookAheadToken":"#{lat_token}","merchantNoteToCustomer":"TestTransaction","merchantId":"#{merchant_id}","merchantReturnToUrl":"#{return_url}","merchantTransactionId":"#{merchant_trans_id}","sandbox":"#{sandbox_mode}","signatureMethod":"#{signature_method}","signatureVersion":"4","timeStamp":"#{current_epoch_time}"} 

			logger.info "Process charge for amzonpay"
			logger.info "=============================#{req_parameters.inspect}======================="

			headers = { "Content-Type" => "application/json", "Accept"=>"application/json","merchantId" => "#{merchant_id}","timeStamp" => "#{current_epoch_time}", "attributableProgram" => "S2S_PAY"}
			api = "Process Charge"
			response = get_response can_request,req_parameters,amzdate,datestamp,headers,host, api,amz_sec_key
	  end
   return response
  end


end

