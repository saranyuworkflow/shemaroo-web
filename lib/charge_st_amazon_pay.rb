module ChargeStAmazonPay

	def charg_generate_signature(can_req_string,amzdate,datestamp,region,service,secret_key)
		can_request = ["GET","amazonpay.amazon.in/payment/charge/status/v1",can_req_string].join("\n")
	    str1 = OpenSSL::Digest.new("sha384").hexdigest(can_request)
		string_to_sign = ["AWS4-HMAC-SHA384","#{amzdate}","#{datestamp}/eu-west-1/AmazonPay/aws4_request","#{str1}"].join("\n")
		sign_key = getSignatureKey(secret_key, "#{datestamp}", region, service)
		signature = OpenSSL::HMAC.digest('sha384', sign_key,string_to_sign)
		signature = Base64.urlsafe_encode64(signature)
		puts "Response for signature:: #{signature} \n\n"
		return signature
	end

	def charg_generate_payload(accessKeyId,merchantId,signature,signatureMethod,signatureVersion,transactionIdType,transactionId,timeStamp,sandbox_mode)
		req_parameters = {"accessKeyId":"#{accessKeyId}","merchantId":"#{merchantId}","signature":"#{signature}","signatureMethod":"#{signatureMethod}","signatureVersion":"#{signatureVersion}","timeStamp":"#{timeStamp}","transactionId":"#{transactionId}","transactionIdType":"#{transactionIdType}"} 
		req_json = req_parameters.to_json
		key = SecureRandom.random_bytes(16)
		iv =  SecureRandom.random_bytes(16)
		cipher = OpenSSL::Cipher::AES.new(128, :GCM).encrypt
		cipher.padding= 0
		cipher.key_len = 16
		cipher.key = key
		cipher.iv_len = 16
		cipher.iv = iv
		cipher_data = cipher.update(req_json) + cipher.final + cipher.auth_tag
		payload_data = Base64.urlsafe_encode64(cipher_data)
		public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq92yAzXaCQbGIid0mMBfulkGK8HqvAardDowtgbfGUZ+hIx6lhYKFMrluTr7bIlQ4qgJY85c9adkZSxHtr/DhTV/ch5CCHDET3YC/DaFTKDp5t2uHKQAIb2Rl/73HQOd/pgImTiaLHPBr/gyz4iztYmlJQIm0vVuPktIANDGpK8qhizdztA3as1bLtILQZ5VtOjNn/xl1HQ+JDtBhUVr13BuJPosecQz6ouhEtR+5i/grg6sUzayqPD1dY6AGRLR9ao/6DCeHT5arSYjlkx6BECuKoiARo7ItDfLameXJ1gLd8lkMzArIG275jbxAiPd4OchHEfcqBADYB51FYDTwQIDAQAB"
		rsa_public_key = OpenSSL::PKey::RSA.new(Base64.decode64(public_key))
		encrypted_string = rsa_public_key.public_encrypt(key, OpenSSL::PKey::RSA::PKCS1_OAEP_PADDING)
		encrypted_key = Base64.urlsafe_encode64(encrypted_string)
		headers = {"Content-Type" => "application/json", "Accept"=>"application/json","merchantId" => "#{merchantId}","isSandbox" => "#{sandbox_mode}","timestamp" => timeStamp}
		iv = Base64.urlsafe_encode64(iv)
		puts "Request for Payloaad date here:: #{payload_data} \n\n"
		puts "Request for iv here:: #{iv} \n\n"
		puts "Request for key here:: #{encrypted_key} \n\n"
		charg_st_url = "https://amazonpay.amazon.in/payment/charge/status/v1"
		aws_chrg_url = charg_st_url+"?iv=#{iv}&key=#{encrypted_key}&payload=#{payload_data}"
		response = Typhoeus::Request.get(aws_chrg_url,:headers => headers, :ssl_verifypeer => false)
		response = JSON.parse(response.body)
		puts "Response for charge status api:: #{response} \n\n"
		return response
	 end

  def get_encoded_dt data
   return URI.encode_www_form_component(data)
  end

  def charge_status_check(accessKeyId,merchantId,transactionId,secret_key,sandbox_mode)
  	service = 'AmazonPay'
    region = 'eu-west-1'
    signatureMethod = 'HmacSHA384'
    signatureVersion = '4'
    transactionIdType = "MERCHANT_TXN_ID"
    amzdate = Time.now.utc.strftime("%Y%m%dT%H%M%SZ")
    datestamp = Time.now.utc.strftime("%Y%m%d")
    timeStamp = (Time.now.to_f * 1000).ceil
    can_req_string = "accessKeyId=#{get_encoded_dt(accessKeyId)}&merchantId=#{get_encoded_dt(merchantId)}&signatureMethod=#{get_encoded_dt(signatureMethod)}&signatureVersion=#{get_encoded_dt(signatureVersion)}&timeStamp=#{get_encoded_dt(timeStamp)}&transactionId=#{get_encoded_dt(transactionId)}&transactionIdType=#{get_encoded_dt(transactionIdType)}"
    aws_sig = charg_generate_signature(can_req_string,amzdate,datestamp,region,service,secret_key)
    api_resp = charg_generate_payload(accessKeyId,merchantId,aws_sig,signatureMethod,signatureVersion,transactionIdType,transactionId,timeStamp,sandbox_mode)
  return api_resp
  end
end