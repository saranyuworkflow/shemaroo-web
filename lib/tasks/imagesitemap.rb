require 'mongoid'
require 'timeout'
require 'typhoeus'
require 'json'
require 'time'
require 'date'
require 'sitemap_generator'

  Mongoid.load!("mongoid.yml", :development)


  class Catalog 
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :name, :type => String #Name of the Catalog
    field :business_group_id, :type => String #unique id from business group
    field :theme, :type => String 
    field :layout_type, :type => String
    field :layout_structure, :type => String
  end

  class LivetvTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :content_id,type: String
    field :channel_name, type: String
    field :friendly_id, type: String
  end

  class ShowTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :title, type: String
    field :status, type: String  # edit, publish and unpublish 
    field :friendly_id, type: String
  end


  class EpisodeTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    belongs_to :subcategory
    belongs_to :show_theme
    field :title, type: String
    field :status, type: String  # edit, publish and unpublish 
    field :friendly_id, type: String
  end

  class MovieTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :title, type: String
    field :status, type: String  # edit, publish and unpublish 
    field :friendly_id, type: String
  end

  class VideoTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :title, type: String
    field :status, type: String  # edit, publish and unpublish 
    field :friendly_id, type: String
  end

  SitemapGenerator::Sitemap.default_host = 'https://www.shemaroome.com'
  SitemapGenerator::Sitemap.create do
    MovieTheme.where(:status => "published",:business_group_id => "548343938").to_a.each do |m|
      puts "movie theme here"
      puts "/movies/#{m['friendly_id']}"
      add("/movies/#{m['friendly_id']}", :images => [{
        :loc => m['thumbnails']['large_16_9']['url']
        }])
    end

    ShowTheme.where(:status => "published",:business_group_id => "548343938").to_a.each do |s|
      puts "show theeme here"
      puts "#{s.inspect}"
      puts "/shows/#{s['friendly_id']}"
      add("/shows/#{s['friendly_id']}",:images => [{
        :loc => s['thumbnails']['medium_16_9']['url']
      }])
    end
    EpisodeTheme.where(:status => "published",:business_group_id => "548343938").to_a.each do |e|
      puts "episdoes theeme here"
      puts "#{e.inspect}"
      show_theme_data = ShowTheme.find(e['show_theme_id'].to_s)
      puts "/shows/#{show_theme_data['friendly_id']}/#{e['friendly_id']}"
      add("/shows/#{show_theme_data['friendly_id']}/#{e['friendly_id']}",:images => [{
        :loc => e['thumbnails']['large_16_9']['url']
      }])
    end
    LivetvTheme.where(:status => "published",:business_group_id => "548343938").to_a.each do |l|
      puts "episdoes theeme here"
      puts "/all-channels/#{l['friendly_id']}"
       add("/all-channels/#{l['friendly_id']}",:images => [{
        :loc => l['thumbnails']['large_16_9']['url']
      }])    
    end
  
     
  end