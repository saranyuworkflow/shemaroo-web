require 'aws-sdk-s3'

namespace :upload_log do
	task upload: :environment do
		region = 'us-east-2'
		access_key_id = 'AKIA3D5VQMBSRXGBGGF2'
		secret_access_key = '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'
		bucket_name = 'international-sales'
		today_date_time = Time.now.strftime("%d-%b-%Y").split(' ').join('_')
		key = "#{today_date_time}_unifi_transaction.log"
		logger = Logger.new(STDOUT)
		logger.info "==================================="
		logger.info "filename:  ---> #{key} <-----------"
		logger.info "======= Upload Start Here ========="
		logger.info "===================================="
	  path = 'log/unifi_transaction.log'
	  s_three_object = Aws::S3::Resource.new(region: region, access_key_id: access_key_id, secret_access_key: secret_access_key)
	  response = s_three_object.bucket(bucket_name).object(key).upload_file(path)
	  if response ==true
	    logger.info "Uploaded Successfully"
	    File.open(path, 'w'){ |file| file.truncate(0) }
	  end
		logger.info "======== Upload End Here ==========="
	end
end
