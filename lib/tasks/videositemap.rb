require 'mongoid'
require 'timeout'
require 'typhoeus'
require 'json'
require 'time'
require 'date'
require 'sitemap_generator'

  Mongoid.load!("mongoid.yml", :development)


  class Catalog 
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :name, :type => String #Name of the Catalog
    field :business_group_id, :type => String #unique id from business group
    field :theme, :type => String 
    field :layout_type, :type => String
    field :layout_structure, :type => String
  end

  class LivetvTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :content_id,type: String
    field :channel_name, type: String
    field :friendly_id, type: String
  end

  class ShowTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :title, type: String
    field :status, type: String  # edit, publish and unpublish 
    field :friendly_id, type: String
  end


  class EpisodeTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    belongs_to :subcategory
    belongs_to :show_theme
    field :title, type: String
    field :status, type: String  # edit, publish and unpublish 
    field :friendly_id, type: String
  end

  class MovieTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :title, type: String
    field :status, type: String  # edit, publish and unpublish 
    field :friendly_id, type: String
  end

  class VideoTheme
    include Mongoid::Document
    include Mongoid::Timestamps
    store_in session: "catalog"
    field :title, type: String
    field :status, type: String  # edit, publish and unpublish 
    field :friendly_id, type: String
  end

  SitemapGenerator::Sitemap.default_host = 'https://www.shemaroome.com'
  SitemapGenerator::Sitemap.create do
    MovieTheme.where(:status => "published",:business_group_id => "548343938").to_a.each do |m|
      puts "movie theme here"
      puts "/movies/#{m['friendly_id']}"
      add("/movies/#{m['friendly_id']}", :video => {
        :thumbnail_loc => m['thumbnails']['large_16_9']['url'],
        :title => m["title"],
        :description => m["description"],
        :publication_date => m['release_date_string'],
        :expiration_date => Time.now()+5.years,
        :restriction_relationship => "allow",
        :category => m['genres'].join(","),
        :family_friendly => "yes",
        :live => "no"
        })
    end
  
     
  end
