namespace :auto_renew do
	task :amazon_pay => :environment do
	  #connect_to_db_and_schema
	  connect_to_db_and_schema
	  
	  time1 = Time.now
	  t1 = time1 
	  t2 = time1 

	  puts "=======Time Now : #{Time.now.inspect}==auto renewal started from : #{t1.inspect} to : #{t2.inspect}=="

    # all_plans = UserPlan.where(:billing_date => (t1 .. t2), :plan_status => "active", :auto_renew=> "true").where.not(device_info: "ios")
	  all_plans = UserPlan.where plan_id: "5b44845fc1df412ee6000000",user_id: "4415196" , id: "576262" 

	  all_plans.each do |plan|
	    succ_tran = Transaction.find_by(txn_id: plan['transaction_id'], payment_gateway: 'amazon_pay_web')
	    tran = Transaction.find_by(order_id: succ_tran['order_id'], payment_gateway: 'amazon_pay_web', txn_status: 'init')
	    if tran.present?
	      puts "===============================User Id : #{tran["user_id"]}===================================="
	      data = {}
	      t_id = DateTime.now.strftime('%Q').to_i
	      data["refresh_token"] = tran["recurring_reference_id"]
	      data["amount"] =  tran["price_charged"]
	      order_id = generate_order_id plan["user_id"], plan
        data["mer_trans_id"] = order_id

	      puts "access token Request :: #{data}"

	      auto_renew_response = get_details data
	      response_body = auto_renew_response["response"]

	      puts "response :: #{response_body.inspect}"
	      
	      activate_plan plan, tran, response_body, t_id
	    end
	  end
	  puts "===================================================================================================="
	end

	def activate_plan plan, trans, response, t_id
	  user = User.find_by id: trans['order_id'].split("_")[1]
	  puts "User : #{user.inspect}"
	  if response["status"] == "SUCCESS"
	    if plan.free_trial_pack == "true"
	      plan.free_trial_pack = "false"
	      plan.billing_date = plan.valid_till
	      plan.save

	      trn_obj = trans.attributes
	      trn_obj['id'] = ""
	      trn_obj['txn_status'] = "autorenewal_success"
	      trn_obj['txn_msg'] = "autorenewal success"
	      trn_obj['txn_id'] = DateTime.now.strftime('%Q').to_i
	      trn_obj['miscellaneous'] = response
	      trn_obj['recurring_reference_id'] = response['refresh_token']
	      trn_obj['google_token'] = response['lookAheadToken']
	      trn_obj['recurring_reference_id'] = trans["recurring_reference_id"]  
	  		trn_obj['third_party_info'] = response['merchantTransactionId']

	      puts "Plan is extended to :: #{plan.billing_date}\n\n"
	      succ_tran = user.transactions.create(trn_obj)
	      puts "success trans :: #{succ_tran.inspect}\n\n"
	    else
	      plan_obj = {}
	      plan_obj["category_id"] = plan.subscription_id
	      plan_obj["category"] =  plan.category
	      plan_obj["plan_id"] = plan.plan_id
	      plan_obj["plan_categories"] = plan.plan_categories
	      plan_obj["email_id"] = trans.email
	      plan_obj["region"] = plan.region

	      trn_obj = trans.attributes
	      trn_obj['id'] = ""
	      trn_obj['txn_status'] = "autorenewal_success"
	      trn_obj['txn_msg'] = "autorenewal success"
	      trn_obj['txn_id'] = DateTime.now.strftime('%Q').to_i
	      trn_obj['miscellaneous'] = response
	      trn_obj['google_token'] = response['lookAheadToken']
	      trn_obj['recurring_reference_id'] = trans["recurring_reference_id"]  
	  		trn_obj['third_party_info'] = response['merchantTransactionId']

	      succ_tran = user.transactions.create(trn_obj)
	      puts "success trans :: #{succ_tran.inspect}"
	      
	      date = plan.valid_till.presence || plan.billing_date
	      plan_obj = plan.attributes 
	      plan_obj['id'] = ""
	      plan_obj['start_date'] = date
	      count = (plan.start_date.to_date..date.to_date).count
	      plan_obj['billing_date'] = plan.billing_date + count.days
	      plan_obj['valid_till'] = plan.billing_date + count.days
	      plan_obj['transaction_id'] = succ_tran['txn_id']
	      plan_obj = user.user_plans.create(plan_obj)
	      puts "plan obj :: #{plan_obj.inspect}"

	    end
	  else
	    trn_obj = trans.attributes
	    trn_obj['id'] = ""
	    trn_obj['txn_status'] = "autorenewal_failed"
	    trn_obj['txn_msg'] = "autorenewal failed"
	    trn_obj['txn_id'] = DateTime.now.strftime('%Q').to_i
	    trn_obj['miscellaneous'] = response
	    trn_obj['google_token'] = response['lookAheadToken']
	    trn_obj['recurring_reference_id'] = trans["recurring_reference_id"]
	    trn_obj['third_party_info'] = response['merchantTransactionId']

	    fail_tran = user.transactions.create(trn_obj)
	    puts "failure trans :: #{fail_tran.inspect}"
	  end
	end

	def connect_to_db_and_schema
	  connection = {adapter: "postgresql", encoding: "utf8", host: "user-db.cjty2q3fnl95.us-east-1.rds.amazonaws.com", pool: "5", username: "postgres", password: "12S#3Wa6()13Ilyu0U2018", database: "ott_services_db", timeout: "5000"}
	  ActiveRecord::Base.establish_connection(connection)
	  ActiveRecord::Base.connection.schema_search_path = "shemaroo_shemaroo_schema, public"    
	end

	def getSignatureKey(key, dateStamp, regionName, serviceName)
	  kDate    = OpenSSL::HMAC.digest('sha384', "AWS4"+key, dateStamp)
	  kRegion  = OpenSSL::HMAC.digest('sha384', kDate, regionName)
	  kService = OpenSSL::HMAC.digest('sha384', kRegion, serviceName)
	  kSigning = OpenSSL::HMAC.digest('sha384', kService, "aws4_request")
	  kSigning
	end

	def get_access_token refresh_token
	  url = "https://api.amazon.com/auth/o2/token"
	  body = {}
	  body['grant_type'] = "refresh_token"
	  body['refresh_token'] = "#{refresh_token}"
	  body['client_id'] = "amzn1.application-oa2-client.44766ffeadb349bcbfc3b366f2e295b5"
	  body['client_secret'] = "612947353006f81ec9eeb3208c2874635118af9c35649f9187f77af2a15d2ec3"

	  response = Typhoeus::Request.post(url,:body => body,:timeout => 1000,headers: {'Content-Type'=> "application/x-www-form-urlencoded"})
	  data = JSON.parse response.body
	  return data
	end

	def get_response can_request,req_parameters,amzdate,datestamp,headers,host,api
	  puts "=========================================API :: #{api}============================================"
	  #secret_key = "DXhOoPOgYdJLYgRzNtV4sPe10DRIzaGE526oYdguMdM0SsfXSJOsmlUr5-SHAWMr"
      secret_key = "3nP-k2SflZijddxWQxzV3bB9oEiE4HpQlXle-DNVnkAXOu-OlT6UTfK9I2CNrP6_"

	  region = 'eu-west-1'
	  service = 'AmazonPay'

	  str1 = OpenSSL::Digest.new("sha384").hexdigest(can_request)
	  string_to_sign = ["AWS4-HMAC-SHA384","#{amzdate}","#{datestamp}/eu-west-1/AmazonPay/aws4_request","#{str1}"].join("\n")
	  sign_key = getSignatureKey(secret_key, "#{datestamp}", region, service)
	  signature = OpenSSL::HMAC.digest('sha384', sign_key,string_to_sign)
	  signature = Base64.urlsafe_encode64(signature)
	  req_parameters['signature'] = signature
	  req_json = req_parameters.to_json
	  key = SecureRandom.random_bytes(16)
	  iv =  SecureRandom.random_bytes(16)
	  cipher = OpenSSL::Cipher::AES.new(128, :GCM).encrypt
	  cipher.padding=0
	  cipher.key_len=16
	  cipher.key = key
	  cipher.iv_len=16
	  cipher.iv = iv
	  cipher_data = cipher.update(req_json) + cipher.final + cipher.auth_tag
	  payload_data = Base64.urlsafe_encode64(cipher_data)

	  # public_key = "-----BEGIN PUBLIC KEY-----\n"+"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq92yAzXaCQbGIid0mMBfulkGK8HqvAardDowtgbfGUZ+hIx6lhYKFMrluTr7bIlQ4qgJY85c9adkZSxHtr/DhTV/ch5CCHDET3YC/DaFTKDp5t2uHKQAIb2Rl/73HQOd/pgImTiaLHPBr/gyz4iztYmlJQIm0vVuPktIANDGpK8qhizdztA3as1bLtILQZ5VtOjNn/xl1HQ+JDtBhUVr13BuJPosecQz6ouhEtR+5i/grg6sUzayqPD1dY6AGRLR9ao/6DCeHT5arSYjlkx6BECuKoiARo7ItDfLameXJ1gLd8lkMzArIG275jbxAiPd4OchHEfcqBADYB51FYDTwQIDAQAB\n"+"-----END PUBLIC KEY-----"
	  # rsa_public_key = OpenSSL::PKey::RSA.new(public_key)
	  # encrypted_string = rsa_public_key.public_encrypt(key, OpenSSL::PKey::RSA::PKCS1_OAEP_PADDING)
	  # encrypted_key = Base64.urlsafe_encode64(encrypted_string)
	  # iv = Base64.urlsafe_encode64(iv)

	public_key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq92yAzXaCQbGIid0mMBfulkGK8HqvAardDowtgbfGUZ+hIx6lhYKFMrluTr7bIlQ4qgJY85c9adkZSxHtr/DhTV/ch5CCHDET3YC/DaFTKDp5t2uHKQAIb2Rl/73HQOd/pgImTiaLHPBr/gyz4iztYmlJQIm0vVuPktIANDGpK8qhizdztA3as1bLtILQZ5VtOjNn/xl1HQ+JDtBhUVr13BuJPosecQz6ouhEtR+5i/grg6sUzayqPD1dY6AGRLR9ao/6DCeHT5arSYjlkx6BECuKoiARo7ItDfLameXJ1gLd8lkMzArIG275jbxAiPd4OchHEfcqBADYB51FYDTwQIDAQAB"
	rsa_public_key = OpenSSL::PKey::RSA.new(Base64.decode64(public_key))
	encrypted_string = rsa_public_key.public_encrypt(key, OpenSSL::PKey::RSA::PKCS1_OAEP_PADDING)
	encrypted_key = Base64.urlsafe_encode64(encrypted_string)
	iv = Base64.urlsafe_encode64(iv)



	  body_data = {payload: payload_data,iv: iv ,key: encrypted_key}
	  
	  response = Typhoeus::Request.post("#{host}", :body => body_data.to_json,:headers => headers, :ssl_verifypeer => false)
	  response = JSON.parse(response.body)
	  
	  return response
	end

	def get_encoded_values values
	  output = []
	  values.each do |v|
	    output << URI.encode_www_form_component(v)
	  end
	  return output
	end

	def get_process_charge_response current_epoch_time,amzdate,datestamp,access_token,merchant_id,token,data
	  #accesskey_id = "f57bbc68-fa6a-4c5e-a281-d39db51a26c6"
	   accesskey_id = "e073caf3-6903-44ee-b572-e46db1940353"

	  host = 'https://amazonpay.amazon.in/payment/charge/AMAZON_PAY_BALANCE/v1'
	  
	  merchant_note_customer = "Renewal Transaction"
	  #return_url = "https://preprod.shemaroome.com/payment/amazonpay"
	  return_url = "https://www.shemaroome.com/payment/amazonpay"
	  signature_method = "HmacSHA384"
	  customer_id_type = "CONSENT_TOKEN"

	  amount = data["amount"]
	  #lat_token = "5ee5f4df-7d8d-44f4-9b09-a7a8c30669d8" #data["lat_token"]
	  lat_token = "301b3891-3ad6-40d0-a027-9dc1da243b6b"
	  merchant_trans_id = data["mer_trans_id"]

	  public_key = "-----BEGIN PUBLIC KEY-----\n"+"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAq92yAzXaCQbGIid0mMBfulkGK8HqvAardDowtgbfGUZ+hIx6lhYKFMrluTr7bIlQ4qgJY85c9adkZSxHtr/DhTV/ch5CCHDET3YC/DaFTKDp5t2uHKQAIb2Rl/73HQOd/pgImTiaLHPBr/gyz4iztYmlJQIm0vVuPktIANDGpK8qhizdztA3as1bLtILQZ5VtOjNn/xl1HQ+JDtBhUVr13BuJPosecQz6ouhEtR+5i/grg6sUzayqPD1dY6AGRLR9ao/6DCeHT5arSYjlkx6BECuKoiARo7ItDfLameXJ1gLd8lkMzArIG275jbxAiPd4OchHEfcqBADYB51FYDTwQIDAQAB\n"+"-----END PUBLIC KEY-----"

	  values = ["#{accesskey_id}","#{amount}","INR","CONSENT_TOKEN","#{token}","#{lat_token}","#{merchant_id}","TestTransaction","#{return_url}","#{merchant_trans_id}","false","#{signature_method}","4","#{current_epoch_time}"]
	  
	  values = get_encoded_values values

	  can_request = ["POST","amazonpay.amazon.in/payment/charge/AMAZON_PAY_BALANCE/v1","accessKeyId=#{values[0]}&amount=#{values[1]}&currencyCode=#{values[2]}&customerIdType=#{values[3]}&customerIdValue=#{values[4]}&lookAheadToken=#{values[5]}&merchantId=#{values[6]}&merchantNoteToCustomer=#{values[7]}&merchantReturnToUrl=#{values[8]}&merchantTransactionId=#{values[9]}&sandbox=#{values[10]}&signatureMethod=#{values[11]}&signatureVersion=#{values[12]}&timeStamp=#{values[13]}"].join("\n")

	  req_parameters = {"accessKeyId":"#{accesskey_id}","amount":"#{amount}","currencyCode":"INR","customerIdType":"CONSENT_TOKEN","customerIdValue":"#{token}","lookAheadToken":"#{lat_token}","merchantNoteToCustomer":"TestTransaction","merchantId":"#{merchant_id}","merchantReturnToUrl":"#{return_url}","merchantTransactionId":"#{merchant_trans_id}","sandbox":"false","signatureMethod":"#{signature_method}","signatureVersion":"4","timeStamp":"#{current_epoch_time}"} 
	    
	  headers = { "Content-Type" => "application/json", "Accept"=>"application/json","merchantId" => "#{merchant_id}","timeStamp" => "#{current_epoch_time}", "attributableProgram" => "S2S_PAY"}
	  api = "Process Charge"

	  response = get_response can_request,req_parameters,amzdate,datestamp,headers,host, api
	end

	def get_details data
	  response = get_access_token data["refresh_token"]
	  puts "access token :: #{response}"
	  access_token = URI.escape(response['access_token'])
	  token = response['access_token']

	  current_epoch_time = (Time.now.to_f * 1000).ceil
	  amzdate = Time.now.utc.strftime("%Y%m%dT%H%M%SZ")
	  datestamp = Time.now.utc.strftime("%Y%m%d")
	  #merchant_id = 'AZ4WQCLDT2DF0'
	  merchant_id = 'A2GMZEZ8TCSYUH'
	  charge_response = ""
	  #get_process_charge_response current_epoch_time,amzdate,datestamp,access_token,merchant_id,token, data
	  return charge_response
	end

	def generate_order_id user_id, plan
	  ext_plan_id = []
	  packs = [plan]
	  packs.each do |p|
	    subscription = SubscriptionTheme.find_by(id: p.subscription_id)
	    plans = subscription.plans
	    plan_type = (plans.select{|a| a["plan_id"] == p.plan_id}).first
	    ext_plan_id << plan_type.ext_plan_id
	  end

	  if ext_plan_id.count > 1
	    ext_plan = "2PC"
	  else
	    ext_plan = ext_plan_id.join('$')
	  end

	  paymentOrderId = "O_"+ user_id.to_s + "_" + rand(32**4).to_s(32).rjust(4,'0') + "_" + ext_plan + "_" + plan.region
	  trn_st = Transaction.find_by(:order_id=>paymentOrderId)
	  if trn_st.nil?
	    return paymentOrderId
	  else
	    order_id = generate_order_id user_id, plan
	  end
	end

end