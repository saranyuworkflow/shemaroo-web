# -*- encoding: utf-8 -*-
# stub: shutup 0.1.2 ruby lib

Gem::Specification.new do |s|
  s.name = "shutup".freeze
  s.version = "0.1.2"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.metadata = { "allowed_push_host" => "https://rubygems.org" } if s.respond_to? :metadata=
  s.require_paths = ["lib".freeze]
  s.authors = ["Lorenzo Sinisi".freeze]
  s.bindir = "exe".freeze
  s.date = "2016-11-19"
  s.description = "Kill the Rails process running in the current folder".freeze
  s.email = ["info@lorenzosinisi.com".freeze]
  s.executables = ["shutup".freeze]
  s.files = ["exe/shutup".freeze]
  s.homepage = "https://github.com/lorenzosinisi/shutup".freeze
  s.licenses = ["MIT".freeze]
  s.post_install_message = "----------------------------------------------\nThank you for installing Shutup!\n\nDocs and support: https://github.com/lorenzosinisi/shutup\n\nby Lorenzo Sinisi\n----------------------------------------------\n".freeze
  s.rubygems_version = "3.2.3".freeze
  s.summary = "Kill the Rails process running in the current folder".freeze

  s.installed_by_version = "3.2.3" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4
  end

  if s.respond_to? :add_runtime_dependency then
    s.add_development_dependency(%q<bundler>.freeze, ["~> 1.13"])
    s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_development_dependency(%q<rspec>.freeze, ["~> 3.0"])
  else
    s.add_dependency(%q<bundler>.freeze, ["~> 1.13"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_dependency(%q<rspec>.freeze, ["~> 3.0"])
  end
end
