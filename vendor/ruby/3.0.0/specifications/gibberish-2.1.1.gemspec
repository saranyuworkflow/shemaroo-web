# -*- encoding: utf-8 -*-
# stub: gibberish 2.1.1 ruby lib

Gem::Specification.new do |s|
  s.name = "gibberish".freeze
  s.version = "2.1.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Mark Percival".freeze]
  s.date = "2019-06-18"
  s.description = "Supports SJCL compatible AES encryption, HMAC, and Digests".freeze
  s.email = ["m@mdp.im".freeze]
  s.homepage = "http://github.com/mdp/gibberish".freeze
  s.licenses = ["MIT".freeze]
  s.required_ruby_version = Gem::Requirement.new(">= 2.0.0".freeze)
  s.rubygems_version = "3.2.3".freeze
  s.summary = "An opinionated ruby encryption library".freeze

  s.installed_by_version = "3.2.3" if s.respond_to? :installed_by_version
end
